/**
* Author :  ROQMetrics 
* Date   : 11/04/2017
* Description : Selector class for Project.
*/
public class ROQProjectSelector{

    /**
    * Date : 11/05/2017
    * Description : Get all Release Plan records related to projectId. 
    * Param : projectId
    * Return : List of Release Plan records. 
    */
    public static List<Program_Increment__c> getReleasePlanByProjectId(Id projectId) {
    
        // SECURITY SCANNER ISSUE ----- 21-12-2017
        List<Program_Increment__c> piList = new List<Program_Increment__c>(); 
        if (Schema.sObjectType.Program_Increment__c.isAccessible()) {
            return [SELECT Id FROM Program_Increment__c WHERE Project_Id__c = :projectId];
        }
        return piList;
    
    }
    
    /**
    * Date : 11/05/2017
    * Description : Get all Projects related to projectId. 
    * Param : projectId
    * Return : List of Projects. 
    */     
    public static List<Project__c> getProjectListByProjectId(Id projectId) {
        
        // SECURITY SCANNER ISSUE ----- 21-12-2017
        List<Project__c> projectList = new List<Project__c>(); 
        if (Schema.sObjectType.Project__c.isAccessible()) {
            projectList = [SELECT Id, Project_Title__c FROM Project__c WHERE Id = :projectId];
        }
        return projectList;
    
    }
    
    
}