/**
* Author : ROQMetrics 
* Date   : 05/06/2017
* Description : Test Class for ApplicationExceptionCodeMap class.
*/

@isTest
private class ApplicationExceptionCodeMapTest {

       static testMethod void test_getApplicationError_UseCase1() {
       
            ApplicationExceptionCodeMap obj01 = new ApplicationExceptionCodeMap();
            ApplicationExceptionCodeMap.exceptionCodes = new Map<String,String>();
            //code change for assert check
            ApplicationExceptionCodeMap.exceptionCodes.put('1001','DML EXCEPTION');
            ApplicationExceptionCodeMap.getApplicationError('1001');
            
            System.assertEquals('DML EXCEPTION', ApplicationExceptionCodeMap.getApplicationError('1001'));
       }
}