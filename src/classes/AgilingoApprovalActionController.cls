/**
* Author :  ROQMetrics 
* Date   : 01/02/2018
* Description : Controller Class for Ligtning Component(Approval Process Buttons).
*/
global class AgilingoApprovalActionController {
 
    /**
    * Date : 01/02/2018
    * Description : To get Lifecycle__c field value and related Object Name.
    * Param : Record Id.
    * Return : ObjectButtonWrapper.
    */
    @AuraEnabled
    global static ObjectButtonWrapper getLifecycleValue(String recId) {
        
        String objectName = Id.valueOf(recId).getSObjectType().getDescribe().getName();
        String soqlQuery = 'SELECT Id, Lifecycle__c FROM {0} WHERE Id =: recId LIMIT 1';
        String queryString = String.format(soqlQuery, new List<String>{objectName});
        Sobject sobj = Database.query(queryString);
        ObjectButtonWrapper wrapper = new ObjectButtonWrapper();
        wrapper.objectName = objectName;
        wrapper.lifecycleValue = String.valueOf(sobj.get(System.Label.LIFECYCLE_FIELD));
        return wrapper;
        
    }
    
    /**
    * Date : 01/02/2018
    * Description : To get Name and Id from User.
    * Param : None.
    * Return : User List.
    */
    @AuraEnabled
    global static List<User> getUsers() {
        return [SELECT Id, Name FROM User];
    }
    
    /**
    * Date : 01/02/2018
    * Description : Wrapper class for Lifecycle__c field value and related Object Name.
    */
    global Class ObjectButtonWrapper {
        @AuraEnabled public string objectName;
        @AuraEnabled public string lifecycleValue;
    }
    
    /**
    * Date : 01/02/2018
    * Description : To get Lifecycle__c field value and related Object Name.
    * Param : User Id, Record Id, Comment from user, Approval Action (MakeEffective, Cancel , Close or Re-Open)
    * Return : ROQResponseWrapper.AgilingoResponseWrapper.
    */
    @AuraEnabled
    global static ROQResponseWrapper.AgilingoResponseWrapper getApprovalResponse (String userId, String recordId, String comment, String approverAction) {
        
        ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
        response.isSuccess = true;
        response.errorList = new List<ROQResponseWrapper.Error>();
        
        String loggedInUser = UserInfo.getUserId();
        String usrId = userId == '' || userId == null ? null : Id.valueOf(userId);
        String reId = Id.valueOf(recordId);
        System.debug('usrId Before calling submitForApproval()'+usrId);
        //Used for Approval Process Submission 
        response.data = ServiceUtil.submitForApproval(usrId, reId, comment, approverAction);
        return response;
    }   
    
    /**
    * Date : 01/02/2018
    * Description : Used to show the Recall Button based on the Approval Process Status of a Record.
    * Param : Record Id.
    * Return : Boolean.
    */
    @AuraEnabled
    global static Boolean getRecallFlag(String recordId) {
        
        Boolean isRecallAvailable = ServiceUtil.getRecallFlag(recordId);
        return isRecallAvailable;
    }   
    
    /**
    * Date : 01/02/2018
    * Description : Used to show the Recall Button based on the Approval Process Status of a Record.
    * Param : Record Id and Lifecycle value(Draft, Effective, cancelled or close).
    * Return : ActionWrapper class.
    */
    @AuraEnabled
    global static List<ActionWrapper> getDependentpicklistValues(String recordId, String lifecycleValue) {
        
        List<ActionWrapper> actionWrapperList = new List<ActionWrapper>();
        try {
            String objectName = Id.valueOf(recordId).getSObjectType().getDescribe().getName();
            String objectLabel = Id.valueOf(recordId).getSObjectType().getDescribe().getLabel();
            Schema.sObjectField approvalActionField = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(System.Label.APPROVAL_ACTION_FIELD);
            
            Schema.sObjectField lifecycleField = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(System.Label.LIFECYCLE_FIELD);
            
            Set<String> filteredLabels = new Set<String>();
            List<String> buttonLabels = new List<String>();
            ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
            response.isSuccess = true;
            response.errorList = new List<ROQResponseWrapper.Error>();
            Map<String, List<String>> picklistValueMap = AgilingoDependentPicklistValues.getDependentOptions(approvalActionField, lifecycleField);
            
            buttonLabels = picklistValueMap.get(lifecycleValue);
            
            filteredLabels = getApprovalActionByGroups(objectLabel, buttonLabels);
            for(String s : filteredLabels) {
                ActionWrapper actionWrapper = new ActionWrapper();
                actionWrapper.label = s;
                actionWrapper.action = s.replace('-','')+'Action';
                actionWrapperList.add(actionWrapper);
            }
            
        } catch(Exception ce) {
            System.debug('=====Error====='+ce.getMessage());
            System.debug('=====LINEnumber====='+ce.getLineNumber());
        }
        
        return actionWrapperList;
    }
    
    /**
    * Date : 01/02/2018
    * Description : Wrapper class for API name and Label of Approval Actions field.
    */
    global class ActionWrapper {
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String action {get;set;}
    }
    
    
    /**
    * Date : 01/02/2018
    * Description : To show the particular Approval Action for User based on the group assignment.
    * Param : object name and Approval Action List.
    * Return : String.
    */
    global static Set<String> getApprovalActionByGroups(String objectName, List<String> approvalActions) {
        
        System.debug('=====objectName====='+objectName);
        System.debug('=====approvalActions====='+approvalActions);
        
        Set<Id> loggedInUserGroups = getGroupIdsForUser(UserInfo.getUserId());
        Set<String> filteredSet = new Set<String>();
        try{
            for (String s : approvalActions) {
                
                  List<Agilingo_Approval_Setup__c> obj = [SELECT Id, Name, Object_Name__c, Approver_Action__c, User_Group__c
                                                             FROM Agilingo_Approval_Setup__c WHERE Object_Name__c =: objectName 
                                                             AND Approver_Action__c =: s];
                  
                  System.debug('=====obj====='+obj);
                  
                  if(obj.size() > 0) {
                    List<String> approvalActionGroups = obj[0].User_Group__c.split(',');
                    for(String grp : approvalActionGroups) {
                        if(loggedInUserGroups.contains(grp)) {
                            filteredSet.add(obj[0].Approver_Action__c);
                        }
                    }    
                }
            }
        } catch(Exception ce) {
            System.debug('=====getApprovalActionByGroups Error====='+ce.getMessage());
            System.debug('=====getApprovalActionByGroups Line ====='+ce.getLineNumber());
        }
        return filteredSet;
    }
    
    /**
    * Date : 01/02/2018
    * Description : To get the group Id of User.
    * Param : User Id.
    * Return : Set of group Id's.
    */
    global static Set<Id> getGroupIdsForUser(String userId) {
        
        //Declaring a Set to aviod Duplicate Group Ids
        Set<Id> results = new Set<Id>();
        
        try {
            ///Declaring a Map for Group with Role
            Map<Id,Id> grRoleMap = new Map<Id,Id>();
            
            //Populating the Map with RelatedID(i.e.UserRoledId) as Key
            for(Group gr : [select id,relatedid,name from Group]) {
                grRoleMap.put(gr.relatedId,gr.id);
            }
            
            //Groups directly associated to user
            Set<Id> groupwithUser = new Set<Id>();
            
            //Populating the Group with User with GroupId we are filtering only  for Group of Type Regular,Role and RoleAndSubordinates
            for(GroupMember  u :[SELECT groupId from GroupMember where UserOrGroupId =: userId and (Group.Type = 'Regular' OR Group.Type='Role' OR Group.Type='RoleAndSubordinates')]) {
                groupwithUser.add(u.groupId);
            }
            
            //Groups with Role
            for(User  u :[select UserRoleId from User where id =: userId]) {
                //Checking if the current User Role is part of Map or not
                if(grRoleMap.containsKey(u.UserRoleId))
                {
                    results.add(grRoleMap.get(u.UserRoleId));
                }
            }
            //Combining both the Set
            results.addAll(groupwithUser);
            
            //Traversing the whole list of Groups to check any other nested Group
            Map<Id,Id> grMap = new Map<Id,Id>();
            for(GroupMember gr : [SELECT Id, UserOrGroupId, Groupid FROM GroupMember WHERE
                                  (Group.Type = 'Regular' OR Group.Type='Role' OR Group.Type='RoleAndSubordinates')]) {
                grMap.put(gr.UserOrGroupId,gr.Groupid);
            }
            for(Id i :results) {
                if(grMap.containsKey(i))
                {
                    results.add(grMap.get(i));
                }
            }
        } catch(Exception ce) {
            System.debug('=====INBuiltMethod Error====='+ce.getMessage());
        }
        
        return results;
    }
    
    
}