public without sharing class AgilingoSignatureController {

    
    public static String getDocusignSignature(String username, String pwd){

        String docusignSignature = getSignature(username, pwd);
        return docusignSignature;

    }

    private static String getSignature(String username, String pwd){

        Map<String, String> logininfo = getLoginInfo(username, pwd);
        String accountId = logininfo.get('accountId');
        String userId = logininfo.get('userId');

        String accountRequestUrl = 'https://demo.docusign.net/restapi/v2/accounts/'+accountId+'/users/'+userId;

        String imageURL = getImageUrl(username, pwd, accountRequestUrl);
        imageURL = 'https://demo.docusign.net/restapi/v2/accounts/'+accountId+imageURL;
        System.debug('----imageUrl--1111-'+imageUrl);

        String imageBody = sendRequest(username, pwd, imageURL);
        
        System.debug('----imageBody---'+imageBody);

        return imageBody;
    }
    
    
    

    private static Map<String, String>  getLoginInfo(String uname, String pwd){
        
        //code change for docusign
        List<BP_App_Config__c> dataList = [SELECT Id,DocuSignURL__c FROM BP_App_Config__c] ;
        String docuSignURL = dataList[0].DocuSignURL__c;

        //String loginInfo = 'https://demo.docusign.net/restapi/v2/login_information';
        String loginInfo = docuSignURL+'/restapi/v2/login_information';
        String response = sendRequest(uname, pwd, logininfo);
        Map<String, String> userInfo = getUserInfo(response); 
        return userInfo;
    }

    private static Map<String, String> getUserInfo(String res){
        
        Map<String, String> responseMap = new Map<String, String>();
        JSONParser parser = JSON.createParser(res);
        
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                if(parser.getText() == 'accountId'){
                    parser.nextToken();
                    responseMap.put('accountId',parser.getText());
                }
                if(parser.getText() == 'userId'){
                    parser.nextToken();
                    responseMap.put('userId',parser.getText());
                }
            }
        }
        return responseMap;
    }

    private static String getImageUrl(String uname, String pwd, String endPoint){

        String response = sendRequest(uname, pwd, endPoint);
        String imageUrl = getImageUrl(response);
        System.debug('----imageUrl---'+imageUrl);
        return imageUrl;
    }

    private static String getImageUrl(String responseOneJSON){
    
        String imageUrl;
        JSONParser parser = JSON.createParser(responseOneJSON);
        
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'signatureImageUri')) {
                 
                 parser.nextToken();
                 imageUrl = parser.getText();
                 
            }
        }
        return imageUrl;
    }

    private static String sendRequest(String uname, String pwd, String endPoint){

        //IntegratorKey is different for production environment ==need to change 
        //String IntegratorKey = 'd8e0a291-87b7-476c-b99e-6d93c42d7fd5';
        //code change for IntegratorKey 
        List<BP_App_Config__c> dataList = [SELECT Id, Integrator_Key__c FROM BP_App_Config__c] ;
        String IntegratorKeyCS = dataList[0].Integrator_Key__c;
        
        String IntegratorKey = IntegratorKeyCS;
        String authorizationHeader = '{"Username":"'+uname+'","Password":"'+pwd+'","IntegratorKey":"'+IntegratorKey+'"}';
        System.debug('-----authorizationHeader---'+authorizationHeader);
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(endPoint);
        req.setHeader('content-type', 'application/json');
        req.setHeader('X-DocuSign-Authentication', authorizationHeader);         
        Http h = new Http();
        HttpResponse res;
        if(Test.isRunningTest()){ 
            res = AgilingoEsignMockResponse.respond(req);
        }else{
            res = h.send(req);
        } 
        System.debug('--response---'+res.getBody());

        if(endPoint.endsWithIgnoreCase('signature_image'))
            return EncodingUtil.base64Encode(res.getBodyAsBlob());
        else
            return res.getBody();

    }

}