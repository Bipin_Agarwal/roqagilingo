@isTest
private class AgilingoApprovalActionControllerTest {

    static testMethod void getLifecycleValueTest() {
        
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard Platform User'];
        
        User u1 = new User(LastName = 'Test',Alias = 'Test', Email = 'kartikey.k@ceptes.com', 
            Username = 'kartikey.kartikey65@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id , 
                TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                    LanguageLocaleKey = 'en_US');
        INSERT u1;
        //List<ROQProjectService.IProjectOption> proList = new List<ROQProjectService.IProjectOption>();
        List<Project__c> proList = new List<Project__c>();
        Strategy__c stg = new Strategy__c(Strategy_Title__c = 'TestStrategy', Strategy_Description__c = 'Description');
        INSERT stg;
        System.assertEquals(stg.Strategy_Title__c,'TestStrategy');
        
        Portfolio__c pf = new Portfolio__c(Portfolio_Title__c = 'TestPortFolio', Parent_Strategy__c = stg.Id,
            Portfolio_Manager_Assignment__c = u1.Id);    
        INSERT pf;
        System.assertEquals(pf.Portfolio_Title__c,'TestPortFolio');
        
        Program__c pgm = new Program__c(Program_Title__c = 'Testpgm', Program_Start_Date__c = System.today(),
             Parent_Portfolio__c = pf.id, Program_Manager__c = u1.Id, Program_Status__c = 'On Track');
        INSERT pgm;
        System.assertEquals(pgm.Program_Title__c,'Testpgm');
        
        Road_Map__c pm = new Road_Map__c(RoadMap_Title__c = 'TestPm', Start_Date__c = system.today(), 
            Status__c = 'On Track', Description__c = 'Test Description',Program_Id__c = pgm.id, End_Date__c = System.today().addDays(+1));
        INSERT pm;
        System.assertEquals(pm.RoadMap_Title__c,'TestPm');
        
        Project__c proTest = new Project__c(Project_Title__c = 'TestPM', Parent_Road_Map__c = pm.Id,
             Lifecycle__c = 'Draft', Approval_Actions__c = 'Cancel', Description__c = 'description Test', Parent_Program__c = pgm.Id);
        INSERT proTest;
        
        AgilingoApprovalActionController.getLifecycleValue(proTest.Id);
        AgilingoDependentPicklistValues.getDependentOptions(Project__c.Approval_Actions__c, Project__c.Lifecycle__c);
        AgilingoApprovalActionController.getDependentpicklistValues(String.valueOf(proTest.Id), 'Draft');
        AgilingoSetupController agsetupnew = new AgilingoSetupController();
        //bpsetupnew.getApproverActions();
        
        System.assertEquals(pm.RoadMap_Title__c,'TestPm');
        
        //proList.add(proTest);
        
    }
    
    static testMethod void getUsersTest() {
    
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard Platform User'];
        
        List<User> userList = new List<User>();
        User u1 = new User(LastName = 'Test',Alias = 'Test', Email = 'kartikey.k@ceptes.com', 
            Username = 'kartikey.kartikey64@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id , 
                TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                    LanguageLocaleKey = 'en_US');
        INSERT u1;
        userList.add(u1);
        AgilingoApprovalActionController.getUsers();
    
    }
    
    static testMethod void agilingoDynamicApprovalProcessCreationTest() {
        
        List< AgilingoDynamicApprovalProcessCreation.ApprovalProcessWrapper> approvalProcessList = new List< AgilingoDynamicApprovalProcessCreation.ApprovalProcessWrapper>();
        String status = '';
        AgilingoDynamicApprovalProcessCreation.ApprovalProcessWrapper aw = new AgilingoDynamicApprovalProcessCreation.ApprovalProcessWrapper(true,'Strategy',status);
        approvalProcessList.add(aw);
        //AgilingoDynamicApprovalProcessCreation.createApprovalProcess(approvalProcessList);
        AgilingoDynamicApprovalProcessCreation.getApprovalProcess();
    }

}