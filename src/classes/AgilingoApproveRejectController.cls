/**
* Author :  ROQMetrics 
* Date   : 13/12/2017
* Description : Class for Approval Process.
*/
global class AgilingoApproveRejectController {
    
    global ProcessInstanceWorkItem pItem{get;set;} 
    global List<String> appliedFields{get;set;} 
    global Map<String, List<Object>> fieldLabelValueMap{get;set;} 
    global Map<String, String> apiLabeMap{get;set;} 
    global Sobject obj{get; set;}
    global Boolean applySignature{get; set;}
    global String status{get;set;}
    
    /**
    * Author :  ROQMetrics 
    * Date   : 13/12/2017
    * Description : Constructor of AgilingoApproveRejectController.
    */
    global AgilingoApproveRejectController(){
        String processDefName;
        String recordId;
        Map<String, String> urlParamentersMap = ApexPages.CurrentPage().getParameters() != null ? ApexPages.CurrentPage().getParameters() : null;
        
        if(urlParamentersMap != null)
            recordId = urlParamentersMap.get('id') != null && urlParamentersMap.get('id') != '' ? urlParamentersMap.get('id') : null;
        System.debug('----recordId----'+recordId);
        
        if(Test.isRunningTest()){
            //remove comment recordId = BPAuthenticationControllerTest.getRecordId();
        }
        
        if(recordId != null && recordId.startsWith('04i')){
        
            try{
                System.debug('try block===');
                status = urlParamentersMap.containsKey('status') ? urlParamentersMap.get('status') : null;
                pItem =  [SELECT Id,CreatedById,ProcessInstanceId,ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name,ProcessInstance.TargetObject.Type,Actor.Name,ProcessInstance.CreatedDate,ProcessInstance.ProcessDefinition.Name FROM ProcessInstanceWorkItem WHERE Id=:recordId LIMIT 1];
                System.debug('pItem==='+pItem);
                if(status == 'Pending')
                    status = pItem.ActorId != UserInfo.getUserId() ? 'PendingInvisible' :  status; 
                System.debug('status===='+status);
            }catch(QueryException qe){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'The approval request is already processed.');
                ApexPages.addMessage(myMsg);
            }
            
            if(pItem != null){
                // Get the process definition name
                processDefName = pItem.ProcessInstance.ProcessDefinition.Name.replace(' ','+');
  
                // Get all approval process applied fields
                appliedFields = getApprovalPageColumns(processDefName);
        
                // Dynamically set the field values
                if(appliedFields != null && appliedFields.size() > 0){
                    setFieldValueMap(pItem.ProcessInstance.TargetObjectId, appliedFields);
                }
                System.debug('pItem.ProcessInstance.TargetObjectId===='+pItem.ProcessInstance.TargetObjectId);
                applySignature = AgilingoAuthenticationController.isSignatureForStep(pItem.ProcessInstance.TargetObjectId);
          }
            
        }else{
            ProcessInstance pinstance;
            if(!Test.isRunningTest()){
                status = ApexPages.currentPage().getParameters().get('status');
                System.debug('===recordId ===='+recordId );
                pinstance = [SELECT Id , (SELECT Id,ProcessInstance.ProcessDefinition.Name FROM StepsAndWorkItems LIMIT 1) FROM  ProcessInstance WHERE TargetObjectId = :recordId LIMIT 1];
            }else{   
              status = 'Pending'; 
              pinstance = new ProcessInstance();
            }
            
            if(pinstance != null){
                
                if(!Test.isRunningTest()){
                    
                    ProcessInstanceHistory ph = pinstance.StepsAndWorkitems;
                    
                    // Get the process definition name
                    processDefName = ph.ProcessInstance.ProcessDefinition.Name.replace(' ','+');
                }else{
                   processDefName = 'Test Process' ;
                }
                // Get all approval process applied fields
                appliedFields = getApprovalPageColumns(processDefName);
                
                // Dynamically set the field values
                if(appliedFields != null && appliedFields.size() > 0 && recordId != null){
                    setFieldValueMap(recordId, appliedFields);
                }
                
                if(recordId != null)
                    applySignature = AgilingoAuthenticationController.isSignatureForStep(recordId);
                    System.debug('========applysignature=============='+applySignature);
                 
            }
        }
        
        //applySignature variable null check
        if(applySignature != null){
            System.debug('========Inside IF applysignature==============');
            BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
            applySignature = appConfig.Validate_Password__c || appConfig.Validate_OTP__c || appConfig.Get_Signature__c ;
        }
          
    }
    
    /**
    * Date : 01/12/2017
    * Description : method used to Approve/Reject if esign is not Configured.
    * Param : Approval Process Status, Parent Id and comment.
    * Return : void.
    */
    @Remoteaction
    global static void approveRejectWithoutESignature(String type, String parentId, String comment){
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
        try{
                AgilingoUtil.ApproveRejectParams param = new AgilingoUtil.ApproveRejectParams();
                param.targetRecordId = parentId;
                param.comment = comment;
                param.status = type;
                
                AgilingoUtil.approveRecord(param);
        
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
    }
    
    /**
    * Date : 01/12/2017
    * Description : Tooling api for assigned columns.
    * Param :  Process Name.
    * Return : String.
    */
    global List<String> getApprovalPageColumns(String processName){
        
        List<String> fields = new List<String>();
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
                String layoutQueryString = getApprovalLayoutQueryString(processName);
                
                String layoutId = getLayoutId(layoutQueryString);
                
                if(layoutId != null){
                    String layoutResponse = getLayoutResponse(layoutId);
                    
                    if(layoutResponse != null){
                        
                        layoutResponse = layoutResponse.contains('sf:') ? layoutResponse.replace('sf:','') : layoutResponse;
                        layoutResponse = layoutResponse.contains('mns:') ? layoutResponse.replace('mns:','') : layoutResponse;
                        fields = getColumnAttrbutes(layoutResponse);
                        
                    }
                }   
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return fields;  
    }
    
    
    private String getApprovalLayoutQueryString(String layoutName){
    
        return 'Select+Id+FROM+Layout+WHERE+Name+LIKE+\'Approval+Page+Layout:+'+layoutName+'%25\'+LIMIT+1';
    }
    
    /**
    * Date : 01/12/2017
    * Description : Tooling api for assigned columns.
    * Param :  Process Name.
    * Return : String.
    */
    private String getLayoutId(String queryString){
        
        String layoutId;
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
            //String endPointURL = URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v39.0/tooling/query?q='+queryString;
            String endPointURL = appConfig.Base_URL__c +'/services/data/v39.0/tooling/query?q='+queryString;
          
            HTTPRequest req = new HTTPRequest();
            req.setEndpoint(endPointURL);
            req.setMethod('GET');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
            req.setHeader('Accept', 'application/json');
            
            Http h = new Http();
            HttpResponse res;
            if(Test.isRunningTest()){
                res = AgilingoEsignMockResponse.approvalLayoutIdResponse();
            }else{
                res = h.send(req);
            }
        
            // Response to a create should be 201
            if (res.getStatusCode() != 201) {
                
                Map<String, Object> recDet = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                if(recDet.containsKey('records')){
                    
                    List<Object> records = (List<Object>) recDet.get('records');
                    Map<String, Object> rec = (Map<String, Object>) records[0];
                    layoutId = (String)rec.get('Id');
                }       
                
            }
        } catch(JSONEXCEPTION je) {
            result.errorlist.add(ROQExceptionService.jsonException(je));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return layoutId;
    }
    
    /**
    * Date : 01/12/2017
    * Description : Approval Process layout data.
    * Param :  Layout related record Id.
    * Return : String.
    */
    private String getLayoutResponse(String layoutRecId){
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        String responseBody;
        try {
            BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
            //String endPointURL = URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v39.0/tooling/sobjects/Layout/'+layoutRecId;
            String endPointURL = appConfig.Base_URL__c+'/services/data/v39.0/tooling/sobjects/Layout/'+layoutRecId;
            
            
            HTTPRequest req = new HTTPRequest();
            req.setEndpoint(endPointURL);
            req.setMethod('GET');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
            req.setHeader('Accept', 'application/xml');
            
            Http h = new Http();
            HttpResponse res;
            if(Test.isRunningTest()){
                res = AgilingoEsignMockResponse.approvalLayoutResponse();
            }else{
                res = h.send(req);
            }
            
            // Response to a create should be 201
            if (res.getStatusCode() != 201) {
                responseBody = res.getBody();
            }
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return responseBody;
        
    }
    
    public List<String> eleList; // List of fields assigned while configuring the approval process
    /* Get list of fields which are used in Approve/Reject page */
    global List<String> getColumnAttrbutes(String pageBody){
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try{
            eleList = new List<String>();
            Dom.Document doc = new Dom.Document();
            doc.load(pageBody);
            Dom.XMLNode rootElement = doc.getRootElement();
            xmlParse(rootElement);
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return  eleList;
        
    }
    
    global void xmlParse(Dom.XMLNode childParse){
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try {
                for(Dom.XMLNode child : childParse.getChildElements()) {
                
                    if(child.getChildElements().size() >0 ){
                        xmlParse(child);
                    }
                    else if(child.getName() == 'field' && child.getParent().getName() == 'layoutItems'){
                        eleList.add(child.getText());
                    }
                
                }
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
    }
    
    public void setFieldValueMap(String recId, List<String> fields){
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
             // Get object api from id
            String objectApi = String.valueOf(Id.valueOf(recId).getSobjectType());
            String objectLabel = Schema.getGlobalDescribe().get(objectApi).getDescribe().getLabel();
            apiLabeMap = new Map<String, String>();
            
            // Construct query string
            String qString = 'SELECT Id,';
            for(String fld : fields){
                qString = qString+fld+',';
            }
            
            qString = qString.removeEnd(',');
           // qString = qString + ' FROM '+objectApi+' WHERE Id=\''+recId+'\'';
            qString = qString + ' FROM '+objectApi+' WHERE Id = :recId';
            // Query the fields
            obj = Database.query(qString);
            
            Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectApi).getDescribe().fields.getMap();
            // Set label for the api
            for(String fld : fields){
                String label = fld == 'Name' ? objectLabel + ' Name' : (fld.equalsIgnoreCase('OwnerId') ? objectLabel +' Owner' :  fieldMap.get(fld).getDescribe().getLabel());
                    apiLabeMap.put(fld,label);
            } 
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
       
    }
}