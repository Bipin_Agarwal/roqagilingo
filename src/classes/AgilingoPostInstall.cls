global class AgilingoPostInstall implements InstallHandler {
    
    global void onInstall(InstallContext context) {
    
        AGILINGO_SETUP__c setup = new AGILINGO_SETUP__c();
        if(Schema.SObjectType.AGILINGO_SETUP__c.isUpdateable()) {
            INSERT setup;
        }
    
    }

}