/*
#    File Name    : ApplicationExceptionManager 
#    Created by   : ROQ metrics
#    Created Date : 17-Mar-2017 
#    Description  : This class handles Standard as well as Custom Exceptions.
*/
global class ApplicationExceptionManager {
    
    global class ApplicationException extends Exception{
    
        global ApplicationExceptionManager.ExceptionWrapper wrapper;
        
        global ApplicationException(ApplicationExceptionManager.ExceptionWrapper wrapper){
            this.wrapper = wrapper;
        }
    }
    
    
    global class ExceptionDetail {
        
        global Exception systemException;
        global String errorCode;
        global String errorMessage;
        global String statusCode;
        global String errorCause;
        global String errorClassMethod;
        global String errorType;
        global String errorRelatedInfo;

    }
    
    global class ExceptionWrapper {
       
        global ExceptionDetail exceptionDet;
        
        global ExceptionWrapper(ExceptionDetail exceptionDet) {
            this.exceptionDet = exceptionDet;
        }
        
    }
    
}