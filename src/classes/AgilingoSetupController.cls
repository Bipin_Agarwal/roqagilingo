/*******************************************************

Apex Class: AgilingoSetupController             
Author : ROQ metrics
Created Date : 
Description : Setup Page Controller, controls custom settings and approval process

******************************************************/


public without sharing class AgilingoSetupController {
    
    //Map of object API name to Object Name
    public static Map<String,String> apiNameMap = new Map<String, String>{'Strategy' => 'ROQA__Strategy__c', 
        'Initiative' => 'ROQA__Initiative__c', 'Roadmap' => 'ROQA__Road_Map__c','Project' => 'ROQA__Project__c',
        'Portfolio' => 'ROQA__Portfolio__c','Program Increment' => 'ROQA__Program_Increment__c','Program' => 'ROQA__Program__c', 
        'Epic Kanban' => 'ROQA__Epic__c', 'Story' => 'ROQA__Story__c', 'Work' => 'ROQA__Work__c', 'Build' => 'ROQA__Build__c', 
        'Sprint' => 'ROQA__Sprint__c'};
        
    public String selectedObject{get;set;}
    public String selectedDefinition{get;set;}
    public String selectedSteps{get;set;}
    public String setupId{get;set;}
    public Boolean pbTableSection{get;set;}
    public String status{get;set;}
    public List<AgilingoDynamicApprovalProcessCreation.ApprovalProcessWrapper> approvalList{get;set;}
    public List<BP_Approval_Steps_Auth_Config__c> approvalStepsList{get;set;}
    public List<ApprovalActionSetting> approverWrapperLst{get;set;}
    public boolean enabled{get;set;}
    private BP_Approval_Steps_Auth_Config__c selectedRow;
    public String errorMessage{get; set;}
    
    //Used for Sorting the tables
    public String tableName = 'ApprovalStepsTable';
    private String sortField = 'Object_Name__c';
    private String sortOrder = 'ASC';
    
    public String sortExpression
    {
        get
        {
            return sortField;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortField)
                sortOrder = (sortOrder == 'ASC')? 'DESC' : 'ASC';
            else
                sortOrder = 'ASC';
            sortField = value;
        }
    }
    
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortOrder == null || sortOrder == '')
            return 'ASC';
        else
            return sortOrder;
    }
    
    public void setSortDirection(String value)
    {  
        sortOrder = value;
    }
    
    
    //For Approval Process
    public String selectedAction{get;set;}
    public String selectedGroup{get;set;}
    public String selectedObjectAP{get;set;}
    public String apSetupId{get;set;}
    public Map<String, String > groupMap{get;set;}
    public List<ApprovalActionSetting> approvalSetting{set;}
   
    // BPSignature Setup Variables
    public BP_App_Config__c appConfig{get; set;}
    
    public String toolingVisualEndpoint {get;set;}
    
    public String esignRecordId {get; set;}
    
    public String apRecordId {get; set;}
    
    /**
    * Date : 10/12/2017
    * Description : Constructor gets all the initial values
    */
    public AgilingoSetupController() {
        if(selectedObject == null) {
            selectedObject = 'None';
        }
        enabled = true;
        pbTableSection = true;
        selectedAction = 'None';
        selectedObjectAP = null;
        selectedRow = null;
        appConfig = BP_App_Config__c.getOrgDefaults();
        toolingVisualEndpoint = URL.getSalesforceBaseUrl().toExternalForm() + '';
        errorMessage = '';
    
    }
    
    public void disable(){
        enabled = false;
    } 
    
    /**
    * Date : 10/12/2017
    * Description : Gets all Records of the BP_Approval_Steps_Auth_Config__c Custom Setting
    * Param : Null.
    * Return : Void.
    */
    public void getstepConfigList() {
        setupId = '';
        errorMessage = '';
        approvalStepsList = new   List<BP_Approval_Steps_Auth_Config__c>();
        
        approvalStepsList= [SELECT Approval_process__c, Object_Name__c, Steps_Require_Signature__c FROM BP_Approval_Steps_Auth_Config__c
                            ORDER BY LastModifiedDate DESC];    
        
    }
    
    /**
    * Date : 10/12/2017
    * Description : For Sorting the BP_Approval_Steps_Auth_Config__c Custom Setting
    * Param : Null.
    * Return : pageReference.
    */
    public pageReference sortStepConfigList(){
        
        approvalStepsList = new   List<BP_Approval_Steps_Auth_Config__c>();
        if(tableName !=null && tableName == 'ApprovalStepsTable') {
            String queryStr = 'SELECT Approval_process__c, Object_Name__c, Steps_Require_Signature__c FROM BP_Approval_Steps_Auth_Config__c ORDER BY '+ sortField+' '+sortOrder;
            approvalStepsList= Database.query(queryStr);
        }
        return null;
    }
    
    
    /**
    * Date : 10/12/2017
    * Description : Gets all Records of the Agilingo_Approval_Setup__c Custom Setting
    * Param : Null.
    * Return : List of Custom Setting.
    */
    public List<Agilingo_Approval_Setup__c> getAPConfigList() {
        
        return Agilingo_Approval_Setup__c.getall().values();
        
    }
    
    
    /**
    * Date : 10/12/2017
    * Description : Get all the Object Names with Approval Process.
    * Param : Null.
    * Return : List of SelectOption.
    */
    public List<SelectOption> getSObjectNames() {
    
        List<SelectOption> objectNames = new List<SelectOption>();
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            Set<String> activeSetupObjects = new Set<String>();
            String objectLabel;
            for (ProcessDefinition pdName: [SELECT TableEnumOrId FROM ProcessDefinition LIMIT 5000]){ 
                List<Schema.DescribeSobjectResult> describeSobjectsResult = Schema.describeSobjects(new List<String> {pdName.TableEnumOrId});
                objectLabel = describeSobjectsResult[0].getLabel();
                activeSetupObjects.add(objectLabel);
            }    
            objectNames.add(new SelectOption('None','None'));
            if(activeSetupObjects.size() > 0){
                for(String obj : activeSetupObjects){
                    objectNames.add(new SelectOption(obj,obj));
                }
            }
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        return objectNames;
    }
    /**
    * Date : 10/12/2017
    * Description : Get all the Process Names.
    * Param : Null.
    * Return : List of SelectOption.
    */
    public List<SelectOption> getProcessDefinitions() {
        
        List<SelectOption> definationNames = new List<SelectOption>();
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            definationNames.add(new SelectOption('None','None'));
            if(selectedObject != 'None'){
            
                List<ProcessDefinition> definationList = [SELECT Id, Name, DeveloperName, TableEnumOrId FROM ProcessDefinition WHERE 
                                                          TableEnumOrId = : apiNameMap.get(selectedObject)];
                
                for (ProcessDefinition pdName: definationList) {  
                    definationNames.add(new SelectOption(pdName.Name,pdName.Name));
                }
            }
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        System.debug('=definationNames=='+definationNames);
        return definationNames;
    }
    
    /**
    * Date : 10/12/2017
    * Description : Get Approval Step Names.
    * Param : Null.
    * Return : List of SelectOption.
    */
    public List<SelectOption> getApprovalSteps() {
        
        List<SelectOption> stepsNames = new List<SelectOption>();
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            List<ProcessNode> pnList = [SELECT Id, Name, DeveloperName, ProcessDefinitionId FROM ProcessNode WHERE 
                                        ProcessDefinition.Name = :selectedDefinition AND ProcessDefinition.TableEnumOrId = :apiNameMap.get(selectedObject)];
            for(ProcessNode pn : pnList) {
                stepsNames.add(new SelectOption(pn.Name,pn.Name));
            }
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        return stepsNames;
    }
    
    /**
    * Date : 10/12/2017
    * Description : To save the Custom Setting.
    * Param : Null.
    * Return : Void.
    */
    public void setAllInputs(){
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try {
                if(Test.isRunningTest()){
                    BP_Approval_Steps_Auth_Config__c approvalStepSetup = new BP_Approval_Steps_Auth_Config__c(Object_Name__c = 'Opportunity',Approval_process__c = 'test Approval',
                                                                                                              Steps_Require_Signature__c = '[Step 1]',Name = 'opptestApproval');
                    insert approvalStepSetup;
                    setupId = approvalStepSetup.Id;
                }
                
                
                BP_Approval_Steps_Auth_Config__c apStep = [SELECT Id, Object_Name__c, Approval_process__c, Steps_Require_Signature__c 
                                                            FROM  BP_Approval_Steps_Auth_Config__c  WHERE Id =: setupId];
                selectedObject = apStep.Object_Name__c ;
                selectedDefinition = apStep.Approval_process__c ;
                selectedSteps = apStep.Steps_Require_Signature__c; 
                selectedRow = apStep;
                 
                 System.debug('===selectedObject=='+selectedObject);
                 System.debug('===selectedDefinition=='+selectedDefinition);
                 System.debug('===selectedStepst=='+selectedSteps);
                 System.debug('===selectedRow=='+selectedRow);
                
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
    }
    
    //name selected should not be exceed 38 character in Custom setting(BP_Approval_Steps_Auth_Config__c)
    /**
    * Date : 10/12/2017
    * Description : To save the Custom Setting.
    * Param : Null.
    * Return : PageReference.
    
    public PageReference save(){
        
        System.debug('=-=-=-apSetupId=-=-='+apSetupId);
        String name = System.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSS');
        PageReference pageRef;
        errorMessage = '';
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            BP_Approval_Steps_Auth_Config__c stepConfig = null;
            List<BP_Approval_Steps_Auth_Config__c> stepConfigList = [SELECT Id, Object_Name__c, Approval_process__c, Steps_Require_Signature__c
                                                                      FROM BP_Approval_Steps_Auth_Config__c WHERE Id =: setupId];
                                                                      
            if(stepConfigList.size() == 0) {
                stepConfig = new BP_Approval_Steps_Auth_Config__c();
            } else {
                stepConfig = stepConfigList.get(0);
            } 
            
            if(selectedSteps != null && selectedSteps != '' && selectedObject != 'None' && selectedDefinition != null){
                stepConfig.Name = name; 
                stepConfig.Object_Name__c = selectedObject;
                stepConfig.Approval_process__c = selectedDefinition;
                stepConfig.Steps_Require_Signature__c = selectedSteps;
                
                if(BP_Approval_Steps_Auth_Config__c.sObjectType.getDescribe().isCreateable() && 
                    BP_Approval_Steps_Auth_Config__c.sObjectType.getDescribe().isUpdateable()){
                    UPSERT stepConfig;
                }
                setupId = null;
                selectedSteps = null;
                pbTableSection = true;
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Saved Successfully'));
            }
            
            if(!Test.isRunningTest()){
                pageRef = new PageReference(ApexPages.currentPage().getUrl());
                pageRef.setRedirect(true);
            }
            
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(DMLException de) {
            result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        sortStepConfigList();
        return null;
    }
    */
    
    private BP_Approval_Steps_Auth_Config__c mapStepConfigRecords(BP_Approval_Steps_Auth_Config__c stepToConfig) {
    
        stepToConfig.Object_Name__c = selectedObject;
        stepToConfig.Approval_process__c = selectedDefinition;
        stepToConfig.Steps_Require_Signature__c = selectedSteps;
        return stepToConfig;
    
    }
    
    
    public pageReference save() {
    
        ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
        response.errorlist = new List<ROQResponseWrapper.Error>();
        errorMessage = '';
        String name = System.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSS');
        
        try{
            BP_Approval_Steps_Auth_Config__c stepConfig;
            
            try{
                stepConfig = [SELECT Id, Approval_process__c, Object_Name__c, Steps_Require_Signature__c 
                              FROM BP_Approval_Steps_Auth_Config__c WHERE Object_Name__c =: selectedObject 
                              AND Approval_process__c =: selectedDefinition LIMIT 1];
            }catch(QueryException qe) {
                System.debug('=-=-=QueryException=-=-'+qe.getMessage());
            }    
            
            if(setupId != null && setupId != '' && (stepConfig == null || stepConfig.Id == setupId) ){
                stepConfig = [SELECT Id, Approval_process__c, Object_Name__c, Steps_Require_Signature__c 
                              FROM BP_Approval_Steps_Auth_Config__c WHERE ID =: setupId];
                  
                stepConfig.Approval_process__c = selectedDefinition;
                stepConfig.Object_Name__c = selectedObject;
                String selSteps = selectedSteps.replace('[','');
                selSteps = selSteps.replace(']','');
                selSteps = selSteps.replace(', ',',');
                stepConfig.Steps_Require_Signature__c = selSteps;
                UPDATE stepConfig;
                getstepConfigList();                             
                
            } else if(stepConfig == null){
                stepConfig = new BP_Approval_Steps_Auth_Config__c();
                stepConfig.Name = name; 
                stepConfig.Approval_process__c = selectedDefinition;
                stepConfig.Object_Name__c = selectedObject;
                String selSteps = selectedSteps.replace('[','');
                selSteps = selSteps.replace(']','');
                selSteps = selSteps.replace(', ',',');
                stepConfig.Steps_Require_Signature__c = selSteps;
                INSERT stepConfig;
                getstepConfigList();
            } else {
                System.debug('-=-=-duplicate error-=--=-=');
                errorMessage = 'Duplicate records found. Edit the existing record';
                
            }
        } catch(QueryException qe) {
            response.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(DMLException de) {
            response.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
            response.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        
        return null;
        
    }
    
    
    
    
    /**
    * Date : 10/12/2017
    * Description : Method to Save and Update Approval Action Setup.
    * Param : Null.
    * Return : PageReference.
    */
    public PageReference saveApprovalProcess() {
        
        ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
        response.errorlist = new List<ROQResponseWrapper.Error>();
        errorMessage = '';
        
        String name = System.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSS');
        try{
            Agilingo_Approval_Setup__c stepConfig;
            
                try{
                    stepConfig = [SELECT Id, Approver_Action__c, Object_Name__c, User_Group__c 
                                  FROM Agilingo_Approval_Setup__c WHERE Object_Name__c =: selectedObjectAP 
                                  AND Approver_Action__c =: selectedAction LIMIT 1];
                }catch(QueryException qe) {
                    System.debug('=-=-=QueryException=-=-'+qe.getMessage());
                }    
            
            
            
            if(apSetupId != null && apSetupId != '' && (stepConfig == null || stepConfig.Id == apSetupId)) {
                stepConfig = [SELECT Id, Approver_Action__c, Object_Name__c, User_Group__c 
                              FROM Agilingo_Approval_Setup__c WHERE ID =: apSetupId];
                  
                stepConfig.Approver_Action__c = selectedAction;
                stepConfig.Object_Name__c = selectedObjectAP;
                System.debug('selectedGroup======'+selectedGroup);
                String selGroup = selectedGroup.replace('[','');
                selGroup = selGroup.replace(']','');
                selGroup = selGroup.replace(', ',',');
                stepConfig.User_Group__c = selGroup;
                UPDATE stepConfig;
                getApprovalSetting();                             
                
            } else if(stepConfig == null){
                stepConfig = new Agilingo_Approval_Setup__c();
                stepConfig.Name = name; 
                stepConfig.Object_Name__c = selectedObjectAP;
                stepConfig.Approver_Action__c = selectedAction;
                String selGroup = selectedGroup.replace('[','');
                selGroup = selGroup.replace(']','');
                selGroup = selGroup.replace(', ',',');
                stepConfig.User_Group__c = selGroup;
                INSERT stepConfig;
                getApprovalSetting();
            } else {
                System.debug('-=-=-duplicate error-=--=-=');
                errorMessage = 'Duplicate records found. Edit the existing record';
                
            }
        } catch(QueryException qe) {
            response.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(DMLException de) {
            response.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
            response.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        
        return null;
        
    }
    
    
    
    
    
    
    
    
    
    
    public void showNewRecordSection(){
        pbTableSection = false;
    }
    
     
    /**
    * Date : 10/12/2017
    * Description : BPSignature Setup save method.
    * Param : Null.
    * Return : PageReference.
    */
    public PageReference getPageRefAndUpdateEsignSettings(){
        
        PageReference pageRef;
        Boolean result; 
        ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
        response.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            if(BP_App_Config__c.sObjectType.getDescribe().isCreateable() && BP_App_Config__c.sObjectType.getDescribe().isUpdateable()){
                upsert appConfig;
            }
            
            if(!Test.isRunningTest()){
                pageRef = ApexPages.currentPage();
            }
        } catch(DMLException de) {
            response.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
            response.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return pageRef;
    }
    
    
    /**
    * Date : 10/12/2017
    * Description : Getting all the Approver Actions.
    * Param : Null.
    * Return : SelectOption.
    */
    public List<SelectOption> getApproverActions() {
    
        List<selectOption> options = new List<selectOption>();
        try {
                options.add(new SelectOption('None','None'));
                if(selectedObjectAP != 'None' && selectedObjectAP != null ){
                    Schema.sObjectField approvalActionField = Schema.getGlobalDescribe().get(apiNameMap.get(selectedObjectAP)).getDescribe().fields.getMap().get(System.Label.APPROVAL_ACTION_FIELD);
                    List<Schema.PicklistEntry> pick_list_values = approvalActionField.getDescribe().getPickListValues();
                    for (Schema.PicklistEntry a : pick_list_values) { 
                        
                        options.add(new selectOption(a.getValue(), a.getLabel())); 
                        
                    }
                }
        } catch(Exception ce) {
            System.debug('=====Message====='+ce.getMessage());
            System.debug('=====Line====='+ce.getLineNumber());
        }
        return options;
    }
    
    /**
    * Date : 10/12/2017
    * Description : get available groups.
    * Param : Null.
    * Return : SelectOption.
    */
    public static List<SelectOption> getGroupsList() {
        
        List<SelectOption> options = new List<SelectOption>();
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            List<Group> groupList = new List<Group>();
            
            groupList = [SELECT Id, Name FROM Group LIMIT 49999];
            for(Group g : groupList) {
                if(g.Name != null) {
                    options.add(new SelectOption(g.Id,g.Name));
                }
            }
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return options;
        
    }
    
    /**
    * Date : 10/12/2017
    * Description : get available groups.
    * Param : Null.
    * Return : Map of Strings.
    */
    public Map<String, String> getGroupMap() {
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        Map<String, String> groupMap = new Map<String, String>();
        
        try{
            List<Group> groupList = new List<Group>();
            
            groupList = [SELECT Id, Name FROM Group LIMIT 49999];
            for(Group g : groupList) {
                if(g.Name != null) {
                    groupMap.put(g.Id, g.Name);
                }
            }
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        
        return groupMap;
    } 
    
    /**
    * Date : 10/12/2017
    * Description : to use for manage the Setup records in vf page.
    * Param : Null.
    * Return : void.
    */ 
    public void setApprovalProcessAllInputs() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            if(Test.isRunningTest()){
                Agilingo_Approval_Setup__c approvalProcessSetup = new Agilingo_Approval_Setup__c(Name = System.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSS'), 
                                                                                                 Object_Name__c = 'Project__c',Approver_Action__c= 'Cancel');
                INSERT approvalProcessSetup;
                apSetupId = approvalProcessSetup.Id;
            }
            
            System.debug('===apSetupId====='+apSetupId);
            Agilingo_Approval_Setup__c approvalSetup = [SELECT Id, Object_Name__c, Approver_Action__c, User_Group__c FROM  Agilingo_Approval_Setup__c WHERE Id =: apSetupId];
            selectedObjectAP = approvalSetup.Object_Name__c ;
            selectedAction = approvalSetup.Approver_Action__c;
            String selGrp = approvalSetup.User_Group__c;
            selectedGroup = selGrp;
            System.debug('selGrp===='+selGrp);
            System.debug('===approvalSetup====='+approvalSetup);
        }catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        
    }
    
    /**
    * Date : 10/12/2017
    * Description : Wrapper Class for Approval Process.
    */ 
    public class ApprovalActionSetting {
        public string objectName{get;set;}
        public string approverAction{get;set;}
        public string userGroup{get;set;}
        public String appSettingId{get;set;}
    }
    
    /**
    * Date : 10/12/2017
    * Description : To get the Custom Settings.
    * Param : Null.
    * Return : void.
    */ 
    public void getApprovalSetting() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        errorMessage = '';
        approverWrapperLst = new List<ApprovalActionSetting >();
        
        System.debug('-==-=-selectedAction-==--='+selectedAction);
        System.debug('-=-=-=-=-selectedObjectAP=-==-'+selectedObjectAP);
        System.debug('-=-=--selectedGroup-=-=-==-'+selectedGroup);
        
        //Truncating the Values from Selected Object and Selected Action after edting and updating the record
        apSetupId = '';
        
        System.debug('-==-=-selectedAction-=after=--='+selectedAction);
        System.debug('-=-=-=-=-selectedObjectAP=-after==-'+selectedObjectAP);
        System.debug('-=-=--selectedGroup-=-=after-==-'+selectedGroup);
        
         
        List<ApprovalActionSetting> approverWrapperList = new List<ApprovalActionSetting>();
        try {
            List<Agilingo_Approval_Setup__c> apUserGroup = [SELECT Id, Object_Name__c, Approver_Action__c, User_Group__c 
                                                            FROM Agilingo_Approval_Setup__c  ORDER BY LastModifiedDate DESC]; //Object_Name__c,Approver_Action__c];
            
            Map<String, String> groupMap = getGroupMap();
            for(Agilingo_Approval_Setup__c a :  apUserGroup) {
                ApprovalActionSetting setting = new ApprovalActionSetting();
                setting.objectName = a.Object_Name__c;
                setting.approverAction = a.Approver_Action__c;
                List<String> groupList;
                if(a.User_Group__c != null){
                    groupList = a.User_Group__c.split(',');
                }
                Integer gpsize;
                if(groupList != null) {
                    gpsize = groupList.size();
                }
                Integer count = 0;
                String usrGrp = ''; 
                
                if(groupList != null) {
                    for(String s : groupList) {
                        usrGrp = usrGrp+groupMap.get(s);
                        count = count + 1;
                        if(count < gpsize) {
                            usrGrp = usrGrp+',';
                        }
                    }
                }
                setting.userGroup = usrGrp;
                setting.appSettingId = a.Id;
                approverWrapperList.add(setting);
                System.debug('====approverWrapperList=='+approverWrapperList);
                
            } }catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
            
        approverWrapperLst =approverWrapperList;
        System.debug('====approverWrapperLst===='+approverWrapperLst);
    }
        
    /**
    * Date : 10/12/2017
    * Description : FOR Sorting ApprovalActionSetting.
    * Param : Null.
    * Return : void.
    */ 
    public pageReference sortApprovalSetting() {
    
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        List<ApprovalActionSetting> approverWrapperList = new List<ApprovalActionSetting>();
            try {
                String queryStr = 'SELECT Id, Object_Name__c, Approver_Action__c, User_Group__c FROM Agilingo_Approval_Setup__c  ORDER BY '+ sortField+' '+sortOrder;
                List<Agilingo_Approval_Setup__c> apUserGroup = new List<Agilingo_Approval_Setup__c>();
                apUserGroup = Database.query(queryStr);
                
                Map<String, String> groupMap = getGroupMap();
                for(Agilingo_Approval_Setup__c a :  apUserGroup) {
                    
                    ApprovalActionSetting setting = new ApprovalActionSetting();
                    setting.objectName = a.Object_Name__c;
                    setting.approverAction = a.Approver_Action__c;
                    List<String> groupList;
                    if(a.User_Group__c != null){
                        groupList = a.User_Group__c.split(',');
                    }
                    Integer gpsize;
                    if(groupList != null) {
                        gpsize = groupList.size();
                    }
                    
                    Integer count = 0;
                    String usrGrp = ''; 
                    
                    if(groupList != null) {
                        for(String s : groupList) {
                            usrGrp = usrGrp+groupMap.get(s);
                            count = count + 1;
                            if(count < gpsize) {
                                usrGrp = usrGrp+',';
                            }
                        }
                    }
                    setting.userGroup = usrGrp;
                    setting.appSettingId = a.Id;
                    approverWrapperList.add(setting);
                    
                } }catch(Exception ex) {
                    result.errorlist.add(ROQExceptionService.parentException(ex));
                }
        approverWrapperLst = approverWrapperList;
        return null;
    }
    
    /**
    * Date : 10/12/2017
    * Description : To delete Custom Setting.
    * Param : Null.
    * Return : void.
    */
    public void deleteCustomSettingEsign() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try{
            
            List<BP_Approval_Steps_Auth_Config__c> delStepConfigList = [SELECT Id, Name, Object_Name__c, Approval_process__c, Steps_Require_Signature__c
                                                                        FROM BP_Approval_Steps_Auth_Config__c WHERE Id =: esignRecordId];
            
            if (Schema.sObjectType.BP_Approval_Steps_Auth_Config__c.isDeletable()) {
                
                DELETE delStepConfigList;
            }
        }catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        sortStepConfigList();
    } 
    
    /**
    * Date : 10/12/2017
    * Description : To delete Custom Setting.
    * Param : Null.
    * Return : void.
    */
    public void deleteCustomSettingAP(){
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try{
            
            List<Agilingo_Approval_Setup__c> delAPConfigList = [SELECT Id, Object_Name__c, Approver_Action__c, User_Group__c FROM  
                                                                Agilingo_Approval_Setup__c WHERE Id =: apRecordId];
            
            if (Schema.sObjectType.Agilingo_Approval_Setup__c.isDeletable()) {
                DELETE delAPConfigList;
            }
        }catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        getApprovalSetting();
    }
    
    /**
    * Date : 10/12/2017
    * Description : To create Approval Process.
    * Param : Null.
    * Return : void.
    */
    public void createApprovalProcess() {
        
        if(approvalList.size() > 0){
            AgilingoDynamicApprovalProcessCreation.createApprovalProcess(approvalList);
        }
    }
    
    /**
    * Date : 10/12/2017
    * Description : To get Approval Process.
    * Param : Null.
    * Return : void.
    */
    public void getApprovalProcess(){
        approvalList = new List<AgilingoDynamicApprovalProcessCreation.ApprovalProcessWrapper>();
        approvalList = AgilingoDynamicApprovalProcessCreation.getApprovalProcess();
    }
    
}