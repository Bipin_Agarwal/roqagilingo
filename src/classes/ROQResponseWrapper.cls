/**
* Author : ROQMetrics 
* Date   : 15/03/2017
* Description : Wrapper Class.
*/
global class ROQResponseWrapper {

    @AuraEnabled public Boolean isSuccess;
    @AuraEnabled public Id recordId;
    @AuraEnabled public List<Error> errorList;
        
    /**
    * Author :  ROQMetrics 
    * Date   : 15/03/2017
    * Description : Inner Class.
    */
    global class AgilingoResponseWrapper {
    
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public Object data;
        @AuraEnabled public List<Error> errorList;
        @AuraEnabled public String Msg;
        @AuraEnabled public Boolean isApproverRequired;
        @AuraEnabled public Boolean isRecallAvailable;
        @AuraEnabled public Boolean isChild;
        @AuraEnabled public Boolean isEpicDeletable;
        //@AuraEnabled public User loggedInUser;
        
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : 15/03/2017
    * Description : Inner Class for Error message.
    */
    public class Error {
    
        @AuraEnabled public String errorCode;
        @AuraEnabled public String errorMessage;
    
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : 26/03/2017
    * Description : Inner class to parse the RoadMapRecord.
    */
     public class RoadMapItem {
        @AuraEnabled public Id id;             
        @AuraEnabled public Id rmId;
        @AuraEnabled public List <Theme> themes;
        @AuraEnabled public String properties;
        @AuraEnabled public String startDate;
        @AuraEnabled public String endDate;
        @AuraEnabled public String dateFormat;
        @AuraEnabled public Boolean isRoadmapCreatable;
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : 26/03/2017
    * Description : Inner class of Theme.
    */
    public class Theme {
        @AuraEnabled public Id themeId;
        @AuraEnabled public Integer ThemeOrder;
        @AuraEnabled public String Name;
        @AuraEnabled public String Description;
        @AuraEnabled public List<Epic> epics;
        @AuraEnabled public String ThemeStyle;
        @AuraEnabled public String LastModifiedDate;
        @AuraEnabled public String LastModifiedBy;
        
      
       public Theme(){}
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : 26/03/2017
    * Description : Inner class of Epic.
    */
    global class Epic {
    
        @AuraEnabled public Id epicId;
        @AuraEnabled public String Name;
        @AuraEnabled public String AutoNumber;
        @AuraEnabled public String Description;
        @AuraEnabled public String EpicStyle;
        @AuraEnabled public String EpicType;
        @AuraEnabled public String Size;
        @AuraEnabled public String Status;
        @AuraEnabled public String StartDate;
        @AuraEnabled public String EndDate;
        @AuraEnabled public Id ParentEpicId;
        @AuraEnabled public List<Epic> featureEpics;
        @AuraEnabled public String LastModifiedDate;
        @AuraEnabled public String LastModifiedBy;
        @AuraEnabled public boolean isEditable;
        @AuraEnabled public String Priority;
        @AuraEnabled public String Category;
        @AuraEnabled public String CategoryTitle;
        @AuraEnabled public String CategoryColor;
        @AuraEnabled public String CategoryAutonumber;
        //Delete list from FE
        @AuraEnabled public List<Epic_Tag__c> epicTagDetails;
        //Selected Tag list for Epic
        @AuraEnabled public List<Epic_Tag__c> Tag;
        //All EpicTag List
        @AuraEnabled public List<Epic_Tag__c> epicTagList;
        @AuraEnabled public String CreatedBy;
        @AuraEnabled public Id epicOwnerId;
        @AuraEnabled public String epicOwnerName;
        @AuraEnabled public String Lifecycle;
        @AuraEnabled public String JiraId;
        @AuraEnabled public String JiraNumber;
        @AuraEnabled public String projectName;
        @AuraEnabled public Boolean popup;
        @AuraEnabled public Boolean isEpicEditable;
        @AuraEnabled public String requirementNumber;
        @AuraEnabled public String regulation;
        @AuraEnabled public String parentEpicTitle;
        //Related Parent epic 
        @AuraEnabled public String parentEpicAutonumber;
        @AuraEnabled public String epicColor;
        @AuraEnabled public String acceptanceCriteria;
        @AuraEnabled public String sprintId;
        @AuraEnabled public String sprint;
        @AuraEnabled public String sprintTitle;
        @AuraEnabled public Decimal fibonacci;
        //Activity section 
        @AuraEnabled public String dateGroomed;
        @AuraEnabled public String dateApproved;
        @AuraEnabled public String dateCommitted;
        @AuraEnabled public String dateCompleted;
        @AuraEnabled public User assignedTo;
        //Activity section Ends
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : 26/03/2017
    * Description : Inner class for Epic-Epic Tag object.
    */
    public class EpicEpicTagWrapper {
    
        @AuraEnabled public Id epicTagId;
        @AuraEnabled public String epicTagName;
        @AuraEnabled public Id epicTagTitle; 
    
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : 29/03/2017
    * Description : Dashboard Wrapper.
    */
    public class DashboardInfoWrapper {
    
        public String loggedInUserName {get;set;}
        public String lastLoginDate {get;set;}
        public String loggedInUserProfileName {get;set;}
        public String loggedInUserId {get;set;}
        public String localeDateFormat {get;set;}
        public String jiraEnabled{get;set;}
        
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : 26/03/2017
    * Description : Inner class to parse the InitiativeRecord.
    */
     public class InitiativeItem {
        @AuraEnabled public Id initiativeId;
        @AuraEnabled public List <Theme> themes;
        @AuraEnabled public String properties;
        @AuraEnabled public Boolean isInitiativeCreatable;
    }
    
    
    /**
    * Author :  ROQMetrics 
    * Date   : /0/2017
    * Description : Inner class for Approval Process.
    */
    global class ApproverResponse {
    
        @AuraEnabled public String recordId;
        @AuraEnabled public String userId;
        @AuraEnabled public String objectName;
        @AuraEnabled public String objectType;
        @AuraEnabled public String mostRecentApprover;
        @AuraEnabled public String dateSubmitted;
        @AuraEnabled public Id workItemId;
    
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : /0/2017
    * Description : Inner class for Theme and related Epic.
    */
    public class ThemeEpicData {
        
        @AuraEnabled public Id themeId;
        @AuraEnabled public Integer ThemeOrder;
        @AuraEnabled public String Name;
        @AuraEnabled public String Description;
        @AuraEnabled public Epic epics;
        
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : /0/2017
    * Description : Inner class for History object.
    */
    public class HistoryWrapper {
    
        @AuraEnabled public String CreatedByName;
        @AuraEnabled public String CreatedDate;
        @AuraEnabled public String Field;
        @AuraEnabled public String NewValue;
        @AuraEnabled public String OldValue;
        @AuraEnabled public String ParentId;
        
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : /0/2017
    * Description : Inner class to validate Epic Deletion.
    */
    public class EpicDeleteValidationWrapper {
        
        @AuraEnabled public boolean isEpicDeletable;
        @AuraEnabled public String msgDisplay;
        
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : /0/2017
    * Description : Inner class for Epic.
    */
    public class GetEpicUsedWrapper {
    
        @AuraEnabled public String Id;
        @AuraEnabled public String Link;
        @AuraEnabled public String Title;
        @AuraEnabled public String Status;
        @AuraEnabled public String Type;
        @AuraEnabled public String LifeCycle;
        @AuraEnabled public String Description;
        @AuraEnabled public String Priority;
        @AuraEnabled public String Tag;
        @AuraEnabled public String Category;
    
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : /0/2017
    * Description : Wrapper class for Object Access.
    */
    public class ObjectPermissionWrapper{
   
        @AuraEnabled public Boolean isEpicCreatable;
        @AuraEnabled public Boolean isThemeCreatable;
        @AuraEnabled public Boolean isRoadmapCreatable;
        @AuraEnabled public Boolean isInitiativeCreatable;
        @AuraEnabled public Boolean isProgramIncrementCreatable;
        @AuraEnabled public Boolean isProjectCreatable;
   
    }
    
    /**
    * Autor : ROQMetrics
    * Date : 23/03/2017
    * Description : Wrapper Class for Epic Bank. 
    */
    public class EpicBankResultWrapper {
   
        @AuraEnabled public List<ROQResponseWrapper.Epic> getEpicsGridViewResult;
        @AuraEnabled public ROQResponseWrapper.ObjectPermissionWrapper getObjectPermissions;
        @AuraEnabled public String totalEpicCount;
        @AuraEnabled public List<String> epicbankColumnList;
        
    }
    
    /**
    * Autor : ROQMetrics
    * Date : 23/03/2017
    * Description : Wrapper Class for Epic Bank. 
    */
    public class EpicSaveWrapper {
        
       @AuraEnabled public List<ROQResponseWrapper.Epic> responseList;
       @AuraEnabled public ROQResponseWrapper.AgilingoResponseWrapper getEpicsGridViewResult;   
    
    }
    
    /**
    * Autor : ROQMetrics
    * Date : 23/03/2017
    * Description : Wrapper Class for Project. 
    */
    public class ProjectSelectOption {
    
        @AuraEnabled public Id projectId;
        @AuraEnabled public String projectName;
        @AuraEnabled public String projectLabel;
        @AuraEnabled public String projectManager;
        @AuraEnabled public String projectManagerName;
        @AuraEnabled public String startDate;
        @AuraEnabled public String endDate;
        @AuraEnabled public String goLive;
        @AuraEnabled public String status;
        @AuraEnabled public String budgetStatus;
        @AuraEnabled public String scheduleStatus;
        @AuraEnabled public String resourceStatus;
        @AuraEnabled public String issueStatus;
        @AuraEnabled public String lifecycle;
        @AuraEnabled public String description;
        @AuraEnabled public String parentProgram;
        @AuraEnabled public String parentProgramTitle;
        @AuraEnabled public String parentRoadmap;
        @AuraEnabled public String parentRoadmapTitle;
        @AuraEnabled public Decimal actual;
        @AuraEnabled public Decimal actualPercentageOfBudget;
        @AuraEnabled public Decimal budget;
        @AuraEnabled public Decimal budgetRemaining;
        @AuraEnabled public Boolean funded;
        @AuraEnabled public String actualEndDate;
        @AuraEnabled public String actualStartDate;
        @AuraEnabled public String ownerId;
        @AuraEnabled public String ownerName;
        @AuraEnabled public String createdByName;
        @AuraEnabled public String createdById;
        @AuraEnabled public String lastModifiedById;
        @AuraEnabled public String lastModifiedByName;
        @AuraEnabled public String plannedEndDate;
        @AuraEnabled public String plannedStartDate;
        @AuraEnabled public Boolean isProjectEditable;
        @AuraEnabled public String parentProgramAutonumber;
        @AuraEnabled public String parentRoadmapAutonumber;
    
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   :  
    * Description : Inner class for Picklist values.
    */
    public class SelectOption {
        
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public Boolean disabled;
        @AuraEnabled public Boolean escapeItem;
        
        public SelectOption(String value, String label) {
            this.value = value;
            this.label = label;
            this.disabled = false;
            this.escapeItem = false;
        }

        public SelectOption(String value, String label, Boolean isDisabled) {
            this.value = value;
            this.label = label;
            this.disabled = isDisabled;
            this.escapeItem = false;
        }
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   :  
    * Description : Inner class for Project Manager user, List of program and Logged in user 
    */
    public class ProjectPicklistWrapper {
        
        @AuraEnabled public List<ServiceUtil.ROQUserWrapper> getProjectUser;
        @AuraEnabled public List<Program__c> getProgramList;
        @AuraEnabled public List<ServiceUtil.ROQUserWrapper> getProjectOwners;
        
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   :  
    * Description : Inner class for project data, object access permission to user, offsetvalue to display records, 
    * and List of columns in Project Bank
    */
    public class ProjectBankResultWrapper {
   
        @AuraEnabled public List<ROQResponseWrapper.ProjectSelectOption> getProjectsGridViewResult;
        @AuraEnabled public ROQResponseWrapper.ObjectPermissionWrapper getObjectPermissions;
        @AuraEnabled public String totalProjectCount;
        @AuraEnabled public List<String> projectBankColumnList;
        
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : /01/2018
    * Description : Inner class for project data and picklist field values related to project used in Project Bank.
    */
    public class ProjectSaveWrapper {
        
       @AuraEnabled public ROQResponseWrapper.ProjectSelectOption response;
       @AuraEnabled public ROQResponseWrapper.AgilingoResponseWrapper getProjectsGridViewResult;   
    
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   :  07/05/2018
    * Description : Inner class to parse the IncrementRecord.
    */
    public class ProgramIncrementItem {
        
        @AuraEnabled public Id id;
        @AuraEnabled public Id piId;
        @AuraEnabled public List<Theme> themes;
        @AuraEnabled public String properties;
        @AuraEnabled public String dateFormat;
        @AuraEnabled public Boolean isProgramIncrementCreatable;
    }  
    
    public class Sprint {
    
        @AuraEnabled public Id id;
        @AuraEnabled public String name;
        @AuraEnabled public String title;
    
    }
    
    public class ApprovalHistory {
    
        @AuraEnabled public List<ProcessInstanceHistory> historyList;
        @AuraEnabled public Id submitterId;
        @AuraEnabled public List<ROQResponseWrapper.SignatureHistory> signHistoryList;
    
    }
    
    public class SignatureHistory{
        @AuraEnabled public DateTime createdDate{get;set;}
        @AuraEnabled public String createdBy{get;set;}
        @AuraEnabled public String stepName{get;set;}
        @AuraEnabled public String signature{get;set;}
        @AuraEnabled public String comments{get;set;}
        @AuraEnabled public Boolean isSignatureApplied{get;set;}
        @AuraEnabled public String objectType{get;set;}
        @AuraEnabled public String objectName{get;set;}
        @AuraEnabled public String status{get;set;}
        @AuraEnabled public String actualApprover{get;set;}
        @AuraEnabled public String actualApproverId{get;set;}
        @AuraEnabled public String TargetObjectId {get;set;}
        @AuraEnabled public Id createdById {get;set;}
    
    }
    
}