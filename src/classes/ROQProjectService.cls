/**
* Author :  ROQMetrics 
* Date   : 23/03/2017
* Description : Service class for Project.
*/
public with sharing class ROQProjectService {
    
    public interface IProjectOption {}
    

/**
* Date : 23/03/2017
* Description : To delete Project record. 
* Param : Record Id to be deleted.
* Return : ROQResponseWrapper  
*/  
    public static ROQResponseWrapper.AgilingoResponseWrapper deleteProject(String recordId) {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = true;
        result.data = null;
        result.errorList = new List<ROQResponseWrapper.Error>();
        try {
            Id projectId = recordId;
            List<Project__c> deleteList = ROQProjectSelector.getProjectListByProjectId(projectId);
            List<Program_Increment__c> deleteReleasePlanList = ROQProjectSelector.getReleasePlanByProjectId(projectId);
            if(deleteReleasePlanList.isEmpty()) {
                if (Schema.sObjectType.Project__c.isDeletable()) {
                    DELETE deleteList;
                    result.Msg = ' '+deleteList[0].Project_Title__c+' '+SYSTEM.LABEL.RECORD_DELETE_MESSAGE;
                    result.isChild = false;
                }
            } else {
                result.isChild = true;
                result.isSuccess = true;
                result.Msg = 'Project '+deleteList[0].Project_Title__c+' '+SYSTEM.LABEL.PROJECT_RECORD_DELETE_MESSAGE;
            }
            System.debug('-=-=-=-result=--=-=-=-'+result);      
            
        } catch(ApplicationExceptionManager.ApplicationException ca) {
            
            // Create the audit record
            result.errorlist.add(ROQExceptionService.auditUtility(ca));
            
        }catch(Exception ex){
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return result;
    }  
    
    public static ROQResponseWrapper.AgilingoResponseWrapper getProjectsGridView(Integer offsetValue, String searchText, Integer limitValue, String order, String fieldName, String projectCount) {
        
        ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
        response.isSuccess = false;
        response.data = null;
        response.errorlist = new List<ROQResponseWrapper.Error>();
        try{
            Map<String,String> fieldsMap = new Map<String, String>
            {'Project Id' => 'Name', 
                'Project Title' => 'Project_Title__c', 
                'Project Manager' => 'Project_Manager__r.Name',
                'Start Date' => 'Start_Date__c',
                'End Date' => 'End_Date__c',
                'Go-Live' => 'Go_Live__c',
                'Status' => 'Status__c',
                'Budget Status' => 'Budget_Status__c',
                'Schedule Status' => 'Schedule_Status__c',
                'Resource Status' => 'Resource_Status__c',
                'Issue Status' => 'Issue_Status__c', 
                'Lifecycle' => 'Lifecycle__c',
                'Description' => 'Description__c',
                'Parent Program' => 'LastModifiedDate',
                'Parent Roadmap' => 'LastModifiedDate',
                'Actual' => 'LastModifiedDate',
                'Actual Budget' => 'LastModifiedDate',
                'Budget' => 'LastModifiedDate',
                'Budget Remaininig' => 'LastModifiedDate',
                'Funded' => 'LastModifiedDate',
                'Actual Start Date' => 'LastModifiedDate',
                'Actual End Date' => 'LastModifiedDate',
                'Owner' => 'LastModifiedDate',
                'Created By' => 'LastModifiedDate',
                'Last Modified By' => 'LastModifiedDate',
                'Planned Start Date' => 'LastModifiedDate',
                'Planned End Date' => 'LastModifiedDate'};
                    
                    String search = String.escapeSingleQuotes(searchText);
            
            String query1 = 'SELECT Id, Name, LastModifiedDate, LastModifiedById, CreatedById, CreatedBy.Name, Description__c, Parent_Program__c, Parent_Program__r.Program_Title__c, Parent_Road_Map__c, Parent_Road_Map__r.RoadMap_Title__c, Actual__c, Project_Title__c, Status__c, Budget_Status__c, Schedule_Status__c, Budget_Remaining__c, Budget__c, Lifecycle__c, Funded__c, Resource_Status__c, Actual_percent_of_Budget__c, OwnerId, Owner.Name, LastModifiedBy.Name, Issue_Status__c, Planned_Start_Date__c, Planned_End_Date__c, Actual_Start_Date__c, Go_Live__c, Actual_End_Date__c, End_Date__c, Project_Manager__c, Start_Date__c,  Project_Manager__r.Name, Parent_Program__r.Name, Parent_Road_Map__r.Name FROM Project__c WHERE CreatedBy.Name LIKE \'%'+search+'%\' OR Parent_Program__r.Program_Title__c LIKE \'%'+search+'%\' OR Parent_Road_Map__r.RoadMap_Title__c LIKE \'%'+search+'%\' OR Project_Title__c LIKE \'%'+search+'%\' OR Status__c LIKE \'%'+search+'%\' OR Budget_Status__c LIKE \'%'+search+'%\' OR Schedule_Status__c LIKE \'%'+search+'%\' OR Lifecycle__c LIKE \'%'+search+'%\' OR Resource_Status__c LIKE \'%'+search+'%\' OR Owner.Name LIKE \'%'+search+'%\' OR LastModifiedBy.Name LIKE \'%'+search+'%\' OR Issue_Status__c LIKE \'%'+search+'%\' OR Project_Manager__r.Name LIKE \'%'+search+'%\' ';
            
            String query2 = (order == null || order == '')
                ? 'ORDER BY LastModifiedDate DESC'
                : 'ORDER BY '+ fieldsMap.get(fieldName) +' '+order+' ';
            
            String query3 = ' LIMIT '+limitValue+' ';
            
            String query4 = (offSetValue == null)
                ? ' '
                : 'OFFSET '+offsetValue+' '; 
            
            String query5 = query1 + query2 + query3 + query4;
            
            System.debug('=====query====='+query5);
            
            List<Project__c> totalProjectList = Database.query(query5);
            
            System.debug('=====totalProjectList====='+totalProjectList);
            
            Integer totalCount = totalProjectList.size();
            
            if(projectCount == ''){
                projectCount = String.valueOf([SELECT Count() FROM Project__c]);
            } else {
                projectCount = projectCount;
            }
            System.debug('=====projectCount====='+projectCount);
            
            Integer updatedCount = Integer.valueOf(projectCount) - totalCount;
            
            System.debug('=====updatedCount====='+updatedCount);
            Set<Id> projectIdSet = new Set<Id>();
            for(Project__c proj : totalProjectList) {
                projectIdSet.add(proj.Id);
            }
            
            Map<Id, Boolean> projectAccessMap = ServiceUtil.getRecordAccess(projectIdSet);
            
            List<User_Preferences__c>  userpList = [SELECT Id, User__c, Project_Bank_Column__c FROM User_Preferences__c ] ;
            
            List<String> columnsList = new List<String>();
            if(userpList.size() > 0){
                String columns = userpList[0].Project_Bank_Column__c;
                System.debug('====userpList======'+userpList[0].Project_Bank_Column__c);
                if(columns != null)
                    columnsList = (List<String>)System.JSON.deserialize(columns, List<String>.class);
            }
            
            List<ROQResponseWrapper.ProjectSelectOption> resultList = new List<ROQResponseWrapper.ProjectSelectOption> ();
            for(Project__c project : totalProjectList) {
                
                ROQResponseWrapper.ProjectSelectOption result = new ROQResponseWrapper.ProjectSelectOption();
                result.projectId = String.valueOf(project.Id);
                result.projectName = project.Name;
                result.projectLabel = project.Project_Title__c;
                result.projectManager = project.Project_Manager__c;
                result.projectManagerName = project.Project_Manager__r.Name;
                result.startDate = project.Start_Date__c != null ? String.valueOf(project.Start_Date__c.format()) :String.valueOf(project.Start_Date__c) ;
                result.endDate = project.End_Date__c != null ? String.valueOf(project.End_Date__c.format()): String.valueOf(project.End_Date__c);
                result.goLive = project.Go_Live__c != null ? String.valueOf(project.Go_Live__c.format()) :String.valueOf(project.Go_Live__c);
                result.status = project.Status__c;
                result.budgetStatus = project.Budget_Status__c;
                result.scheduleStatus = project.Schedule_Status__c;
                result.resourceStatus = project.Resource_Status__c;
                result.issueStatus = project.Issue_Status__c;
                result.lifecycle = project.Lifecycle__c;
                result.description = project.Description__c;
                result.parentProgram = project.Parent_Program__c;
                result.parentProgramTitle = project.Parent_Program__r.Program_Title__c;
                result.parentRoadmap = project.Parent_Road_Map__c;
                result.parentRoadmapTitle = project.Parent_Road_Map__r.RoadMap_Title__c;
                result.actual = project.Actual__c;
                result.actualPercentageOfBudget = project.Actual_percent_of_Budget__c;
                result.budget = project.Budget__c;
                result.budgetRemaining = project.Budget_Remaining__c;
                result.funded = project.Funded__c;
                result.actualEndDate = project.Actual_End_Date__c != null ? String.valueOf(project.Actual_End_Date__c.format()): String.valueOf(project.Actual_End_Date__c);
                result.actualStartDate = project.Actual_Start_Date__c != null ? String.valueOf(project.Actual_Start_Date__c.format()): String.valueOf(project.Actual_Start_Date__c);
                result.ownerId = project.OwnerId;
                result.ownerName = project.Owner.Name;
                result.createdByName = project.CreatedBy.Name;
                result.createdById = project.CreatedById;
                result.lastModifiedById = project.LastModifiedById;
                result.lastModifiedByName = project.LastModifiedBy.Name;
                result.plannedEndDate = project.Planned_End_Date__c != null ?String.valueOf(project.Planned_End_Date__c.format()):String.valueOf(project.Planned_End_Date__c);
                result.plannedStartDate = project.Planned_Start_Date__c != null ? String.valueOf(project.Planned_Start_Date__c.format()):String.valueOf(project.Planned_Start_Date__c);
                result.isProjectEditable = projectAccessMap.get(project.Id);
                result.parentProgramAutonumber = project.Parent_Program__r.Name;
                result.parentRoadmapAutonumber = project.Parent_Road_Map__r.Name;
                resultList.add(result);
                
            }
            System.debug('===-=-=resultList Project-=-=-=-=-=-'+resultList);
            ROQResponseWrapper.ProjectBankResultWrapper projectGridResult = new ROQResponseWrapper.ProjectBankResultWrapper();
            projectGridResult.getProjectsGridViewResult = resultList;
            projectGridResult.getObjectPermissions = ServiceUtil.getObjectPermissions();
            projectGridResult.totalProjectCount = String.valueOf(updatedCount);
            projectGridResult.projectbankColumnList = columnsList ;
            response.isSuccess = true;
            response.data = projectGridResult;
            System.debug('-==-=result-=-= '+JSON.serialize(resultList)); 
        } catch(ApplicationExceptionManager.ApplicationException ca) {
            response.errorlist.add(ROQExceptionService.auditUtility(ca));            
        } catch(Exception ex) {
            response.errorlist.add(ROQExceptionService.parentException(ex));
        }
        return response;
        
    }
    
    public static ROQResponseWrapper.AgilingoResponseWrapper getProjectPicklistValues() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        Map<String, List<ROQResponseWrapper.SelectOption>> picklistValueMap = new Map<String, List<ROQResponseWrapper.SelectOption>>();
        
        List<ROQResponseWrapper.SelectOption> picklistOptions = new List<ROQResponseWrapper.SelectOption>();
        
        for(Schema.PicklistEntry pickVal : Project__c.Budget_Status__c.getDescribe().getPicklistValues()){
            picklistOptions.add(new ROQResponseWrapper.SelectOption(pickVal.getLabel(), pickVal.getValue()));
        }
        
        picklistValueMap.put('Budget_Status__c', picklistOptions);
        picklistOptions = new List<ROQResponseWrapper.SelectOption>();
        
        for(Schema.PicklistEntry pickVal : Project__c.Issue_Status__c.getDescribe().getPicklistValues()){
            picklistOptions.add(new ROQResponseWrapper.SelectOption(pickVal.getLabel(), pickVal.getValue()));
        }
        
        picklistValueMap.put('Issue_Status__c', picklistOptions);
        picklistOptions = new List<ROQResponseWrapper.SelectOption>();
        
        for(Schema.PicklistEntry pickVal : Project__c.Lifecycle__c.getDescribe().getPicklistValues()){
            picklistOptions.add(new ROQResponseWrapper.SelectOption(pickVal.getLabel(), pickVal.getValue()));
        }
        
        picklistValueMap.put('Lifecycle__c', picklistOptions);
        picklistOptions = new List<ROQResponseWrapper.SelectOption>();
        
        for(Schema.PicklistEntry pickVal : Project__c.Schedule_Status__c.getDescribe().getPicklistValues()){
            picklistOptions.add(new ROQResponseWrapper.SelectOption(pickVal.getLabel(), pickVal.getValue()));
        }
        
        picklistValueMap.put('Schedule_Status__c', picklistOptions);
        picklistOptions = new List<ROQResponseWrapper.SelectOption>();
        
        for(Schema.PicklistEntry pickVal : Project__c.Status__c.getDescribe().getPicklistValues()){
            picklistOptions.add(new ROQResponseWrapper.SelectOption(pickVal.getLabel(), pickVal.getValue()));
        }
        
        picklistValueMap.put('Status__c', picklistOptions);
        
        picklistOptions = new List<ROQResponseWrapper.SelectOption>();
        
        for(Schema.PicklistEntry pickVal : Project__c.Resource_Status__c.getDescribe().getPicklistValues()){
            picklistOptions.add(new ROQResponseWrapper.SelectOption(pickVal.getLabel(), pickVal.getValue()));
        }
        
        picklistValueMap.put('Resource_Status__c', picklistOptions);
        
        result.data = picklistValueMap;
        
        return result;
        
    }
    
    public static List<Program__c> getProgramList() {
        
        List<Program__c> programList = [SELECT Id, Name, Program_Title__c FROM Program__c LIMIT 49999];
        return programList;
        
    }
    
    public static ROQResponseWrapper.ProjectPicklistWrapper getProjectInfo() {
        
        List<PermissionSetAssignment> psaList = [SELECT Id, PermissionSetId, PermissionSet.Label,PermissionSet.Name, AssigneeId, Assignee.Name, SystemModstamp FROM PermissionSetAssignment WHERE PermissionSet.Name = 'AGILINGO_Project_Team'];
        List<ServiceUtil.ROQUserWrapper> userList = new List<ServiceUtil.ROQUserWrapper>();
        System.debug('=====psaList====='+psaList);
        for(PermissionSetAssignment psa : psaList) {
            ServiceUtil.ROQUserWrapper userWrapper = new ServiceUtil.ROQUserWrapper();
            userWrapper.UserId = psa.AssigneeId;
            userWrapper.UserLabel = psa.Assignee.Name;
            userList.add(userWrapper);
        }
        
        ROQResponseWrapper.ProjectPicklistWrapper pklistWrapper = new ROQResponseWrapper.ProjectPicklistWrapper();
        pklistWrapper.getProjectUser = userList;
        pklistWrapper.getProgramList = getProgramList();
        pklistWrapper.getProjectOwners = getProjectUser();
        
        return pklistWrapper;
        
    }
    
    public static List<Road_Map__c> getRelatedRoadmapsToProgram(String programId) {
        
        Id progId = Id.valueOf(programId);
        List<Road_Map__c> roadmapList = [SELECT Id, Name, RoadMap_Title__c FROM Road_Map__c WHERE Program_Id__c =: progId LIMIT 49999];
        return roadmapList;
        
    }
    
    /**
* Date : 23/03/2017
* Description : Creation of Project wizard Page. 
* Param : project
* Return : Project record   
*/
    
    public static ROQResponseWrapper.AgilingoResponseWrapper doSaveProject(String project, Integer offsetValue, String searchText, Integer limitValue, String order, String fieldName, String projectCount) {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
        result.errorlist = new List<ROQResponseWrapper.Error>();
        System.debug('-=--=-project=-=-=-'+project);
        try{
            ROQResponseWrapper.ProjectSelectOption projectRecord = (ROQResponseWrapper.ProjectSelectOption)JSON.deserialize(project, ROQResponseWrapper.ProjectSelectOption.class);
            
            ROQProjectService.setProjectRecord(projectRecord);
            
            
            ROQResponseWrapper.ProjectSaveWrapper projectSaveResult = new ROQResponseWrapper.ProjectSaveWrapper();
            projectSaveResult.response = projectRecord;
            projectSaveResult.getProjectsGridViewResult = ROQProjectService.getProjectsGridView(offsetValue, searchText, limitValue, order, fieldName, projectCount);
            result.isSuccess = true;
            result.data = projectSaveResult;                    
            result.Msg = SYSTEM.LABEL.PROJECT_BANK_SAVE_MESSAGE;
            result.data = projectSaveResult;
            
            System.debug('--=-=-=result-=-=-=-='+result);
            
        } catch(ApplicationExceptionManager.ApplicationException ca) {
            result.errorlist.add(ROQExceptionService.auditUtility(ca));            
        } catch(DMLException de) {
            result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        } 
        return result;
        
    }
    
    
    public static Project__c setProjectRecord(ROQResponseWrapper.ProjectSelectOption project) {
       
        
        System.debug('=====project data===='+project);
        Project__c projectRecord = new Project__c(
            Budget_Status__c = project.budgetStatus,
            Description__c = project.description,
            Issue_Status__c =  project.issueStatus,
            Lifecycle__c = project.lifecycle,
            Parent_Program__c = project.parentProgram,
            Project_Manager__c = project.projectManager,
            Resource_Status__c = project.resourceStatus,
            Parent_Road_Map__c = project.parentRoadmap != null && project.parentRoadmap != '' ? project.parentRoadmap : null,  
            Schedule_Status__c = project.scheduleStatus,
            Status__c = project.status,
            Project_Title__c = project.projectLabel
            
        );
        
        System.debug('=====projectRecord=1===='+projectRecord);
        
        projectRecord.Actual__c = project.actual != null ? project.actual  : null ;
        
       	projectRecord.Actual_End_Date__c = project.actualEndDate != null && project.actualEndDate != '' ?
            											ServiceUtil.formatDate(project.actualEndDate)  :
        																			   			  null ;
																					   
	   projectRecord.Actual_Start_Date__c = project.actualStartDate != null && project.actualStartDate != '' ?
            												ServiceUtil.formatDate(project.actualStartDate)  :
        																			   					null ;        
        
        projectRecord.Budget__c = project.budget != null ? project.budget : null ;
        
        projectRecord.Go_Live__c = project.goLive != null && project.goLive != '' ? ServiceUtil.formatDate(project.goLive) : null ;     
        
        projectRecord.Funded__c = project.funded != null ? project.funded  : false ;
        
      	projectRecord.Planned_End_Date__c = project.plannedEndDate != null && project.plannedEndDate != '' ?
            											  ServiceUtil.formatDate(project.plannedEndDate) :
        																			                null ;   

	 projectRecord.Planned_Start_Date__c = project.plannedStartDate != null && project.plannedStartDate != '' ?
            												ServiceUtil.formatDate(project.plannedStartDate)  :
        																			   					null  ;   		
        
        if(project.ownerId != null){
            projectRecord.put('OwnerId',project.ownerId);
        }
        
        if(Schema.sObjectType.Project__c.isUpdateable()){
            System.debug('=====Coming=====');
            projectRecord.Id = (''+project.projectId).trim() == '' ? null : project.projectId ;
        } 
        
        //try {
            UPSERT projectRecord;
        //} catch(DMLException de){
             //de.getMessage();
        //}
        
        System.debug('=====projectRecord====='+projectRecord);
        return projectRecord; 
    }
    
    
    public static List<ServiceUtil.ROQUserWrapper> getProjectUser() {
        
        List<ServiceUtil.ROQUserWrapper> userList = new List<ServiceUtil.ROQUserWrapper>();
        List<PermissionSetAssignment> psaList = [SELECT Id, AssigneeId, Assignee.Name, PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name = 'AGILINGO_Project_Team'];
        User user = [SELECT Id, Name FROM USER WHERE Id =: userInfo.getUserId() LIMIT 1];
        
        for(PermissionSetAssignment assignment : psaList){
            
            ServiceUtil.ROQUserWrapper userWrapper = new ServiceUtil.ROQUserWrapper();
            userWrapper.UserId = assignment.AssigneeId;
            userWrapper.UserLabel = assignment.Assignee.Name;
            userWrapper.LoggedInUserId = user.Id;
            userWrapper.LoggedInUserName = user.Name;
            
            userList.add(userWrapper);
        }
        return userList;
        
    }
    
    /**
* Date : 22/03/2018
* Description : Wrapper Class for Project Bank Columns Preference. 
*/
    public class ProjectBankColumn{
        
        public List<String> fieldList;
        
    }
    
    /**
* Date : 22/03/2018
* Description : To save the Fields selected by user.
* Param : List of fields and UserId.
* Return : ROQResponseWrapper.AgilingoResponseWrapper.
*/
    public static ROQResponseWrapper.AgilingoResponseWrapper doSaveProjectBankColumns(List<String> fieldsList) {
        
        System.debug('=== fieldsList ====='+fieldsList);
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.data = null;
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try {
            
            ProjectBankColumn columns = new ProjectBankColumn();
            columns.fieldList = fieldsList;
            
            User_Preferences__c userPre = new User_Preferences__c();
            userPre.Epic_Bank_Column__c =  JSON.serialize(columns.fieldList);
            userPre.User__c = UserInfo.getUserId();
            
            List<User_Preferences__c>  userpreferenceList = [ SELECT Id, User__c,  Project_Bank_Column__c FROM User_Preferences__c LIMIT 1] ;
            
            System.debug('=====userpreferenceList====='+userpreferenceList);
            
            if(userpreferenceList.size() == 0){
                System.debug('===== Coming in if=====');
                INSERT userPre; 
                result.data = userPre;
            } else {
                
                userpreferenceList[0].Project_Bank_Column__c  = JSON.serialize(columns.fieldList);
                System.debug('===== Coming in else=====');
                UPDATE userpreferenceList[0];       
                result.data = userpreferenceList[0];
            }
            
        } catch(DMLException de){
            result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(JSONException je){
            result.errorlist.add(ROQExceptionService.jsonException(je));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        return result;
    }
    
}