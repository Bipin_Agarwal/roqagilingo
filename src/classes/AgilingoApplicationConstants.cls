/**
* Author :  ROQMetrics 
* Date   : 23/06/2017
* Description : Class consisting of all Constants.
*/
public class AgilingoApplicationConstants{

    //static String nameSpace = [SELECT Id, Name, NamespacePrefix FROM Organization LIMIT 1].NamespacePrefix;
    
    public static string ADMINISTRATOR = 'Administrator';
    public static string EXECUTIVE = 'Executive';
    public static string PORTFOLIO_MANAGER = 'Portfolio_Team';
    public static string PROGRAM_MANAGER = 'Program_Team';
    public static string PROJECT_MANAGER = 'AGILINGO_Project_Team';
    public static string SYSTEM_ADMINISTRATOR = 'System Administrator';
    
}