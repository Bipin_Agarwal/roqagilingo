public without sharing class ApproveRejectSF1ComponentController{

    public List<ROQResponseWrapper.SignatureHistory> sgnHistories{get; set;}
    public Boolean showPage{get; set;}
    
    public Map<String, String> userIdWItemIdMap{get; Set;}
    
    
    public ApproveRejectSF1ComponentController(){
    
        Map<String, String> urlParamentersMap = ApexPages.CurrentPage().getParameters();
        String recordId = urlParamentersMap.get('id');
        String isUrlReturn = Apexpages.currentPage().getUrl();
        
        userIdWItemIdMap = new Map<String,String>();
        
        if(!Test.isRunningTest())
            showPage = isUrlReturn.contains('vfRetURLInSFX') ? true : false;
        
        sgnHistories = AgilingoSignatureHistoryController.getSignatureHistoryList(recordId);
        
        try{
           for(ProcessInstanceWorkitem witem : [SELECT Id,ActorId FROM ProcessInstanceWorkitem WHERE  ProcessInstance.TargetObjectId = :recordId AND (ActorId = :Userinfo.getUserId() OR CreatedById = :Userinfo.getUserId())]){
           
               userIdWItemIdMap.put(witem.ActorId, witem.Id);
           
           }             
        }catch(QueryException qe){}
        
        System.debug('--------------sgnHistories-----------'+sgnHistories);
    }
    
      
}