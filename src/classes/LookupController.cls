/* Refer ServiceUtilTest class for Test coverage */ 
public with sharing class LookupController {
   
    /* Method to query records using SOSL*/
    @AuraEnabled
    public static String search(String objectAPIName, String searchText,
                                List<String> whereClause, List<String> extrafields){
                                    
                                    objectAPIName = String.escapeSingleQuotes(objectAPIName);
                                    searchText = String.escapeSingleQuotes(searchText);
                                    String searchQuery = 'FIND \'' + searchText + '*\' IN ALL FIELDS RETURNING ' + objectAPIName + '(Id,Name' ;
                                    if(!extrafields.isEmpty()){
                                        searchQuery = searchQuery + ',' + String.join(extrafields, ',') ;
                                    }
                                    system.debug(whereClause);
                                    if(!whereClause.isEmpty()){
                                        searchQuery = searchQuery + ' WHERE ' ;
                                        searchQuery = searchQuery + String.join(whereClause, 'AND') ;
                                    }
                                    searchQuery = searchQuery + ' LIMIT 5 ) ';
                                    system.debug('=====searchQuery in search method========='+searchQuery);
                                    return JSON.serializePretty(search.query(searchQuery)) ;
                                }
    
    /* Method to query records using SOQL*/
    @AuraEnabled
    public static List<SObject> getRecentlyViewed(
        String objectAPIName,
        List<String> whereClause,
        List<String> extrafields){
            
            String searchQuery = 'SELECT Id, Name';
            // modified by vipul
            if(!extrafields.isEmpty()){
                for(String extField :extrafields ){
                    searchQuery =  !extField.equalsIgnoreCase('Id') && !extField.equalsIgnoreCase('Name') ?
                                              								  searchQuery + ','+ extField :
                                                                                              searchQuery ;
                }
            }
            //modification ends
           /* if(!extrafields.isEmpty()){
                    searchQuery = searchQuery + ',' + String.join(extrafields, ',') ;
                }*/
            searchQuery = searchQuery + ' FROM ' + objectAPIName;
            if(!whereClause.isEmpty()){
                searchQuery = searchQuery + ' WHERE ' ;
                searchQuery = searchQuery + String.join(whereClause, 'AND') ;
                system.debug('=========='+searchQuery);
            }
            searchQuery = searchQuery + ' ORDER BY LastModifiedDate DESC LIMIT 5' ;
            System.debug('searchQuery====='+searchQuery);
            List<SObject> objectList =  new List<SObject>();
            objectList = Database.query(searchQuery);
            System.debug('objectList====='+objectList);
            return objectList;
        }
}