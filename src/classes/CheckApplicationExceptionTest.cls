@isTest
private class CheckApplicationExceptionTest{
   static testMethod void test_UseCase1(){
    ROQ_Exception__c exAudit = new ROQ_Exception__c (Error_code__c = '1001');
    INSERT exAudit;
    System.assertEquals('1001',exAudit.Error_code__c);
    
  }
   static testMethod void test_UseCase2(){
    ROQ_Exception__c exAudit = new ROQ_Exception__c (Error_code__c = '1001');
    INSERT exAudit;
    System.assertEquals('1001',exAudit.Error_code__c);
    
  }
}