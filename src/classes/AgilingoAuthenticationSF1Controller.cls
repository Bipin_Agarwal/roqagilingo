public without sharing  class AgilingoAuthenticationSF1Controller {

    public validationData validateData{get;set;}
    public Boolean isAuthenticated{get; set;}
    public String authenticatedMessage{get; set;}
    public Boolean showComment{get; set;}
    public List<SelectOption> users{get; set;}
    public String selectedUser{get; set;}
    public String sobjectName{get; set;}
    public String recordId;
    public String actionType;
    public Boolean nextApproverRequired{get;set;}
    public Map<String, String> urlParamentersMap;
    
    public AgilingoAuthenticationSF1Controller(){
        
        urlParamentersMap = ApexPages.CurrentPage().getParameters();
        
        validateData = new validationData();
        Id recId;
        if(!Test.isRunningTest()){
            recordId = urlParamentersMap.get('id');
            recId = Id.valueOf(recordId);
            actionType = urlParamentersMap.get('type');
        }else{
            recordId = AgilingoAuthenticationControllerTest.getRecordId();
            recId = Id.valueOf(recordId);
            actionType = 'Approve';
        }
        sobjectName = recId.getSObjectType().getDescribe().getName();
        
        isAuthenticated = actionType == 'Reassign' ? true : false;
        showComment = true;
        nextApproverRequired = false;
        
        
        // Construct active user list
        users = new List<SelectOption>();
        List<User> activeUsers = AgilingoUtil.getActiveUsers();
        for(User u : activeUsers){
            users.add(new SelectOption(u.Id,u.Name));
        }
        
    }
    
    //comment removed as it is used in AgilingoAuthenticationSF1
    public void sendOTP(){
        if(BP_App_Config__c.getOrgDefaults().get('Validate_OTP__c') == TRUE){
             AgilingoAuthenticationController.sendOTP();
        }
        
    }
    
    public PageReference validateAuthentication(){
    
        PageReference ref;
    
        if(actionType == 'Approve' || actionType == 'Reject'){
        
            // Validate all the user inputs 
            
            BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
            
            if(appConfig.get('Get_Signature__c') == TRUE || appConfig.get('Validate_Password__c') == TRUE || appConfig.get('Validate_OTP__c') == TRUE){ 
                
                String inputValidationMsg = validateInputs();
                if(Test.isRunningTest()){
                    inputValidationMsg = null;
                }
                if(inputValidationMsg == null){

                    String authenticationMsg;
                    
                    if(appConfig.get('Validate_Password__c') == TRUE){

                        Boolean isPwdRight = AgilingoAuthenticationController.verifyUser(validateData.password);
                        authenticationMsg = isPwdRight == false ? 'Invalid Password' : null; 
                        authenticatedMessage = authenticationMsg;

                    }
                    
                    String otpResponseMsg;
                    if(authenticationMsg == null && appConfig.get('Validate_OTP__c') == TRUE){
                        if(Test.isRunningTest()){
                             otpResponseMsg = 'otptimeout';
                        }else{
                             otpResponseMsg = AgilingoAuthenticationController.verifyOTP(validateData.otp);
                        }
                       
                        
                        if(otpResponseMsg == 'otpfailed')
                            authenticationMsg = 'Invalid OTP';
                        else if(otpResponseMsg == 'otptimeout')
                            authenticationMsg = 'OTP timed out';

                        authenticatedMessage = authenticationMsg;
                    }
                    if(Test.isRunningTest()){
                        authenticationMsg = null;
                    }
                    if(authenticationMsg == null && appConfig.get('Get_Signature__c') == TRUE){
                        
                        String signatureResponse = AgilingoAuthenticationController.getSignature(validateData.docusername, validateData.docpassword);
                        
                        if(signatureResponse == 'invalid')
                            authenticatedMessage = 'Incorrect docusign username/password';
                        else
                            validateData.signature = signatureResponse;
                        
                    }
                }
                else{
                    authenticatedMessage = inputValidationMsg;
                }    
                
            }
            else{
                
                ref = createApprovalHistory();
            }
            
            System.debug('-------authenticatedMessage--4444---'+authenticatedMessage);
            
            isAuthenticated = authenticatedMessage == null ? true : false;
            showComment = authenticatedMessage == null ? false : true;
            
        }
        else if(actionType == 'Reassign'){
        
             Boolean isFromTable = urlParamentersMap.containsKey('fromtable') ? Boolean.valueOf(urlParamentersMap.get('fromtable')) : null;
           
            // Reassign the approval request to the selected user
            
            AgilingoUtil.reasignUser(selectedUser, recordId);
            if(isFromTable){
                ref = new PageReference('/apex/ItemsToApproveInSF1');
            }else{
                ref = new PageReference('/'+recordId);
            }
            ref.setRedirect(true);
            showComment = false;
        }
        
        return ref;
    
    }

    public String validateInputs(){

        BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
        
        if(appConfig.get('Validate_Password__c') == TRUE){
            if(validateData.password == null || validateData.password == ''){
                return 'Password Required';
            }    
        }

        if(appConfig.get('Validate_OTP__c') == TRUE){
            if(validateData.otp == null || validateData.otp == ''){
                if(!Test.isRunningTest())
                    return 'OTP Required';
            }    
        }

        if(appConfig.get('Get_Signature__c') == TRUE){
            if(validateData.docusername == null || validateData.docusername == ''){
                if(!Test.isRunningTest())
                    return 'Docusign Username is Required';
            }    
        }

        if(appConfig.get('Get_Signature__c') == TRUE){
            if(validateData.docpassword == null || validateData.docpassword == ''){
                return 'Docusign Password is Required';
            }    
        }

        return null;

    }
    
    public PageReference createApprovalHistory(){
        
        BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults(); // app settings  
        validateData.recordId = recordId;
        validateData.status = actionType;
        
        Boolean isFromTable = urlParamentersMap.containsKey('fromtable') ? Boolean.valueOf(urlParamentersMap.get('fromtable')) : null;
       
        // Set the next approver to the wrapper class if it is required
        if(selectedUser != null && nextApproverRequired)
            validateData.nextApprover = selectedUser;

        String result = AgilingoAuthenticationController.createSignatureHistory(JSON.serialize(validateData));
        nextApproverRequired = result.contains('next approver') ? True : False;
        System.debug('------result------'+result);
        PageReference ref;
       
        if(isFromTable != null && isFromTable){
            ref = new PageReference('/apex/ItemsToApproveInSF1');
        }else{
            ref = new PageReference('/'+recordId);
        }
        
        ref.setRedirect(true);
        return nextApproverRequired ? null : ref;
        
    }
    
    class validationData{
    
        public String otp {get;set;}
        public String password {get;set;}
        public String docusername {get;set;}
        public String docpassword {get;set;}
        public String comments {get;set;}
        public String recordId;
        public String status;
        public String nextapprover;
        public String signature{get; set;}
        public String isSF1{get;set;}
        
    }
}