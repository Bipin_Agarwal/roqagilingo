/*
#    File Name    : ApplicationExceptionManager 
#    Created by   : ROQ metrics
#    Created Date : 17-Mar-2017 
#    Description  : This class is used for holding exception codes.
*/
public class ApplicationExceptionCodeMap {

    // Started with 1000 - System exceptions
    // Started with 2000 - Application exceptions
    // Started with 3000 - Custom exceptions
    // Started with 4000 - Other exceptions
    
    public static Map<String,String> exceptionCodes = new Map<String,String>{
        
            '1001' => 'DML EXCEPTION',
            '1002' => 'JSON EXCEPTION',
            '1003' => 'QUERY EXCEPTION',
            '3001' => 'EXCEPTION',    
            '4001' => 'OTHER EXCEPTION'
               
                
    };

    public static String getApplicationError(String code){
        System.debug('-- >exceptionCodes.get(code)>>> '+exceptionCodes.get(code));
        return exceptionCodes.containsKey(code) 
            ? exceptionCodes.get(code) 
            : null;

    }

}