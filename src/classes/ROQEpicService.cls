/**
* Author :  ROQMetrics 
* Date   : 16/03/2017
* Description : Service class for Epic.
*/
global class ROQEpicService {
    
    global interface IEpicOption {}
    
/**
* Date : 16/03/2017
* Description : Wrapper Class for Epic. 
* Param : Null.
* Return : Void.
*/
    public class EpicWrapper implements IEpicOption {
        
        public Id epicId;
        public String epicLabel;
        public String description;
        public String epicSize;
        public String epicType;
        public String epicStatus;
        public String priority;
        public String category;
        public List<EpicEpicTagWrapper> epicTagList;
        
    }
    
    public class EpicEpicTagWrapper {
        
        public Id epicTagId;
        public String epicTagName;
        public Id epicTagTitle; 
        
    }
    
    /**
* Date : 16/03/2017
* Description : To get all available Epic by parameters.
* Param : Epic Name ,noOfRecord, isVisible and Epic Type.
* Return : List of Epics. 
*/
    
    
    /**
* Date : 16/03/2017
* Description :Get all the available Epics.
* Param : epics
* Return : epicList. 
*/
    public static List<Epic__c> getEpicsFromEpicWrapper(List<ROQResponseWrapper.Epic> epics) {
        
        List<Epic__c> epicList = new List<Epic__c>();
        if(epics != null){
            for(ROQResponseWrapper.Epic epic : epics){
                epicList.add(setEpicRecord(epic));
            }
        }
        return epicList;
    }
    
    /**
* Date : 16/03/2017
* Description :Get available Epic Record.
* Param : epic
* Return : Epic record. 
*/    
    public static Epic__c setEpicRecord(ROQResponseWrapper.Epic epic) {
        
        Epic__c epicRecord = new Epic__c(
            Epic_Title__c = epic.name,
            Epic_Size__c   = epic.size,
            Epic_Status__c = epic.status,
            Description__c = epic.Description,
            Epic_Type__c = epic.EpicType,
            Priority__c = epic.priority,
            Lifecycle__c = epic.Lifecycle,
            REQ__c = epic.requirementNumber,
            Regulation__c =  epic.regulation
        );
        if(epic.Category != null){
            epicRecord.put('Category__c',epic.Category);
        }
        if(epic.epicOwnerId != null){
            epicRecord.put('OwnerId',epic.epicOwnerId);
        }
        epicRecord.Date_Groomed__c = epic.dateGroomed != null && epic.dateGroomed != '' ?
            																Date.valueOf(epic.dateGroomed) :
        																	null;
        if(epic.dateApproved != null && epic.dateApproved != '') {
            epicRecord.put('Date_Approved__c', Date.valueOf(epic.dateApproved));
        } else {
            epicRecord.put('Date_Approved__c', null);
        }
        
        if(epic.dateCommitted != null && epic.dateCommitted != '') {
            epicRecord.put('Date_Committed__c', Date.valueOf(epic.dateCommitted));
        } else{
            epicRecord.put('Date_Committed__c', null);
        }
        
        if(epic.dateCommitted != null && epic.dateCommitted != '') {
            epicRecord.put('Date_Completed__c', Date.valueOf(epic.dateCompleted));
        } else{
         	epicRecord.put('Date_Completed__c', null);   
        }
        
        if(epic.assignedTo != null) {
            epicRecord.put('Assigned_To__c', epic.assignedTo.Id);
        }else{
            epicRecord.put('Assigned_To__c', null);
        }
        
        System.debug('================epicRecord====='+epicRecord);
        if(Schema.sObjectType.Epic__c.isUpdateable()){
            System.debug('=====================Inside if setEpic==============================================');
            epicRecord.Id = (''+epic.epicId).trim() == '' ? null : epic.epicId ;
        } 
        
        return epicRecord; 
    }
    
    public static Epic__c setEpicRecord(ROQResponseWrapper.Epic epic, String epicId) {
        
        Epic__c epicRecord = new Epic__c(
            Epic_Title__c = epic.name,
            Epic_Size__c   = epic.size,
            Epic_Status__c = epic.status,
            Description__c = epic.Description,
            Epic_Type__c = epic.EpicType,
            Priority__c = epic.priority,
            Lifecycle__c = epic.Lifecycle,
            REQ__c = epic.requirementNumber,
            Regulation__c =  epic.regulation
            
            
        );
        
        if(epicId != null){
            epicRecord.put('Parent_Epic__c',epicId);
        }
        
        if(epic.epicOwnerId != null){
            epicRecord.put('OwnerId',epic.epicOwnerId);
        }
        
        if(epic.Category != null){
            epicRecord.put('Category__c',epic.Category);
        }
        
        epicRecord.Date_Groomed__c = epic.dateGroomed != null && epic.dateGroomed != '' ?
            																Date.valueOf(epic.dateGroomed) :
        																	null;
        if(epic.dateApproved != null && epic.dateApproved != '') {
            epicRecord.put('Date_Approved__c', Date.valueOf(epic.dateApproved));
        } else {
            epicRecord.put('Date_Approved__c', null);
        }
        
        if(epic.dateCommitted != null && epic.dateCommitted != '') {
            epicRecord.put('Date_Committed__c', Date.valueOf(epic.dateCommitted));
        } else{
            epicRecord.put('Date_Committed__c', null);
        }
        
        if(epic.dateCompleted != null && epic.dateCompleted != '') {
            epicRecord.put('Date_Completed__c', Date.valueOf(epic.dateCompleted));
        } else{
         	epicRecord.put('Date_Completed__c', null);   
        }
        
        if(epic.assignedTo != null) {
            epicRecord.put('Assigned_To__c', epic.assignedTo.Id);
        }else{
            epicRecord.put('Assigned_To__c', null);
        }
        
        if(Schema.sObjectType.Epic__c.isUpdateable()){
            epicRecord.Id = (''+epic.epicId).trim() == '' ? null : epic.epicId ;
        } 
        
        return epicRecord; 
    }
    
    /**
* Date : 16/03/2017
* Description : To get all available Epic with Feature Epics.
* Param : epics
* Return : Map of Epics and Feature Epics. 
*/
    public static Map < String, List < Epic__c >> getFeatureEpic(List < ROQResponseWrapper.Epic > epics) {
        
        //Map of parent Epic Id with respective Feature Epic 
        Map < String, List < Epic__c >> epicMap = new Map < String, List < Epic__c >> ();
        
        for (ROQResponseWrapper.epic epic: epics) {
            epicMap.put(epic.epicId, getFeatureEpicsFromEpicWrapper(epic.featureEpics,epic.epicId));
        }
        return epicMap;
        
    }
    
    /**
* Date : 16/03/2017
* Description :Get all the available Feature Epics.
* Param : feautureEpics, epicId
* Return : featureEpicList. 
*/
    public static List < Epic__c > getFeatureEpicsFromEpicWrapper(List<ROQResponseWrapper.Epic> feautureEpics,String epicId) {
        
        List < Epic__c > featureEpicList = new List < Epic__c > ();
        
        //To create the Feature Epic record
        for (ROQResponseWrapper.Epic epic: feautureEpics) {
            featureEpicList.add(setFeatureEpicRecord(epic,epicId));
        }
        
        return featureEpicList;
    }
    
    /**
* Date : 16/03/2017
* Description : To Set the Feature Epic record with respective epicId.
* Param : epic, epicId
* Return : epicRecord. 
*/    
    public static Epic__c setFeatureEpicRecord(ROQResponseWrapper.Epic epic, String epicId) {
        
        //To set the feature epic record
        Epic__c epicRecord = new Epic__c (
            Epic_Title__c = epic.name,
            Description__c = epic.Description,
            Epic_Size__c   = epic.Size,
            Epic_Status__c = epic.Status,
            Priority__c = epic.Priority,
            Lifecycle__c = epic.Lifecycle,
            OwnerId = epic.epicOwnerId,
            REQ__c = epic.requirementNumber,
            Regulation__c =  epic.regulation
        );
        if(epic.Category != null){
            epicRecord.put('Category__c ',epic.Category );
        }
        if(epic.ParentEpicId != null){
            epicRecord.put('Parent_Epic__c',epic.ParentEpicId);
        }
    	
        epicRecord.Date_Groomed__c = epic.dateGroomed != null && epic.dateGroomed != '' ?
            																Date.valueOf(epic.dateGroomed) :
        																	null;
        if(epic.dateApproved != null && epic.dateApproved != '') {
            epicRecord.put('Date_Approved__c', Date.valueOf(epic.dateApproved));
        } else {
            epicRecord.put('Date_Approved__c', null);
        }
        
        if(epic.dateCommitted != null && epic.dateCommitted != '') {
            epicRecord.put('Date_Committed__c', Date.valueOf(epic.dateCommitted));
        } else{
            epicRecord.put('Date_Committed__c', null);
        }
        
        if(epic.dateCompleted != null && epic.dateCompleted != '') {
            epicRecord.put('Date_Completed__c', Date.valueOf(epic.dateCompleted));
        } else{
         	epicRecord.put('Date_Completed__c', null);   
        }
        
        if(epic.assignedTo != null) {
            epicRecord.put('Assigned_To__c', epic.assignedTo.Id);
        }else{
            epicRecord.put('Assigned_To__c', null);
        }
        
        if(Schema.sObjectType.Epic__c.isUpdateable()) {
            epicRecord.Id = (''+epic.epicId).trim() == '' ? null : epic.epicId ; 
        }
        return epicRecord;
    }
    
    /**
* Date : 16/03/2017
* Description : To Set the Feature Epic record with parent EpicId which is refered in Release Plan Service class.
* Param : epic data.
* Return : feature epicRecord. 
*/
    public static Epic__c setFeatureEpicRecord(ROQResponseWrapper.Epic epic) {
        
        Epic__c epicRecord = new Epic__c (
            Epic_Title__c = epic.name,
            Description__c = epic.Description,
            Epic_Type__c = epic.EpicType,
            Epic_Size__c   = epic.Size,
            Epic_Status__c = epic.Status,
            Priority__c = epic.Priority,
            Lifecycle__c = epic.Lifecycle,
            OwnerId = epic.epicOwnerId,
            REQ__c = epic.requirementNumber,
            Regulation__c =  epic.regulation
        );
        if(epic.ParentEpicId != null){
            epicRecord.put('Parent_Epic__c',epic.ParentEpicId);
        }
        if(epic.Category != null){
            epicRecord.put('Category__c',epic.Category);
        }
        epicRecord.Date_Groomed__c = epic.dateGroomed != null && epic.dateGroomed != '' ?
            																Date.valueOf(epic.dateGroomed) :
        																	null;
        if(epic.dateApproved != null && epic.dateApproved != '') {
            epicRecord.put('Date_Approved__c', Date.valueOf(epic.dateApproved));
        } else {
            epicRecord.put('Date_Approved__c', null);
        }
        
        if(epic.dateCommitted != null && epic.dateCommitted != '') {
            epicRecord.put('Date_Committed__c', Date.valueOf(epic.dateCommitted));
        } else{
            epicRecord.put('Date_Committed__c', null);
        }
        
        if(epic.dateCompleted != null && epic.dateCompleted != '') {
            epicRecord.put('Date_Completed__c', Date.valueOf(epic.dateCompleted));
        } else{
         	epicRecord.put('Date_Completed__c', null);   
        }
        
        if(epic.assignedTo != null) {
            epicRecord.put('Assigned_To__c', epic.assignedTo.Id);
        }else{
            epicRecord.put('Assigned_To__c', null);
        }
        
        //Check the field can be updateable or not before set the data
        if (Schema.sObjectType.Epic__c.isUpdateable()){
            epicRecord.Id = (''+epic.epicId).trim() == '' ? null : epic.epicId ;
        }
        
        return epicRecord;
    }
    
    public static  List<ROQResponseWrapper.Epic> getFeatureEpicWrapper(List<Epic__c> epicList, Map <Integer, ROQResponseWrapper.Epic> tagMap) {
        List<ROQResponseWrapper.Epic> epicWrapRecordList = new List<ROQResponseWrapper.Epic>();
        for(Epic__c epic : epicList){
            ROQResponseWrapper.Epic epicWrapRecord = new ROQResponseWrapper.Epic();
            epicWrapRecord.name = epic.Epic_Title__c;
            epicWrapRecord.epicId = epic.Id;
            epicWrapRecord.Description = epic.Description__c;
            epicWrapRecord.EpicType = epic.Epic_Type__c ;
            epicWrapRecord.Size = epic.Epic_Size__c;
            epicWrapRecord.Status = epic.Epic_Status__c ;
            epicWrapRecord.Priority = epic.Priority__c;
            epicWrapRecord.Category = epic.Category__c;
            epicWrapRecord.requirementNumber = epic.REQ__c;
            epicWrapRecord.regulation = epic.Regulation__c;
            if(epic.Parent_Epic__c != null){
                epicWrapRecord.ParentEpicId = epic.Parent_Epic__c;
            }
            
            if(epic.Sequence__c != null && Integer.ValueOf(epic.Sequence__c) > -1) {
                List<Epic_Tag__c> tagList = tagMap.get(Integer.ValueOf(epic.Sequence__c)).Tag;
                epicWrapRecord.Tag = tagList ;
                epicWrapRecord.epicTagDetails = tagMap.get(Integer.ValueOf(epic.Sequence__c)).epicTagDetails;
                
            }
            
            
            epicWrapRecordList.add(epicWrapRecord);
        }
        
        return epicWrapRecordList;
    }
    
    /**
* Date : 27/07/2017
* Description : To check the Initiative Epic used as Parent Epic in Roadmap.
* Param : Epic Id.
* Return : Boolean.
*/
    public static Boolean isInitiativeEpicDeletable(Id parentEpicId){
        
        List<Road_Map_Item__c> roadmapItemList = new List<Road_Map_Item__c>();
        if (Schema.sObjectType.Road_Map_Item__c.isAccessible()) {
            roadmapItemList = [SELECT Id, Epic__c  FROM Road_Map_Item__c WHERE 
                               Epic__r.Parent_Epic__c =:parentEpicId];
        }
        if(roadmapItemList.size() > 0){
            return false;
        } else {
            return true;
        }
    }
    
    /**
* Date : 27/07/2017
* Description : To check the Roadmap Epic used as Parent Epic in Increment.
* Param : Epic Id.
* Return : Boolean.
*/
    public static Boolean isRoadmapEpicDeletable(Id parentEpicId) {
        
        List<Program_Increment_Item__c> piItemList = new List<Program_Increment_Item__c>();
        if (Schema.sObjectType.Program_Increment_Item__c.isAccessible()) {
            piItemList = [SELECT Id, Feature_Epic__c FROM Program_Increment_Item__c WHERE 
                          Feature_Epic__r.Parent_Epic__c =:parentEpicId ];
        }
        if(piItemList.size() > 0) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
* Date : 13/02/2018
* Description : To save the category color selected by user.
* Param : Color(hex code) and Record Id.
* Return : ROQResponseWrapper.AgilingoResponseWrapper.
*/
    public static ROQResponseWrapper.AgilingoResponseWrapper doSaveCategoryColor(String color, String recordId) {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.data = null;
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try {
            
            Epic_Category__c epicCategory = new Epic_Category__c ();
            epicCategory.Properties__c = color;
            epicCategory.Id = recordId;
            
            if (Schema.sObjectType.Epic_Category__c.isUpdateable()) {
                UPSERT epicCategory;
            }
            
            result.data = epicCategory;
        } catch(DMLException de){
            result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(JSONException je){
            result.errorlist.add(ROQExceptionService.jsonException(je));
            
        }
        
        return result;
    }
    
    /**
* Date : 13/02/2018
* Description : To save the Tag color selected by user.
* Param : Color(hex code) and Record Id.
* Return : ROQResponseWrapper.AgilingoResponseWrapper.
*/
    public static ROQResponseWrapper.AgilingoResponseWrapper doSaveTagColor (String color, String recordId){
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.data = null;
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try {
            Epic_Tag__c epicTag = new Epic_Tag__c ();
            epicTag.Properties__c = color;
            epicTag.Id = recordId;
            
            if (Schema.sObjectType.Epic_Tag__c.isUpdateable()) {
                UPSERT epicTag;
            }
            result.data = epicTag;
        }catch(DMLException de){
            result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(JSONException je){
            result.errorlist.add(ROQExceptionService.jsonException(je));
        }
        return result;
    }
    
    
    
    public static String getTagProperties(String recordId) {
    
        Id recId = Id.valueOf(recordId);
        Epic_Tag__c epicTag = [SELECT Properties__c FROM Epic_Tag__c WHERE Id =: recId];
        return epicTag.Properties__c;
    
    }
    
    public static String getCategoryProperties(String recordId) {
    
        Id recId = Id.valueOf(recordId);
        Epic_Category__c epicCategory = [SELECT Properties__c FROM Epic_Category__c WHERE Id =: recId];
        return epicCategory.Properties__c;
    
    }
    
    
    //@deprecated
    global static ROQResponseWrapper.AgilingoResponseWrapper getEpicCategories() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
        result.errorList = new List<ROQResponseWrapper.Error>();
        
        try {
            List<Epic_Category__c> epicCategoryList = new List<Epic_Category__c>();
            // SECURITY SCANNER ISSUE ----- 21-12-2017 
            if (Schema.sObjectType.Epic_Category__c.isAccessible()) {
                epicCategoryList = [SELECT Id, Title__c, Description__c FROM Epic_Category__c ORDER BY Title__c ASC LIMIT 49999];
            }
            result.isSuccess = true;
            result.data = epicCategoryList;
            
            //  System.debug('=====epicCategoryList====='+epicCategoryList);
            
        } catch(ApplicationExceptionManager.ApplicationException ca) {
            result.errorlist.add(ROQExceptionService.auditUtility(ca));            
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return result;
    }
    
    //@deprecated
    global static ROQResponseWrapper.AgilingoResponseWrapper getEpicTags() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
        result.errorList = new List<ROQResponseWrapper.Error>();
        
        try {
            List<Epic_Tag__c> epicTagList = new List<Epic_Tag__c>();
            
            // SECURITY SCANNER ISSUE ----- 21-12-2017 
            if (Schema.sObjectType.Epic_Tag__c.isAccessible()) {
                epicTagList = [SELECT Id, Title__c, Description__c FROM Epic_Tag__c ORDER BY Title__c ASC LIMIT 49999];
            }
            result.isSuccess = true;
            result.data = epicTagList;
        } catch(ApplicationExceptionManager.ApplicationException ca) {
            result.errorlist.add(ROQExceptionService.auditUtility(ca));            
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return result;
    }
    
    //@deprecated
    global static ROQResponseWrapper.AgilingoResponseWrapper getEpicPriorityValues() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        List<SelectOption> options = new List<SelectOption>();
        
        // To get the Piclist value of the Field Epic_Size__c   
        Schema.DescribeFieldResult fieldResult = Epic__c.Priority__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        try{
            for(Schema.PicklistEntry value : ple){
                options.add(new SelectOption(value.getLabel(), value.getValue()));
                result.isSuccess = true;
                result.data = options;
            }
        } catch(ApplicationExceptionManager.ApplicationException ca) {
            
            // Create the audit record
            result.errorlist.add(ROQExceptionService.auditUtility(ca));
            
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        return result;
    }
    
}