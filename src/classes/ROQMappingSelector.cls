public Class ROQMappingSelector {

     public static List<Theme__c> getThemes(String themeName, Integer numberOfRowsToSkip) {
    
        List<SObject> result = new List<SObject> ();
        try{
            String soqlQuery = 'SELECT Id, Description__c, Theme_Title__c FROM Theme__c {0} LIMIT 20 OFFSET '+numberOfRowsToSkip;
            String filterCondtion = String.isBlank(themeName) ? 'WHERE Theme_Title__c LIKE \'%' +String.escapeSingleQuotes(themeName)+'%\'' :  'WHERE Theme_Title__c LIKE \'%' +String.escapeSingleQuotes(themeName)+'%\'';
            String queryString = String.format(soqlQuery, new List<String>{filterCondtion});
            result = Database.query(queryString);
        }catch(Exception e){
            ApplicationExceptionUtility.createAudit('4001', e.getMessage(), e);
        }
        
        return result;
    }
    
    public static List<Epic__c> getEpicsByEpicName(String epicName, Integer noOfRecord, String type) {
    
        List<SObject> result = new List<SObject> ();
        
        String soqlQuery;
        if (Schema.sObjectType.Epic__c.isAccessible()) {
            soqlQuery = 'SELECT Id, Name, Epic_Title__c, Description__c, Epic_Size__c, Epic_Type__c, Epic_Status__c, OwnerId, Owner.Name, REQ__c, Regulation__c, Parent_Epic__c, Parent_Epic__r.Epic_Title__c, '
             +' Priority__c, Category__c, Category__r.Title__c,Category__r.Name, (SELECT Id, EpicTag__c, EpicTag__r.Name, EpicTag__r.Title__c FROM Epic_EpicTags__r) FROM Epic__c WHERE Epic_Title__c != null  {0} ORDER BY Epic_Title__c ASC LIMIT '+noOfRecord;
        }
        
        //String search = String.isBlank(epicName) ? String.escapeSingleQuotes(epicName) : '';
        System.debug('=-=-=soqlQuery-=-=-=-'+soqlQuery);
        //String filterCondtion =  'AND Epic_Title__c LIKE \'%' +search+'%\'' +'OR Name LIKE \'%'+search+'%\'' ;
       	String filterCondtion = String.isBlank(epicName) ? 'AND Epic_Title__c LIKE \'%' +String.escapeSingleQuotes(epicName)+'%\'' :  'AND Epic_Title__c LIKE \'%' +String.escapeSingleQuotes(epicName)+'%\'';
        String filterConditionRoadmap = ' AND Epic_Type__c= \'' + 'Roadmap' + '\'';
        String filterConditionInitiative = ' AND Epic_Type__c= \'' + 'Initiative' + '\'';
        String filterConditionFeature = ' AND Epic_Type__c= \'' + 'Feature' + '\'';
        String queryString;
        System.debug('=-=-=filterCondtion-=-=-=-'+filterCondtion);
        try{
            if(type == 'Initiative'){
                queryString = String.format(soqlQuery, new List<String>{filterCondtion + filterConditionInitiative});
            }
            if(type == 'Roadmap') {
                    queryString = String.format(soqlQuery, new List<String>{filterCondtion + filterConditionRoadmap});
            }
            if(type == 'Feature') {
                queryString = String.format(soqlQuery, new List<String>{filterCondtion + filterConditionFeature});
            }
            result = Database.query(queryString);
        }catch(Exception e){
            ApplicationExceptionUtility.createAudit('4001', e.getMessage(), e);
        }
        return result;
    }
    
    /**
    * Date : 09/05/2017
    * Description : Get all Planning Map Items related By roadmapId. 
    * Param : roadmapId 
    * Return : List of Planning Map Iem. 
    */    
    public static List<Road_Map_Item__c> getRMItemByRoadmapId(String roadmapId) {
        
        return [SELECT Id, Parent_Road_Map__c, Theme__c, Epic__c, Epic__r.Epic_Title__c, RMI_Epic_End_Date__c, RMI_Epic_Start_Date__c ,
                Properties__c,Theme_Order__c  FROM Road_Map_Item__c WHERE Parent_Road_Map__c = :roadmapId ];
    }
    
    /**
    * Date : 09/05/2017
    * Description : Get Planning Map Items by Roadmap Id.
    * Param : roadmapId
    * Return : List of Planning Map Item.  
    */    
    public static List<Road_Map_Item__c> getRoadMapItemByRoadmapId(String roadmapId) {
        
        return [SELECT Id, Theme__c,Theme__r.Theme_Title__c, Name, RMI_Epic_End_Date__c,RMI_Epic_Start_Date__c,  
                Properties__c, Parent_Road_Map__c, Epic__c,Epic__r.Epic_Title__c,Epic__r.Name,Epic__r.Parent_Epic__c, Epic__r.Category__r.Title__c,
                Epic__r.Description__c,Epic__r.Epic_Type__c, Epic__r.Epic_Size__c,Epic__r.Epic_Status__c, Epic__r.Priority__c,
                Epic__r.Category__c,Epic__r.Category__r.Name, Epic__r.Lifecycle__c, Epic__r.OwnerId, Epic__r.Owner.Name, Epic__r.Regulation__c, Epic__r.REQ__c,
                Epic__r.Parent_Epic__r.Epic_Title__c, Theme__r.Description__c,Theme_Order__c, Parent_Road_Map__r.Start_Date__c,
                Parent_Road_Map__r.End_Date__c,Epic__r.CreatedBy.Name, Epic__r.LastModifiedBy.Name, Epic__r.Date_Groomed__c,
                Epic__r.Date_Approved__c,Epic__r.Date_Committed__c,Epic__r.Date_Completed__c,Epic__r.Assigned_To__c,Epic__r.Fibonacci__c
                FROM Road_Map_Item__c WHERE Parent_Road_Map__c = : roadmapId AND Epic__r.Epic_Type__c = 'Roadmap' ORDER BY Theme_Order__c, 
                CreatedDate ];
    }
    
    /**
    * Date : 09/05/2017
    * Description : Get Id List from Road Map Item.
    * Param : roadmapId 
    * Return : List of Road Map Iem. 
    */    
    public static List<Road_Map_Item__c> getRoadMapItemId(List<Id> deleteItemIdList) {
        
        return [SELECT Id FROM Road_Map_Item__c WHERE Id IN :deleteItemIdList];
    }

    /**
    * Date : 09/05/2017
    * Description : Get all Planning Map Items related By roadmapId. 
    * Param : roadmapId 
    * Return : List of Planning Map Iem. 
    */    
    public static List<Initiative_Item__c> getInitiativeItemByInitiativeId(String initiativeId) {
        
        List<Initiative_Item__c> initiaitveList = new List<Initiative_Item__c>();
        // SECURITY SCANNER ISSUE ----- 21-12-2017 
        if (Schema.sObjectType.Initiative_Item__c.isAccessible()) {
            initiaitveList = [SELECT Id, Epic__c, Epic__r.Epic_Title__c, Parent_Initiative__c, Properties__c, INI_Epic_End_Date__c, INI_Epic_Start_Date__c,
                              Theme__c, Theme_Order__c, Name FROM Initiative_Item__c WHERE Parent_Initiative__c =: initiativeId ];
        }
        return initiaitveList;  
    }
    
    /*
    * Date : 09/05/2017
    * Description : Get Id List from InitiativeItem.
    * Param : initiativeId 
    * Return : List of InitiativeItem. 
    */    
    public static List<Initiative_Item__c> getInitiativeItemId(List<Id> deleteItemIdList) {
        
        List<Initiative_Item__c> initiaitveList = new List<Initiative_Item__c>();
        // SECURITY SCANNER ISSUE ----- 21-12-2017 
        if (Schema.sObjectType.Initiative_Item__c.isAccessible()) {
            initiaitveList = [SELECT Id FROM Initiative_Item__c WHERE Id IN : deleteItemIdList];
        }
        return initiaitveList;
    }
    
    public static List<Initiative_Item__c> getInitiativeItemsByInitiativeId(String initiativeId) {
        
        List<Initiative_Item__c> initiativeList = new List<Initiative_Item__c>();
        if (Schema.sObjectType.Initiative_Item__c.isAccessible()) {
            initiativeList = [SELECT Id, Theme__c, Theme__r.Theme_Title__c, Name, INI_Epic_End_Date__c, INI_Epic_Start_Date__c, Properties__c, 
                              Parent_Initiative__c, Epic__c, Epic__r.Name, Epic__r.Epic_Title__c, Epic__r.Description__c, 
                              Epic__r.Epic_Type__c, Epic__r.Owner.Name,Epic__r.Category__r.Title__c,Epic__r.Category__r.Name,
                              Epic__r.Lifecycle__c, Epic__r.Epic_Size__c, Epic__r.Epic_Status__c, Epic__r.Priority__c, Epic__r.Category__c, Epic__r.Fibonacci__c,
                              Epic__r.OwnerId, Epic__r.REQ__c, Epic__r.Regulation__c,Theme__r.Description__c, Theme_Order__c,  
                              Epic__r.CreatedBy.Name, Epic__r.LastModifiedBy.Name ,Epic__r.Date_Groomed__c,Epic__r.Date_Approved__c,
                              Epic__r.Date_Committed__c, Epic__r.Date_Completed__c, Epic__r.Assigned_To__c FROM Initiative_Item__c 
                              WHERE Parent_Initiative__c = : initiativeId AND Epic__r.Epic_Type__c = 'Initiative' ORDER BY Theme_Order__c, CreatedDate];
        }
        return initiativeList;
    }
    
    /**
    * Date : 09/05/2017
    * Description : Getting Id and Roadmap from releasePlanId from Release Plan.(Used in manageReleasePlan--Id planningMapId)
    * Param : releasePlanId 
    * Return : rpList.
    */
    public static Id getRoadmapByReleasePlanId(Id releasePlanId) {
    
        // SECURITY SCANNER ISSUE ----- 21-12-2017
        Id rmId;
        if (Schema.sObjectType.Program_Increment__c.isAccessible()) {
            rmId = [SELECT Id, Road_Map__c FROM Program_Increment__c WHERE Id =: releaseplanId].Road_Map__c;
        }
        return rmId;
        
    }
    
    /**
    * Date : 09/05/2017
    * Description : Get Increment Item List By releasePlanId.
    * Param : incrementId 
    * Return : Program Increment Item List.
    */
    
    public static List<Program_Increment_Item__c> getIncrementItemByIncrementId(String incrementId) {
    
        List<Program_Increment_Item__c> rpiList = new List<Program_Increment_Item__c>();
        if (Schema.sObjectType.Program_Increment_Item__c.isAccessible()) {
            rpiList = [SELECT Id, Name, Road_Map_Item__r.Epic__c, Theme__c, Road_Map_Item__r.Epic__r.Epic_Title__c,
                       Feature_Epic__r.Parent_Epic__c,Feature_Epic__r.Parent_Epic__r.Epic_Title__c, Feature_Epic__r.Priority__c, Feature_Epic__r.Category__c, 
                       Feature_Epic__r.Category__r.Title__c ,Feature_Epic__r.Category__r.Name,Theme__r.Theme_Title__c, Theme__r.Description__c, Theme_Order__c,
                       Feature_Epic__c, Feature_Epic__r.Epic_Title__c, Feature_Epic__r.Description__c,Feature_Epic_Start_Date__c, Feature_Epic_End_Date__c, 
                       Feature_Epic__r.Epic_Size__c,Feature_Epic__r.Epic_Type__c, Feature_Epic__r.Epic_Status__c, 
                       Feature_Epic__r.Lifecycle__c, Feature_Epic__r.OwnerId, Feature_Epic__r.Owner.Name,Feature_Epic__r.REQ__c,
                       Feature_Epic__r.Regulation__c, Program_Increment__c, Properties__c,Feature_Epic__r.LastModifiedBy.Name,Feature_Epic__r.CreatedBy.Name, 
                       Feature_Epic__r.Name , Feature_Epic__r.Date_Groomed__c, Feature_Epic__r.Date_Approved__c,  
                       Feature_Epic__r.Date_Committed__c, Feature_Epic__r.Date_Completed__c, Feature_Epic__r.Assigned_To__c, Feature_Epic__r.Fibonacci__c         
                       FROM Program_Increment_Item__c WHERE Program_Increment__c = :incrementId AND Feature_Epic__r.Epic_Type__c ='Feature' ORDER BY Theme_Order__c,CreatedDate ];
        }
        return rpiList;
    
    }
    
    /**
    * Date : 09/05/2017
    * Description : Getting Id, Theme and Epic from Roadmap with ProgramIncrement Item data (Used in manageReleasePlan)
    * Param : releasePlanId 
    * Return : rpList.
    */
    public static List<Road_Map_Item__c> getRoadmapItemsByRoadmapId(String planningMapId) {
    
        // SECURITY SCANNER ISSUE ----- 21-12-2017
        List<Road_Map_Item__c> rmiList = new List<Road_Map_Item__c>();
        if (Schema.sObjectType.Road_Map_Item__c.isAccessible()) {
            rmiList = [SELECT Id, Parent_Road_Map__c, Theme__c, Epic__c, (SELECT Id, Feature_Epic__c, Road_Map_Item__c,
             Road_Map_Item__r.Theme__c, Road_Map_Item__r.Epic__c, Feature_Epic_End_Date__c, Feature_Epic_Start_Date__c, 
                Program_Increment__c , Properties__c FROM Program_Increment_Items__r) FROM Road_Map_Item__c WHERE Parent_Road_Map__c  = :planningMapId];
        }
        return rmiList;
    
    }
    
    /**
    * Date : 09/05/2017
    * Description : Get Release plan Item List By NOT IN releasePlanItemList.
    * Param : List<Program_Increment_Item__c> releasePlanItemList 
    * Return : Release Plan Item List.
    */
    public static List<Program_Increment_Item__c> getReleasePlanItems(List<Program_Increment_Item__c> programIncrementItemList, Id programIncrementId) {
        System.debug('===================programIncrementItemList in selector class========================'+programIncrementItemList);
        /*Id piId;
        if(programIncrementItemList.size() > 0) {
            piId = programIncrementItemList[0].Program_Increment__c;
        }*/
        List<Program_Increment_Item__c> rpiList = new List<Program_Increment_Item__c>();
        if (Schema.sObjectType.Program_Increment_Item__c.isAccessible()) {
            rpiList = [SELECT Id FROM Program_Increment_Item__c WHERE Id NOT IN :programIncrementItemList AND Program_Increment__c = : programIncrementId ];
        }
        return rpiList;
    
    }
    
    
    public static List<Program_Increment_Item__c> getPIItemByIncrementId(String iniId) {
        
        return [ SELECT Id, Road_Map_Item__c,Theme__c,Theme_Order__c, Properties__c, Program_Increment__c, Feature_Epic__c, Feature_Epic__r.Epic_Title__c, 
                Feature_Epic_Start_Date__c, Feature_Epic_End_Date__c FROM Program_Increment_Item__c 
                WHERE Program_Increment__c =: iniId ];
        
    }

    public static List<Program_Increment_Item__c> getIncrementItemId(List<Id> deleteItemIdList) {
        
        return [SELECT Id FROM Program_Increment_Item__c WHERE Id IN :deleteItemIdList];
    }


}