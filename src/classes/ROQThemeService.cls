/**
* Author :  ROQMetrics 
* Date   : 16/03/2017
* Description : Service Class for Theme.
*/
public class ROQThemeService{
    
    /**
    * Author :  ROQMetrics 
    * Date   : 16/03/2017
    * Description : Interface Class for Theme.
    */
    public interface IThemeOption{}
    
    /**
    * Date : 16/03/2017
    * Description : Wrapper Class for Theme with Id and Label. 
    * Param : Null.
    * Return : Void.
    */
    public class ThemeWrapper implements IThemeOption { 
    
        public Id themeId;
        public String themeLabel;
        public String description;
        public String themeType;
        public Boolean ischecked;
        
    }
    
    /**
    * Date : 17/03/2017
    * Description :Get all the available Themes and Epics.
    * Param : themes.
    * Return : themeRecordMap. 
    */
    public static Map<Theme__c, List<Epic__c>> getThemesFromThemeWrapper(List<ROQResponseWrapper.Theme> themes) {
    
        Map<Theme__c, List<Epic__c>> themeRecordMap = new Map<Theme__c, List<Epic__c>>();
        try{
            for(ROQResponseWrapper.Theme theme : themes){
                themeRecordMap.put(setThemeRecord(theme), ROQEpicService.getEpicsFromEpicWrapper(theme.epics));
            }
        }catch(Exception e){
            ApplicationExceptionUtility.createAudit('4001', e.getMessage(), e);
        }
        
        
        return themeRecordMap;
    }

    /**
    * Date : 17/03/2017
    * Description :Get available Themes with Id, Name and Description.
    * Param : Null.
    * Return : Void. 
    */
    public static Theme__c setThemeRecord(ROQResponseWrapper.Theme theme) {
        
            Theme__c themeRecord = new Theme__c(
                Theme_Title__c = theme.name,
                Description__c = theme.description
            );
        try {
            System.debug('coming---00-0-0-0-0-');
            themeRecord.Id = (''+theme.themeId).trim() == '' ? null : theme.themeId;
            System.debug('=-=-=-=-=-=themeRecordID==-=-=-=-=-'+themeRecord.Id);
        } catch(Exception e){
            ApplicationExceptionUtility.createAudit('4001', e.getMessage(), e);
        }
        
        return themeRecord;    
    }
    
    /**
    * Date : 18/04/2017
    * Description : verify the existing theme and avoid the duplicate theme
    * Param : Theme title set.
    * Return : themeRecordMap. 
    */
    
    public static Map<String, Theme__c> manageExistingThemes(Set<String> themeTitleSet, Map<String, Theme__c> themeMap) {
         try{
            if (themeTitleSet.size() > 0) { 
                Map<String, Id> themeTitleIdMap = new Map<String, Id>();
                  
                for(Theme__c theme : ROQThemeSelector.getThemeName(themeTitleSet)){
                themeTitleIdMap.put(theme.Theme_Title__c , theme.Id);
                }
                
                if (themeTitleIdMap.size() > 0) {
                    for(Theme__c theme : themeMap.values()) {
                        if (themeTitleIdMap.containsKey(theme.Theme_Title__c)) {
                            theme.Id = themeTitleIdMap.get(theme.Theme_Title__c); 
                        }
                    }    
                } 
            }
            
       }catch(QueryException qe){
            ApplicationExceptionUtility.createAudit('1003', qe.getMessage(), qe);
        }catch(Exception e){
            ApplicationExceptionUtility.createAudit('4001', e.getMessage(), e);
        }
        
        return themeMap;
    }
    
    /**
    * Date : 25/04/2017
    * Description :Get Map of Theme Id with Epic Id.
    * Param : List<ROQResponseWrapper.Theme>
    * Return : Map<String,String>
    */
    public static Map<String,String> getThemeEpicMap(List<ROQResponseWrapper.Theme> themes) {
    
        Map<String,String> themeEpicMap = new  Map<String,String>();
        try{
            for(ROQResponseWrapper.Theme theme : themes){
                for(ROQResponseWrapper.Epic epic : theme.epics){
                    themeEpicMap.put(theme.themeId+'~'+epic.epicId,theme.themeId);
                }            
            }
        }catch(ApplicationExceptionManager.ApplicationException ca) {
            
            // Create the audit record
            if(ca.wrapper.exceptionDet.errorType == 'C'){
                //
                ApplicationExceptionUtility.createAudit(ca.wrapper.exceptionDet);
            }else{
                //
                ApplicationExceptionUtility.createAudit(
                    ca.wrapper.exceptionDet.errorCode, 
                    ca.wrapper.exceptionDet.errorRelatedInfo, 
                    ca.wrapper.exceptionDet.systemException
                );
            }
        }catch(Exception e){
            ApplicationExceptionUtility.createAudit('4001', e.getMessage(), e);
        }
        
        return themeEpicMap;
    }
    
 
    /*public static ROQResponseWrapper.AgilingoResponseWrapper isVisibleInitiative(Id programId) {
    
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
        
        Boolean isVisibleInitiativeTheme = [SELECT COUNT() FROM Program__c WHERE Parent_Initiative__c != null AND Id =: programId] > 0
        ? true
        : false;
        
        result.isSuccess = true;
        result.data = isVisibleInitiativeTheme;
        return result;
    }*/
    
    
     
    
}