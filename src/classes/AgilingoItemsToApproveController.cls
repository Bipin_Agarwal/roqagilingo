public without sharing  class AgilingoItemsToApproveController
 {
    
    public List<ProcessInstanceWorkItem> itemsToApproveList{get; set;}
    
    public AgilingoItemsToApproveController () {
        
        itemsToApproveList =  [ SELECT Id,ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name,ProcessInstance.TargetObject.Type,Actor.Name,ProcessInstance.CreatedDate FROM ProcessInstanceWorkItem WHERE ActorId= :UserInfo.getUserId() AND ProcessInstance.Status = 'Pending' ORDER BY ProcessInstance.CreatedDate DESC];
    }
    
    @AuraEnabled
    public static List<ProcessInstanceWorkItem> getItemsToApprove(){
        return [ SELECT Id,ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name,ProcessInstance.TargetObject.Type,Actor.Name,ProcessInstance.CreatedDate FROM ProcessInstanceWorkItem WHERE ActorId= :UserInfo.getUserId() AND ProcessInstance.Status = 'Pending' ORDER BY ProcessInstance.CreatedDate DESC];
        
    }

}