/*
*    File Name    : ApplicationExceptionUtility 
*    Created by   : ROQ metrics
*    Created Date : 17-Mar-2017 
*    Description  : This class is used for commiting Errors/Exceptions in ROQ Exception Audit object
*/
public class ApplicationExceptionUtility {  
    
    //This method is used to create audit record for Custom Exceptions 
    public static String createAudit(ApplicationExceptionManager.ExceptionDetail exceptionParams){
        
      ROQ_Exception__c exAudit = new ROQ_Exception__c ();
        exAudit.Error_Related_Info__c = (exceptionParams.errorRelatedInfo != null && AGILINGO_SETUP__c.getOrgDefaults().get('Log_Error_Related_Info__c') == true) ? exceptionParams.errorRelatedInfo : null;
        exAudit.Error_Class_Method__c = exceptionParams.errorType == 'C' ? exceptionParams.errorClassMethod : exceptionParams.systemException.getStackTraceString();
        exAudit.Error_Message__c = exceptionParams.errorType == 'C' ?  exceptionParams.errorMessage : exceptionParams.systemException.getMessage();
        exAudit.Error_Type__c = exceptionParams.errorType == 'C' ? 'Custom Exception' : exceptionParams.systemException.getTypeName();
        exAudit.Error_Code__c = exceptionParams.errorCode;
                
        return createAudit(exAudit);
        
    }
    
    //This method is used to create audit record for Standard Exceptions 
    public static String createAudit(String errorCode, String errorRelatedInfo, Exception standardException){
    
        System.debug('===='+standardException.getStackTraceString());
        ROQ_Exception__c exAudit = new ROQ_Exception__c ();
        exAudit.Error_Related_Info__c = (AGILINGO_SETUP__c.getOrgDefaults().get('Log_Error_Related_Info__c') == true) ? errorRelatedInfo : null;
        exAudit.Error_Class_Method__c = standardException.getStackTraceString();
        exAudit.Error_Message__c = standardException.getMessage();
        exAudit.Error_Type__c = standardException.getTypeName();
        exAudit.Error_Code__c = errorCode;
        
        System.debug('===exAudit=='+exAudit);
        return createAudit(exAudit);
        
        
    }    
    
    public static String createAudit(String errorCode, String errorRelatedInfo, Database.Error err){
    
        
        ROQ_Exception__c exAudit = new ROQ_Exception__c ();
        exAudit.Error_Related_Info__c = (AGILINGO_SETUP__c.getOrgDefaults().get('Log_Error_Related_Info__c') == true) ? errorRelatedInfo : null;
        //exAudit.Error_Class_Method__c =.getStackTraceString();
        exAudit.Error_Message__c = err.getMessage();
        //exAudit.Error_Type__c = standardException.getTypeName();
        exAudit.Error_Code__c = errorCode;
        
        System.debug('===exAudit=='+exAudit);
        return createAudit(exAudit);
        
        
    }    
    
    //This method is used to insert both Standard as well as Custom Exceptions
    private static String createAudit(ROQ_Exception__c auditRecord){
       
        String errorMessage;

        try{
            
            errorMessage = ApplicationExceptionCodeMap.getApplicationError(auditRecord.Error_Code__c); 
            errorMessage = String.isBlank(errorMessage) 
                ? auditRecord.Error_Message__c 
                : errorMessage; 
            
            if(AGILINGO_SETUP__c.getOrgDefaults().get('Log_Server_Error__c') == true ){
                if(Schema.SObjectType.AGILINGO_SETUP__c.isUpdateable()) {
                    INSERT auditRecord;
                }
                
            } 
            if(AGILINGO_SETUP__c.getOrgDefaults().get('Send_Email_Error_Notification__c') == true ){
                   sendErrorEmail(auditRecord.Id);
            }


        }catch(Exception e){  

            errorMessage =  e.getMessage();
        }

        return errorMessage;
        
    }    
    
    //This method is used to catch Custom Exceptions
    public static ApplicationExceptionManager.ApplicationException getException(String statusCode, String type, String clsMethod, String errorCode, String errMsg , String errInfo){
        
        ApplicationExceptionManager.ExceptionDetail exceptionDet = new ApplicationExceptionManager.ExceptionDetail();
        
        exceptionDet.statusCode = statusCode;
        exceptionDet.errorType = 'C';
        exceptionDet.errorClassMethod = clsMethod;
        exceptionDet.errorCode = errorCode;
        exceptionDet.errorMessage = errMsg;
        exceptionDet.errorRelatedInfo = errInfo;
        
        ApplicationExceptionManager.ExceptionWrapper exWrapper = new ApplicationExceptionManager.ExceptionWrapper( exceptionDet );
         
        return new ApplicationExceptionManager.ApplicationException(exWrapper);
    } 
    
    //This method is used to catch Standard Exceptions
     public static ApplicationExceptionManager.ApplicationException getException(String errorCode, String errInfo, Exception ex){
        
        ApplicationExceptionManager.ExceptionDetail exceptionDet = new ApplicationExceptionManager.ExceptionDetail();
        
        exceptionDet.errorCode = errorCode;
        exceptionDet.errorRelatedInfo = errInfo;
        exceptionDet.errorType = 'S';
        exceptionDet.systemException = ex;
        
        ApplicationExceptionManager.ExceptionWrapper exWrapper = new ApplicationExceptionManager.ExceptionWrapper( exceptionDet );
         
        return new ApplicationExceptionManager.ApplicationException(exWrapper);
    }
    
     //This method is used to send error log to the user or email specified in custom settings.
    public static void sendErrorEmail(String auditRecId){

        if(AGILINGO_SETUP__c.getOrgDefaults().get('Send_Email_Error_Notification__c') == true){
        
            String emailId = AGILINGO_SETUP__c.getOrgDefaults().get('Set_Error_Email__c') != null 
                			 ? String.valueOf(AGILINGO_SETUP__c.getOrgDefaults().get('Set_Error_Email__c'))
                			 : UserInfo.getUserEmail();
            
            String templateName = String.valueOf(AGILINGO_SETUP__c.getOrgDefaults().get('Set_Error_Template__c'));
            System.debug('========templateName========='+templateName);
            templateName = templateName == null ? 'ROQ Exception Template' : templateName;
            List<String> emailIds = new List<String>();
            Id templateId = [SELECT Id,Name FROM EmailTemplate WHERE Name = :templateName LIMIT 1].Id;
            emailIds.add(emailId);
            
            List<Messaging.SingleEmailMessage> msgList= new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg.setTemplateID(templateId);
            msg.setSaveAsActivity(false);
            msg.setTargetObjectId(UserInfo.getUserId());
            msg.setToAddresses(emailIds);
            msg.setWhatId(auditRecId);
            msgList.add(msg);

            Messaging.sendEmail(msgList);
            
        }
    }    


    
}