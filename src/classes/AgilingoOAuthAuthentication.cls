global without sharing class  AgilingoOAuthAuthentication {   
    
    /* Validate credentials using oauth */
    @RemoteAction  
    global static Boolean isValidCredentials(String uName,String password){
        Boolean isValid = false;
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
                System.debug('=========Inside oauth Login==========');
                JSONParser parser =  JSON.createParser(getSessionDetails(uName,password));
                parser.nextToken();           
                while (parser.nextToken() != null) {
                    System.debug('-----next text----'+parser.getText());
                    if(parser.getCurrentName() == 'access_token'){
                        parser.nextToken();
                        if(parser.getText() != null)
                            isValid = true;                  
                        System.debug('-----session id----'+parser.getText());   
                        break;
                    }
                }                    
            
        } catch(JSONEXCEPTION je) {
            result.errorlist.add(ROQExceptionService.jsonException(je));
        } catch(Exception ex) {
            System.debug('---Error while validating user details---'+ex);
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return isValid ;         
    }     
    
    public static String getSessionDetails(String uName,String password){
        String sessionDet;
        BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try{
            String endPoint = appConfig.OAuth_Endpoint_URL__c != null ? appConfig.OAuth_Endpoint_URL__c : null;  
            String clientId =  appConfig.Client_Id__c != null ? appConfig.Client_Id__c : null;  
            String clientSecret = appConfig.Client_Secret__c != null ? appConfig.Client_Secret__c : null;
            // String reqbody = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+uName+'&password='+password;
            String reqbody = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+EncodingUtil.urlEncode(uName, 'UTF-8')+'&password='+EncodingUtil.urlEncode(password, 'UTF-8'); // Encoded due to special characters in password in 22/05/2015
            System.debug('-----reqbody ----'+reqbody);
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setBody(reqbody);
            req.setMethod('POST');
            req.setEndpoint(endPoint+'/services/oauth2/token');
            HttpResponse res ;
            if(Test.isRunningTest()){
                res = AgilingoEsignMockResponse.salesforceSessionResponse();
            }else{
                res = h.send(req);
            }
            sessionDet = res.getBody();
            System.debug('response body oauth ========'+res.getBody());
            System.debug('--------sessionDet----'+sessionDet); 
        
        } catch(Exception ex) {
            System.debug('---Error while validating user details---'+ex);
            result.errorlist.add(ROQExceptionService.parentException(ex));
        } 
        return sessionDet ;         
    }     
}