public class ROQExceptionService {

    public static ROQResponseWrapper.Error parentException(Exception ex) {
        
        ApplicationExceptionUtility.createAudit('4001',ex.getMessage(), ex);
        ROQResponseWrapper.Error e = new ROQResponseWrapper.Error();
        e.errorCode  = '4001';
        //e.errorMessage = ex.getMessage();
        e.errorMessage = System.Label.AGILINGO_ERROR_MESSAGE;
        return e;

    }
   
    public static ROQResponseWrapper.Error queryException(Exception qe) {
   
        ApplicationExceptionUtility.createAudit('1003', qe.getMessage(), qe);
        ROQResponseWrapper.Error e = new ROQResponseWrapper.Error();
        e.errorCode  = '1003';
        e.errorMessage  = System.Label.AGILINGO_ERROR_MESSAGE;
        return e;
        
    }
    
    public static ROQResponseWrapper.Error jsonException(Exception je){
        
        ApplicationExceptionUtility.createAudit('1002', je.getMessage(), je);    
        ROQResponseWrapper.Error e = new ROQResponseWrapper.Error();
        e.errorCode  = '1002';
        e.errorMessage  = System.Label.AGILINGO_ERROR_MESSAGE;
        return e;
    }
    
    public static ROQResponseWrapper.Error dmlException(Exception de){
        
        ApplicationExceptionUtility.createAudit('1001', de.getMessage(), de);    
        ROQResponseWrapper.Error e = new ROQResponseWrapper.Error();
        e.errorCode  = '1001';
        if(de.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
           e.errorMessage = de.getMessage().substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,',':');
        }else{
            e.errorMessage  = System.Label.AGILINGO_ERROR_MESSAGE;
        }
        
        return e;
    }
    
    public static ROQResponseWrapper.Error auditUtility(ApplicationExceptionManager.ApplicationException ca) {
        
        ROQResponseWrapper.Error e = new ROQResponseWrapper.Error();
        if(ca.wrapper.exceptionDet.errorType == 'C') {
            ApplicationExceptionUtility.createAudit(ca.wrapper.exceptionDet);
            e.errorCode  = '0001';
            e.errorMessage  = String.valueOf(ca.wrapper.exceptionDet.errorMessage);
        } else {
            ApplicationExceptionUtility.createAudit(
                    ca.wrapper.exceptionDet.errorCode, 
                    ca.wrapper.exceptionDet.errorRelatedInfo, 
                    ca.wrapper.exceptionDet.systemException
            );
            e.errorCode  = '0002';
            e.errorMessage  = System.Label.AGILINGO_ERROR_MESSAGE;
        }
        return e ;
        
    }
    
    public static void throwApplicationException(String statusCode, String type, String clsMethod, String errorCode, String errMsg , String errInfo) {
    
        throw ApplicationExceptionUtility.getException(statusCode, type, clsMethod, errorCode, errMsg, errInfo);
    
    }
    
}