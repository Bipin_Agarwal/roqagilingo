/**
* Author : ROQMetrics 
* Date   : 05/06/2017
* Description : Test Class for ApplicationExceptionUtility class.
*/
@isTest
private class ApplicationExceptionUtility_Test{
    
    static testMethod void ExceptionDetailTest() { 
    
        ApplicationExceptionManager.ExceptionWrapper exWrap = new ApplicationExceptionManager.ExceptionWrapper(new ApplicationExceptionManager.ExceptionDetail());
        ApplicationExceptionManager.ApplicationException appException = new ApplicationExceptionManager.ApplicationException(exWrap);
        ROQ_Exception__c exAudit = new ROQ_Exception__c (Error_code__c = '1001');
        INSERT exAudit;
        System.assertEquals('1001',exAudit.Error_code__c);
    }
   
   
    static testMethod void CreateAudit_UseCase1() {
    
        System.assert((ApplicationExceptionUtility.createAudit('test data','test data',new DmlException()) != null));
    
    }
    
   static testMethod void GetException_UseCase1() {
    
        System.assert((ApplicationExceptionUtility.getException('404','c','TestClass.TestMethod','4001','Custom Exception','test data'))!= null);
    
    }
   
    static testMethod void GetException_UseCase2() {
    
        System.assert((ApplicationExceptionUtility.getException('1001','Standard Exception',new DmlException())) != null);
    
    }

    
    @isTest

    static void testInstallScript() {

        AgilingoPostInstall postInstall = new AgilingoPostInstall();
        Test.testInstall(postInstall, null);
        System.assert(AGILINGO_SETUP__c.getInstance(UserInfo.getProfileId()) != null);
    }
}