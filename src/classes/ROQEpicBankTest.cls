/**
* Author :  ROQMetrics 
* Date   :  16/02/2018
* Description : Test Class for ROQEpicBank class.
*/
@isTest
public class ROQEpicBankTest {

    /**
    * Date : 16/02/2018
    * Description : Test Class for getPicklistValues.
    */
    static testMethod void getPicklistValuesTest() {
    
        List<Epic_Category__c> epicCategoryList = new List<Epic_Category__c>();
        Epic_Category__c epicCategory = new Epic_Category__c(Title__c = 'test', Description__c = 'Description');
        INSERT epicCategory;
        epicCategoryList.add(epicCategory);
        System.assertEquals('test',epicCategory.Title__c);
        
        Epic__c epic = new Epic__c(Epic_Size__c = 'Small');
        List<Epic__c> epicList = new List<Epic__c>();
        epicList.add(epic);
        System.assertEquals('Small',epic.Epic_Size__c);
        
        AgilingoWizardController.getPicklistValues();
    }
    
    static testMethod void getEpicUsedTest() {
    
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard Platform User'];
        
        User u1 = new User(LastName = 'Test',Alias = 'Test', Email = 'kartikey.k@ceptes.com', 
            Username = 'kartikey.kartikey21@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id , 
                TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                    LanguageLocaleKey = 'en_US');
        INSERT u1;
        
        List<Initiative_Item__c> iniItemList = new List<Initiative_Item__c>();
        
        Strategy__c stg = new Strategy__c(Strategy_Title__c = 'TestStrategy', Strategy_Description__c = 'Description');
        INSERT stg;
        
        Portfolio__c pf = new Portfolio__c(Portfolio_Title__c = 'TestPortfolio', Parent_Strategy__c = stg.Id, 
            Portfolio_Manager_Assignment__c = u1.Id );    
        INSERT pf;
        
        Program__c pgm = new Program__c(Program_Title__c = 'Testpgm', Program_Start_Date__c = System.today(), 
            Parent_Portfolio__c = pf.id, Program_Manager__c = u1.Id, Program_Status__c = 'On Track' );
        INSERT pgm;
        
        Initiative__c initiative = new Initiative__c( Initiative_Title__c = 'Test1', Parent_Portfolio__c = pf.Id, 
            Description__c = 'Description1', Start_Date__c = System.today(), End_Date__c = System.today()+10, 
            Status__c = 'On Track', Initiative_Manager_Assignment__c = u1.Id);
        INSERT initiative;
        
        Road_Map__c rm = new Road_Map__c(RoadMap_Title__c = 'TestRoadmap', Start_Date__c = system.today(), Status__c = 'On Track', 
            Program_Id__c = pgm.id, End_Date__c = System.today().addDays(+1), Description__c = 'test Description');
        INSERT rm;
        
        Theme__c theme1 = new Theme__c(Theme_Title__c = 'testTheme1', Description__c = 'Description');
        INSERT theme1;
        
        Theme__c theme2 = new Theme__c(Theme_Title__c = 'INI Theme', Description__c = 'Description');
        INSERT theme2;
        
        Theme__c theme3 = new Theme__c(Theme_Title__c = 'INI Theme1', Description__c = 'Description');
        INSERT theme3;
        
        Epic__c epic1 = new Epic__c(Epic_Title__c = 'Epic',Description__c = 'Description',Epic_Type__c ='Initiative');
        INSERT epic1;
        System.debug('=========Epic INI1========='+epic1.Id);
        
        Epic__c epic3 = new Epic__c(Epic_Title__c = 'Epic 2',Description__c = 'Description',Epic_Type__c ='Initiative', Epic_Size__c = 'Medium');
        INSERT epic3;
        
        Epic__c epic2 = new Epic__c(Epic_Title__c = 'Epic Name1',Description__c = 'Description',Epic_Type__c ='Roadmap',Parent_Epic__c =epic1.Id);
        INSERT epic2;
        
        Road_Map_Item__c rmItem = new Road_Map_Item__c (Parent_Road_Map__c = rm.Id, Theme__c = theme1.Id,Epic__c = epic2.Id );
        INSERT rmItem;
        
        Initiative_Item__c iniItem = new Initiative_Item__c (Epic__c = epic1.Id,Parent_Initiative__c= initiative.Id,Theme__c = theme2.Id);
        INSERT iniItem;
        System.debug('=========iniItem INI========='+iniItem.Id);
        
        Initiative_Item__c iniItem1 = new Initiative_Item__c (Epic__c = epic3.Id,Parent_Initiative__c= initiative.Id,Theme__c = theme3.Id);
        INSERT iniItem1;
        
        iniItemList.add(iniItem);
        iniItemList.add(iniItem1);
        String epicId = String.valueOf(epic1.Id);
        System.debug('===============ID NN================='+epicId);
        ROQEpicBank.getEpicUsed(epicId);
    }
    
    static testMethod void getEpicCategoriesTest() {
    
        List<Epic_Category__c> epicCategoryList = new List<Epic_Category__c>();
        Epic_Category__c epicCategory = new Epic_Category__c(Title__c = 'test', Description__c = 'Description');
        INSERT epicCategory;
        Epic_Category__c epicCategory1 = new Epic_Category__c(Title__c = 'test1', Description__c = 'Description');
        INSERT epicCategory1;
        epicCategoryList.add(epicCategory);
        AgilingoWizardController.getEpicCategories();
        System.assertEquals('test',epicCategory.Title__c);
    
    }
    
    static testMethod void getEpicTagsTest() {
    
        List<Epic_Tag__c> epicTagList = new List<Epic_Tag__c>();
        Epic_Tag__c epicTag = new Epic_Tag__c(Title__c = 'test', Description__c = 'Description');
        INSERT epicTag;
        epicTagList.add(epicTag);
        AgilingoWizardController.getEpicTags();
        System.assertEquals('test',epicTag.Title__c);
    }
    
     static testmethod void getEpicsGridViewTest() { 
    
        Epic__c epic1 = new Epic__c(Epic_Title__c = 'test', Epic_Type__c = 'Initiative', Description__c = 'Description');
        INSERT epic1;
        
        Epic__c epic2 = new Epic__c(Epic_Title__c = 'test1', Epic_Type__c = 'Initiative', Description__c = 'Description');
        INSERT epic2;
        
        List<Epic_Tag__c> epicTagList = new List<Epic_Tag__c>();
        Epic_Tag__c epicTag = new Epic_Tag__c(Title__c = 'tagTest', Description__c = 'test');
        INSERT epicTag;
        epicTagList.add(epicTag);
        
        Epic_EpicTag__c epicEpicTag = new Epic_EpicTag__c(Epic__c = epic1.Id, EpicTag__c = epicTag.Id);
        INSERT epicEpicTag;
        
        List<ROQResponseWrapper.Epic> epicWrapList = new List<ROQResponseWrapper.Epic>();
        
        ROQResponseWrapper.Epic result1 = new ROQResponseWrapper.Epic();
        result1.epicId = epic1.Id;
        result1.Name = epic1.Epic_Title__c;
        result1.Description = epic1.Description__c;
        result1.Size = epic1.Epic_Size__c;
        result1.EpicType = epic1.Epic_Type__c;
        result1.Status = epic1.Epic_Status__c;
        result1.Tag = epicTagList;
        epicWrapList.add(result1);
        
        AgilingoWizardController.getEpicsGridView(1,'epic',2,'2');
        System.assert(result1.epicId != null, 'epic not inserted');
        
    }
    
    static testmethod void doSaveEpicBankColumnsTest() {
        
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard User'];
        
        User u1 = new User(LastName = 'Test',Alias = 'Test', Email = 'niveditha.n@ceptes.com', 
            Username = 'kartikey.kartikey21@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id , 
                TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                    LanguageLocaleKey = 'en_US');
        INSERT u1;
        
        String fieldsList = '"Epic Id","Title","Description","Type","Size","Lifecycle","Priority","Status","Tag","Owner" ';
        
        User_Preferences__c userPre = new User_Preferences__c (Epic_Bank_Column__c = fieldsList ,User__c= u1.Id); 
        INSERT userPre;
    }

    
}