/*
File Name          : BPMockResponse
Description        : Generate the mock response for test class.
*/
@isTest
global class AgilingoEsignMockResponse implements HttpCalloutMock {

    // get docusign mock response
    global static HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"loginAccounts": [ {"name": "Ceptes Software", "accountId": "2963521","baseUrl": "https://demo.docusign.net/restapi/v2/accounts/2963521",'+
         '"isDefault": "true", "userName": "priya panigrahy","userId": "b577d1fd-e68b-4426-86fd-8c46b221be6d", "email": "priya@ceptes.com",'+
        ' "siteDescription": "" } ]}');
        res.setStatusCode(200);
        
         System.assert(true);
        
        return res;
    }
    
    // get approval process layout Id mock response
    global static testMethod HTTPResponse approvalLayoutIdResponse() {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"size":1,"totalSize":1,"done":true,"queryLocator":null,"entityTypeName":"Layout","records":[{"attributes":{"type":"Layout","url":"/services/data/v39.0/tooling/sobjects/Layout/00h6A000000Kcq0QAC"},"Id":"00h6A000000Kcq0QAC"}]}');
        res.setStatusCode(200);
        System.assert(true);
        return res;
    }
    
    // get approval process layout mock response
    global static testMethod HTTPResponse approvalLayoutResponse() {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><Layout type="Layout" xmlns:mns="urn:metadata.tooling.soap.sforce.com">'+
                        '<Id>00h6A000000Kcq0QAC</Id><Name>Approval Page Layout: Approve Opportunity Closed Won 04a6A0000004IEq</Name><ManageableState>unmanaged</ManageableState>'+
                        '<TableEnumOrId>Opportunity</TableEnumOrId><LayoutType>ProcessDefinition</LayoutType><CreatedDate>2017-05-30T08:14:08.000Z</CreatedDate><CreatedById>0056A000000QbDvQAK</CreatedById>'+
                        '<LastModifiedDate>2017-05-30T08:14:08.000Z</LastModifiedDate><LastModifiedById>0056A000000QbDvQAK</LastModifiedById><ShowSubmitAndAttachButton>false</ShowSubmitAndAttachButton><sf:Metadata><mns:layoutSections>'+
                        '<mns:customLabel>false</mns:customLabel><mns:detailHeading>false</mns:detailHeading><mns:editHeading>true</mns:editHeading><mns:label>Opportunity Information</mns:label><mns:layoutColumns><mns:layoutItems><mns:behavior>Required</mns:behavior>'+
                        '<mns:field>Name</mns:field></mns:layoutItems><mns:layoutItems><mns:behavior>Readonly</mns:behavior><mns:field>OwnerId</mns:field></mns:layoutItems><mns:layoutItems><mns:behavior>Required</mns:behavior><mns:field>StageName</mns:field></mns:layoutItems>'+
                        '<mns:layoutItems><mns:behavior>Readonly</mns:behavior><mns:field>Amount</mns:field></mns:layoutItems></mns:layoutColumns><mns:style>OneColumn</mns:style></mns:layoutSections><mns:layoutSections><mns:customLabel>false</mns:customLabel><mns:detailHeading>false</mns:detailHeading>'+
                        '<mns:editHeading>false</mns:editHeading><mns:layoutColumns/><mns:style>CustomLinks</mns:style></mns:layoutSections><mns:relatedContent><mns:relatedContentItems><mns:layoutItem><mns:behavior>Readonly</mns:behavior><mns:field>AccountId</mns:field></mns:layoutItem></mns:relatedContentItems>'+
                        '<mns:relatedContentItems><mns:layoutItem><mns:behavior>Readonly</mns:behavior><mns:field>OwnerId</mns:field></mns:layoutItem></mns:relatedContentItems></mns:relatedContent><mns:relatedObjects>AccountId</mns:relatedObjects><mns:showEmailCheckbox>false</mns:showEmailCheckbox><mns:showHighlightsPanel>false</mns:showHighlightsPanel>'+
                        '<mns:showInteractionLogPanel>false</mns:showInteractionLogPanel><mns:showRunAssignmentRulesCheckbox>false</mns:showRunAssignmentRulesCheckbox><mns:showSubmitAndAttachButton>false</mns:showSubmitAndAttachButton></sf:Metadata><FullName>Opportunity-Approval Page Layout%3A Approve Opportunity Closed Won 04a6A0000004IEq</FullName>'+
                        '<EntityDefinitionId>Opportunity</EntityDefinitionId></Layout>');
        res.setStatusCode(200);
        System.assert(true);
        return res;
    }
    
    // get salesforce authentication mock response
    global static testMethod HTTPResponse salesforceSessionResponse() {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"access_token":"SESSION_ID_REMOVED","instance_url":"https://montenexdev-dev-ed.my.salesforce.com","id":"https://login.salesforce.com/id/00D6A000000eWEXUA2/0056A000000QbDvQAK","token_type":"Bearer","issued_at":"1498459173354","signature":"Db3kTxu6UU26RHDhWFZXhvUG95cdLik2Ua5JITqQzdo="}');
        res.setStatusCode(200);
        System.assert(true);
        return res;
    }
    
    // get twilio service mock response
    
    // Uncommented the method as it was giving error in test classes (8th jan 2018)
    
    global static testMethod HTTPResponse twilioServiceResponse() {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"code": 21608, "message": "The number +917970000000 is unverified. Trial accounts cannot send messages to unverified numbers; verify +917970000000 at twilio.com/user/account/phone-numbers/verified, or purchase a Twilio number to send messages to unverified numbers.", "more_info": "https://www.twilio.com/docs/errors/21608", "status": 400}');
        res.setStatusCode(200);
        System.assert(true);
        return res;

    }
}