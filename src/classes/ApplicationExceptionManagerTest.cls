/**
* Author : ROQMetrics 
* Date   : 05/06/2017
* Description : Test Class for ApplicationExceptionManager class.
*/

@isTest
private class ApplicationExceptionManagerTest{

    static testMethod void ApplicationExceptionTest () {
        ApplicationExceptionManager.ExceptionDetail exdet = new ApplicationExceptionManager.ExceptionDetail();
        exdet.errorCode = '1001';
        ApplicationExceptionManager.ExceptionWrapper exWrap = new ApplicationExceptionManager.ExceptionWrapper(exdet);
            
        //code change
        
        ApplicationExceptionManager.ApplicationException appException = new ApplicationExceptionManager.ApplicationException(exWrap);
        
        System.assertEquals('1001', appException.wrapper.exceptionDet.errorCode, 'code not same');
        
    }
}