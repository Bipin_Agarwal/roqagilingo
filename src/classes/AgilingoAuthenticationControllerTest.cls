@isTest
public class AgilingoAuthenticationControllerTest {
    
    static testMethod void authenticationTest(){
        Organization org = [SELECT Id, IsSandbox FROM Organization LIMIT 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='admimTest@testorg.com', 
                          EmailEncodingKey='UTF-8',MobilePhone = '+9175000000000', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id, OTP__c = AgilingoAuthenticationController.encryptData('12345'),
                          OTP_Sent_Time__c = System.now(),
                          TimeZoneSidKey='America/Los_Angeles', UserName='admimTest@testorg.com');
        insert u;
        System.assert(true);

        System.runAs(u){
            
            // Create custom settings
            BP_App_Config__c appConfig = new BP_App_Config__c();
            appConfig.OAuth_Endpoint_URL__c = org.IsSandbox == true ? 'https://test.salesforce.com' : 'https://login.salesforce.com';
            appConfig.Validate_Password__c = true;
            appConfig.Validate_OTP__c = true;
            appConfig.Get_Signature__c = true;
            appConfig.Send_OTP_via_SMS__c = true;
            insert appConfig;
            
            Opportunity opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), StageName = 'Prospecting' );
            insert opp;
            
            AgilingoAuthenticationController.getInitialSettings();
            //AgilingoAuthenticationController.sendOTP();
            
            appConfig.Send_OTP_via_SMS__c = false;
            update appConfig; 
            
            //BPAuthenticationController.sendOTP();
            
            User user1 = [SELECT Id, OTP__c FROM User WHERE Id = :u.Id];
            
            AgilingoAuthenticationController.isSignatureForStep(opp.Id);
            
            String OTP = AgilingoAuthenticationController.decryptData(user1.OTP__c);
            
            Map<String,String> approvalProcessDataMap = new Map<String,String>();
            approvalProcessDataMap.put('password','test123');
            approvalProcessDataMap.put('otp',OTP);
            approvalProcessDataMap.put('docusername','priya@ceptes.com');
            approvalProcessDataMap.put('docpassword','ceptes123');
            approvalProcessDataMap.put('comments','test comments');
            approvalProcessDataMap.put('recordId',opp.Id);
            approvalProcessDataMap.put('status','Approve');
            approvalProcessDataMap.put('nextapprover',user1.Id);
            
            String jsonApprovalData = JSON.serialize(approvalProcessDataMap);
            
            //verify Signature 
            AgilingoAuthenticationController.verifySignature(jsonApprovalData);
            
            // verify sendOTP
            
            AgilingoAuthenticationController.sendOTP();
            
            AgilingoAuthenticationController.getUserName();
            
            AgilingoAuthenticationController.getRandomNumber(5);
            // create signature history
            AgilingoAuthenticationController.createSignatureHistory(jsonApprovalData);
            
            appConfig.Validate_Password__c = false;
            update appConfig;
            
             //verify Signature 
            AgilingoAuthenticationController.verifySignature(jsonApprovalData);
            
            // recall approval process
            AgilingoAuthenticationController.recallApprovalProcess(opp.Id);
            
            // check for next Approver
            AgilingoAuthenticationController.isNextApprover(opp.Id);
           
            // get active users
            AgilingoAuthenticationController.getActiveUsers();
            
            // reassign user
            AgilingoAuthenticationController.reassignUser(user1.Id,opp.Id);
            
            //get Approval History List
            AgilingoAuthenticationController.getApprovalHistoryList(opp.Id);
            
            // create signature history
            Signature_History__c sh = new Signature_History__c(Signature__c = 'eeeee', Is_Signature_Retrieved__c = true, Comments__c = 'approved',
                                                                        Parent_Record_Id__c = opp.Id, Approval_History__c = '000000011111');
            insert sh;
            
            //get signature history list
            AgilingoSignatureHistoryController.getSignatureHistoryList(opp.Id);
            
            AgilingoSetupController bpsetup = new AgilingoSetupController();
            bpsetup.getSObjectNames();
            bpsetup.getProcessDefinitions();
            bpsetup.getApprovalSteps();
            bpsetup.setAllInputs();
            bpsetup.save();
            bpsetup.getPageRefAndUpdateEsignSettings();
            bpsetup.saveApprovalProcess();
            bpsetup.getApproverActions();
            bpsetup.setApprovalProcessAllInputs();
            bpsetup.showNewRecordSection();
            bpsetup.getApprovalSetting();
            bpsetup.getAPConfigList();
            bpsetup.getstepConfigList();
            
            // get docusign signature
            AgilingoSignatureController.getDocusignSignature('priya@ceptes.com','ceptes123');
            
            AgilingoItemsToApproveController ita = new AgilingoItemsToApproveController();
            
            AgilingoItemsToApproveSF1Controller itaSf1 = new AgilingoItemsToApproveSF1Controller();
            
            AgilingoApproveRejectController appRej = new AgilingoApproveRejectController();
            System.debug('=========appRej=============='+appRej);
            AgilingoApproveRejectController.approveRejectWithoutESignature('Approve',opp.Id,'Approved');
            appRej.getApprovalPageColumns('test approval');
            List<String> fieldNameList = new List<String>{'Name','CloseDate','StageName'};
            appRej.setFieldValueMap(opp.Id,fieldNameList);
             
            ApproveRejectSF1ComponentController appRejSf1 = new ApproveRejectSF1ComponentController();
        } 
    }
    
    public static String getRecordId(){
         Opportunity opp = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), StageName = 'Prospecting' );
         insert opp;
        
         System.assert(true);
         return opp.Id;
    }
    
    static testMethod void getGroupsListTest() {
        Group testGroup = new Group(Name='QUEUE NAME', Type='Queue');
        INSERT testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId())) {
        
        QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
                    insert testQueue;
        }
        
        List<QueueSobject> q1 = [Select Id,q.Queue.Name,q.Queue.ID from QueueSobject q ORDER BY q.Queue.Name] ;
        System.debug(q1[0].Queue.Name);
        
        testGroup.Name = 'DIFFERENT QUEUE NAME' ;
                update testGroup;
        
        List<QueueSobject> q2 = [Select Id,q.Queue.Name,q.Queue.ID from QueueSobject q ORDER BY q.Queue.Name] ;
        System.debug(q2[0].Queue.Name);
        
        AgilingoSetupController.getGroupsList();
        //AgilingoSetupController. new getGroupMap();
        AgilingoSetupController bpsetup = new AgilingoSetupController();
        bpsetup.getGroupMap();
        bpsetup.setApprovalProcessAllInputs();
        bpsetup.getApprovalSetting();
    }

}