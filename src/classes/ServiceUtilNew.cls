global class ServiceUtilNew {
    
    global class GetEpicUsedWrapper {
    
        public String Id;
        public String Link;
        public String Title;
        public String Status;
        public String LifeCycle;
        public String Description;
        public String Priority;
        public String Tag;
        public String Category;
    
    }
    
    global static List<ServiceUtilNew.EpicDetails> getEpicDetails(String epicId) {
    
        List<EpicDetails> epicDetails = new List<EpicDetails>();
        
        // SECURITY SCANNER ISSUE ----- 21-12-2017 
        /*Epic__c epic = new Epic__c();
        if (Schema.sObjectType.Epic__c.isAccessible()) {
            epic = [SELECT Id, Name, Epic_Title__c, Epic_Size__c, Epic_Status__c, Description__c, 
            Epic_Type__c, Parent_Epic__c, Parent_Epic__r.Name, Parent_Epic__r.Epic_Title__c, Parent_Epic__r.Epic_Size__c, 
            Parent_Epic__r.Epic_Status__c, Parent_Epic__r.Description__c, Parent_Epic__r.Epic_Type__c,
            Parent_Epic__r.Parent_Epic__c, (SELECT Id, Name, Epic_Title__c, Epic_Size__c, Epic_Status__c, 
            Description__c, Epic_Type__c, Parent_Epic__c FROM Epics__r) FROM Epic__c WHERE Id =: epicId];
        }
        System.debug('=====epic====='+JSON.serialize(epic));
        
        EpicDetails ed = new EpicDetails();
        ed.epicObjectDetails = epic;
        //ed.epicUsed = ServiceUtilNew.getEpicUsed(epicId);
        epicDetails.add(ed);*/
        return epicDetails;
    
    }
    
    global class EpicDetails {
    
        Epic__c epicObjectDetails;   
        Map<String,ServiceUtilNew.GetEpicUsedWrapper> epicUsed;
    
    }
    
    public static void getEpicTagsForMapping(Map<Integer, Epic__c> epicMap, Map<Integer, ROQResponseWrapper.Epic> epicWrapperMap) {
    
        Map<Id, set<Id>> epicTagsMap = new Map<Id, set<Id>> ();
        
        //Map of Sequence and Epic Id
        Map<Integer, Id> seqIdMap = new Map<Integer, Id>();
        for(Integer seq : epicMap.keySet()) {
            seqIdMap.put(seq, epicMap.get(seq).Id);
            
        }
        List<Epic_EpicTag__c> jnAddList = new List<Epic_EpicTag__c>();
        List<Epic_EpicTag__c> jnDelList = new List<Epic_EpicTag__c>();
        Set<Id> tagRecIdSet = new Set<Id>();
        Id epicRecId = null;
        for(Epic_EpicTag__c ept : [SELECT Id,Epic__c,EpicTag__c FROM Epic_EpicTag__c WHERE Epic__c in :seqIdMap.values()]){
            if(epicTagsMap.containsKey(ept.Epic__c)){
                Set<Id> tagIds = epicTagsMap.get(ept.Epic__c);
                tagIds.add(ept.EpicTag__c);
                epicTagsMap.put(ept.Epic__c,tagIds);
            }else{
                epicTagsMap.put(ept.Epic__c,new Set<Id>{ept.EpicTag__c});
            }
        }
        for(Integer seq : epicWrapperMap.keyset()) {  
            ROQResponseWrapper.Epic epicObj = epicWrapperMap.get(seq);
            epicRecId = seqIdMap.get(seq);
            if(epicObj.Tag != null && ! epicObj.Tag.isEmpty()) {
                for(Epic_Tag__c etag : epicObj.Tag) {
                    if( epicTagsMap.keySet().size() == 0 || !epicTagsMap.containsKey(epicRecId) || (epicTagsMap.containsKey(epicRecId) && ! epicTagsMap.get(epicRecId).contains(etag.Id))){
                        Epic_EpicTag__c jnObj = new Epic_EpicTag__c(Epic__c = epicRecId, EpicTag__c = etag.Id);
                        jnAddList.add(jnObj);
                    }
				}
            }
            
            if(epicObj.epicTagDetails != null && !epicObj.epicTagDetails.isEmpty()) {
                for(Epic_Tag__c etag : epicObj.epicTagDetails) {
                    tagRecIdSet.add(etag.Id);
                    Epic_EpicTag__c jncObj = new Epic_EpicTag__c(Epic__c = epicRecId, EpicTag__c = etag.Id); 
                    jnDelList.add(jncObj);
                }
            }
        }
        
      System.debug('=====jnAddList====='+jnAddList);
      System.debug('=====jnDelList====='+jnDelList);
      Map<String, Id> jnRecMap = new Map<String, Id>();
        
        System.debug('====seqIdMap.values()== '+seqIdMap.values());
        List<Epic_EpicTag__c> EETagList = new List<Epic_EpicTag__c>();
        if (Schema.sObjectType.Epic_EpicTag__c.isAccessible()) {
            EETagList = [SELECT Id,Epic__c,EpicTag__c FROM Epic_EpicTag__c WHERE Epic__c in :seqIdMap.values() AND EpicTag__c in :tagRecIdSet];
        }
        for(Epic_EpicTag__c jnObj : EETagList){
            jnRecMap.put(jnObj.Epic__c + '~' + jnObj.EpicTag__c, jnObj.Id);
        }
        System.debug('====EETagList=== '+EETagList);
        System.debug('====jnRecMap==='+JSON.serialize(jnRecMap));
        
        List<Epic_EpicTag__c> finalDeleteList = new List<Epic_EpicTag__c>();
        if(jnRecMap.size() > 0) {
            String jnKey = null;
            for(Epic_EpicTag__c jnObj : jnDelList){
                jnKey = jnObj.Epic__c + '~' + jnObj.EpicTag__c;
                if(jnRecMap.containsKey(jnKey)) {
                    jnObj.Id = jnRecMap.get(jnKey);
                    finalDeleteList.add(jnObj);
                }
            }
        
        System.debug('=====finalDeleteList==1==='+finalDeleteList);
        
        //    System.debug('====finalDeleteList=== '+printList(finalDeleteList));
            if(finalDeleteList.size() > 0) {
                if(Schema.sObjectType.Epic_EpicTag__c.isDeletable()) {
                    DELETE finalDeleteList;
                }
            }
        }
       System.debug('=====finalDeleteList==2==='+finalDeleteList);
       
        System.debug('===size====='+jnAddList.size());
        if(jnAddList.size() > 0){
            if(Schema.SObjectType.Epic_EpicTag__c.isUpdateable()) {
                INSERT jnAddList;
            }
        }
        
    }
    
    //NOT USED
    global static Map<String, List<ServiceUtil.ROQUserWrapper>> epicUserMap() {
    
        Map<String, List<ServiceUtil.ROQUserWrapper>> epicUserMap = new Map<String, List<ServiceUtil.ROQUserWrapper>>();
        //Set<String> epicType = getEpicTypes();
       /* String soqlquery;
        if (Schema.sObjectType.PermissionSetAssignment.isAccessible()) {
            soqlquery = 'SELECT Id, AssigneeId, Assignee.Name, PermissionSet.Name FROM PermissionSetAssignment ';
        }
         
        
        String ProgramManager = AgilingoApplicationConstants.PROGRAM_MANAGER;
        String ProjectManager = AgilingoApplicationConstants.PROJECT_MANAGER;
        String PortfolioManager = AgilingoApplicationConstants.PORTFOLIO_MANAGER;
        
        String queryFilterProgram = 'WHERE PermissionSet.Name = \'' +String.escapeSingleQuotes(ProgramManager)+'\'';
        String queryFilterProject = 'WHERE PermissionSet.Name = \'' +String.escapeSingleQuotes(ProjectManager)+'\'';
        String queryFilterPortfolio = 'WHERE PermissionSet.Name = \'' +String.escapeSingleQuotes(PortfolioManager)+'\'';
        String query;
        
        List<ServiceUtil.ROQUserWrapper> roadmapUserList = new List<ServiceUtil.ROQUserWrapper>();
        List<ServiceUtil.ROQUserWrapper> initiativeUserList = new List<ServiceUtil.ROQUserWrapper>();
        List<ServiceUtil.ROQUserWrapper> featureUserList = new List<ServiceUtil.ROQUserWrapper>();
        
        //if(epicType.contains('Roadmap')) {
            query = soqlquery + queryFilterProgram;
            
            System.debug('=====query=roadmap===='+query);
            
            List<PermissionSetAssignment> psaList = Database.query(query);
            for(PermissionSetAssignment assignment : psaList){
                    ServiceUtil.ROQUserWrapper userWrapper = new ServiceUtil.ROQUserWrapper();
                    userWrapper.UserId = assignment.AssigneeId;
                    userWrapper.UserLabel = assignment.Assignee.Name;
                    roadmapUserList.add(userWrapper);
            }
            epicUserMap.put('Roadmap', roadmapUserList);
            
            System.debug('==Roadmap=='+epicUserMap);    
        }
        
        //if(epicType.contains('Initiative')) {
            query = soqlquery + queryFilterPortfolio;
            
            System.debug('=====query==Initiative==='+query);
            
            List<PermissionSetAssignment> psaList = Database.query(query);
            for(PermissionSetAssignment assignment : psaList){
                    ServiceUtil.ROQUserWrapper userWrapper = new ServiceUtil.ROQUserWrapper();
                    userWrapper.UserId = assignment.AssigneeId;
                    userWrapper.UserLabel = assignment.Assignee.Name;
                    initiativeUserList.add(userWrapper);
            }
            epicUserMap.put('Initiative', initiativeUserList);
            
            System.debug('==Initiative=='+epicUserMap);
            
        }
        
        if(epicType.contains('Feature')) {
            query = soqlquery + queryFilterProject;
            
            
            System.debug('=====query==Feature==='+query);
            
            List<PermissionSetAssignment> psaList = Database.query(query);
            for(PermissionSetAssignment assignment : psaList){
                    ServiceUtil.ROQUserWrapper userWrapper = new ServiceUtil.ROQUserWrapper();
                    userWrapper.UserId = assignment.AssigneeId;
                    userWrapper.UserLabel = assignment.Assignee.Name;
                    featureUserList.add(userWrapper);
            }
            epicUserMap.put('Feature', featureUserList);

            System.debug('==PI=='+epicUserMap);
        }
        
        System.debug('==epicMap=='+JSON.serialize(epicUserMap));*/
        
        return epicUserMap;
    }
    
    global static ROQResponseWrapper.AgilingoResponseWrapper getCompactLayoutFields(String objName){
    
        
        ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
        /*response.isSuccess = false;
        response.data = null;
        response.errorList = new List<ROQResponseWrapper.Error>();
        System.debug(objName);
        
        try{
            Httprequest req =new HttpRequest();
            req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v36.0/compactLayouts?q='+objName);
            req.setHeader('Authorization','OAuth ' + UserInfo.getSessionId());
            req.setMethod('GET');
            HttpResponse res = new Http().send(req);
            String result = res.getBody();
            System.debug('==========result==============='+result);
            Map<String, Object> jsonObjMap = (Map<String, Object>)JSON.deserializeUntyped(result);
            for(String objKey : jsonObjMap.keySet()){
                Map<String, Object> resultMap = (Map<String, Object>) jsonObjMap.get(objKey);
                System.debug('====resultMap===='+resultMap);
                List<Object> fieldItems = (List<Object>) resultMap.get('fieldItems');
                Map<Object, Object> dataMap = new Map<Object, Object>();
                for(Object obj : fieldItems){
                    Map<String, Object> objMap = (Map<String, Object>) obj;
                    Object label = objMap.get('label');
                    System.debug('====LABEL====='+objMap.get('label'));
                    List<Object> layoutComponents = (List<Object>) objMap.get('layoutComponents');
                    Map<String, Object> detailsMap = (Map<String, Object>) layoutComponents.get(0);
                    System.debug('====detailsMap keys ===='+detailsMap.keySet());
                    Object value = detailsMap.get('value');
                    System.debug('====VALUE===='+detailsMap.get('value'));
                    dataMap.put(value,label);
                }
                response.data = dataMap;
                System.debug('====dataMap===='+dataMap);
            }
        
        } catch(ApplicationExceptionManager.ApplicationException ca) {
            response.errorlist.add(ROQExceptionService.auditUtility(ca));
        } catch(QueryException qe) {
            response.errorlist.add(ROQExceptionService.queryException(qe));        
        } catch(Exception ex) {
            response.errorlist.add(ROQExceptionService.parentException(ex));
        }*/
       return response;  
    }
    
    global static ROQResponseWrapper.AgilingoResponseWrapper getAllMetaData() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        /*List<String> objectList = new List<String> {Label.STRATEGY, Label.PORTFOLIO, Label.INITIATIVE, Label.PROGRAM, Label.ROADMAP, Label.PROJECT, Label.INCREMENT, Label.EPIC};
        
        Map<String, Map<String, Set<Field>>> finalFieldMap1 = new Map<String, Map<String, Set<Field>>>();
        
        try{
            result.isSuccess = true;
            for(String obj : objectList) {
                Schema.SObjectType objType = Schema.getGlobalDescribe().get(obj);
                Schema.DescribeSObjectResult res = Schema.getGlobalDescribe().get(obj).getDescribe(); 
                
                System.debug('=====objTypeBefore====='+objType);
                Map<String,Schema.SObjectField> objMap = objType.getDescribe().fields.getMap();
                Map<String, Schema.FieldSet> FsMap = objType.getDescribe().fieldSets.getMap();     
                
                //Schema.FieldSet objFieldSet = FsMap.get('Agilingo');
                System.debug('=====FsMap.keyset()====='+FsMap.keyset());
                List<Field> reqFieldList = new List<Field>();
                

                for(String st : FsMap.keyset()) {
                    Map<String, Set<Field>> reqfieldMap1 = finalFieldMap1.containsKey(obj)
                             ? finalFieldMap1.get(obj)
                             : new Map<String, Set<Field>>();

                    Set<Field> reqFieldSet1 = reqfieldMap1.containsKey(st) 
                        ? reqfieldMap1.get(st)
                        : new Set<Field>();
                   
                    if(st != null) {
                        
                        for(Schema.FieldSetMember member : fsMap.get(st).getFields()) {
                                Field reqField = new Field();
                                reqField.label = member.getLabel();
                                reqField.value = member.getFieldPath(); //API Name
                                reqField.isReadOnly = true;
                                reqField.type = String.valueOf(member.getType());
                                if(reqField.type == 'DOUBLE' || reqField.type == 'CURRENCY'){
                                    reqField.maxLength = res.fields.getMap().get(member.getFieldPath()).getDescribe().getPrecision();
                                    
                                } else {
                                    reqField.maxLength = res.fields.getMap().get(member.getFieldPath()).getDescribe().getLength();
                                }
                                
                                System.debug('=====reqField.maxLength====='+reqField.maxLength);
                                reqField.isRequired = member.getRequired();
                                
                                if(reqField.type == 'PICKLIST' || reqField.type == 'MULTIPICKLIST') {
                                    reqField.picklistValues = getPicklistValue(objMap.get(member.getFieldPath()));
                                }   
                                
                            reqFieldSet1.add(reqField);

                            System.debug('-- reqFieldSet1 -- '+reqFieldSet1);
                        }

                        System.debug('=====reqFieldListToCheckAgain====='+reqFieldList);
                        System.debug('====stcheck====('+st+')');

                        reqfieldMap1.put(st, reqFieldSet1);  
                        
                    }

                    finalFieldMap1.put(obj, reqfieldMap1);
                }
            
           }

            result.data = finalFieldMap1;
        } catch(ApplicationExceptionManager.ApplicationException ca) {
            result.errorlist.add(ROQExceptionService.auditUtility(ca));
            
        //Query exception and Parent Exception in catch.    
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));        
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }*/
        
        return result;
    }
    
    
}