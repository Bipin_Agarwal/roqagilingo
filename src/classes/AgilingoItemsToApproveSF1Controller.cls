/*******************************************************

Apex Class: AgilingoItemsToApproveSF1Controller             
Author : Ceptes 
Created Date : 
Purpose : To display list of all the records pending for approval action in Salesforce1
 
******************************************************/

public class AgilingoItemsToApproveSF1Controller {
    public List<ProcessInstanceWorkItem> itemsToApproveList{get; set;}
    
    // Gets all the records pendings records in the name of current user
    
    public AgilingoItemsToApproveSF1Controller(){
    
        itemsToApproveList =  [SELECT Id,ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name,ProcessInstance.TargetObject.Type,Actor.Name,ProcessInstance.CreatedDate 
        FROM ProcessInstanceWorkItem WHERE ActorId= :UserInfo.getUserId() AND ProcessInstance.Status = 'Pending' ORDER BY CreatedDate DESC Limit 100];
   
    }
}