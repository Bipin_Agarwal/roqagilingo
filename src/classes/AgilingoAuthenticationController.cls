/*******************************************************

Apex Class: AgilingoAuthenticationController             
Author : ROQMetrics
Created Date : 
Description : Authenticate various ways of approval process
 
******************************************************/

public without sharing class AgilingoAuthenticationController {    
    
    //Send OTP through Email
    @AuraEnabled
    public static String sendVerificationEmail() {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                System.debug('======Enter =sendVerificationEmail========');
                String userId = UserInfo.getUserId();
                User currentUser = [SELECT Id, UserName, OTP__c, OTP_Sent_Time__c, Email FROM User WHERE Id = :userId LIMIT 1];
                if(currentUser != null){      
                    Integer OtpToken = getRandomNumber(100000);
                    String OTP = String.valueOf(OtpToken);
                    String encryptedOTP = encryptData(OTP);
                    System.debug('======OTP========='+OTP);
                    currentUser.OTP__c = encryptedOTP;
                    currentUser.OTP_Sent_Time__c = System.now();
                    
                    if(User.sObjectType.getDescribe().isUpdateable()){
                        update currentUser;
                    }
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                    String emailBody = 'Hello your OTP is <b> '+ OTP + ' <b> <br/><br/> Your OTP is valid for 120 seconds';
                    mail.setSenderDisplayName('Salesforce Support');
                    mail.setSubject('Verification Code');
                    //mail.setTargetObjectId(userId);
                    String[] toAddresses = new String[] {UserInfo.getUserEmail()};
                        //mail.setToAddresses(toAddresses);
                        Id uId = UserInfo.getUserId();
                    mail.setTargetObjectId(uId);
                    mail.setSaveAsActivity(false);
                    mail.setHtmlBody(emailBody);
                    try{ 
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail}); 
                    }catch(Exception e){
                        System.debug('--Exception Caught---'+e.getMessage());
                        return 'Exception';
                    }   
                }
            }catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        return 'OK';
    }
    
    
    //Send OTP
    @AuraEnabled
    public static String sendOTP(){
        BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
        String status;
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try{
            if(appConfig.Send_OTP_via_SMS__c){
            //status = sendVerificationSMS();
            }else{
               status = sendVerificationEmail();
            }
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(DMLException de) {
            result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return status;
    }
    
    // Verify the OTP entered by the user
    @AuraEnabled
    public static String verifyOTP(String enteredOTP){
        
        String status = '';
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
        try{
                String userId = UserInfo.getUserId();
                User currentUser = [SELECT Id, UserName,OTP__c,OTP_Sent_Time__c,Email FROM User WHERE Id = :userId LIMIT 1];
                enteredOTP = enteredOTP.trim();
                String decryptedOTP = decryptData(String.valueOf(currentUser.OTP__c));
                System.debug('=====decryptedOTP=========='+decryptedOTP);
                
                if( getDifferenceFromCurrentTime(currentUser.OTP_Sent_Time__c) <= 120 ){
                    if(decryptedOTP == enteredOTP){
                        status = 'true';    
                    }else{
                        status = 'otpfailed'; 
                    }
                }else{
                    status = 'otptimeout';
                }
            } catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        return status;
    } 
    
    // Get current UserName  
    @AuraEnabled
    public static String getUserName() {
        return Userinfo.getUserName();
    }
    
    // get docusign signature
    @AuraEnabled
    public static String getSignature(String username,String password){
        
        String uname = username;
        String pwd = password;
        System.debug('-----username---'+uname);
        System.debug('-----password---'+pwd);
        String signature;
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
            result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                try{
                    signature = AgilingoSignatureController.getDocusignSignature(uname, pwd);
                    System.debug('---signature ---'+signature);
                    if(signature.contains('errorCode')){
                        signature ='invalid'; 
                    }
                }catch(Exception e){
                    System.debug('--Docusign Exception----'+e.getMessage());
                    signature = 'invalid';
                } 
                
            } catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        return signature ;
    }
    
    // Get current UserName  
    @AuraEnabled
    public static Map<String, String> getInitialSettings() {
        
        Map<String, String> settings = new Map<String, String>();
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                settings.put('user_name', Userinfo.getUserName());
                settings.put('theme_displayed', UserInfo.getUiThemeDisplayed());
                settings.put('theme', UserInfo.getUiTheme());
                settings.put('name', UserInfo.getName());
                settings.put('pwd_setup', String.valueOf(BP_App_Config__c.getOrgDefaults().get('Validate_Password__c')));
                settings.put('otp_setup', String.valueOf(BP_App_Config__c.getOrgDefaults().get('Validate_OTP__c')));
                settings.put('signature_setup', String.valueOf(BP_App_Config__c.getOrgDefaults().get('Get_Signature__c')));
            } catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        System.debug('----settings----'+settings);
        return settings;
    }
    
    // Verify current user's password
    @AuraEnabled
    public static Boolean verifyUser(String password){
        System.debug('----password----'+password);
        System.debug('=====All details-----'+AgilingoOAuthAuthentication.isValidCredentials(Userinfo.getUserName(), password));
        return  AgilingoOAuthAuthentication.isValidCredentials(Userinfo.getUserName(), password);
       
    }
    
    // update last E-Signature applied time
    /* @AuraEnabled
    public static void updateESignAppliedTime(String recordId){
    System.debug('---recordId----'+recordId);
    Lead l = [SELECT Id,E_Signature_Applied_On__c FROM Lead WHERE Id = '00Q50000016wnEw' LIMIT 1];
    l.E_Signature_Applied_On__c = System.now();
    // update l;
    
    }
    */
 
    //Get the Random Value as verification code
    public static Integer getRandomNumber(Integer size){
        return (math.random() * size).intValue();
    }
    
    // Encrypt the data
    public static String encryptData(String dataToEncrypt){
        
        String b64Data;
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                //Blob cryptoKey = Crypto.generateAesKey(128); 
                Blob cryptoKey = Blob.valueOf('380db410e8b11fa9');
                Blob data = Blob.valueOf(dataToEncrypt);
                Blob encryptedData = Crypto.encryptWithManagedIV('AES128', cryptoKey , data );
                b64Data = EncodingUtil.base64Encode(encryptedData);
            } catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        System.debug('Printing encryptData '+b64Data);  
        return b64Data;
    }
    
    
    // Decrypt the data
    public static String decryptData(String dataToDecrypt){
        
        String decryptDataString;
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                //Blob cryptoKey = Crypto.generateAesKey(128);
                Blob cryptoKey = Blob.valueOf('380db410e8b11fa9');
                Blob data = EncodingUtil.base64Decode(dataToDecrypt);
                Blob decryptedData = Crypto.decryptWithManagedIV('AES128', cryptoKey , data);
                decryptDataString = decryptedData.toString();
            }catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        System.debug('Printing decryptDataString '+decryptDataString);
        return decryptDataString;
    }
    
    // get the difference from current dateTime in seconds 
    public static Integer getDifferenceFromCurrentTime(DateTime t1){
        Integer differenceInSeconds;
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                datetime currentTime = System.now();
                datetime otpSentTime = DateTime.valueOf(t1);
                decimal millisecs = decimal.valueOf(currentTime.getTime() - otpSentTime.getTime());
                system.debug('millisecs: ' +millisecs/1000); 
                differenceInSeconds = Math.Abs(Integer.valueOf(millisecs/1000));
            }catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        return differenceInSeconds;
    }
    
    //verify ESign Date: 20/5/2017
    @AuraEnabled
    public static String verifySignature(String signData){
        System.debug('--signData----'+signData);
        Boolean isPasswordValidated = false;
        Boolean isOTPValidated = false;
        Boolean isSignatureValidated = false;
        String verifiedResponse = '';
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                    Map<String, String> dataMap = signData != null ? (Map<String,String>) JSON.deserialize(signData, Map<String,String>.class) : null;
                    isPasswordValidated = dataMap.containsKey('isPasswordValidated') ? Boolean.valueOf(dataMap.get('isPasswordValidated')) : isPasswordValidated;
                    isOTPValidated = dataMap.containsKey('isOTPValidated') ? Boolean.valueOf(dataMap.get('isOTPValidated')) : isOTPValidated;
                    isSignatureValidated = dataMap.containsKey('isSignatureValidated') ? Boolean.valueOf(dataMap.get('isSignatureValidated')) : isOTPValidated;
                    BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
                    
                    System.debug('=====appConfig====='+appConfig);
                    
                    if(appConfig.Validate_Password__c && isPasswordValidated != true){
                        if(dataMap.get('password') != null && dataMap.get('password') != ''){
                            verifiedResponse = String.valueOf(verifyUser(dataMap.get('password')));
                        }else{
                            System.debug('=====Wrong Password=====');
                            verifiedResponse = 'pwdblank';
                        }
                        System.debug('----verifiedResponse-------'+verifiedResponse);
                    }
                    
                    if(appConfig.Validate_OTP__c && isOTPValidated != true){
                        if(dataMap.get('otp') != null && dataMap.get('otp') != '' &&
                           (verifiedResponse == 'true' || verifiedResponse == '')){
                               verifiedResponse = verifyOTP(dataMap.get('otp'));    
                           }else if(verifiedResponse != 'pwdblank' && verifiedResponse != 'false'){
                               verifiedResponse = 'otpblank'; 
                           }
                    }
                    
                    if(appConfig.Get_Signature__c){
                        if(verifiedResponse == 'OK' || verifiedResponse == 'true' || verifiedResponse == ''){
                            if(dataMap.get('docusername') != null && dataMap.get('docusername') != null  && 
                               dataMap.get('docpassword') != null && dataMap.get('docpassword') != ''){
                                   verifiedResponse = getSignature(dataMap.get('docusername'),dataMap.get('docpassword'));
                               }else if(verifiedResponse != 'pwdblank' && verifiedResponse != 'otpblank'){
                                   verifiedResponse = 'docblank'; 
                               }
                        }
                    }
            } catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        return verifiedResponse;
    }
    
    //create signature history after verification
    @AuraEnabled
    public static String createSignatureHistory(String data) {
        System.debug('======data=========='+data);
        String finalSubmitStatus;
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                Map<String, String> dataMap = data != null ? (Map<String,String>) JSON.deserialize(data, Map<String,String>.class) : null;
                
                Boolean isPasswordValidated = dataMap.containsKey('isPasswordValidated') ? Boolean.valueOf(dataMap.get('isPasswordValidated')) : false;
                Boolean isOTPValidated = dataMap.containsKey('isOTPValidated') ? Boolean.valueOf(dataMap.get('isOTPValidated')) : false;
                Boolean isSignatureValidated = dataMap.containsKey('isSignatureValidated') ? Boolean.valueOf(dataMap.get('isSignatureValidated')) : false;
                Boolean isESignAppliedInSF1 = false;
        
                // get user info
                String userId = UserInfo.getUserId();
                User currentUser = [SELECT Id, UserName,Email FROM User WHERE Id = :userId LIMIT 1];      
                String recordId;
                String comments;
                String signature;
                String status;
                String nextApproverId;
                String currentStep;
                BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults(); // app settings  
                
                if(dataMap.containsKey('password') && dataMap.containsKey('otp') && dataMap.containsKey('signature') && dataMap.containsKey('isSF1')){
                    if((appConfig.Validate_Password__c && dataMap.get('password') != null) || (appConfig.Validate_OTP__c && dataMap.get('otp') != null) || 
                                   (appConfig.Get_Signature__c && dataMap.get('signature') != null) ){
                        
                        isESignAppliedInSF1 = true;
                    }
                }
                
                if(dataMap != null){
                    comments = dataMap.get('comments') != null && dataMap.get('comments') != '' ? dataMap.get('comments') : null;
                    recordId = dataMap.get('recordId') != null && dataMap.get('recordId') != '' ? dataMap.get('recordId') : null;
                    signature = dataMap.get('signature') != null && dataMap.get('signature') != '' ? dataMap.get('signature') : null ; 
                    status = dataMap.get('status') != null && dataMap.get('status') != '' ? dataMap.get('status') : null ;
                    if(!Test.isRunningTest())
                        currentStep = getCurrentApprovalStepName(recordId);
                    
                    AgilingoUtil.ApproveRejectParams setApproveRejectParams = new AgilingoUtil.ApproveRejectParams();
                    setApproveRejectParams.targetRecordId = recordId != null ? recordId : null;
                    setApproveRejectParams.comment = comments != null ? comments : null;
                    setApproveRejectParams.status = status != null ? status : null;
                    setApproveRejectParams.nextApproverId = dataMap.get('nextapprover') != null && dataMap.get('nextapprover') != '' ? dataMap.get('nextapprover') : null; 
                    //Aprove/Reject process
                    String approvalHistoryId = AgilingoUtil.approveRecord(setApproveRejectParams);
                    String stepStatus = status == 'Approve' ? 'Approved' : status == 'Reject' ? 'Rejected' : null;
                    
                    if(approvalHistoryId != null && approvalHistoryId.startsWith('04g')){
                        
                        //get processNodeId
                        ProcessInstance pInstance = [SELECT Id, (SELECT ID, StepStatus FROM StepsAndWorkitems WHERE StepStatus = :stepStatus  ) FROM ProcessInstance WHERE Id = :approvalHistoryId LIMIT 1];
                        
                        // create signature history
                        Signature_History__c sh = new Signature_History__c();
                        sh.Comments__c = comments;
                        sh.Parent_Record_Id__c = recordId;
                        sh.Is_Credential_Authenticated__c = appConfig.Validate_Password__c;
                        sh.Is_OTP_Validated__c = appConfig.Validate_OTP__c;
                        
                        if((appConfig.Validate_Password__c && isPasswordValidated) || (appConfig.Validate_OTP__c && isOTPValidated) || 
                           (appConfig.Get_Signature__c && isSignatureValidated) || isESignAppliedInSF1){
                               sh.Is_Signature_Retrieved__c = true;
                           }else{
                               sh.Is_Signature_Retrieved__c = false;
                           }
                        
                        sh.Signature__c = signature;
                        sh.Step_Name__c = currentStep;
                        if(!Test.isRunningTest())
                        sh.Approval_History__c = pInstance.StepsAndWorkitems[0].Id;
                        if(Signature_History__c.sObjectType.getDescribe().isCreateable()){
                            insert sh;
                        }
                        System.debug('====signature history id========'+sh.id);
                        finalSubmitStatus = 'SUCCESS';
                    }else{
                        finalSubmitStatus = approvalHistoryId;
                    }
                }
            } catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        return finalSubmitStatus;
    }
    
    // check if current user is next approver
    @AuraEnabled
    public static Boolean isNextApprover(String recId){
        System.debug('recid---------------'+recId);
        Boolean isApprover = false;
        List<ProcessInstanceWorkitem> pitemList;
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                if(!Test.isRunningTest()){
                    pitemList = [SELECT Id,ProcessInstance.TargetObjectId,ActorId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId = :recId];
                }else{
                    pitemList = new List<ProcessInstanceWorkitem>();
                }
                
                if(pitemList != null){
                    for(ProcessInstanceWorkitem pitem : pitemList){
                        if(pitem.ActorId == UserInfo.getUserId() || [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name == 'System Administrator'){
                            isApprover = true;
                        }
                    }
                }
            
            } catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        return isApprover;
    }
    
    // check if signature required for the current approval step
    @AuraEnabled
    public static Boolean isSignatureForStep(String recId) {
    
        Boolean isSignatureRequired = true;
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        
        try{
            BP_App_Config__c appConfig = BP_App_Config__c.getOrgDefaults();
            
            if(!appConfig.Get_Signature__c && !appConfig.Validate_OTP__c && !appConfig.Validate_Password__c){
                isSignatureRequired = false;
            } else {   
            // Get object api from id
                String objectApi = String.valueOf(Id.valueOf(recId).getSobjectType());
                String objectLabel = Schema.getGlobalDescribe().get(objectApi).getDescribe().getLabel();
                
                List<ProcessInstance> pi = [SELECT ProcessDefinitionId,ProcessDefinition.Name, TargetObjectId, Id, Status FROM ProcessInstance WHERE TargetObjectId = :recId AND Status =: 'Pending' LIMIT 1];
                
                System.debug('=====pi====='+pi);
                String qString;
                if(pi.size() > 0) {
                    qString = 'SELECT Id,Object_Name__c,Approval_process__c, Steps_Require_Signature__c FROM BP_Approval_Steps_Auth_Config__c WHERE Object_Name__c=\''+objectLabel+'\' AND Approval_process__c =\''+pi[0].ProcessDefinition.Name+'\'';
                }
                
                
                
                System.debug('=====qString====='+qString);
                
                System.debug('=====query result====='+Database.query(qString));
                
                // Query the fields
                if(Database.query(qString).size() > 0) {
                    SObject obj = Database.query(qString);
                    
                    ProcessInstanceWorkItem pItem =  [ SELECT Id,ProcessInstanceId,ProcessInstance.TargetObjectId FROM ProcessInstanceWorkItem WHERE ProcessInstance.TargetObjectId =:recId LIMIT 1];
                    
                    //Check the Signature_Required_Steps__c field setup
                    if(obj.get('Steps_Require_Signature__c') != null && obj.get('Steps_Require_Signature__c') != ''){
                        ProcessInstanceNode pNode = [SELECT Id, ProcessNodeName FROM ProcessInstanceNode Where ProcessInstanceId = :pItem.ProcessInstanceId AND NodeStatus = 'Pending' LIMIT 1];
                        String assignedSteps = String.valueOf(obj.get('Steps_Require_Signature__c'));
                        isSignatureRequired = assignedSteps.containsIgnoreCase(pNode.ProcessNodeName) ? true : false;
                    }
                } else {
                    isSignatureRequired = false;
                }
                System.debug('====Debugged by karthik===sign required=='+isSignatureRequired);
            }
        } catch(QueryException qe) {
        result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(DMLException de) {
        result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
        result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return isSignatureRequired;
    }
    
    // get the current approval process step name (covered in ServiceUtilTest)
    public static String getCurrentApprovalStepName(String recId) {
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        ProcessInstanceNode pNode;    
        try{
                ProcessInstanceWorkItem pItem =  [ SELECT Id,ProcessInstanceId,ProcessInstance.TargetObjectId FROM ProcessInstanceWorkItem WHERE ProcessInstance.TargetObjectId =:recId LIMIT 1];
                pNode = [SELECT Id, ProcessNodeName FROM ProcessInstanceNode Where ProcessInstanceId = :pItem.ProcessInstanceId AND NodeStatus = 'Pending' LIMIT 1];
                
                System.debug('=====pItem====='+pItem);
                System.debug('=====pNode====='+pNode);
                
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(DMLException de) {
            result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        
        return pNode.ProcessNodeName;
    }
    
    //Recall approval process
    @AuraEnabled
    public static void recallApprovalProcess(String recId){
        
        Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();
        
        try{
            List<ProcessInstance> procIns = new List<ProcessInstance>([select Id from ProcessInstance where Status = 'Pending' and TargetObjectId = :recId]);
            
            List<ProcessInstanceWorkitem>  workitem = new List<ProcessInstanceWorkitem>([select Id from ProcessInstanceWorkitem where ProcessInstanceId = :procIns[0].id]);
            if ((workitem != null) && (workitem.size() > 0))
            {
                pwr.SetComments('Recalled');
                
                pwr.setWorkItemId(workitem[0].id);
                pwr.setAction('Removed'); 
                
                Approval.ProcessResult pr = Approval.process(pwr);
            }
        }catch(Exception e){
           System.debug('Exception caught : '+e.getMessage()); 
        }
        
    }
    
    //Get all active users
    @AuraEnabled
    public static List<User> getActiveUsers(){
    
        return AgilingoUtil.getActiveUsers();
        
    }
    
    //Reassign the user
    @AuraEnabled
    public static void reassignUser(String assignedUserId, String targetRecordId){
    
        AgilingoUtil.reasignUser(assignedUserId, targetRecordId);
        
    }
    
    // Get the Approval History List 
    @AuraEnabled
    public static ROQResponseWrapper.ApprovalHistory getApprovalHistoryList(String recordId){
    
        ROQResponseWrapper.ApprovalHistory ah = new ROQResponseWrapper.ApprovalHistory();
        ah.historyList = AgilingoUtil.getApprovalHistory(recordId);
        ah.submitterId = UserInfo.getUserId();
            
        return ah;
        
    }

}