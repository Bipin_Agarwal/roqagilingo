/**
* Author :  ROQMetrics 
* Date   : 25/05/2017
* Description : Selector class for AgilingoDynamicApprovalProcessCreation.
*/
public class AgilingoDynamicAPCreationSelector {

    /**
    * Date : 11/05/2017
    * Description : Get all existing Approval Process available for object. 
    * Param : objectName
    * Return : List of Approval Process for object. 
    */
    public static List<ProcessDefinition> getexistingApprovals(Set<String> objectNames) {
    
        List<ProcessDefinition> approvalProcessList = [SELECT Id,TableEnumOrId, Name FROM ProcessDefinition WHERE  TableEnumOrId  IN : objectNames];
        return approvalProcessList;    
    
    }

}