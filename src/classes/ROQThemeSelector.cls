/**
* Author :  ROQMetrics 
* Date   : 11/04/2017
* Description : Selector Class for Theme.
*/
public class ROQThemeSelector{

    /**
    * Date : 10/05/2017
    * Description :Get all the available Themes.
    * Param : themeName and numberOfRecord.
    * Return : List of Themes. 
    */ 
    
    
    public static List<Theme__c> getThemes(String themeName, Integer noOfRecord) {
    
        List<SObject> result = new List<SObject> ();
        try{
            // SECURITY SCANNER ISSUE ----- 21-12-2017 
            String soqlQuery;
            if (Schema.sObjectType.Theme__c.isAccessible()) {
                soqlQuery = 'SELECT Id, Description__c, Theme_Title__c FROM Theme__c {0} LIMIT '+noOfRecord;
            }
            String filterCondtion = String.isBlank(themeName) ? 'WHERE Theme_Title__c LIKE \'%' +String.escapeSingleQuotes(themeName)+'%\'' :  'WHERE Theme_Title__c LIKE \'%' +String.escapeSingleQuotes(themeName)+'%\'';
            
            String queryString = String.format(soqlQuery, new List<String>{filterCondtion});
                      
            result = Database.query(queryString);
       }catch(Exception e){
            ApplicationExceptionUtility.createAudit('4001', e.getMessage(), e);
        }
        
        return result;
    }
    /**
    * Date : 10/05/2017
    * Description :Get all the available Theme Names.
    * Param : Set<String> themeTitle
    * Return : List of Themes. 
    */
    public static List<Theme__c> getThemeName(Set<String> themeTitleSet) {
        
        // SECURITY SCANNER ISSUE ----- 21-12-2017 
        List<Theme__c> themeList = new List<Theme__c>();
        if (Schema.sObjectType.Theme__c.isAccessible()) {
            themeList = [SELECT Id, Theme_Title__c FROM Theme__c WHERE Theme_Title__c IN :themeTitleSet LIMIT 49999];
        }
        return themeList;
    
    }
    
   /* public static List<Theme__c> getThemesGridView() {
        
        // SECURITY SCANNER ISSUE ----- 21-12-2017 
        List<Theme__c> themeList = new List<Theme__c>();
        if (Schema.sObjectType.Theme__c.isAccessible()) {
            themeList = [SELECT Id, Theme_Title__c, Description__c FROM Theme__c LIMIT 49999];
        }
        return themeList;
        
    } */
    
 }