/**
* Author : ROQMetrics 
* Date   : 08/02/2018
* Description : Epic Bank Selector Class.
*/
public Class ROQEpicBankSelector {

    /*public static List<Epic__c> getEpicsGridView(Integer offsetValue, Integer limitValue) {
        
        String query = 'SELECT Id, Name, Epic_Title__c, Description__c, Epic_Size__c, Epic_Type__c, Epic_Status__c,'+ 
                    'LastModifiedDate, LastModifiedBy.Name, Priority__c, Category__c, Category__r.Title__c, CreatedBy.Name,'+
                    'OwnerId, Owner.Name,Lifecycle__c FROM Epic__c ORDER BY LastModifiedDate DESC LIMIT '+limitValue+' OFFSET '+offsetValue;
        
        return Database.query(query);
        
    }    */
    
    public static List<Epic_EpicTag__c> getEpicGridView(Integer offSetValue, Integer limitValue, String searchText, String fieldName, String order) {
        
        System.debug('-=-=-=-fieldName=-==='+fieldName);
        System.debug('-=-=-=-order=-==='+order);
        
        String search = String.escapeSingleQuotes(searchText);
        
        Map<String,String> fieldsMap = new Map<String, String>
        {'Epic Id' => 'Epic__r.Name', 
        'Title' => 'Epic__r.Epic_Title__c', 
        'Description' => 'Epic__r.Description__c',
        'Type' => 'Epic__r.Epic_Type__c',
        'Size' => 'Epic__r.Epic_Size__c',
        'Status' => 'Epic__r.Epic_Status__c',
        'Priority' => 'Epic__r.Priority__c',
        'Category' => 'Epic__r.Category__c',
        //'Tag' => '',
        'Lifecycle' => 'Epic__r.Lifecycle__c',
        'Owner' => 'Epic__r.Owner',
        'Last Modified By' => 'Epic__r.LastModifiedBy',
        'Last Modified Date' => 'Epic__r.LastModifiedDate'};
        
        String queryString1 = 'SELECT Epic__c, Epic__r.Id, Epic__r.Name, Epic__r.Epic_Title__c, Epic__r.Description__c,'+ 
                        'Epic__r.Epic_Size__c, Epic__r.Epic_Type__c, Epic__r.Epic_Status__c, Epic__r.LastModifiedDate,'+ 
                        'Epic__r.Parent_Epic__c , Epic__r.LastModifiedBy.Name, Epic__r.Priority__c, Epic__r.Category__c,'+ 
                        'Epic__r.Category__r.Title__c, Epic__r.Category__r.Properties__c, Epic__r.CreatedBy.Name, Epic__r.OwnerId, Epic__r.Owner.Name,'+
                        'Epic__r.Lifecycle__c, EpicTag__c, EpicTag__r.Title__c,EpicTag__r.Properties__c FROM Epic_EpicTag__c '+
                        'WHERE Epic__r.Category__r.Title__c LIKE \'%'+String.escapeSingleQuotes(searchText)+'%\' '+ 
                        'OR Epic__r.Owner.Name LIKE \'%'+search+'%\' '+ 
                        'OR Epic__r.Parent_Epic__r.Epic_Title__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Title__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Size__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.LastModifiedBy.Name LIKE \'%'+search+'%\' '+ 
                        'OR Epic__r.CreatedBy.Name LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Lifecycle__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Status__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Size__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Type__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Status__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Priority__c LIKE \'%'+search+'%\' '+
                        'OR EpicTag__r.Title__c LIKE \'%'+search+'%\'' ;
        
        //code change if condtion 
        String queryString2 = (fieldName == '')
                            ? 'ORDER BY CreatedDate DESC '
                            : 'ORDER BY '+ fieldsMap.get(fieldName) +' '+ order +'';
        
        String queryString3 = ' LIMIT '+limitValue+' ';
        
        String queryString4 = (offSetValue == null)
                            ? ' '
                            : 'OFFSET '+offsetValue+' ';
        
        String queryFinal = queryString1 + queryString2 + queryString3 ;
        
        System.debug('-=-=query-=--='+queryFinal);
        
        return Database.query(queryFinal);
        
    }
    
    /*public static List<Epic_EpicTag__c> getEpicGridView(Integer offSetValue, Integer limitValue, String searchText, String fieldName, String order) {
        
        System.debug('-=-=-=-fieldName=-==='+fieldName);
        System.debug('-=-=-=-order=-==='+order);
        
        String search = String.escapeSingleQuotes(searchText);
        
        String queryString1 = 'SELECT Epic__c, Epic__r.Id, Epic__r.Name, Epic__r.Epic_Title__c, Epic__r.Description__c,'+ 
                        'Epic__r.Epic_Size__c, Epic__r.Epic_Type__c, Epic__r.Epic_Status__c, Epic__r.LastModifiedDate,'+ 
                        'Epic__r.Parent_Epic__c , Epic__r.LastModifiedBy.Name, Epic__r.Priority__c, Epic__r.Category__c,'+ 
                        'Epic__r.Category__r.Title__c, Epic__r.Category__r.Properties__c, Epic__r.CreatedBy.Name, Epic__r.OwnerId, Epic__r.Owner.Name,'+
                        'Epic__r.Lifecycle__c, EpicTag__c, EpicTag__r.Title__c,EpicTag__r.Properties__c FROM Epic_EpicTag__c '+
                        'WHERE Epic__r.Category__r.Title__c LIKE \'%'+String.escapeSingleQuotes(searchText)+'%\' '+ 
                        'OR Epic__r.Owner.Name LIKE \'%'+search+'%\' '+ 
                        'OR Epic__r.Parent_Epic__r.Epic_Title__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Title__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Size__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.LastModifiedBy.Name LIKE \'%'+search+'%\' '+ 
                        'OR Epic__r.CreatedBy.Name LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Lifecycle__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Status__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Size__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Type__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Epic_Status__c LIKE \'%'+search+'%\' '+
                        'OR Epic__r.Priority__c LIKE \'%'+search+'%\' '+
                        'OR EpicTag__r.Title__c LIKE \'%'+search+'%\' ORDER BY CreatedDate DESC ' ;
                        
        
        String queryString3 = ' LIMIT '+limitValue+' ';
        
        String queryString4 = (offSetValue == null)
                            ? ' '
                            : 'OFFSET '+offsetValue+' ';
        
        String queryFinal = queryString1 + queryString3 + queryString4 ;
        
        System.debug('-=-=query-=--='+queryFinal);
        
        return Database.query(queryFinal);
        
    }*/
    
    
}