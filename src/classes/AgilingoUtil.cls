/*******************************************************

Apex Class: AgilingoSetupController             
Author : Ceptes 
Created Date : 
Description : Setup Page Controller, controls custom settings and approval process
 
******************************************************/

public class AgilingoUtil {
    
    
    // Submit for approve/reject
    public static String approveRecord(ApproveRejectParams innerParam){
        
        String approveRejectStatus;
        ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
        response.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments(innerParam.comment);
                req.setAction(innerParam.status);
                
                System.debug('------innerParam-----'+innerParam);
                if(innerParam.nextApproverId != null){
                    req.setNextApproverIds(new Id[] {innerParam.nextApproverId});
                }
                if(!Test.isRunningTest()) {
                    
                    //Id workItemId;
                    Id workItemId = [SELECT Id FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId = :innerParam.targetRecordId AND ActorId = :Userinfo.getUserId() Limit 1].Id;
                    for(ProcessInstanceWorkitem item : [SELECT Id,ActorId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId = :innerParam.targetRecordId]){
                        
                        if(item != null && (item.ActorId == UserInfo.getUserId() || [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name == 'System Administrator')){
                            workItemId = item.Id;
                        }
                    }
                    
                    if(workItemId != null)
                        req.setWorkitemId(workItemId);
                        
                    System.debug('workItemId---------'+workItemId);
                }
                
                try{
                    // Submit the request for approval
                    Approval.ProcessResult result =  Approval.process(req);
                    approveRejectStatus = result.getInstanceId();
                }catch(Exception e){
                    System.debug('Exception caught: '+e.getmessage());
                    if(e.getmessage().contains('REQUIRED_FIELD_MISSING') && e.getmessage().contains('nextApproverIds')){
                        approveRejectStatus = 'next approver required';
                    }else
                        approveRejectStatus = e.getmessage();
                }
        
            } catch(QueryException qe) {
                response.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                response.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                response.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        return approveRejectStatus; 
    } 
    
    
    // Re assign the user approver
    @AuraEnabled
    public static String reasignUser(String assignedUserId, String targetRecordId){
       
       ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
       result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                    try{
                            ProcessInstance process = [
                                Select Id, TargetObjectId, isDeleted, Status,
                                ( SELECT Id,ActorId FROM Workitems Order By Createddate Desc Limit 1)
                                From ProcessInstance
                                Where isDeleted = false 
                                and TargetObjectId = :targetRecordId
                                and Status = 'Pending'
                                Order By Createddate Desc Limit 1
                            ];
                            
                            if(process != null && process.Workitems != null && process.Workitems.size() > 0){
                                
                                ProcessInstanceWorkitem myStep = process.Workitems[0];
                                myStep.ActorId = assignedUserId;
                                if(ProcessInstanceWorkitem.sObjectType.getDescribe().isUpdateable()){
                                    update myStep;
                                }
                            }
                    
                }catch(Exception e){
                    System.debug('---Error while reassigning user---'+e.getMessage());
                }
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            } 
        
        return null;
    }   
    
    @AuraEnabled
    public static List<User> getActiveUsers(){
        return [SELECT Id,Name FROM User WHERE IsActive=true];
    }
    
    // Get the Approval History List 
    public static List<ProcessInstanceHistory> getApprovalHistory(String recordId){
        List<ProcessInstanceHistory> pinstanceList = new List<ProcessInstanceHistory>();
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                for(ProcessInstance pinstance : [SELECT Id,(SELECT ID, ActorId, Actor.Name,CreatedBy.Name, Comments, CreatedById, CreatedDate, IsDeleted, IsPending, OriginalActorId, OriginalActor.Name, ProcessInstanceId, ProcessNodeId, ProcessNode.Name , RemindersSent, StepStatus, TargetObject.Type, TargetObject.Name, TargetObjectId FROM StepsAndWorkitems WHERE StepStatus = 'Pending' AND (ActorId = :Userinfo.getUserId() OR CreatedById = :Userinfo.getUserId())  ORDER BY CreatedDate DESC, Id DESC) FROM ProcessInstance WHERE TargetObjectId = :recordId ORDER BY LastModifiedDate DESC,Id DESC LIMIT 1]){
                    if(pinstance != null){
                        pinstanceList.addAll(pinstance.StepsAndWorkitems);
                    }
                }
                
                for(ProcessInstance pinstance : [SELECT Id, (SELECT ID, ActorId, Actor.Name,CreatedBy.Name, Comments, CreatedById, CreatedDate, IsDeleted, IsPending, OriginalActorId, OriginalActor.Name, ProcessInstanceId, ProcessNodeId, ProcessNode.Name , RemindersSent, StepStatus, TargetObject.Type, TargetObject.Name, TargetObjectId FROM StepsAndWorkitems WHERE StepStatus != 'Pending' ORDER BY CreatedDate DESC, Id DESC) FROM ProcessInstance WHERE TargetObjectId = :recordId ORDER BY LastModifiedDate DESC,Id DESC]){
                    if(pinstance != null){
                        pinstanceList.addAll(pinstance.StepsAndWorkitems);
                    }
                }
        
            }catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
        
        return pinstanceList;
    }
    
    public class ApproveRejectParams{
        
        public String targetRecordId;
        public String comment;
        public String status;
        public String nextApproverId;
        
    }
    
}