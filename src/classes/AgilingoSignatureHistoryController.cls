public without sharing  class AgilingoSignatureHistoryController {
    
    public List<ROQResponseWrapper.SignatureHistory> sgnHistories{get; set;}
    
    public AgilingoSignatureHistoryController(){
        Map<String, String> urlParamentersMap = ApexPages.CurrentPage().getParameters();
        String parentRecordId = urlParamentersMap.get('id');
        sgnHistories = getSignatureHistoryList(parentRecordId);
    }
    
    @AuraEnabled
    public static ROQResponseWrapper.ApprovalHistory getSignatureHistory(String recordId){
        ROQResponseWrapper.ApprovalHistory ah = new ROQResponseWrapper.ApprovalHistory();
        ah.submitterId = UserInfo.getUserId();
        ah.signHistoryList = getSignatureHistoryList(recordId);
        return ah;
    }
    
    //Signature History (current)
    @AuraEnabled
    public static List<ROQResponseWrapper.SignatureHistory> getSignatureHistoryList(String recordId) {
  
        List<ROQResponseWrapper.SignatureHistory> shList = new List<ROQResponseWrapper.SignatureHistory>();
        ROQResponseWrapper.SignatureHistory sigHistory; 
        
        Map<String,ProcessInstanceHistory> approvalHistoryMap = new Map<String,ProcessInstanceHistory>();
        List<ProcessInstanceHistory> pinstanceList = new List<ProcessInstanceHistory>();
        
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
            
            try{
                pinstanceList = AgilingoUtil.getApprovalHistory(recordId);
                
                for(ProcessInstanceHistory piH : pinstanceList){
                    approvalHistoryMap.put(piH.Id,piH);
                }
                Map<String,Signature_History__c> shMap = new Map<String,Signature_History__c>();
                
                String recId = recordId+'%';
                
                for(Signature_History__c sh : [SELECT Id,Step_Name__c,CreatedBy.Name,CreatedDate,Approval_History__c,Comments__c,Signature__c, Is_Signature_Retrieved__c FROM Signature_History__c WHERE Parent_Record_Id__c LIKE :recId]){
                    if(sh.Approval_History__c != null)
                        shMap.put(sh.Approval_History__c,sh);
                }
                
                if(Test.isRunningTest()){
                  approvalHistoryMap.put('000000011111',new ProcessInstanceHistory());  
                }
                
                for(String instanceId : approvalHistoryMap.keyset()){
                        ProcessInstanceHistory p = approvalHistoryMap.get(instanceId);
                        sigHistory = new ROQResponseWrapper.SignatureHistory();
                    
                        sigHistory.stepName = p.ProcessNode.Name != null ? p.ProcessNode.Name : null ;
                        sigHistory.status = p.StepStatus != null ? p.StepStatus : null ;
                        sigHistory.objectName = p.TargetObject.Name != null ? p.TargetObject.Name : null ;
                        sigHistory.objectType = p.TargetObject.Type != null ? p.TargetObject.Type : null ;
                        sigHistory.actualApprover = p.Actor.Name != null ? p.Actor.Name : null ;
                        sigHistory.actualApproverId = p.ActorId;
                        sigHistory.createdById = p.CreatedById;
                        sigHistory.TargetObjectId = p.TargetObjectId != null ? p.TargetObjectId : null;
                        Signature_History__c sh = shMap.containsKey(instanceId) ? shMap.get(instanceId) : null;
                        if(sh != null){
                            sigHistory.createdDate = sh.CreatedDate;
                            sigHistory.signature = sh.Signature__c;
                            sigHistory.createdBy = sh.CreatedBy.Name;
                            sigHistory.isSignatureApplied = sh.Is_Signature_Retrieved__c;
                            sigHistory.comments = sh.Comments__c;
                        } else {
                            sigHistory.createdDate = p.CreatedDate;
                            sigHistory.createdBy = p.CreatedBy.Name;
                            sigHistory.comments = p.Comments;
                        }
                        
                        shList.add(sigHistory);
                }
            } catch(QueryException qe) {
                result.errorlist.add(ROQExceptionService.queryException(qe));
            } catch(DMLException de) {
                result.errorlist.add(ROQExceptionService.dmlException(de));
            } catch(Exception ex) {
                result.errorlist.add(ROQExceptionService.parentException(ex));
            }
       
        return shList;
    }
    
    //Recall the approval process
    @AuraEnabled
    public static void recallApprovalProcess(String recId){
        Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.errorlist = new List<ROQResponseWrapper.Error>();
        try{
            List<ProcessInstance> procIns = new List<ProcessInstance>([select Id from ProcessInstance where Status = 'Pending' and TargetObjectId = :recId]);
            
            List<ProcessInstanceWorkitem>  workitem = new List<ProcessInstanceWorkitem>([select Id from ProcessInstanceWorkitem where ProcessInstanceId = :procIns[0].id]);
            if ((workitem != null) && (workitem.size() > 0))
            {
                pwr.SetComments('Recalled');
                
                pwr.setWorkItemId(workitem[0].id);
                pwr.setAction('Removed'); 
                
                Approval.ProcessResult pr = Approval.process(pwr);
            }
        } catch(QueryException qe) {
            result.errorlist.add(ROQExceptionService.queryException(qe));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
    }
    
    @AuraEnabled
    public static Boolean isSignatureForStep(String recId){
        
        return AgilingoAuthenticationController.isSignatureForStep(recId);
    }
    
    @AuraEnabled
    public static Boolean isNextApprover(String recId){
        return AgilingoAuthenticationController.isNextApprover(recId);
    }

}