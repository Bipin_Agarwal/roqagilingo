/**
* Author :  ROQMetrics 
* Date   : 31/03/2017
* Description : Test Class for ROQStrategyService class.
*/
@isTest
public class ROQThemeServiceTest {
 
   static testMethod void getThemesFromThemeWrapperTest() {
   
       List<ROQResponseWrapper.Theme> themeTestNewList = new List<ROQResponseWrapper.Theme>();
       ROQResponseWrapper.Theme theme = new ROQResponseWrapper.Theme();
       themeTestNewList.add(theme);
       Map<Theme__c, List<Epic__c>> themeEpicMap = ROQThemeService.getThemesFromThemeWrapper(themeTestNewList);
       System.assert(themeEpicMap.size() > 0);
   
   }
   
   static testMethod void getThemeEpicMapTest() {
   
       ROQResponseWrapper.Epic epicTest1 = new ROQResponseWrapper.Epic();
       ROQResponseWrapper.Epic epicTest2 = new ROQResponseWrapper.Epic();
       List<ROQResponseWrapper.Epic> epicWrapList = new List<ROQResponseWrapper.Epic>();
       epicWrapList.add(epicTest1);
       epicWrapList.add(epicTest2);
       
       ROQResponseWrapper.Theme themeTest = new ROQResponseWrapper.Theme();
       themeTest.epics = epicWrapList;
       List<ROQResponseWrapper.Theme> themeTestNewList = new List<ROQResponseWrapper.Theme>();
       themeTestNewList.add(themeTest);
       Map<String,String> themeEpicMap = ROQThemeService.getThemeEpicMap(themeTestNewList);
       System.assert(themeEpicMap.size() > 0);
   }
}