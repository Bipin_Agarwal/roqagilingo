/**
* Author :  ROQMetrics 
* Date   : 31/03/2017
* Description : Test Class for Service class.
*/
@isTest
private class ServiceUtilTest {
     
     // Test class commented for Owner issue in DEV Org.
     
     /*static testMethod void submitForApprovalTest() {
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard Platform User'];
            
        User user1 = new User(LastName = 'Test',Alias = 'Test', Email = 'kartikey.k@ceptes.com', 
                Username = 'kartikey.kartikey1012@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id ,
                    TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                        LanguageLocaleKey = 'en_US');
        INSERT user1;
            
         
        Strategy__c stg = new Strategy__c(Strategy_Title__c = 'TestStrategy', Strategy_Description__c = 'Description');
        INSERT stg;
        System.assertEquals(stg.Strategy_Title__c,'TestStrategy');
        
        Portfolio__c  portfolio = new Portfolio__c(Portfolio_Title__c = 'portfolioTest', Parent_Strategy__c = stg.Id,Portfolio_Manager_Assignment__c= user1.Id);
        insert  portfolio;
        System.assertEquals(portfolio.Portfolio_Title__c,'portfolioTest');
        
        Program__c program = new Program__c(
            Program_Start_Date__c = Date.today(),
            Parent_Portfolio__c = portfolio.id,
            Program_Title__c = 'Test record',
            Program_Manager__c = user1.Id
          );
        insert program;
        System.assertEquals(program.Program_Title__c,'Test record');
        
        Road_Map__c planningMap = new Road_Map__c(
            RoadMap_Title__c = 'Test Record',
            Status__c = 'On track',
            Start_Date__c = Date.today(),
            Program_Id__c = program.id,
            End_Date__c = System.today().addDays(+1),
            Description__c = 'Description'
        );
        insert planningMap;
        System.assertEquals(planningMap.RoadMap_Title__c,'Test Record');
        
        Project__c project1 = new Project__c(
            Parent_Road_Map__c = planningMap.id,
            Project_Title__c = 'Test Record',
            Parent_Program__c = program.id,
            Description__c = 'Desc Test',
            Program_Manager__c=user1.Id
        );
        insert project1;
        System.assertEquals(project1.Project_Title__c,'Test Record'); 
        
         Program_Increment__c increment = new Program_Increment__c(
             Increment_Title__c  = 'Test Release Record',
             Road_Map__c = planningMap.id,
             Description__c = 'Test Description',
             Project_Id__c = project1.id,
             Start_Date__c = Date.today(),
             End_Date__c = System.today().addDays(+1),
             Program__c=program.id,
             Status__c = 'On Track',
             Lifecycle__c = 'Draft',
             Approval_Actions__c = 'Make Effective'
         );
        insert increment;
        
        System.debug('=====increment====='+increment);
        
        
        
        //User user2 = [SELECT Id, Name FROM User WHERE Id = '005410000034JLVAA2'];
            
        // Create an approval request
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(increment.id);
        
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(user1.Id); 
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        // Submit the record to specific process and skip the criteria evaluation
        
        req1.setProcessDefinitionNameOrId('Make_Effective_Approval_Process');
        req1.setSkipEntryCriteria(true);
        
        
        System.debug('=====req1====='+req1);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        
        // Verify the result
        System.assert(result.isSuccess());
        
        System.assertEquals(
            'Pending', result.getInstanceStatus(), 
            'Instance Status'+result.getInstanceStatus());
        
        // Approve the submitted request
        // First, get the ID of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 = 
            new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));
        
        
        System.debug('=====req2====='+req2);
        
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(req2);
        
        // Verify the results
        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
        AgilingoApprovalActionController.getApprovalResponse(String.valueOf(user1.Id), String.valueOf(stg.Id), 'comment','Make Effective');
        AgilingoApprovalActionController.getRecallFlag(String.valueOf(stg.Id));
        AgilingoAuthenticationController.getCurrentApprovalStepName(stg.Id);
            
    }*/
    
    /*static testMethod void getApproverList() {
    
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard Platform User'];
        
        User u1 = new User(LastName = 'Test',Alias = 'Test', Email = 'kartikey.k@ceptes.com', 
            Username = 'kartikey.kartikey1012@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id ,
                TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                    LanguageLocaleKey = 'en_US');
        INSERT u1;
        
        Strategy__c stg = new Strategy__c(Strategy_Title__c = 'TestStrategy', Strategy_Description__c = 'Description');
        INSERT stg;
        
        Portfolio__c pf = new Portfolio__c(Portfolio_Title__c = 'TestPortFolio', Parent_Strategy__c = stg.Id, Portfolio_Manager_Assignment__c = u1.Id );    
        INSERT pf;
        
        User u = [SELECT Id, Username, Name, FirstName, LastName FROM User WHERE LastName = 'Executive'];
        
        Program__c pgm = new Program__c(Program_Title__c = 'Testpgm', Program_Start_Date__c = System.today(), Parent_Portfolio__c = pf.id, Program_Manager__c = u.Id);
        INSERT pgm;
        
        Road_Map__c pm = new Road_Map__c(RoadMap_Title__c = 'TestPM', Start_Date__c = system.today(), Status__c = 'On Track', Program_Id__c = pgm.id, Description__c = 'test Description', End_Date__c = System.today());
        INSERT pm;
        
        Project__c project = new Project__c(
            Project_Title__c = 'testProject',
            Parent_Road_Map__c = pm.id,
            Description__c = 'descTest',
            Parent_Program__c = pgm.id,
            Status__c = 'On Track',
             Program_Manager__c = u1.Id
        );
        INSERT project;
        
        Program_Increment__c releasePlan = new Program_Increment__c(
             Increment_Title__c  = 'Test Release Record',
             Road_Map__c = pm.id,
             Description__c = 'Test Description',
             Project_Id__c = project.id,
             Start_Date__c = Date.today(),
              End_Date__c=Date.today(),
             Program__c=pgm.id,
             Status__c = 'On Track'
         );
        INSERT releasePlan;
        
        //AgilingoWizardController.getApproverList(project.Id);
        //RoadmapWizardController.getApproverList(releasePlan.Id);
        
    }*/
    // Test method for LookupController class
    // Date : July 6 2018
    static testMethod void searchTest() {
        
      Account acc = new Account (Name = 'Acc1', BillingCity = 'Banglore');
      INSERT acc;
        
        LookupController.search('Account','Acc1',new List<String>{'Name != null'},new List<String>{'BillingCity'});
        LookupController.getRecentlyViewed('Account',new List<String>{'Name != null'},new List<String>{'BillingCity'});
    }
    /*static testMethod void getApproverActionValueTest() {
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard Platform User'];
        
        User u1 = new User(LastName = 'Test',Alias = 'Test', Email = 'kartikey.k@ceptes.com', 
            Username = 'kartikey.kartikey1011@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id ,
                TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                    LanguageLocaleKey = 'en_US');
        INSERT u1;
        
        Strategy__c stg = new Strategy__c(Strategy_Title__c = 'TestStrategy', Strategy_Description__c = 'Description');
        INSERT stg;
        
        Portfolio__c pf = new Portfolio__c(Portfolio_Title__c = 'TestPortFolio', Parent_Strategy__c = stg.Id,Portfolio_Manager_Assignment__c= u1.Id );    
        INSERT pf;
        
        Program__c pgm = new Program__c(Program_Title__c = 'Testpgm', Program_Start_Date__c = System.today(), Parent_Portfolio__c = pf.id,Program_Manager__c= u1.Id);
        INSERT pgm;
        
        Road_Map__c pm = new Road_Map__c(RoadMap_Title__c = 'TestPM', Start_Date__c = system.today(), Status__c = 'On Track', Program_Id__c = pgm.id, Description__c = 'test Description', End_Date__c = System.today());
        INSERT pm;
    
        Project__c project = new Project__c(
            Project_Title__c = 'testProject',
            Parent_Road_Map__c = pm.id,
            Description__c = 'descTest',
            Parent_Program__c = pgm.id,
            Status__c = 'On Track',
            Lifecycle__c = 'Draft',
            Approval_Actions__c = 'Make Effective',
             Program_Manager__c = u1.Id
        );
        INSERT project;
        
        Program_Increment__c releasePlan = new Program_Increment__c (
             Increment_Title__c  = 'Test Release Record',
             Road_Map__c = pm.id,
             Description__c = 'Test Description',
             Project_Id__c = project.id,
             Start_Date__c = Date.today(),
             End_Date__c=Date.today(),
             Program__c=pgm.id,
             Status__c = 'On Track',
             Lifecycle__c = 'Draft',
             Approval_Actions__c = 'Make Effective'
         );
        INSERT releasePlan;
        
        List<Project__c> projectList = new List<Project__c>();
        projectList.add(project);
        List<Program_Increment__c> releasePlanList = new List<Program_Increment__c>();
        releasePlanList.add(releasePlan);
        
        //RoadmapWizardController.getApprovalActionValues(project.Id);
        //RoadmapWizardController.getApprovalActionValues(releasePlan.Id);
    }*/
   
    
}