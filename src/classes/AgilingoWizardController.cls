/**
* Author : ROQMetrics 
* Date   : 15/02/2018
* Description : Main Controller of Agilingo Application.
*/
public class AgilingoWizardController {
    
    /**
    * Date : /02/2018
    * Description : Getting count of Sobjects.
    * Param : ProgramId and Epic Type(Initiative,Roadmap and feature)
    * Return : Epic.  
    */    
    @RemoteAction @AuraEnabled
    public static ROQMappingService.EpicInfo getEpicInfo(String programId, String epicType, String roadmapId) {
        return ROQMappingService.getEpicInfo(programId, epicType, roadmapId);
    }
    
    /**
    * Date : /02/2018
    * Description : Getting Themes and related Epic of Roadmap.
    * Param : Parent Roadmap Id
    * Return : RoadmapItem.  
    */ 
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getItemByRoadmapId(String roadmapId){
        return ROQMappingService.getItemByRoadmapId(roadmapId);
    }
    
    /**
    * Date : /02/2018
    * Description : To save the Themes and related Epic of Roadmap added by the user.
    * Param : Roadmap JSON data and Parent Roadmap Id 
    * Return : RoadmapItem.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper manageDoSaveRoadmap(String roadMapRecord, String rmId){
        return ROQMappingService.manageDoSaveRoadmap(roadMapRecord, rmId);
    }
    
    /**
    * Date : /02/2018
    * Description : To get the Theme searched by user in Mapping Pages of Initiative and Roadmap.
    * Param : Theme Title , Offset value,  true/false and Type Roadmap/Initiative 
    * Return : Theme data.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getThemeList(String themeName, Integer numberOfRowsToSkip) {
        return ROQMappingService.getThemeList(themeName, numberOfRowsToSkip);
    }
    
    /**
    * Date : /02/2018
    * Description : To get the Epic searched by user in Mapping Pages of Initiative,Roadmap and Program Increment.
    * Param : Epic Title , Offset value,  true/false and Type Roadmap/Initiative or Feature 
    * Return : Epic data.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getAvailableEpics(String epicName, Integer noOfRecord, String type) {
        return ROQMappingService.getAvailableEpics(epicName, noOfRecord, type);
    }
    
    /**
    * Date : /02/2018
    * Description : To get the Records data by its Id.
    * Param : Record Id.
    * Return : Record Data.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getRecordDetailsById(Id recordId) {
        return ROQMappingService.getRecordDetailsById(recordId);
    }
    
    /**
    * Date : /02/2018
    * Description : Getting Themes and related Epic of Initiative.
    * Param : Parent Initiative Id
    * Return : InitiativeItem.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getItemByInitiativeId(String initiativeId) {
        return ROQMappingService.getItemByInitiativeId(initiativeId);
    }  
    
    /**
    * Date : /02/2018
    * Description : To save the Themes and related Epic of Initiative added by the user.
    * Param : Initiative JSON data and Parent Initiative Id 
    * Return : InitiativeItem.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper manageDoSaveInitiative(String initiativeRecord, String initId){
        return ROQMappingService.manageDoSaveInitiative(initiativeRecord, initId);
    }
    
    /**
    * Date : /02/2018
    * Description : To save the Themes and related Epic of Program Increment added by the user.
    * Param : Program Increment JSON data and Parent ProgramIncrement Id 
    * Return : InitiativeItem.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper manageDoSaveProgramIncrement(String releaseRecord, String piId) {
        return ROQMappingService.manageDoSaveProgramIncrement(releaseRecord, piId);
    }
    
    /**
    * Date : /02/2018
    * Description : Getting Themes of Roadmap and related Epic of Initiative.
    * Param : Parent Program Increment Id.
    * Return : Program Increment Item.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getItemByProgramIncrementId(String releasePlanId) {
        return ROQMappingService.getItemByProgramIncrementId(releasePlanId);
    }
    
    /**
    * Date : /02/2018
    * Description : To get Epic of type Initiative in Roadmap Mapping Page as ParentEpic.
    * Param : Program Id.
    * Return : Epic.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getInitiativeEpicsByProgramId(String programId) {
        return ROQMappingService.getInitiativeEpicsByProgramId(programId);
    }
    
    /**
    * Date : /02/2018
    * Description : To Delete the Epic in Mapping Pages.
    * Param : Type Initiative,Roadmap or Program Increment and Epic record Id.
    * Return : EpicDeleteValidationWrapper.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.EpicDeleteValidationWrapper getEpicsDeleatableInfo (String type, String recordId) {
        return ROQMappingService.getEpicsDeleatableInfo(type, recordId);
    }
    
    /**
    * Date : /02/2018
    * Description : To get all Epics in Epic Bank.
    * Param : Offset Value and Limit value for query.
    * Return : Epic Wrapper class.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getEpicsGridView(Integer offsetValue, String searchText, Integer limitValue, String epicCount) {
        return ROQEpicBank.getEpicsGridView(offsetValue, searchText, limitValue, epicCount);
    }
    
    /**
    * Date : 13/02/2018
    * Description : To save the Category color.
    * Param : color.
    * Return : ROQResponseWrapper.AgilingoResponseWrapper.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper doSaveCategoryColor(String color, String recordId) {
        return ROQEpicService.doSaveCategoryColor(color,recordId); 
    }
    
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper doSaveTagColor(String color, String recordId) {
        return ROQEpicService.doSaveTagColor(color,recordId); 
    }
    
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper doSaveEpics(String epicRecords ,Integer offsetValue, String searchText, Integer limitValue, String epicCount) {
        return ROQMappingService.doSaveEpics(epicRecords, offsetValue, searchText, limitValue, epicCount);
    }
    
    /**
    * Date : 14/02/2018
    * Description : To get the History of record from custom objects.
    * Param : Record Id.
    * Return : ROQResponseWrapper.AgilingoResponseWrapper.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getRecordHistory(String recordId) {
        return ROQMappingService.getRecordHistory(recordId);
    }
    
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getPicklistValues() {
        return ROQEpicBank.getPicklistValues();
    }
    
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getEpicCategories() {
        return ROQEpicBank.getEpicCategories();
    }
    
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getEpicTags() {
        return ROQEpicBank.getEpicTags();
    }
    
    @RemoteAction @AuraEnabled
    public static List<ROQEpicBank.EpicDetails> getEpicDetails(String epicId) {
        return ROQEpicBank.getEpicDetails(epicId);
    } 
    
    /**
    * Date : 19/02/2018
    * Description : To Delete the Epic in Epic Bank.
    * Param : Type Initiative,Roadmap or Program Increment and Epic record Id.
    * Return : EpicDeleteValidationWrapper.  
    */
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.EpicDeleteValidationWrapper deleteEpics(String type, String recordId) {
        return ROQMappingService.deleteEpics(type, recordId);
    }
    
    @RemoteAction @AuraEnabled
    public static List<ROQResponseWrapper.Epic> getSortedEpicRecords(Integer offSetValue, Integer limitValue, String fieldName, String order) {
        return ROQEpicBank.getSortedEpicRecords(offSetValue, limitValue, fieldName, order);
    }
    
    @RemoteAction @AuraEnabled
    public static ROQResponseWrapper.AgilingoResponseWrapper getProjectsGridView(Integer offsetValue, String searchText, Integer limitValue, String order, String fieldName, String projectCount) {
        return ROQProjectService.getProjectsGridView(offsetValue, searchText, limitValue, order, fieldName, projectCount);

    }
    
   @RemoteAction @AuraEnabled
   public static Map<String, List<ServiceUtil.ROQUserWrapper>> getEpicUsers() {
       return ROQMappingService.getEpicUsers();
   }
   
   @RemoteAction @AuraEnabled
   public static ROQResponseWrapper.AgilingoResponseWrapper getProjectPicklistValues() {
       return ROQProjectService.getProjectPicklistValues();
   }
   
   @RemoteAction @AuraEnabled
   public static ROQResponseWrapper.ProjectPicklistWrapper getProjectInfo() {
       return ROQProjectService.getProjectInfo();
   }
   
   
   @RemoteAction @AuraEnabled
   public static ROQResponseWrapper.AgilingoResponseWrapper doSaveEpicBankColumns(List<String> fieldsList) {
       return ROQEpicBank.doSaveEpicBankColumns(fieldsList);
   }
   
   @RemoteAction @AuraEnabled
   public static List<Road_Map__c> getRelatedRoadmapsToProgram(String programId) {
       return ROQProjectService.getRelatedRoadmapsToProgram(programId);
   }
   
   @RemoteAction @AuraEnabled
   public static ROQResponseWrapper.AgilingoResponseWrapper deleteProject(String recordId) {
       return ROQProjectService.deleteProject(recordId);
   }
   
   @RemoteAction @AuraEnabled
   public static ROQResponseWrapper.AgilingoResponseWrapper doSaveProject(String project, Integer offsetValue, String searchText, Integer limitValue, String order, String fieldName, String projectCount) {
       return ROQProjectService.doSaveProject(project, offsetValue, searchText, limitValue, order, fieldName, projectCount);
   }
   
   @RemoteAction @AuraEnabled
   public static ROQResponseWrapper.AgilingoResponseWrapper doSaveProjectBankColumns(List<String> fieldsList) {
       return ROQProjectService.doSaveProjectBankColumns(fieldsList);
   }
   
   @RemoteAction @AuraEnabled
   public static ROQResponseWrapper.AgilingoResponseWrapper getParentEpics(String epicType) {
       return ROQEpicBank.getParentEpics(epicType);
   }
   
   @RemoteAction @AuraEnabled
   public static String getTagProperties(String recordId) {
       return ROQEpicService.getTagProperties(recordId);    
   }
   
   @RemoteAction @AuraEnabled
   public static String getCategoryProperties(String recordId) {
       return ROQEpicService.getCategoryProperties(recordId);
   }
   
   @RemoteAction @AuraEnabled
   public static ROQResponseWrapper.AgilingoResponseWrapper getSprints(String searchText){
       return ROQEpicBank.getSprints(searchText);
   }
   
}