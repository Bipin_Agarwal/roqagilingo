/**
* Author : ROQMetrics 
* Date   : 31/03/2017
* Description : Test Class for ROQProjectService class.
*/

@isTest
public class ROQProjectServiceTest {

   
        
    
    static testMethod void getProjectListTest() {
    
        List<Project__c> projectList = new List<Project__c>();
        Project__c project = new Project__c(Project_Title__c = 'TestPM');
        Project__c project1 = new Project__c(Project_Title__c = 'Test');
        projectList.add(project);
        projectList.add(project1);
        system.assertEquals(2,projectList.size());
    
    } 
    
    static testMethod void getProjectPicklistValuesTest() {
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard Platform User'];
        
        User u1 = new User(LastName = 'Test',Alias = 'Test', Email = 'kartikey.k@ceptes.com', 
                           Username = 'kartikey.kartikey12@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id , 
                           TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                           LanguageLocaleKey = 'en_US');
        INSERT u1;
        
        Strategy__c stg = new Strategy__c(Strategy_Title__c = 'TestStrategy', Strategy_Description__c = 'Description');
        INSERT stg;
        
        Portfolio__c pf = new Portfolio__c(Portfolio_Title__c = 'TestPortfolio', Parent_Strategy__c = stg.Id, 
                                               Portfolio_Manager_Assignment__c = u1.Id );    
        INSERT pf;
        
        Program__c pgm = new Program__c(Program_Title__c = 'Testpgm', Program_Start_Date__c = System.today(), 
                                            Parent_Portfolio__c = pf.id, Program_Manager__c = u1.Id, 
                                            Program_Status__c = 'On Track' );
        INSERT pgm;
        
        Road_Map__c rm = new Road_Map__c(RoadMap_Title__c = 'TestRoadmap', Start_Date__c = system.today(), Status__c = 'On Track', 
                                             Program_Id__c = pgm.id, End_Date__c = System.today().addDays(+1), Description__c = 'testDescription');
        INSERT rm;  

        Project__c project = new Project__c( Project_Title__c = 'TestProject', Description__c='test',Parent_Program__c=pgm.Id,
                                                Project_Manager__c = u1.Id, Parent_Road_Map__c=rm.Id, Status__c='On Track',
                                                Schedule_Status__c='Off Track',Resource_Status__c='On Track', Lifecycle__c='Draft', 
                                                Budget_Status__c='On Track', Issue_Status__c='Major Issues'); 
        INSERT project;
    }
    
    static testmethod void doSaveProjectBankColumnsTest() {
        
        Profile p1 = [SELECT Id, Name FROM Profile WHERE Name = 'Standard User'];
        
        User u1 = new User(LastName = 'Test',Alias = 'Test', Email = 'niveditha.n@ceptes.com', 
            Username = 'kartikey.kartikey21@ceptes.com', CommunityNickname = 'Test', ProfileId = p1.Id , 
                TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
                    LanguageLocaleKey = 'en_US');
        INSERT u1;
        
        String fields = '"Title","Description","Lifecycle","Status","Funded", "Budget"';
        String fields2 = '"Title","Description"';
        String fields3 = '"Title"';
        User_Preferences__c userPre = new User_Preferences__c (Project_Bank_Column__c = fields ,User__c= u1.Id); 
        INSERT userPre;
        
        userPre.Project_Bank_Column__c = fields2;
        UPDATE userPre;
        
        User_Preferences__c userPreference = new User_Preferences__c (Project_Bank_Column__c = fields2, User__c= u1.Id); 
        INSERT userPreference;

        userPreference.Project_Bank_Column__c = fields3;
        UPDATE userPreference;
        
        List<String> fieldsList = new List<String>();
        fieldsList.add(fields); 
        fieldsList.add(fields2); 
        
        ROQProjectService.doSaveProjectBankColumns(fieldsList);
        
    }

    
    
    
    
}