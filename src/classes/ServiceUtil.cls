/**
* Author :  ROQMetrics 
* Date   : 24/03/2017
* Description : Service class for ServiceUtil.
*/
global with sharing class ServiceUtil {

    
    /**
    * Author :  ROQMetrics 
    * Date   : 24/03/2017
    * Description : Interface for ServiceUtil.
    */    
    global interface IServiceUtil {}
    
    /**
    * Date : /0/2018
    * Description : 
    * Param  : 
    * Return :
    */ 
    global static ROQResponseWrapper.AgilingoResponseWrapper submitForApproval(Id userId, Id recordId, String comment, String approverAction) {
        
        System.debug('===usrId==='+userId);
        
        
        ROQResponseWrapper.AgilingoResponseWrapper response = new ROQResponseWrapper.AgilingoResponseWrapper();
        response.isSuccess = true;
        response.errorList = new List<ROQResponseWrapper.Error>();
        
        try {
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments(comment);
            req1.setObjectId(recordId);
            req1.setSubmitterId(UserInfo.getUserId()); 
            
            // Code change on 4th Jan
            
            String objName = recordId.getSObjectType().getDescribe().getName();
            Schema.sObjectField approvalActionField = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap().get('Approval_Actions__c');
            
            String query = 'SELECT Id, Approval_Actions__c FROM '+objName+' WHERE Id =: recordId';
            Sobject obj = Database.query(query);
            System.debug('=====objCheck====='+obj);
            
            obj.put('Approval_Actions__c', approverAction);
            UPSERT obj;
            
            req1.setSkipEntryCriteria(true);
            if(userId != null) {
                List<Id> userIdList = new List<Id>();
                for(User u : [SELECT Id FROM User WHERE Id =:userId]) {  
                    userIdList.add(u.id);
                System.debug('===user==='+userIdList);
                }
                req1.setNextApproverIds(userIdList);
            } 
            Approval.ProcessResult result = Approval.process(req1);
            System.debug(result.getInstanceId());
            System.debug(result.getNewWorkitemIds());
            response.Msg = SYSTEM.LABEL.APPROVAL_PROCESS_SAVE_MESSAGE;
            response.isApproverRequired = false;
            System.debug('===automated Response==='+response); 
        } catch(Exception ce) {
            System.debug('=====APPROVALERRORMESSAGE====='+ce.getMessage());
            System.debug('=====APPROVALERRORMESSAGE====='+ce.getMessage());
            Schema.SObjectType sobjectType = recordId.getSObjectType();
            String sobjectName = sobjectType.getDescribe().getLabel();
            if(ce.getMessage().contains('NO_APPLICABLE_PROCESS')) {
                System.debug('Catch Block'+ce.getMessage());
                response.isSuccess = true;
                response.Msg = SYSTEM.LABEL.APPROVAL_PROCESS_ERROR_MESSAGE+' '+ sobjectName ;
                System.debug('=====response.Msg====='+response.Msg);
                response.isApproverRequired = false;
            } else if(ce.getMessage().contains('ENTITY_IS_LOCKED')) {
                System.debug('Catch Block'+ce.getMessage());
                response.isSuccess = false;
                response.Msg = SYSTEM.LABEL.RECORD_LOCK_MESSAGE;
                ROQResponseWrapper.Error error = new ROQResponseWrapper.Error();
                error.errorCode  = '1002';
                error.errorMessage  = ce.getMessage();
                response.errorList = new List<ROQResponseWrapper.Error>();
                response.errorlist.add(error);
            }else if(ce.getMessage().contains('missing required field: [nextApproverIds]') || ce.getMessage().contains('INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST')) {
                System.debug('Catch Block'+ce.getMessage());
                response.isSuccess = true;
                response.Msg = SYSTEM.LABEL.APPROVAL_PROCESS_NEXT_APPROVER_ERROR_MESSAGE;
                response.isApproverRequired = true;
            } else {
                response.isSuccess = false;
                response.Msg = System.Label.AGILINGO_ERROR_MESSAGE;
                ROQResponseWrapper.Error error = new ROQResponseWrapper.Error();
                error.errorCode  = '1001';
                error.errorMessage  = System.Label.AGILINGO_ERROR_MESSAGE;
                response.errorList = new List<ROQResponseWrapper.Error>();
                response.errorlist.add(error);
            }
            System.debug('==response.data=='+response.data);
        }
        
        return response;
    }
    
    /**
    * Author :  ROQMetrics 
    * Date   : 29/03/2017
    * Description : Inner class of ServiceUtil.
    */  
    global class ROQUserWrapper Implements IServiceUtil{
       @AuraEnabled public String UserId;
       @AuraEnabled public String UserLabel;
       @AuraEnabled public String LoggedInUserId;
       @AuraEnabled public String LoggedInUserName;
    }
    
    
    
    /**
    * Date : /0/2018
    * Description : Used to show the Recall button. 
    * Param  : Record Id
    * Return : Boolean 
    */ 
    global static Boolean getRecallFlag(String recordId) {
    
        List<ProcessInstanceHistory> phList = new List<ProcessInstanceHistory>();
        for(ProcessInstance pi : [SELECT Id, (SELECT TargetObjectId, ProcessInstance.TargetObject.Name, StepStatus, ActorId, Actor.Name, OriginalActorId, OriginalActor.Name, Comments, CreatedDate FROM StepsAndWorkitems) FROM ProcessInstance WHERE TargetObjectId =: recordId AND Status = 'Pending']){
            for(ProcessInstanceHistory ph:pi.StepsAndWorkitems ) {
                phList.add(ph);
            }
        }
        if(phList.size() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
    * Date : /0/2018
    * Description : Used to convert Date from String to Date datatype.
    * Param  : Date in String  
    * Return : Date
    */ 
    public static Date parseDate(String dateInString) {
        
        Date convertedDate=null;
        try {
              convertedDate=Date.parse(dateInString);
            
        } catch(Exception e) {
              convertedDate=Date.valueOf(dateInString);
        }
        return convertedDate;
    }
    
    /**
    * Date : /0/2018
    * Description : Used to replace the Special Character from the InputData
    * Param  : Text data  
    * Return : String
    */
    public static string replaceSpecialCharacters(String text) {
    
        String s = text; 
        String regExp = '[<>();]';
        String newtext = s.replaceAll(regExp,'');
        return newtext;
        
    }
    
    //@deprecated
    global static ROQResponseWrapper.AgilingoResponseWrapper getPermissionSets() {
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
       
        List<String> userList = new List<String>();
        for(PermissionSetAssignment user : [SELECT Id, PermissionSet.Name, AssigneeId FROM PermissionSetAssignment 
                                                WHERE AssigneeId = :UserInfo.getUserId()]) {
           userList.add(user.PermissionSet.Name);
        }
          result.isSuccess = true;
          result.data = userList;
          System.debug('======================result.data===================='+result.data);
        return result ;
    }
    
    //@deprecated
    global static ROQResponseWrapper.AgilingoResponseWrapper getUserInfo() {
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
    
        User loggedInUser = [SELECT Id, Name, ProfileId, Profile.Name, LastLoginDate 
            FROM User WHERE Id= :UserInfo.getUserId() LIMIT 1];
            
        String jiraEnabled = String.valueOf(AGILINGO_SETUP__c.getOrgDefaults().get('Jira_Enabled__c'));
        System.debug('=======jiraEnabled ========'+jiraEnabled);
        
        
        /*List<FMA_Setup__c> cusList = FMA_Setup__c.getall().values();
        System.debug('=======cusList========'+cusList);*/
        
        
        ROQResponseWrapper.DashboardInfoWrapper dashboardInfo = new ROQResponseWrapper.DashboardInfoWrapper();
        
        dashboardInfo.loggedInUserName = loggedInUser.Name;
        dashboardInfo.lastLoginDate = loggedInUser.LastLoginDate.format();
        dashboardInfo.loggedInUserProfileName = loggedInUser.Profile.Name;
        dashboardInfo.loggedInUserId = loggedInUser.Id;
        dashboardInfo.localeDateFormat = ServiceUtil.getUserDateFormat();
        dashboardInfo.jiraEnabled = jiraEnabled ; //String.valueOf(cusList[0].Is_Active__c);
        result.isSuccess = true;
        result.data = dashboardInfo;
        return result;
            
    }  
    
    public static String getUserDateFormat() {
        String userLocale  = UserInfo.getLocale();
        getLocaleToDateTimeFmtMap();
        if (!localeToDateTimeFmtMap.containsKey(userLocale))    return 'yyyy-mm-dd';
        return localeToDateTimeFmtMap.get(userLocale);
    }
    
      //Not Used
    public class RecordToObjectDetails {
        
        public Sobject records;
        public Map<String, String> sobjRecord;
        public Boolean isRecallAvailable;
        
        public override String toString() {
            return 'RecordToObjectDetails[isRecallAvailable='+isRecallAvailable+', sobjRecord='+JSON.serialize(sobjRecord)+']';
        }
    }
    
    //@deprecated
    global static ROQResponseWrapper.AgilingoResponseWrapper getRecordDetailsById(Id recordId) {
        ROQResponseWrapper.AgilingoResponseWrapper result = new ROQResponseWrapper.AgilingoResponseWrapper();
        result.isSuccess = false;
        result.data = null;
        result.errorList = new List<ROQResponseWrapper.Error>();
        try{
            //RecordToObjectDetails 
            DescribeSObjectResult describeResult = recordId.getSObjectType().getDescribe(); 
            System.debug( '====================describeResult========================================='+describeResult);          
            List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );      
            List<String> newFieldNames = new List<String>();
            Map<String, String> responseFieldsMap = new Map<String, String>();
            Map<String,Schema.SObjectField> fieldsMap = Schema.describeSObjects(new List<String>{describeResult.getName()})[0].fields.getMap();
            System.debug('====fieldsMap===='+fieldsMap);
            Map<String, String> newfieldNamesMap = new Map<String, String>();
            String actualFieldName = '', oldFieldName = '', newFieldName = '';
            Boolean isCustomField = null;
            for(integer i=0;i<fieldNames.size(); i++){
                Schema.DescribeFieldResult sobjFldResult = fieldsMap.get(fieldNames.get(i)).getDescribe();
                isCustomField = sobjFldResult.isCustom();
                actualFieldName = sobjFldResult.getName();
                oldFieldName = actualFieldName.toLowerCase();
                if(sobjFldResult.getType() == Schema.DisplayType.Reference){
                    newFieldName = isCustomField ? oldFieldName.replace('__c', '__r.Title__c') : (oldFieldName.trim() == 'id' ? 'id' : oldFieldName.substring(0, oldFieldName.length()-2) + '.Name');
                    newfieldNamesMap.put(oldFieldName, newFieldName);
                    newFieldNames.add(newFieldName);
                    responseFieldsMap.put(newFieldName, actualFieldName.replace('__c', '__Title__c'));
                }
                else {
                    newFieldNames.add(oldFieldName);
                    responseFieldsMap.put(oldFieldName, actualFieldName);
                }
            }
            System.debug('====fieldNames===='+JSON.serialize(fieldNames));
            System.debug('====newFieldNames===='+JSON.serialize(newFieldNames));
            System.debug('====newfieldNamesMap===='+JSON.serialize(newfieldNamesMap));
            System.debug('====responseFieldsMap===='+JSON.serialize(responseFieldsMap));
            String query = ' SELECT ' + String.join(newFieldNames, ',') + ' FROM ' + describeResult.getName() + ' WHERE id = :recordId LIMIT 1';
            System.debug( '==========query==========================='+query);      
            // return generic list of sobjects or typecast to expected type     
            SObject records = Database.query(query).get(0);
            Map<String, Set<Id>> otherObjectsMap = new Map<String, Set<Id>>();
            Map<String, String> sobjRec = new Map<String, String>();
            Map<String, Set<String>> objectFieldsMap = new Map<String, Set<String>>();
            Map<String, Id> fieldValueMap = new Map<String, Id>();
            Set<String> newFieldNamesSet = new Set<String>(newFieldNames);
            System.debug('====newFieldNamesSet===='+JSON.serialize(newFieldNamesSet));
            Id recId = null;
            String objName = null;
            for(String sobjFld : fieldNames){
                System.debug('====sobjFld===='+sobjFld);
                if(newFieldNamesSet.contains(sobjFld.toLowerCase())){
                    sobjRec.put(responseFieldsMap.get(sobjFld), String.valueOf(records.get(sobjFld)));
                    System.debug('====contains===='+sobjFld);
                }
                else {
                    recId = (Id) records.get(sobjFld);
                    objName = recId.getSobjectType().getDescribe().getName();
                    fieldValueMap.put(sobjFld, recId);
                    Set<Id> recIdSet = otherObjectsMap.containsKey(objName) ? otherObjectsMap.get(objName) : new Set<Id>();
                    recIdSet.add(recId);
                    otherObjectsMap.put(objName, recIdSet);
                    Set<String> fieldsSet = objectFieldsMap.containsKey(objName) ? objectFieldsMap.get(objName) : new Set<String>();
                    fieldsSet.add(sobjFld);
                    objectFieldsMap.put(objName, fieldsSet);
                }
            }
            System.debug('====fieldValueMap===='+fieldValueMap);
            System.debug('====otherObjectsMap===='+otherObjectsMap);
            System.debug('====objectFieldsMap===='+objectFieldsMap);            
            for(String name : otherObjectsMap.keySet()){
                Set<Id> ridSet = otherObjectsMap.get(name);
                Map<Id, sObject> sobjectMap = new Map<Id, sObject>(Database.query('SELECT Id, Name, Title__c FROM ' + name + ' WHERE Id in :ridSet'));
                System.debug('====sobjectMap===='+sobjectMap);
                Set<String> fieldsSet = objectFieldsMap.get(name);
                String sobjField = null, respField = null;
                for(String fldName : fieldsSet) {
                    sobjField = fieldsMap.get(fldName).getDescribe().getName().trim();
                    respField = responseFieldsMap.get(newfieldNamesMap.get(fldName)).trim();
                    sobjRec.put(sobjField, String.valueOf(sobjectMap.get(fieldValueMap.get(fldName)).get('Id')));
                    if(sobjField != respField){
                        sobjRec.put(respField, String.valueOf(sobjectMap.get(fieldValueMap.get(fldName)).get('Title__c')));
                    }
                }
            }
            result.isSuccess = true;
            RecordToObjectDetails rd= new RecordToObjectDetails();
            rd.sobjRecord = sobjRec;
            rd.isRecallAvailable = ServiceUtil.getRecallFlag(recordId);
            result.data = rd;
       } catch(ApplicationExceptionManager.ApplicationException ca) {            
            // Create the audit record
            result.errorlist.add(ROQExceptionService.auditUtility(ca));        
        } catch(DMLException de) {
            result.errorlist.add(ROQExceptionService.dmlException(de));
        } catch(Exception ex) {
            result.errorlist.add(ROQExceptionService.parentException(ex));
        }
        System.debug('===Result===='+result.toString());
        return result;
    } 
    
    private static Map<String,String> localeToDateTimeFmtMap;
    
    public static Map<String,String> getLocaleToDateTimeFmtMap () {
    if (localeToDateTimeFmtMap == null)
        localeToDateTimeFmtMap  = new Map<String,String> {
            'ar'            => 'dd/MM/yyyy',
            'ar_AE'         => 'dd/MM/yyyy',
            'ar_BH'         => 'dd/MM/yyyy',
            'ar_JO'         => 'dd/MM/yyyy',
            'ar_KW'         => 'dd/MM/yyyy',
            'ar_LB'         => 'dd/MM/yyyy',
            'ar_SA'         => 'dd/MM/yyyy',
            'bg_BG'         => 'yyyy-M-d',
            'ca'            => 'dd/MM/yyyy',
            'ca_ES'         => 'dd/MM/yyyy',
            'ca_ES_EURO'    => 'dd/MM/yyyy',
            'cs'            => 'd.M.yyyy',
            'cs_CZ'         => 'd.M.yyyy',
            'da'            => 'dd-MM-yyyy',
            'da_DK'         => 'dd-MM-yyyy',
            'de'            => 'dd.MM.yyyy',
            'de_AT'         => 'dd.MM.yyyy',
            'de_AT_EURO'    => 'dd.MM.yyyy',
            'de_CH'         => 'dd.MM.yyyy',
            'de_DE'         => 'dd.MM.yyyy',
            'de_DE_EURO'    => 'dd.MM.yyyy',
            'de_LU'         => 'dd.MM.yyyy',
            'de_LU_EURO'    => 'dd.MM.yyyy',
            'el_GR'         => 'd/M/yyyy',
            'en_AU'         => 'd/MM/yyyy',
            'en_B'          => 'M/d/yyyy',
            'en_BM'         => 'M/d/yyyy',
            'en_CA'         => 'dd/MM/yyyy',
            'en_GB'         => 'dd/MM/yyyy',
            'en_GH'         => 'M/d/yyyy',
            'en_ID'         => 'M/d/yyyy',
            'en_IE'         => 'dd/MM/yyyy',
            'en_IE_EURO'    => 'dd/MM/yyyy',
            'en_IN'         => 'd/MM/yyyy',
            'en_NZ'         => 'd/MM/yyyy',
            'en_SG'         => 'M/d/yyyy',
            'en_US'         => 'M/d/yyyy',
            'en_ZA'         => 'yyyy/MM/dd',
            'es'            => 'd/MM/yyyy',
            'es_AR'         => 'dd/MM/yyyy',
            'es_BO'         => 'dd-MM-yyyy',
            'es_CL'         => 'dd-MM-yyyy',
            'es_CO'         => 'd/MM/yyyy',
            'es_CR'         => 'dd/MM/yyyy',
            'es_EC'         => 'dd/MM/yyyy',
            'es_ES'         => 'd/MM/yyyy',
            'es_ES_EURO'    => 'd/MM/yyyy',
            'es_GT'         => 'd/MM/yyyy',
            'es_HN'         => 'MM-dd-yyyy',
            'es_MX'         => 'd/MM/yyyy',
            'es_PE'         => 'dd/MM/yyyy',
            'es_PR'         => 'MM-dd-yyyy',
            'es_PY'         => 'dd/MM/yyyy',
            'es_SV'         => 'MM-dd-yyyy',
            'es_UY'         => 'dd/MM/yyyy',
            'es_VE'         => 'dd/MM/yyyy',
            'et_EE'         => 'd.MM.yyyy',
            'fi'            => 'd.M.yyyy',
            'fi_FI'         => 'd.M.yyyy',
            'fi_FI_EURO'    => 'd.M.yyyy',
            'fr'            => 'dd/MM/yyyy',
            'fr_BE'         => 'd/MM/yyyy',
            'fr_CA'         => 'yyyy-MM-dd',
            'fr_CH'         => 'dd.MM.yyyy',
            'fr_FR'         => 'dd/MM/yyyy',
            'fr_FR_EURO'    => 'dd/MM/yyyy',
            'fr_LU'         => 'dd/MM/yyyy',
            'fr_MC'         => 'dd/MM/yyyy',
            'hr_HR'         => 'yyyy.MM.dd',
            'hu'            => 'yyyy.MM.dd',
            'hy_AM'         => 'M/d/yyyy',
            'is_IS'         => 'd.M.yyyy',
            'it'            => 'dd/MM/yyyy',
            'it_CH'         => 'dd.MM.yyyy',
            'it_IT'         => 'dd/MM/yyyy',
            'iw'            => 'dd/MM/yyyy',
            'iw_IL'         => 'dd/MM/yyyy',
            'ja'            => 'yyyy/MM/dd',
            'ja_JP'         => 'yyyy/MM/dd',
            'kk_KZ'         => 'M/d/yyyy',
            'km_KH'         => 'M/d/yyyy',
            'ko'            => 'yyyy. M. d',
            'ko_KR'         => 'yyyy. M. d',
            'lt_LT'         => 'yyyy.M.d',
            'lv_LV'         => 'yyyy.d.M',
            'ms_MY'         => 'dd/MM/yyyy',
            'nl'            => 'd-M-yyyy',
            'nl_BE'         => 'd/MM/yyyy',
            'nl_NL'         => 'd-M-yyyy',
            'nl_SR'         => 'd-M-yyyy',
            'no'            => 'dd.MM.yyyy',
            'no_NO'         => 'dd.MM.yyyy',
            'pl'            => 'yyyy-MM-dd',
            'pt'            => 'dd-MM-yyyy',
            'pt_AO'         => 'dd-MM-yyyy',
            'pt_BR'         => 'dd/MM/yyyy',
            'pt_PT'         => 'dd-MM-yyyy',
            'ro_RO'         => 'dd.MM.yyyy',
            'ru'            => 'dd.MM.yyyy',
            'sk_SK'         => 'd.M.yyyy',
            'sl_SI'         => 'd.M.y',
            'sv'            => 'yyyy-MM-dd',
            'sv_SE'         => 'yyyy-MM-dd',
            'th'            => 'M/d/yyyy',
            'th_TH'         => 'd/M/yyyy',
            'tr'            => 'dd.MM.yyyy',
            'ur_PK'         => 'M/d/yyyy',
            'vi_VN'         => 'dd/MM/yyyy',
            'zh'            => 'yyyy-M-d',
            'zh_CN'         => 'yyyy-M-d',
            'zh_HK'         => 'yyyy-M-d',
            'zh_TW'         => 'yyyy/M/d',
            'zh_HK_STROKE'  => 'd/M/yyyy'
        };
        return localeToDateTimeFmtMap;
    }    
    //@deprecated
    public static  Set<String> permissionSetAllocation(){
         return null;
    }
    
    public static ROQResponseWrapper.ObjectPermissionWrapper getObjectPermissions() {
    
        ROQResponseWrapper.ObjectPermissionWrapper resultWrapper = new ROQResponseWrapper.ObjectPermissionWrapper();
        resultWrapper.isEpicCreatable = (Schema.sObjectType.Epic__c.isCreateable() || Schema.sObjectType.Epic__c.isUpdateable());
        resultWrapper.isThemeCreatable = (Schema.sObjectType.Theme__c.isCreateable() || Schema.sObjectType.Theme__c.isUpdateable());
        resultWrapper.isRoadmapCreatable = (Schema.sObjectType.Road_Map__c.isCreateable() || Schema.sObjectType.Road_Map__c.isUpdateable());
        resultWrapper.isInitiativeCreatable = (Schema.sObjectType.Initiative__c.isCreateable() || Schema.sObjectType.Initiative__c.isUpdateable());
        resultWrapper.isProgramIncrementCreatable = (Schema.sObjectType.Program_Increment__c.isCreateable() || Schema.sObjectType.Program_Increment__c.isUpdateable());
        resultWrapper.isProjectCreatable = (Schema.sObjectType.Project__c.isCreateable() || Schema.sObjectType.Project__c.isUpdateable());
        return resultWrapper;
        
    }
    
    public static User getLoggedInUser() {
    
        User user = [SELECT Id, Name FROM User WHERE Id =: userInfo.getUserId() LIMIT 1];
        return user;
    
    }
    
    public static Map<Id, Boolean> getRecordAccess(Set<Id> recordIdSet) {
    
        Map<Id, Boolean> epicRecordAccessMap = new Map<Id, Boolean>();
        
        /*UserRecordAccess usrRecordAcess = [SELECT RecordId, HasReadAccess, HasEditAccess, HasAllAccess, HasDeleteAccess,
                                           MaxAccessLevel FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() 
                                           AND RecordId IN :recordIds LIMIT 49999]; */
        
        for(UserRecordAccess usrRecordAccess : [SELECT RecordId, HasEditAccess FROM UserRecordAccess 
            WHERE UserId = :UserInfo.getUserId() AND RecordId IN :recordIdSet LIMIT 49999]) {
            
            epicRecordAccessMap.put(usrRecordAccess.RecordId, usrRecordAccess.HasEditAccess);
        
        }
        
        return epicRecordAccessMap;
    }
    
    public static Map<Id, Set<Epic_Tag__c>> getEpicTags(Id epicId){
        
        //Map of epic Id and junction obj(epic and epicTag)
        Map<Id, Set<Epic_EpicTag__c>> epicTagMap = new Map<Id, Set<Epic_EpicTag__c>>();
        
        //Epic and Tag from junction object
        List<Epic_EpicTag__c> epicEpicTagList = [SELECT Id, Epic__c, EpicTag__c,EpicTag__r.Title__c,EpicTag__r.Properties__c FROM 
                                                    Epic_EpicTag__c LIMIT 49999];
        
        for(Epic_EpicTag__c epicEpicTag : epicEpicTagList ) {
            
            Set<Epic_EpicTag__c> epicEpicTagSet = epicTagMap.containsKey(epicEpicTag.Epic__c)
                ? epicTagMap.get(epicEpicTag.Epic__c)
                : new Set<Epic_EpicTag__c> ();
            
            epicEpicTagSet.add(epicEpicTag);
            
            epicTagMap.put(epicEpicTag.Epic__c, epicEpicTagSet);
        }
        
        ROQResponseWrapper.Epic result = new ROQResponseWrapper.Epic();
        Map<Id, Set<Epic_Tag__c>> epictagListMap = new Map<Id, Set<Epic_Tag__c>>();
        
        if (epicTagMap.containsKey(epicId)) {
            Set<Epic_Tag__c> epicTagSet = new Set<Epic_Tag__c> ();
            
            System.debug('-=-=- epicTagMap.get(epicId) -=-=- '+epicTagMap.get(epicId));
            
            for (Epic_EpicTag__c epicEpicTag : epicTagMap.get(epicId)) {
                epicTagSet.add(new Epic_Tag__c(Id = epicEpicTag.EpicTag__c, Title__c = epicEpicTag.EpicTag__r.Title__c, Properties__c = epicEpicTag.EpicTag__r.Properties__c));
            }
            
            System.debug('-- epicTagSet -- '+epicTagSet);
            result.Tag = new List<Epic_Tag__c>();
            if (epicTagSet.size() > 0) {
                //result.Tag.addAll(epicTagSet);
                epictagListMap.put(epicId, epicTagSet);
            }
            
        }
        return epictagListMap; 
    }
    
    public static Date formatDate(String dateToFormat){
	
        if((dateToFormat).indexOf('/') > -1) {
            List<String> dateArray =  (dateToFormat).split('/');
            if((dateArray[1].length()) < 2 || (dateArray[0].length()) < 2) {                
                String dateArray_month = (Integer.valueOf(dateArray[0]) < 10) ? '0' + dateArray[0] : dateArray[0];
                String dateArray_day = (Integer.valueOf(dateArray[1]) < 10) ? '0' + dateArray[1] : dateArray[1];
                String dateArray_Final = dateArray[2] + '-' + dateArray_month + '-' + dateArray_day;
                dateToFormat = dateArray_Final;
            }
        }
        
        System.debug('=======dateToFormat======'+dateToFormat);
        Integer day = Date.ValueOf(dateToFormat).day();
        Integer month = Date.ValueOf(dateToFormat).month();
        Integer year = Date.ValueOf(dateToFormat).year();
        DateTime formatDate = DateTime.newInstance(year, month, day);
        Date formatedDate = Date.ValueOf(formatDate.format('yyyy-MM-dd HH:mm:ss'));
        
        System.debug('=======formatedDate======'+formatedDate);
        return formatedDate;
	
	}
    
}