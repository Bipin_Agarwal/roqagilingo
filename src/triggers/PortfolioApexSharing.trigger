trigger PortfolioApexSharing on Portfolio__c (after insert, before update) {
    if(trigger.isInsert){
        List<Portfolio__Share> portfolioShares = new List<Portfolio__Share>();
        for(Portfolio__c p : trigger.new){
            Portfolio__Share portfolioShareRecord = new Portfolio__Share();
            portfolioShareRecord.ParentId = p.Id;
            portfolioShareRecord.UserOrGroupId = p.Portfolio_Manager_Assignment__c;
            portfolioShareRecord.AccessLevel = 'Edit';
            portfolioShareRecord.RowCause = 'Manual';
            portfolioShares.add(portfolioShareRecord);
        }
        Database.SaveResult[] portfolioShareInsertResult = Database.insert(portfolioShares,false);
    }
    
    List<Portfolio__Share> deleteList = new List<Portfolio__Share>();
    
    if(trigger.isUpdate) {
        List<Portfolio__Share> portfolioShareList = [SELECT Id, ParentId, UserOrGroupId FROM Portfolio__Share WHERE ParentId IN : trigger.newMap.keyset() AND RowCause = 'Manual'];
        for(Portfolio__Share ps : portfolioShareList) {
            if(trigger.newMap.keyset().contains(ps.ParentId)){
                System.debug('=====ps====in if='+ps);
                deleteList.add(ps);
            }
        }
        DELETE deleteList;
        
        List<Portfolio__Share> portfolioSharesNew = new List<Portfolio__Share>();
        for(Portfolio__c p : trigger.new){
            Portfolio__Share portfolioShareRecordNew = new Portfolio__Share();
            portfolioShareRecordNew.ParentId = p.Id;
            portfolioShareRecordNew.UserOrGroupId = p.Portfolio_Manager_Assignment__c;
            portfolioShareRecordNew.AccessLevel = 'Edit';
            portfolioShareRecordNew.RowCause = 'Manual';
            portfolioSharesNew.add(portfolioShareRecordNew);
        }
        Database.SaveResult[] portfolioShareInsertResult = Database.insert(portfolioSharesNew,false);
    }
}