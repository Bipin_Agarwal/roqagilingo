trigger ProgramApexSharing on Program__c (after insert, before update) {
    
    //Program__c oldProg;
    //Program__c newProg;
    if(trigger.isInsert) {
        List<Program__Share> programShares = new List<Program__Share>();
        for(Program__c p : trigger.new) {
            Program__Share programShareRecord = new Program__Share();
            programShareRecord.parentId = p.Id;
            programShareRecord.UserOrGroupId = p.Program_Manager__c;
            programShareRecord.AccessLevel = 'Edit';
            programShareRecord.RowCause = 'Manual';
            programShares.add(programShareRecord);
        }
        Database.SaveResult[] programShareRecord = Database.insert(programShares, false); 
    }
    
    List<Program__Share> deleteList = new List<Program__Share>();
    //if(oldProg != newProg) {
        if(trigger.isUpdate) {
            List<Program__Share> programShareList = [SELECT Id, ParentId, UserOrGroupId FROM Program__Share 
                WHERE ParentId IN : trigger.newMap.keyset() AND RowCause = 'Manual'];
            for(Program__Share ps : programShareList) {
                if(trigger.newMap.keyset().contains(ps.ParentId)){
                    deleteList.add(ps);
                }
            }
        
            DELETE deleteList;
            
            List<Program__Share> programSharesNew = new List<Program__Share>();
            for(Program__c p : trigger.new) {
                Program__Share programShareRecordNew = new Program__Share();
                programShareRecordNew.parentId = p.Id;
                programShareRecordNew.UserOrGroupId = p.Program_Manager__c;
                programShareRecordNew.AccessLevel = 'Edit';
                programShareRecordNew.RowCause = 'Manual';
                programSharesNew.add(programShareRecordNew);
            }
            Database.SaveResult[] programShareRecord = Database.insert(programSharesNew, false);
        }  
    //}
    
}