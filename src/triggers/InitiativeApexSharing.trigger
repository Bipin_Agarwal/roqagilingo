trigger InitiativeApexSharing on Initiative__c (after insert, before update) {
    if(trigger.isInsert) {
        List<Initiative__Share> initiativeShares = new List<Initiative__Share>();
        for(Initiative__c i : trigger.new) {
            Initiative__Share initiativeShareRecord = new Initiative__Share();
            initiativeShareRecord.parentId = i.Id;
            initiativeShareRecord.UserOrGroupId = i.Initiative_Manager_Assignment__c;
            initiativeShareRecord.AccessLevel = 'Edit';
            initiativeShareRecord.RowCause = 'Manual';
            initiativeShares.add(initiativeShareRecord);
        }
        Database.SaveResult[] initiativeShareResult = Database.insert(initiativeShares, false); 
    }
    
    List<Initiative__Share> deleteList = new List<Initiative__Share>();
    
    if(trigger.isUpdate) {
        List<Initiative__Share> initiativeShareList = [SELECT Id, ParentId, UserOrGroupId FROM Initiative__Share WHERE ParentId IN : trigger.newMap.keyset() AND RowCause = 'Manual'];
        for(Initiative__Share is : initiativeShareList) {
            if(trigger.newMap.keyset().contains(is.ParentId)){
                deleteList.add(is);
            }
        }
        DELETE deleteList;
        
        List<Initiative__Share> initiativeSharesNew = new List<Initiative__Share>();
        for(Initiative__c i : trigger.new) {
            Initiative__Share initiativeShareRecordNew = new Initiative__Share();
            initiativeShareRecordNew.parentId = i.Id;
            initiativeShareRecordNew.UserOrGroupId = i.Initiative_Manager_Assignment__c;
            initiativeShareRecordNew.AccessLevel = 'Edit';
            initiativeShareRecordNew.RowCause = 'Manual';
            initiativeSharesNew.add(initiativeShareRecordNew);
        }
        Database.SaveResult[] initiativeShareResult = Database.insert(initiativeSharesNew, false);
    }
}