trigger ProjectApexSharing on Project__c (after insert, before update) {
    if(trigger.isInsert) {
        List<Project__Share> projectShares = new List<Project__Share>();
        for(Project__c p : trigger.new) {
            Project__Share projectShareRecord = new Project__Share();
            projectShareRecord.parentId = p.Id;
            projectShareRecord.UserOrGroupId = p.Project_Manager__c;
            projectShareRecord.AccessLevel = 'Edit';
            projectShareRecord.RowCause = 'Manual';
            projectShares.add(projectShareRecord);
        }
        Database.SaveResult[] projectShareRecord = Database.insert(projectShares, false); 
    }
    
    List<Project__Share> deleteList = new List<Project__Share>();
    
    if(trigger.isUpdate) {
        List<Project__Share> projectShareList = [SELECT Id, ParentId, UserOrGroupId FROM Project__Share WHERE ParentId IN : trigger.newMap.keyset() AND RowCause = 'Manual'];
        for(Project__Share ps : projectShareList) {
            if(trigger.newMap.keyset().contains(ps.ParentId)){
                deleteList.add(ps);
            }
        }
        DELETE projectShareList;
        
        List<Project__Share> projectSharesNew = new List<Project__Share>();
        for(Project__c p : trigger.new) {
            Project__Share projectShareRecordNew = new Project__Share();
            projectShareRecordNew.parentId = p.Id;
            projectShareRecordNew.UserOrGroupId = p.Project_Manager__c;
            projectShareRecordNew.AccessLevel = 'Edit';
            projectShareRecordNew.RowCause = 'Manual';
            projectSharesNew.add(projectShareRecordNew);
        }
        Database.SaveResult[] projectShareRecord = Database.insert(projectSharesNew, false); 
    }
}