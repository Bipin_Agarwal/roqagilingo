({
    doInit : function(component, event, helper) {
        var epicList = component.get("v.epicList");
        var index = component.get("v.index");
        var color = (epicList[index].ThemeStyle !== undefined && epicList[index].ThemeStyle !== '') ? epicList[index].ThemeStyle.split(':')[1] : helper.generateRandomColor();
        epicList[index].ThemeStyle = "background:"+color;
        component.set("v.epicData",epicList[index]);
        var epicData = component.get("v.epicData");
        if(epicData.epics.length > 0){
            helper.setThemeHeight(component, event, helper, epicData);
            helper.createEpicBar(component, event, helper, epicData);
        }
        setTimeout(function(){
            var themeObj = document.getElementsByClassName('theme_panel');
            $(themeObj).droppable({
                drop:function(event, ui){
                    var newParentId = $(this).attr('id');
                    var oldParentId = localStorage.getItem("oldParentId");
                    var epicIndex = localStorage.getItem("epicIndex");
                    var epicList = component.get("v.epicList");
                    
                    if($A.util.isEmpty(oldParentId)){
                        helper.openModal(component, event, helper, 'Encountered exception on drop,reloading the mapping page','Application exception');
                        $A.enqueueAction(component.get("v.redesignMap"));
                    }
                    else if(oldParentId != newParentId){
                        for(var index=0;index<epicList[oldParentId].epics.length;index++){
                            if(index == epicIndex){
                                //component.set("v.callController",true);
                                var epics = Object.assign({},epicList[oldParentId].epics[index]);
                                helper.calculateThemeHeight(component, event, helper, newParentId, oldParentId, epics, epicIndex);
                                helper.changeThemeOrder(component, event, helper);
                                $A.enqueueAction(component.get("v.redesignMap"));
                                //component.set("v.callController",false);
                                break;
                            }
                        }
                    }else if(oldParentId == newParentId){
                        helper.setThemeHeight(component, event, helper, component.get("v.epicData"));
                    }
                }
            });
        },0);
    },
    newTheme:function(component, event, helper){
        var selectedTheme = event.currentTarget.getAttribute('data-name');
        component.set("v.selectedTheme",selectedTheme);
        $A.enqueueAction(component.get("v.openThemeModel"));
    },
    editThemeModel:function(component, event, helper){
        var epicData = component.get("v.epicData");
        var selectedTheme = event.currentTarget.getAttribute('data-name');
        component.set("v.selectedTheme",selectedTheme);
        $A.createComponent(
            "ROQA:ROQTheme",
            {
                header:'Edit: '+epicData.Name,
                oldName:epicData.Name,
                themeRecord:epicData,
                hyperLink:false,
                epicList:component.get("v.epicList"),
                editedThemeRecord:component.getReference("v.editedThemeRecord"),
                addTheme:component.getReference("c.editTheme")
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('epicContainer');
                    var body = targetCmp.get("v.body");
                    body.push(newButton);
                    targetCmp.set("v.body",body);
                }
            }
        );
    },
    editTheme:function(component, event, helper){  
        var editedThemeRecord = component.get("v.editedThemeRecord");
        var selectedTheme = component.get("v.selectedTheme");
        var epicList = component.get("v.epicList");
        component.set("v.epicData",editedThemeRecord);
        for(var index=0;index<epicList.length;index++){
            var themeId = (epicList[index].themeId !== undefined ? epicList[index].themeId : '');
            if(selectedTheme.split(' ').join('')+themeId === epicList[index].Name.split(' ').join('')+themeId){
                epicList[index].Name = editedThemeRecord.Name;
                epicList[index].Description = editedThemeRecord.Description;
                component.set("v.epicList",epicList);
                break;
            }
        }
    },
    removeTheme:function(component, event, helper){
        var epicData = component.get("v.epicData");
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : 'Are you sure to remove '+epicData.Name+'?',
                header : 'Remove Theme',
                type:'Confirm',
                callBack:component.getReference("c.removeThemeFunction")
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('epicContainer');
                    var body = targetCmp.get("v.body");
                    body.push(newButton);
                    targetCmp.set("v.body",body);
                }
            }
        );
    },
    removeThemeFunction:function(component, event, helper){
        var epicList = component.get("v.epicList");
        var themeName = component.get("v.epicData.Name");
        for(var index=0;index<epicList.length;index++){
            var themeId = (epicList[index].themeId !== undefined ? epicList[index].themeId : '');
            if(themeName.split(' ').join('')+themeId === epicList[index].Name.split(' ').join('')+themeId){
                epicList.splice(index, 1);
                component.set("v.epicList",epicList);
                component.set("v.actionPerformed",true);
                helper.changeThemeOrder(component, event, helper);
                $A.enqueueAction(component.get("v.redesignMap"));
                break;
            }
        }
    },
    availableTheme:function(component, event, helper){
        var selectedTheme = event.currentTarget.getAttribute('data-name');
        component.set("v.selectedTheme",selectedTheme);
        component.set("v.themeRecord",{});
        $A.createComponent(
            "ROQA:AvailableTheme",
            {
                epicList:component.get("v.epicList"),
                type:component.get("v.epicType"),
                roadMapId:component.get("v.roadMapId"),
                themeRecord:component.getReference("v.themeRecord"),
                addTheme:component.getReference("c.addAvailTheme")
            },
            function(newButton, status, errorMessage){
                if(status === "SUCCESS"){
                    var targetCmp = component.find('epicContainer');
                    var body = targetCmp.get("v.body");
                    body.push(newButton);
                    targetCmp.set("v.body",body);
                }
            }
        );
    },
    addAvailTheme:function(component,event,helper){
        $A.enqueueAction(component.get("v.addTheme"));
    },
    reorderTheme:function(component, event, helper){
        $A.enqueueAction(component.get("v.themeReorder"))
    },
    openEpicModel:function(component,event,helper){
        var epicData = component.get("v.epicData");
        var thimList = [];
        component.set("v.epicRecord",{});
        component.set("v.epicRecord.theme",'');
        thimList.push({label :epicData.Name,value : epicData.Name});
        $A.createComponent(
            "ROQA:ROQEpic",
            {
                epicData:component.get("v.epicData"),
                epicType:component.get("v.epicType"),
                roadMapId:component.get("v.roadMapId"),
                programId:component.get("v.programId"),
                epicRecord:component.getReference("v.epicRecord"),
                addEpic:component.getReference("c.addEpic"),
                themeStartDate:component.getReference("v.themeStartDate"),
                themeEndDate:component.getReference("v.themeEndDate"),
                availEpic:component.getReference("c.availEpicModel"),
                thimList:thimList
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('epicContainer');
                    var body = targetCmp.get("v.body");
                    body.push(newButton);
                    targetCmp.set("v.body",body);
                }
            }
        );
    },
    addEpic:function(component,event,helper){
        var epicRecord = component.get("v.epicRecord");
        var epicData = component.get("v.epicData");
        var epicOrder = component.get("v.epicOrder");
        epicRecord.EpicStyle = helper.setEpicTop(component, event, helper, epicData);
        epicData.epics.splice(++epicOrder,0,epicRecord);
        component.set("v.epicData",epicData);
        helper.destroyBar(component, event, helper);
        helper.setThemeHeight(component, event, helper, component.get("v.epicData"));
        helper.createEpicBar(component, event, helper, epicData);
    },
    editEpicModel:function(component,event,helper){
        var epicData = component.get("v.epicData");
        var epicRecord = epicData.epics[component.get("v.epicOrder")];
        var epicList = component.get("v.epicList");
        var thimList = [];
        for(var index=0;index<epicList.length;index++){
            thimList.push({label :epicList[index].Name,value :epicList[index].Name});
        }
        epicRecord.theme = epicData.Name;
        component.set("v.epicRecord",epicRecord);
        $A.createComponent(
            "ROQA:ROQEpic",
            {
                header:'Edit: '+epicRecord.Name,
                epicData:epicData,
                hyperLink:false,
                thimList:thimList,
                epicType:component.get("v.epicType"),
                roadMapId:component.get("v.roadMapId"),
                programId:component.get("v.programId"),
                themeStartDate:component.get("v.themeStartDate"),
                themeEndDate:component.get("v.themeEndDate"),
                epicRecord:component.get("v.epicRecord"),
                newEpicRecord:component.getReference("v.newEpicRecord"),
                addEpic:component.getReference("c.editEpic"),
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('epicContainer');
                    var body = targetCmp.get("v.body");
                    body.push(newButton);
                    targetCmp.set("v.body",body);
                }
            }
        );
    },
    editEpic:function(component,event,helper){
        var epicRecord = component.get("v.newEpicRecord");
        var epicData = component.get("v.epicData");
        var epicOrder = component.get("v.epicOrder");
        if(epicData.Name == epicRecord.theme){
            epicData.epics[epicOrder] = epicRecord;
            component.set("v.epicData",epicData);
            helper.destroyBar(component, event, helper);
            helper.createEpicBar(component,event,helper,epicData);
        }else{
            epicData.epics.splice(epicOrder,1);
            component.set("v.epicData",epicData);
            helper.changeEpicStyle(component, event, helper);
            helper.themeChanged(component, event, helper);
            $A.enqueueAction(component.get("v.redesignMap"));
        }
    },
    deleteEpic:function(component, event, helper){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : 'Are you sure to remove '+component.get("v.epicData.epics")[component.get("v.epicOrder")].Name+'?',
                header : 'Remove Epic',
                type:'Confirm',
                callBack:component.getReference("c.removeEpic")
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('epicContainer');
                    var body = targetCmp.get("v.body");
                    body.push(newButton);
                    targetCmp.set("v.body",body);
                }
            }
        );
    },
    removeEpic:function(component, event, helper){
        var epicData = component.get("v.epicData");
        var epicOrder = component.get("v.epicOrder");
        epicData.epics.splice(epicOrder,1);
        component.set("v.epicData",epicData);
        helper.destroyBar(component, event, helper);
        helper.setThemeHeight(component, event, helper, component.get("v.epicData"));
        helper.createEpicBar(component, event, helper, epicData);
    },
    availEpicModel:function(component, event, helper){
        component.set("v.epicRecord",{});
        $A.createComponent(
            "ROQA:AvailableEpics",
            {
                parentEpicList:component.get("v.epicList"),
                type:component.get("v.epicType"),
                epicRecord:component.getReference("v.epicRecord"),
                addEpic:component.getReference("c.availEpic")
            },
            function(newButton, status, errorMessage){
                if(status === "SUCCESS"){
                    var targetCmp = component.find('epicContainer');
                    var body = targetCmp.get("v.body");
                    body.push(newButton);
                    targetCmp.set("v.body",body);
                }
            }
        );
    },
    availEpic:function(component, event, helper){
        var epicData = component.get("v.epicData");
        component.set("v.epicRecord.theme",epicData.Name);
        var epicRecord = component.get("v.epicRecord");
        var thimList = [];
        thimList.push({label :epicData.Name,value :epicData.Name});
        $A.createComponent(
            "ROQA:ROQEpic",
            {
                thimList:thimList,
                epicData:component.get("v.epicData"),
                epicType:component.get("v.epicType"),
                programId:component.get("v.programId"),
                roadMapId:component.get("v.roadMapId"),
                themeStartDate:component.get("v.themeStartDate"),
                themeEndDate:component.get("v.themeEndDate"),
                epicRecord:component.getReference("v.epicRecord"),
                addEpic:component.getReference("c.addEpic"),
                availEpic:component.getReference("c.availEpicModel"),
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('epicContainer');
                    var body = targetCmp.get("v.body");
                    body.push(newButton);
                    targetCmp.set("v.body",body);
                }
            }
        );
    },
    addEpicBar:function(component, event, helper){
        var epicRecord = event.getParam("epicRecord");
        var theme = event.getParam("theme");
        var epicData = component.get("v.epicData");
        var themeId = (epicData.themeId !== undefined ? epicData.themeId : '');
        if(epicData.Name.split(' ').join('')+themeId === theme){
            epicRecord.EpicStyle = helper.setEpicTop(component, event, helper, epicData);
            epicData.epics.splice(epicData.epics.length,0,epicRecord);
            component.set("v.epicData",epicData);
            helper.destroyBar(component, event, helper);
            helper.setThemeHeight(component, event, helper, component.get("v.epicData"));
            helper.createEpicBar(component, event, helper, component.get("v.epicData"));
        }
    }
})