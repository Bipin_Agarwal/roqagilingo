({
    callBack:function(component, event, helper){
        $A.enqueueAction(component.get("v.callBack"));
        component.destroy();
    },
	closeModal : function(component, event, helper) {
		component.destroy();
	}
})