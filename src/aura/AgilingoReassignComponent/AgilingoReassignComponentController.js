({
    doInit: function(component,event, helper) {
        var action = component.get('c.getActiveUsers');
        action.setCallback(this, function(actionResult) {
            component.set('v.activeUsers', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    onSelectChange: function(component, event, helper) {
        var selectedUser = component.find("chooseuser").get("v.value");
        component.set("v.reassignedTo",selectedUser);
        console.log('========='+selectedUser);
    },
    
    reassign: function(component, event, helper) {
         console.log('====ID====='+component.get("v.recordId"));
         console.log('=====TO===='+component.get("v.reassignedTo"));
        var action = component.get("c.reasignUser");
        action.setParams({
            				"assignedUserId" : component.get("v.reassignedTo"),
                            "targetRecordId" : component.get("v.recordId")
        				});
        action.setCallback(this, function(response) {
            if(component.get("v.homeView") == true) {
            $A.enqueueAction(component.get("v.refreshApprovalList"));
            }  
            helper.closeReassignModel(component, event);
        });
        console.log('====before reassign==');
        $A.enqueueAction(action);
    },
    
	closeModel: function(component, event, helper) {    
      helper.closeReassignModel(component, event); 
    },
    
})