({
    closeReassignModel: function(component, event) {
        /*var event = $A.get("e.c:CloseModal");
        event.setParams({
            "navigate" : "true"
        });
        event.fire(); */
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('MB-Back');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');  
    },
    
    navigateToSignatureHistoryComponent : function(component, event) { 
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:AgilingoApprovalHistoryComponent",
            componentAttributes: {
                "recordId" : component.get("v.recordId")
            }
        });
        evt.fire(); 
    }
})