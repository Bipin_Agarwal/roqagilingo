({
	doInit : function(component, event, helper) {
		var getEpicInfo = component.get("c.getRecordHistory");
        getEpicInfo.setParams({
            'recordId' : component.get("v.recordId")
        });
        getEpicInfo.setCallback(this,function(response){
           var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.epicResult",response.getReturnValue().data);
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(getEpicInfo);
	},
    print:function(component, event, helper) {
        event.preventDefault();
        var header= component.get("v.header");
        window.open('/apex/ROQA__AgilingoEpicPrintPreview?id='+component.get("v.recordId")+'&title='+header,'_blank');
        
    },
    closeModal:function(component, event, helper){
        component.destroy();
    }
})