({
    doInit : function(component, event, helper) {
        
        var action = component.get("c.getUsers");
        action.setCallback(this,function(response){
            
            if(response.getState()=='SUCCESS'){
                component.set("v.usersList",response.getReturnValue());
            }else{
                // Do Nothing
            }
        })
        $A.enqueueAction(action);
    },
    
    submitAction : function(component, event, helper){
        var recordId = component.get("v.recId");
        var approverAction = component.get("v.appAction");
        var userId = component.get("v.userId");
        var comment = component.get("v.comment");
        var action = component.get("c.getApprovalResponse");
        
        console.log('==recordId==='+recordId);
        console.log('==approverAction==='+approverAction);
        console.log('==userId==='+userId);
        console.log('==comment==='+comment);
        console.log('==action==='+action);
        
        action.setParams({
            userId : userId,
            recordId : recordId,
            comment : comment,
            approverAction : approverAction
        }); 
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //var div = document.getElementById('message');
                //div.setAttribute("style", "color: white ;");
                //document.getElementById("message").innerHTML="Record has been successfully submitted for approval.";
                //console.log(response.getReturnValue());
                var result = response.getReturnValue();
                //console.log(result);
                if(result.data.Msg !== undefined){
                    component.set("v.messageDisplay", result.data.Msg);
                }
                $A.enqueueAction(component.get("v.disableBtn"));
                //call application event to refresh approval history
                $A.get("e.ROQA:ApprovalHistoryEvent").fire();
                component.destroy();
            } else {
                
            }
            
            
        });
        if(userId.length > 0){
            $A.enqueueAction(action);
        }
    },
    
    close : function(component, event, helper){
        component.destroy();
    }
})