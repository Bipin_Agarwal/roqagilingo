({
    doInit: function(component, event, helper){
        var cmpName = component.get("v.cmpName");
        $A.createComponent(
            "ROQA:"+cmpName,
            {
                recordId:component.get("v.recordId"),
                isZoom:false
            },
            function(newButton, status, errorMessage){
                if(status === "SUCCESS"){
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body",body);
                }
            }
        );
    },
    close:function(component, event, helper) {
        $A.enqueueAction(component.get("v.doInit"));
        component.destroy();
    }
})