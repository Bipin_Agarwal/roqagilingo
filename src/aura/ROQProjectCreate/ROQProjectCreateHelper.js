({
    clearHTML:function(component, event, helper){
        document.getElementById('missingName').innerHTML = '';
        document.getElementById('missingProgManager').innerHTML = '';
        document.getElementById('missingProgram').innerHTML = '';
        document.getElementById('missingDescription').innerHTML = '';
        document.getElementById('missingStatus').innerHTML = '';
        document.getElementById('missingOwner').innerHTML = '';
    },
    getProjectUser:function(component, event, helper){
        var userList = component.get("c.getProjectInfo");
        var projectRecord = component.get("v.projectRecord");
        userList.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                var returnedValue = response.getReturnValue();
                component.set("v.programManagerList",returnedValue);
                if(component.get("v.header") === 'New Project'){
                    projectRecord.ownerId = returnedValue.getProjectOwners[0].LoggedInUserId;
                    projectRecord.ownerName = returnedValue.getProjectOwners[0].LoggedInUserName;
                    component.set("v.projectRecord",projectRecord);
                }else{
                    component.find('InputDynamicOwner').set("v.value",projectRecord.ownerId);
                }
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(userList);
    },
    setRoadmap:function(component, event, helper, programId){
        var roadmapList = component.get("c.getRelatedRoadmapsToProgram");
        roadmapList.setParams({
            'programId' : programId
        });
        roadmapList.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.roadmapList",response.getReturnValue());
                component.set("v.projectRecord.parentProgram",programId);
                (component.get("v.header") != 'New Project' || component.get("v.clone")) ? component.find('InputDynamicRoadmap').set("v.value",component.get("v.projectRecord.parentRoadmap")) : '' ;
            }
        });
        $A.enqueueAction(roadmapList);
    },
    setOwnerValue:function(component, event, helper){
        var projectRecord = component.get("v.projectRecord");
        var ownerId = component.find('InputDynamicOwner').get("v.value");
        var userList = component.get("v.programManagerList.getProjectOwners")
        for(var index=0;index<userList.length;index++){
            if(ownerId === userList[index].UserId){
                projectRecord.ownerId = ownerId;
                projectRecord.ownerName = userList[index].UserLabel;
                component.set("v.projectRecord",projectRecord);
                break;
            }
        }
    },
    setValue:function(component, event, helper, projectRecord){
        (!$A.util.isEmpty(projectRecord.status) ? '' : component.set("v.projectRecord.status",'On Track'));
        (!$A.util.isEmpty(projectRecord.budgetStatus) ? '' : component.set("v.projectRecord.budgetStatus",'On Track')); 
        (!$A.util.isEmpty(projectRecord.scheduleStatus) ? '' : component.set("v.projectRecord.scheduleStatus",'On Track'));
        (!$A.util.isEmpty(projectRecord.resourceStatus) ? '' : component.set("v.projectRecord.resourceStatus",'On Track')); 
        (!$A.util.isEmpty(projectRecord.issueStatus) ? '' : component.set("v.projectRecord.issueStatus",''));
        component.find('InputDynamicStatus').set("v.value",component.get("v.projectRecord.status"));
        component.find('InputDynamicBudget').set("v.value",component.get("v.projectRecord.budgetStatus"));
        component.find('InputDynamicResource').set("v.value",component.get("v.projectRecord.resourceStatus"));
        component.find('InputDynamicSchedule').set("v.value",component.get("v.projectRecord.scheduleStatus"));
        component.find('InputDynamicIssue').set("v.value",component.get("v.projectRecord.issueStatus"));
        component.find('InputDynamicUser').set("v.value",component.get("v.projectRecord.projectManager"));
        //component.find('InputDynamicProgram').set("v.value",component.get("v.projectRecord.parentProgram"));
        //component.find('InputDynamicRoadmap').set("v.value",component.get("v.projectRecord.parentRoadmap"));
        component.find('boxCheck').set("v.value",component.get("v.projectRecord.funded"));
        if(!$A.util.isEmpty(projectRecord.parentProgram)){
            component.set("v.programId",component.get("v.projectRecord.parentProgram"));
            component.set("v.programRecord.Id",projectRecord.parentProgram);
			component.set("v.programRecord.Name",projectRecord.parentProgramAutonumber);
        }
        if(!$A.util.isEmpty(projectRecord.parentRoadmap)){
            component.set("v.roadmapRecord.Id",projectRecord.parentRoadmap);
			component.set("v.roadmapRecord.Name",projectRecord.parentRoadmapAutonumber);
        }
    },
    checkDate:function(component, event, helper, record){
        var returnedValue = true;
        if(!$A.util.isEmpty(record.plannedStartDate) && !moment(record.plannedStartDate).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.actualStartDate) && !moment(record.actualStartDate).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.plannedEndDate) && !moment(record.plannedEndDate).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.actualEndDate) && !moment(record.actualEndDate).isValid()){
            returnedValue = false;
       }if(!$A.util.isEmpty(record.goLive) &&!moment(record.goLive).isValid()){
            returnedValue = false;
       }
        return returnedValue;
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception',
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
	openSuccess:function(component, event, helper, message, style,iconName){
        $A.createComponent(
            "ROQA:ROQSuccessComponent",
            {
                saveStatus : message,
                style : style,
                iconName:iconName,
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.openModal(component, event, helper,message,'Response From Server.');
    },
})