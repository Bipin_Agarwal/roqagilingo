({
    doInit : function(component, event, helper) {
        helper.getProjectUser(component, event, helper);
        var fieldValues = component.get("c.getProjectPicklistValues");
        fieldValues.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.pickListValues",response.getReturnValue().data);
                component.set("v.callController",false);
                helper.setValue(component, event, helper,component.get("v.projectRecord"));
            }else{
                component.set("v.callcontroller",false);
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        component.set("v.callController",true);
        $A.enqueueAction(fieldValues);
    },
    setStatus:function(component, event, helper){
        component.set("v.projectRecord.status",component.find('InputDynamicStatus').get("v.value"));
    },
	selectedProgramValues:function(component, event, helper){
        if(!$A.util.isEmpty(component.get("v.programRecord.Name"))){
            component.set("v.programId",component.get("v.programRecord.Id"));
            component.set("v.projectRecord.parentProgram",component.get("v.programRecord.Id"));
			component.set("v.projectRecord.parentProgramAutonumber",component.get("v.programRecord.Name"));
            component.set("v.projectRecord.parentProgramTitle",component.get("v.programRecord.ROQA__Title__c"));
        }else{
            component.set("v.programId",'');
            component.set("v.projectRecord.parentProgram",'');
			component.set("v.projectRecord.parentProgramAutonumber",'');
            component.set("v.projectRecord.parentProgramTitle",'');
            component.set("v.projectRecord.parentRoadmap", null);
            component.set("v.roadmapRecord",{});
        }
    },
	selectedRoadmapValues:function(component, event, helper){
        if(!$A.util.isEmpty(component.get("v.roadmapRecord.Name"))){
            component.set("v.projectRecord.parentRoadmap",component.get("v.roadmapRecord.Id"));
			component.set("v.projectRecord.parentRoadmapAutonumber",component.get("v.roadmapRecord.Name"));
            component.set("v.projectRecord.parentRoadmapTitle",component.get("v.roadmapRecord.ROQA__Title__c"));
        }else{
            component.set("v.projectRecord.parentRoadmap",'');
			component.set("v.projectRecord.parentRoadmapAutonumber",'');
            component.set("v.projectRecord.parentRoadmapTitle",'');
        }
        
    },/*
    setProgram:function(component, event, helper){
        var programId = component.find('InputDynamicProgram').get("v.value");
        if(programId != '' && programId != null){
            helper.setRoadmap(component, event, helper, programId);
        }else{
            component.set("v.roadmapList", []);
            component.set("v.projectRecord.parentProgram",'');
            component.find('InputDynamicRoadmap').set("v.value",'');
            component.set("v.projectRecord.parentRoadmap", null);
        }
    },*/
    setOwner:function(component, event, helper){
        helper.setOwnerValue(component, event, helper);
    },/*
    setRoadmap:function(component, event, helper){
        component.set("v.projectRecord.parentRoadmap",component.find('InputDynamicRoadmap').get("v.value"));
    },*/
    setProjectManager:function(component, event, helper){
        component.set("v.projectRecord.projectManager", component.find('InputDynamicUser').get("v.value"));
    },
    setBudget:function(component, event, helper){
        component.set("v.projectRecord.budgetStatus",component.find('InputDynamicBudget').get("v.value"));
    },
    setResource:function(component, event, helper){
        component.set("v.projectRecord.resourceStatus",component.find('InputDynamicResource').get("v.value"));
    },
    setSchedule:function(component, event, helper){
        component.set("v.projectRecord.scheduleStatus",component.find('InputDynamicSchedule').get("v.value"));
    },
    setIssue:function(component, event, helper){
        component.set("v.projectRecord.issueStatus",component.find('InputDynamicIssue').get("v.value"));
    },
    checkBoxValue:function(component, event, helper){
            component.set("v.projectRecord.funded",component.find('boxCheck').get("v.value"));
    },
    saveEpic:function(component, event, helper){
        var clickedButton = event.currentTarget.getAttribute('data-label');
        var projectRecord = component.get("v.projectRecord");
		var validDate = true;
        validDate = helper.checkDate(component, event, helper, projectRecord);
        helper.clearHTML(component, event, helper);
        (component.get("v.header") === 'New Project') ? component.set("v.projectRecord.lifecycle",'Draft') : '';
        if(!$A.util.isEmpty(projectRecord.projectLabel) && !$A.util.isEmpty(projectRecord.description)
           && !$A.util.isEmpty(projectRecord.status) && !$A.util.isEmpty(projectRecord.projectManager) && validDate
           && !$A.util.isEmpty(projectRecord.parentProgram) && !$A.util.isEmpty(projectRecord.ownerId)){
            ((component.get("v.header") !== 'New Project') ? component.set("v.projectRecord.recordStatus",'update') : component.set("v.projectRecord.recordStatus",'New'));
            ((clickedButton === 'save') ? $A.enqueueAction(component.get("v.addToGrid")) : $A.enqueueAction(component.get("v.addAndOpen")));
            
        }else{
			var message = '';
            if($A.util.isEmpty(projectRecord.projectLabel)){
				message = 'Title is required';
                document.getElementById('missingName').innerHTML = 'Title is required';
            }else if($A.util.isEmpty(projectRecord.description)){
				message = 'Description is required';
                document.getElementById('missingDescription').innerHTML = 'Description is required';
            }else if($A.util.isEmpty(projectRecord.status)){
				message = 'Status is required';
                document.getElementById('missingStatus').innerHTML = 'Status is required';
            }else if($A.util.isEmpty(projectRecord.projectManager)){
				message = 'Project manager is required';
                document.getElementById('missingProgManager').innerHTML = 'Program manager is required';
            }else if($A.util.isEmpty(projectRecord.parentProgram)){
				message = 'Parent program is required';
                document.getElementById('missingProgram').innerHTML = 'Parent program is required';
            }else if($A.util.isEmpty(projectRecord.ownerId)){
				message = 'Owner is required';
                document.getElementById('missingOwner').innerHTML = 'Owner is required';
            }else if(!validDate && !moment(projectRecord.plannedStartDate).isValid()){
                message = 'Error in Planned Start Date field';
                document.getElementById("plannedStart").innerHTML = 'Planned Start Date should be in correct format.';
            }else if(!validDate && !moment(projectRecord.actualStartDate).isValid()){
                message = 'Error in Actual Start Date field';
                document.getElementById("actualStart").innerHTML = 'Actual Start Date should be in correct format.';
            }else if(!validDate && !moment(projectRecord.plannedEndDate).isValid()){
                message = 'Error in Planned End Date field';
                document.getElementById("plannedEnd").innerHTML = 'Planned End Date should be in correct format.';
            }else if(!validDate && !moment(projectRecord.actualEndDate).isValid()){
                message = 'Error in Actual End Date field';
                document.getElementById("actualEnd").innerHTML = 'Actual End Date should be in correct format.';
            }else if(!validDate && !moment(projectRecord.goLive).isValid()){
                message = 'Error in Go-Live field';
                document.getElementById("goLive").innerHTML = 'Go-Live should be in correct format.';
            }
			helper.openSuccess(component, event, helper, message,'slds-theme_error','utility:error');
        }
    },
    closeModel:function(component, event, helper){
        component.destroy();
    }
})