({
    closeApprovalModal: function(component, event) {
        var event = $A.get("e.c:CloseModal");
        event.setParams({
            "navigate" : "true"
        });
        event.fire(); 
    },
    
    createESignHistory: function(cmp, event){
        var action = cmp.get("c.createSignatureHistory");
        action.setParams({"data":JSON.stringify(cmp.get("v.finalDataMap"))});
        action.setCallback(this, function(response) {
            if(response.getReturnValue() == 'SUCCESS'){
                this.closeApprovalModal(cmp, event);
            }else if(response.getReturnValue() == 'next approver required'){
                cmp.set("v.isNextApproverRequired",true);
            }else{
                cmp.set("v.errorMessage",response.getReturnValue());
                var errorDiv = cmp.find('error');
                $A.util.addClass(errorDiv,'slds-show');
            }
        });
        $A.enqueueAction(action);
    }
    
})