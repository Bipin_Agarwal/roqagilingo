({
    doInit:function(component,event, helper) {
        var action = component.get('c.getActiveUsers');
        action.setCallback(this, function(actionResult) {
            component.set('v.activeUsers', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    
    onSelectChange: function(component, event, helper) {
        var selectedUser = component.find("chooseuser").get("v.value");
        console.log('-------------------'+selectedUser);
        component.set("v.nextApprover",selectedUser);
    },
    
    finalSubmit: function(component, event, helper) { 
        var myMap = component.get("v.finalDataMap");
        myMap['recordId'] = component.get("v.recordId");
        myMap['comments'] = component.get("v.comments");
        myMap['status'] = component.get("v.type");
        myMap['nextapprover'] = component.get("v.nextApprover");
        component.set("v.finalDataMap", myMap);
        helper.createESignHistory(component, event);  	
    },
    
    closeModel: function(component, event, helper) {
        helper.closeApprovalModal(component, event);    
    },
    
})