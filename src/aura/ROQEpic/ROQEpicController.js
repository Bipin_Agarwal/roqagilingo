({
    doInit:function(component, event, helper) {
        var epicRecord = component.get("v.epicRecord");
        var epicInfo = component.get("c.getEpicInfo");
        epicInfo.setParams({
            'programId': component.get("v.programId"),
            'epicType': component.get("v.epicType"),
            'roadmapId' : component.get("v.roadMapId")
        });
        epicInfo.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var returnedValue = response.getReturnValue();
                component.set("v.optionList", returnedValue);
                helper.fieldsDetail(component, event, helper,epicRecord)
                helper.epicTypeProcess(component, event, helper);
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
            component.set("v.callController", false);
        });
        component.set("v.callController", true);
        $A.enqueueAction(epicInfo);
    },
    checkBoxValue:function(component, event, helper){
        var boxValue = component.find('boxCheck').get("v.value");
        if(component.get("v.epicType") === 'Roadmap'){
            (boxValue) ? component.set("v.initiativeEpics",component.get("v.optionList.initiativeEpics.data")) :
            component.set("v.initiativeEpics",component.get("v.optionList.parentEpics.data"));
        }else if(component.get("v.epicType") === 'Feature'){
            (boxValue) ? component.set("v.roadmapEpics",component.get("v.optionList.roadmapEpics.data")) :
            component.set("v.roadmapEpics",component.get("v.optionList.parentEpics.data"));
        }
    },
    setSize:function(component, event, helper){
        component.set("v.epicRecord.Size",component.find('InputDynamicSize').get("v.value"));
    },
    setStatus:function(component, event, helper){
        component.set("v.epicRecord.Status",component.find('InputDynamicStatus').get("v.value"));
    },
    setPriority:function(component, event, helper){
        component.set("v.epicRecord.Priority",component.find('InputDynamicPriority').get("v.value"));
    },
    selectedAssignedToUser:function(component, event, helper){
        component.set("v.epicRecord.assignedTo",component.get("v.assignedToUser"));
    },
    setOwner:function(component,event,helper){
        var epicRecord = component.get("v.epicRecord");
        var ownerDetails = component.find("InputDynamicOwner").get("v.value");
        if(ownerDetails.indexOf('-') > -1){
            epicRecord.epicOwnerId = ownerDetails.split('-')[0];
            epicRecord.epicOwnerName = ownerDetails.split('-')[1];
        }else{
            epicRecord.epicOwnerId = ownerDetails;
            epicRecord.epicOwnerName = ownerDetails;
        }
        component.set("v.epicRecord", epicRecord);
    },
   /* setCategory:function(component,event,helper){
        var epicRecord = component.get("v.epicRecord");
        var categoryDetails = component.find("InputDynamicCategory").get("v.value");
        if(categoryDetails.indexOf('-') > -1){
            epicRecord.CategoryTitle = categoryDetails.split('-')[1];
            epicRecord.Category = categoryDetails.split('-')[0];
        }else{
            epicRecord.CategoryTitle = categoryDetails;
            epicRecord.Category = categoryDetails;
        }
        component.set("v.epicRecord", epicRecord);
    },*/
	selectedCategory:function(component, event, helper){
        if(!$A.util.isEmpty(component.get("v.categoryRecord.Name"))){
            component.set("v.epicRecord.Category",component.get("v.categoryRecord.Id"));
            component.set("v.epicRecord.CategoryTitle",component.get("v.categoryRecord.ROQA__Title__c"));
            component.set("v.epicRecord.CategoryAutonumber",component.get("v.categoryRecord.Name"));
        }else{
            component.set("v.epicRecord.Category",'');
            component.set("v.epicRecord.CategoryTitle",'');
            component.set("v.epicRecord.CategoryAutonumber",'');
        }
    },
    selectedTag:function(component,event,helper){
        var selectedId = component.find('selectedTag').get("v.value");
        var selectedIndex = helper.getSelectedTag(component, event, helper, selectedId);
        var selectedTag = component.get("v.optionList.epicTags.data")[selectedIndex];
        if(!$A.util.isEmpty(selectedTag)){
            var epicRecord = component.get("v.epicRecord");
            if(!epicRecord.Tag){
                epicRecord.Tag = [];
            }
            var existingEpic = helper.checkTag(component, event, helper,selectedTag, epicRecord);
            if(existingEpic == 0){
                epicRecord.Tag.push(selectedTag);
                component.set("v.epicRecord",epicRecord);
            }
        }
        component.find('selectedTag').set("v.value",'');
    },
    removeTag:function(component, event, helper){
        var index = event.currentTarget.getAttribute('data-index');
        var epicRecord = component.get("v.epicRecord");
        var tags = epicRecord.Tag;
        if(!epicRecord.epicTagDetails){
            epicRecord.epicTagDetails = [];
        }
        var tagRemoved = tags[index];
        tags.splice(index,1);
        epicRecord.epicTagDetails.push(tagRemoved);
        component.set("v.epicRecord.epicTagDetails",epicRecord.epicTagDetails);
        component.set("v.epicRecord.Tag",tags);
    },
    setInitiative:function(component, event, helper){
        component.set("v.epicRecord.ParentEpicId",component.find("InputDynamicInitiative").get("v.value"));
    },
    setEpic:function(component, event, helper){
        component.set("v.epicRecord.ParentEpicId",component.find("InputDynamicEpic").get("v.value"));
    },
    
    saveEpic:function(component, event, helper) {
        component.set("v.disableButton",true);
        (component.get("v.header") === 'New Epic') ? component.set("v.epicRecord.Lifecycle",'Backlog') : '';
        component.set("v.epicRecord.EpicType",component.get("v.epicType"));
        var record = component.get("v.epicRecord");
        var epicList = component.get("v.epicList");
        var epicData = component.get("v.epicData");
        var themeStartDate = component.get("v.themeStartDate");
        var themeEndDate = component.get("v.themeEndDate");
        component.set("v.newEpicRecord",record);
        var validEpic = true, validDate = true, validActivity=true;
        
		validActivity = helper.checkActivity(component, event, helper, record);
        helper.clearHTML(component, event, helper);
        validDate = helper.validStartEnd(component, event, helper, record, themeStartDate, themeEndDate);
        validEpic = ((component.get("v.epicType") === 'Initiative') ? (validDate ? helper.validDate(component, event, helper, record) : true) : true);
        (!validEpic) ? helper.openModal(component, event, helper,'Epic date range should be more than a month','Date validation') : '';
        
        if(!$A.util.isEmpty(record.Name !== undefined ? record.Name.trim() : record.Name)
           && validEpic && validDate && validActivity
           && !$A.util.isEmpty(record.Description !== undefined ? record.Description.trim() : record.Description) 
           && !$A.util.isUndefined(record.StartDate) && !$A.util.isEmpty(record.theme)
           && !$A.util.isUndefined(record.EndDate) && !$A.util.isEmpty(record.Size)
           && !$A.util.isEmpty(record.Status)&& !$A.util.isEmpty(record.epicOwnerId)){
            $A.enqueueAction(component.get("v.addEpic"));
            component.destroy();
        }
        else{
            var message = '';
            component.set("v.disableButton",false);
            if($A.util.isUndefined(record.Name) || $A.util.isEmpty(record.Name.trim())){
                message = 'Error in title field';
                document.getElementById("missingName").innerHTML = 'Title is required.';
            }else if($A.util.isUndefined(record.Description) || $A.util.isEmpty(record.Description.trim())){
                message = 'Error in description field';
                document.getElementById("missingDescription").innerHTML = 'Description is required.';
            }else if($A.util.isEmpty(record.theme) || record.theme == '---Select---'){
                message = 'Error in theme field';
                document.getElementById("missingTheme").innerHTML = 'Theme is required.';
            }else if(!moment(record.StartDate,'YYYY-M-DD').isValid()){
                message = 'Error in start date field';
                document.getElementById("startDate").innerHTML = 'Start date is required and should be in correct format.';
            }else if(!moment(record.EndDate,'YYYY-M-DD').isValid()){
                message = 'Error in end date field';
                document.getElementById("endDate").innerHTML = 'End date is required and should be in correct format.';
            }else if($A.util.isUndefined(record.StartDate)){
                message = 'Error in start date field';
                document.getElementById("startDate").innerHTML = 'Start date is required.';
            }else if($A.util.isUndefined(record.EndDate)){
                message = 'Error in end date field';
                document.getElementById("endDate").innerHTML = 'End date is required.';
            }else if($A.util.isEmpty(record.Size)){
                message = 'Error in size field';
                document.getElementById("missingSize").innerHTML = 'Size is required.';
            }else if($A.util.isEmpty(record.Status)){
                message = 'Error in status field';
                document.getElementById("missingStatus").innerHTML = 'Status is required.';
            }else if($A.util.isEmpty(record.epicOwnerId)){
                message = 'Error in owner field';
                document.getElementById("missingOwner").innerHTML = 'Owner is required.';
            }else if($A.util.isEmpty(record.StartDate)){
                message = 'Start date cannot be blank';
                document.getElementById("startDate").innerHTML = 'Start date cannot be blank';
            }else if($A.util.isEmpty(record.EndDate)){
                message = 'End date cannot be blank';
                document.getElementById("endDate").innerHTML = 'End date cannot be blank';
            }else if(moment(record.StartDate, "YYYY-M-DD").isBefore(moment(themeStartDate, "YYYY-M-DD"))){
                message = 'Error in start date field';
                document.getElementById("startDate").innerHTML = 'Epic start date cannot be prior to '+component.get("v.epicType")+' start date : '+moment(themeStartDate).format('MMM-DD-YYYY');
            }else if(moment(record.StartDate, "YYYY-M-DD").isSameOrAfter(moment(themeEndDate, "YYYY-M-DD"))){
                message = 'Error in start date field';
                document.getElementById("startDate").innerHTML = 'Epic start date cannot be after the '+component.get("v.epicType")+' end date : '+moment(themeEndDate).format('MMM-DD-YYYY');
            }else if(moment(record.EndDate, "YYYY-M-DD").isSameOrBefore(moment(themeStartDate, "YYYY-M-DD"))){
                message = 'Error in end date field';
                document.getElementById("endDate").innerHTML = 'Epic end date cannot be prior to '+component.get("v.epicType")+' start date : '+moment(themeStartDate).format('MMM-DD-YYYY');
            }else if(moment(record.EndDate, "YYYY-M-DD").isAfter(moment(themeEndDate, "YYYY-M-DD"))){
                message = 'Error in end date field';
                document.getElementById("endDate").innerHTML = 'Epic end date cannot be after the '+component.get("v.epicType")+' end date : '+moment(themeEndDate).format('MMM-DD-YYYY');
            }else if(moment(record.StartDate, "YYYY-M-DD").isSameOrAfter(moment(record.EndDate, "YYYY-M-DD"))){
                message = 'Error in start date field';
                document.getElementById("startDate").innerHTML = 'Epic start date cannot be after the epic end date : '+moment(record.EndDate).format('MMM-DD-YYYY');
            }else if(moment(record.EndDate, "YYYY-M-DD").isSameOrBefore(moment(record.StartDate, "YYYY-M-DD"))){
                message = 'Error in end date field';
                document.getElementById("endDate").innerHTML = "Epic end date cannot be prior to the epic start date : "+moment(record.StartDate).format('MMM-DD-YYYY');
            }else if(!validActivity && !moment(record.dateGroomed).isValid()){
                message = 'Error in Date Groomed field';
                document.getElementById("groomed").innerHTML = 'Date Groomed should be in correct format.';
            }else if(!validActivity && !moment(record.dateApproved).isValid()){
                message = 'Error in Date Approved field';
                document.getElementById("approved").innerHTML = 'Date Approved should be in correct format.';
            }else if(!validActivity && !moment(record.dateCommitted).isValid() && !validActivity){
                message = 'Error in Date Committed field';
                document.getElementById("committed").innerHTML = 'Date Committed should be in correct format.';
            }else if(!validActivity && !moment(record.dateCompleted).isValid() && !validActivity){
                message = 'Error in Date Completed field';
                document.getElementById("completed").innerHTML = 'Date Completed should be in correct format.';
            }
            helper.errorMsg(component, event, helper, message, 'slds-theme_error','utility:error');
        }
    },
    availableEpics : function(component, event, helper){
        $A.enqueueAction(component.get("v.availEpic"));
        component.destroy();
    },
    closeModel : function(component, event, helper) {
        component.destroy();
    }
})