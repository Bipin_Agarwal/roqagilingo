({
    clearHTML : function(component, event, helper) {
        document.getElementById("missingName").innerHTML = '';
        document.getElementById("missingDescription").innerHTML = '';
        document.getElementById("missingTheme").innerHTML = '';
        document.getElementById("startDate").innerHTML = '';
        document.getElementById("endDate").innerHTML='';
        document.getElementById("missingSize").innerHTML = '';
        document.getElementById("missingStatus").innerHTML = '';
        document.getElementById("missingOwner").innerHTML = '';
    },
    validStartEnd:function(component, event, helper, record, themeStartDate , themeEndDate){
        if(!$A.util.isEmpty(record.StartDate) && !$A.util.isEmpty(record.EndDate)
           && (moment(record.StartDate, "YYYY-M-DD").isSameOrAfter(moment(themeStartDate, "YYYY-M-DD")))
           && (moment(record.StartDate, "YYYY-M-DD").isBefore(moment(themeEndDate, "YYYY-M-DD")))
           && (moment(record.EndDate, "YYYY-M-DD").isAfter(moment(themeStartDate, "YYYY-M-DD")))
           && (moment(record.EndDate, "YYYY-M-DD").isSameOrBefore(moment(themeEndDate, "YYYY-M-DD")))
           && (moment(record.StartDate, "YYYY-M-DD").isBefore(moment(record.EndDate, "YYYY-M-DD")))
           && (moment(record.EndDate, "YYYY-M-DD").isAfter(moment(record.StartDate, "YYYY-M-DD")))){
            return true;
        }
        return false;
    },
    validDate:function(component, event, helper, record){
        var daysDifference = new moment(record.EndDate).diff(new moment(record.StartDate),'days');
        var returnValue = false;
        if(daysDifference >= 29){
            return true;
        }
        else if(moment(record.StartDate).month() == 1){
            var leapYear = ((moment(record.StartDate).year() % 4 === 0 && moment(record.StartDate).year() % 100 != 0) || moment(record.StartDate).year() % 400 === 0) ? true : false;
            (leapYear) ? (daysDifference == 28 ? returnValue = true : '') : (daysDifference == 27 ? returnValue = true : '');
            return returnValue;
        }
    },
    getSelectedTag:function(component, event, helper, selectedId){
        var tagData = component.get("v.optionList.epicTags.data");
        for(var index=0;index<tagData.length;index++){
            if(selectedId == tagData[index].Id){
                return index;
            }
        }
        return -1;
    },
    checkTag:function(component, event, helper, selectedTag, epicRecord){
        var tags = epicRecord.Tag;
        for(var index=0;index<tags.length;index++){
            if(tags[index].Id == selectedTag.Id){
                return 1;
            }
        }
        return 0;
    },
    checkActivity:function(component, event, helper, record){
        var returnedValue = true;
        if(!$A.util.isEmpty(record.dateGroomed) && !moment(record.dateGroomed).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.dateApproved) &&!moment(record.dateApproved).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.dateCommitted) &&!moment(record.dateCommitted).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.dateCompleted) &&!moment(record.dateCompleted).isValid()){
            returnedValue = false;
       }
        return returnedValue;
    },
    fieldsDetail:function(component, event, helper, epicRecord){
        var optionList = component.get("v.optionList");
        if((component.get("v.selectedAvailEpic") || component.get("v.header") != 'New Epic') && !$A.util.isEmpty(epicRecord.epicOwnerId)){
            component.find('InputDynamicOwner').set("v.value",epicRecord.epicOwnerId+'-'+epicRecord.epicOwnerName);
        }else{
            component.set("v.epicRecord.epicOwnerName",optionList.loggedInUser.Name);
            component.set("v.epicRecord.epicOwnerId",optionList.loggedInUser.Id);
            component.find("InputDynamicOwner").set("v.value",component.get("v.epicRecord.epicOwnerId")+'-'+component.get("v.epicRecord.epicOwnerName"));
        }
        if(!$A.util.isEmpty(epicRecord.Category)){
            component.set("v.categoryRecord.Id",epicRecord.Category);
            component.set("v.categoryRecord.Name",epicRecord.CategoryAutonumber);
            component.set("v.categoryRecord.Title__c",epicRecord.CategoryTitle);
        }
        (!$A.util.isEmpty(epicRecord.Status) ? '' : component.set("v.epicRecord.Status",'On Track'));
        (!$A.util.isEmpty(epicRecord.assignedTo)) ? component.set("v.assignedToUser",epicRecord.assignedTo) : '';
        component.find('InputDynamicSize').set("v.value",epicRecord.Size);
        component.find('InputDynamicPriority').set("v.value",epicRecord.Priority);
        component.find('InputDynamicStatus').set("v.value",component.get("v.epicRecord.Status"));
    },
    epicTypeProcess:function(component, event, helper){
        var epicType = component.get("v.epicType");
        if(epicType === 'Feature'){
            var epicRecord = component.get("v.epicRecord");
            epicRecord.ParentEpicId = ($A.util.isEmpty(epicRecord.ParentEpicId) ? '' : epicRecord.ParentEpicId);
            component.set("v.roadmapEpics",component.get("v.optionList.parentEpics.data"));
            component.find("InputDynamicTheme").set("v.options",component.get("v.thimList"));
            component.find("InputDynamicEpic").set("v.value",epicRecord.ParentEpicId);
            component.set("v.epicRecord",epicRecord);
        }else if(epicType === 'Roadmap'){
            var epicRecord = component.get("v.epicRecord");
            epicRecord.ParentEpicId = ($A.util.isEmpty(epicRecord.ParentEpicId) ? '' : epicRecord.ParentEpicId);
            component.set("v.initiativeEpics",component.get("v.optionList.parentEpics.data"));
            component.find("InputDynamicInitiative").set("v.value",epicRecord.ParentEpicId);
            component.find("InputDynamicTheme").set("v.options",component.get("v.thimList"));
            component.set("v.epicRecord",epicRecord);
        }else{
            component.find("InputDynamicTheme").set("v.options",component.get("v.thimList"));
        }
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.openModal(component, event, helper,message,'Response from server.');
        component.set("v.disableButton",true);
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception'
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    errorMsg:function(component, event, helper, message, style, iconName){
        $A.createComponent(
            "ROQA:ROQSuccessComponent",
            {
                saveStatus : message,
                style:style,
                iconName:iconName
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    }
})