({
    getInitiative:function(component, event, helper) {
        var initiative = component.get("c.getRecordDetailsById");
        initiative.setParams({
            'recordId' : component.get("v.recordId")
        });
        
        initiative.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var returnedValue = response.getReturnValue();
                if(returnedValue.data.sobjRecord != undefined){
                    var startDateString = moment(returnedValue.data.sobjRecord.ROQA__Start_Date__c,'YYYY-MM-DD').format('YYYY-MM-DD');
                    var endDateString = moment(returnedValue.data.sobjRecord.ROQA__End_Date__c, 'YYYY-MM-DD').format('YYYY-MM-DD');
                    if((moment(endDateString,'YYYY-MM-DD').isAfter(moment(startDateString,'YYYY-MM-DD')))){
                        startDateString = moment(startDateString,'YYYY-MM-DD').startOf('month').format('YYYY-MM-DD');
                        endDateString = moment(endDateString,'YYYY-MM-DD').endOf('month').format('YYYY-MM-DD');
                        component.set("v.themeStartDate", startDateString);
                        component.set("v.themeEndDate", endDateString);
                        var result = helper.calenderStructure(component, helper, startDateString, endDateString);
                        component.set("v.calenderList",result);
                        helper.calenderDOMStructure(component,event,helper,result);
                        helper.getInitiativeItem(component, event, helper, 'search');
                    }
                    else{
                        var message = "Start Date and End Date should be given in Proper Order";
                        var header = "Initiative Start Date and End Date Issue";
                        component.set("v.callController",false);
                        component.set("v.disableButton",true);
                        helper.openModal(component, event, helper, message, header);
                    }
                }
            }else{
                component.set("v.callController",false);
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        component.set("v.callController", true);
        $A.enqueueAction(initiative);
    },
    calenderStructure : function(component, helper, startDate, EndDate) {
        var startDate = moment(startDate, "YYYY");
        var endDate = moment(EndDate, "YYYY");
        var allYearsInPeriod = [];
        while(startDate.isSameOrBefore(endDate)) {
            allYearsInPeriod.push(startDate.format("YYYY"));
            startDate = startDate.add(1, "year");
        };
        return allYearsInPeriod.map(function(obj){
            return{
                label:obj,
                days: 12,
                dateList: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                width:15
            }
        });
    },
    calenderDOMStructure:function(component,event,helper, result){
        $A.createComponent(
            "ROQA:ROQCalenderStructure",
            {
                dateList: result,
                pixelValue: 15,
                className:'monthGridSection'
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var calender = component.get("v.calender");
                    calender.push(newButton);
                    component.set("v.calender", newButton);
                }
            }
        ); 
    },
    createStructure:function(component,event,helper,list, order){
        var themeHeight = 0;
        component.set("v.structure",[]);
        for(var index=order;index<list.length;index++){
            themeHeight += (list[index].epics.length * 50 >= 200 ? ((list[index].epics.length * 50) + 50) : 200);
            $A.createComponent(
                "ROQA:RoqEpicBar",
                {
                    index:index,
                    pixelValue:15,
                    format:'MMM-YYYY',
                    type:'months',
                    epicType:'Initiative',
                    rmId:component.get("v.recordId"),
                    themeStartDate:component.get("v.themeStartDate"),
                    themeEndDate:component.get("v.themeEndDate"),
                    creatable:component.get("v.isInitiativeCreatable"),
                    height:component.getReference("v.height"),
                    calender:component.getReference("v.calender"),
                    selectedTheme:component.getReference("v.selectedTheme"),
                    themeRecord:component.getReference("v.themeRecord"),
                    epicList:component.getReference("v.epicList"),
                    actionPerformed:component.getReference("v.actionPerformed"),
                    addTheme:component.getReference("c.addTheme"),
                    openThemeModel:component.getReference("c.openThemeModel"),
                    themeReorder:component.getReference("c.themeReorder"),
                    redesignMap:component.getReference("c.redesignMap")
                },
                function(newButton, status, errorMessage){
                    if (status === "SUCCESS") {
                        var structure = component.get("v.structure");
                        structure.push(newButton);
                        component.set("v.structure", structure);
                        component.set("v.selectedTheme",'');
                        component.set("v.disableButton", false);
                    }
                }
            ); 
        }
        component.set("v.height",themeHeight);
        component.set("v.callController",false);
    },
    createTheme:function(component,event,helper,themeIndex){
        var epicList = component.get("v.epicList")[themeIndex];
        component.set("v.height",component.get("v.height")+200);
        $A.createComponent(
            "ROQA:RoqEpicBar",
            {
                index:themeIndex,
                pixelValue:15,
                format:'MMM-YYYY',
                type:'months',
                epicType:'Initiative',
                rmId:component.get("v.recordId"),
                themeStartDate:component.get("v.themeStartDate"),
                themeEndDate:component.get("v.themeEndDate"),
                creatable:component.get("v.isInitiativeCreatable"),
                height:component.getReference("v.height"),
                calender:component.getReference("v.calender"),
                selectedTheme:component.getReference("v.selectedTheme"),
                themeRecord:component.getReference("v.themeRecord"),
                epicList:component.getReference("v.epicList"),
                actionPerformed:component.getReference("v.actionPerformed"),
                addTheme:component.getReference("c.addTheme"),
                openThemeModel:component.getReference("c.openThemeModel"),
                themeReorder:component.getReference("c.themeReorder"),
                redesignMap:component.getReference("c.redesignMap")
            },
            function(newButton, status, errorMessage){
                if(status === "SUCCESS"){
                    var structure = component.get("v.structure");
                    structure.splice(themeIndex, 0, newButton);
                    component.set("v.structure",structure);
                    component.set("v.selectedTheme",'');
                }
                
            }
        );
    },
    changeThemeOrder:function(component, event, helper){
        var epicList = component.get("v.epicList");
        for(var index=0;index<epicList.length;index++){
            epicList[index].ThemeOrder = index+1;
        }
        component.set("v.epicList",epicList);
    },
    updateId:function(component, event, helper, themeIndex){
        var themeBar = document.getElementsByClassName('theme_panel');
        var themeBarLength = themeBar.length;
        for(var index=0;index<themeBarLength;index++){
            if(index >= themeIndex){
                themeBar[index].setAttribute('id',parseInt(themeBar[index].getAttribute('id')) + 1);
            }
        }
    },
    openSuccess:function(component, event, helper, message, style,iconName){
        $A.createComponent(
            "ROQA:ROQSuccessComponent",
            {
                saveStatus : message,
                style:style,
                iconName:iconName
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception'
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    getInitiativeItem:function(component, event, helper, type){
        var getInitiative = component.get("c.getItemByInitiativeId");
        getInitiative.setParams({
            'initiativeId':component.get("v.recordId"),
            'Offset':component.get("v.paginationIndex")
        });
        getInitiative.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var returnedValue = response.getReturnValue();
                component.set("v.isInitiativeCreatable",returnedValue.data.isInitiativeCreatable);
                if(returnedValue.data !== undefined && returnedValue.data.themes.length > 0){
                    var order = 0;
                    if(type === 'search'){
                        component.set("v.epicList",returnedValue.data.themes);
                    }else{
                        var epicList = component.get("v.epicList");
                        order = epicList.length;
                        epicList = epicList.concat(returnedValue.data.themes);
                        component.set("v.epicList",epicList);
                    }
                    helper.createStructure(component,event,helper, component.get("v.epicList"), order);
                }else{
                    if(returnedValue.isSuccess){
                        component.set("v.disableButton",false);
                        if(type === 'search'){
                            helper.openSuccess(component, event, helper, 'No Themes Found for Initiative. Add New Themes.','slds-theme_info','utility:info');
                        }
                    }
                    else{
                        component.set("v.disableButton",true);
                        helper.openModal(component, event, helper, returnedValue.errorList[0].errorMessage, 'Response From Server');
                    }
                }
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
            component.set("v.callController", false);
        });
        component.set("v.callController", true);
        $A.enqueueAction(getInitiative);
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        component.set("v.disableButton",false);
        helper.openModal(component, event, helper,message,'Response From Server.');
    }
})