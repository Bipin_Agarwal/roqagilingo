({
    doInit : function(component, event, helper){
        helper.epicBankInfo(component, event, helper,'search');
        jQuery(document).trigger("stickyTable");
        var roqEpicBank = document.getElementsByClassName('sticky-table');
		var epicBank = document.getElementsByClassName('epicBank');
		$(roqEpicBank).width($(epicBank).width());
		$(roqEpicBank).height($(epicBank).height() - 100);
        $(roqEpicBank).css('max-height',$(epicBank).height() - 100);
        //$('.sticky-table').css('max-height',$('.epicBank').height() - 100);
		var lastScrollTop = 0;
		$(roqEpicBank).scroll(function(){
		var documentScrollTop = $(this).scrollTop();
		if (lastScrollTop != documentScrollTop) {
			lastScrollTop = documentScrollTop;
			if(parseInt(component.get("v.totalRecords")) > 0 && ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)) {
				var index = component.get("v.paginationIndex"); 
				var count = index+50;
				component.set("v.paginationIndex", count);
				helper.epicBankInfo(component, event, helper,'scroll');
			}
		}
		});
    },
    openEpicModel:function(component, event, helper){
        helper.epicModel(component, event, helper);
    },
    selectedRecord:function(component, event, helper){
        var epicList = component.get("v.epicList");
        var buttonIndex = component.get("v.buttonIndex");
        if(buttonIndex.button === 'delete'){
            helper.deleteRow(component, event, helper, epicList, buttonIndex.index);
        }else if(buttonIndex.button === 'preview'){
            helper.epicInfo(component, event, helper, epicList, buttonIndex.index);
        }else if(buttonIndex.button === 'edit'){
            component.set("v.epicRecord",Object.assign({},epicList[buttonIndex.index]));
            helper.editRecord(component, event, helper, epicList[buttonIndex.index].Name, epicList[buttonIndex.index].EpicType);
        }else if(buttonIndex.button === 'chat'){
            component.set("v.callController",true);
            helper.chatterFeed(component, event, helper, epicList, buttonIndex.index);
        }else if(buttonIndex.button === 'copy'){
            var epicRecord = Object.assign({}, epicList[buttonIndex.index]);
            delete epicRecord.epicId;
            delete epicRecord.AutoNumber;
            epicRecord.Tag = [];
            epicRecord.epicTagDetails = [];
            component.set("v.epicRecord",epicRecord);
            helper.copyRecord(component, event, helper, epicList[buttonIndex.index].EpicType);
        }
    },
    addToGrid:function(component, event, helper){
        var epicRecord = component.get("v.epicRecord");
        var epicList = component.get("v.epicList");
        epicList.unshift(epicRecord);
        component.set("v.epicList",epicList);
        helper.saveEpic(component, event, helper, 'close');
    },
    addAndOpen:function(component, event, helper){
        var epicRecord = component.get("v.epicRecord");
        var epicList = component.get("v.epicList");
        epicList.unshift(epicRecord);
        component.set("v.epicList",epicList);
        helper.saveEpic(component, event, helper, 'open');
    },
    updateToGrid:function(component, event, helper){
        var epicRecord = component.get("v.epicRecord");
        var index = component.get("v.buttonIndex.index");
        var epicList = component.get("v.epicList");;
        epicList[index] = epicRecord;
        component.set("v.epicList",epicList);
        helper.saveEpic(component, event, helper,'close');
    },
    openModel:function(component, event, helper){
        $A.createComponent(
            "ROQA:ManageColumns",
            {
                selectedValues : component.getReference("v.selectedValues"),
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.epicModal");
                    body.push(newButton);
                    component.set("v.epicModal",body);
                }
            }
        );
    },
    searchRecords:function(component, event, helper){
        if(event.getParams().keyCode == 13 || $A.util.isEmpty(component.get("v.searchName"))){
            var getRecords = component.get("c.getEpicsGridView");
            getRecords.setParams({
                offsetValue: 0,
                limitValue: 50,
                searchText:component.get("v.searchName"),
                epicCount: component.get("v.totalRecords")
            });
            getRecords.setCallback(this,function(response){
                var state = response.getState();
                if(state === 'SUCCESS'){
                    if(response.getReturnValue().data.getEpicsGridViewResult.length > 0){
                        component.set("v.epicList",response.getReturnValue().data.getEpicsGridViewResult);
                        component.set("v.body",[]);
                        helper.appendData(component, event, helper, 0);
                    }else{
						component.set("v.callController",false);
                        helper.openModal(component, event, helper, 'No Records Found', 'Search Result');
                    }
                }else{
                    helper.handleServerException(component, event, helper, response.getError());
                }
            });
            component.set("v.callController",true);
            $A.enqueueAction(getRecords);
        }
    },
    deleteRowItem:function(component, event, helper){
        var epicList = component.get("v.epicList");
        var index = component.get("v.buttonIndex.index");
        var deleteEpic = component.get("c.deleteEpics");
        deleteEpic.setParams({
            'type': epicList[index].EpicType,
            'recordId': epicList[index].epicId
        });
        deleteEpic.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                if(response.getReturnValue().isEpicDeletable){
                    epicList.splice(index,1);
                    component.set("v.epicList",epicList);
                    component.set("v.body",[]);
                    helper.appendData(component, event, helper, 0);
                    helper.openSuccess(component, event, helper, response.getReturnValue().msgDisplay, 'slds-theme_success', 'utility:success');
                }else{
                    helper.openModal(component, event, helper, response.getReturnValue().msgDisplay, 'Application Exception');
                }
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(deleteEpic);
    },
    sortColumns:function(component, event, helper){
        var order;
        component.set("v.selectedHeader",event.currentTarget.getAttribute('data-header'));
        (component.get("v.arrowDirection") === 'arrowUp') ? order = 'DESC' : order = 'ASC' ;
        helper.sortColumn(component, event, helper, component.get("v.selectedHeader"), order);
    }
})