({
    epicBankInfo : function(component, event, helper, type) {
        var epicBank = component.get("c.getEpicsGridView");
        epicBank.setParams({
            offsetValue:  component.get("v.paginationIndex"),
            limitValue: 50,
            searchText: component.get("v.searchName"),
            epicCount: component.get("v.totalRecords")
        });
        epicBank.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                var returnedValue = response.getReturnValue();
                var order = 0;
                component.set("v.totalRecords",returnedValue.data.totalEpicCount);
                component.set("v.isEpicCreatable",returnedValue.data.getObjectPermissions.isEpicCreatable);
                if(response.getReturnValue().data.epicbankColumnList.length > 0){
                    response.getReturnValue().data.epicbankColumnList.unshift('Save as Default');
                    component.set("v.selectedValues",response.getReturnValue().data.epicbankColumnList);
                }
                if(returnedValue.data.getEpicsGridViewResult.length === 0){
                    component.set("v.callController",false);
                }
               if(type === 'search'){
                    component.set("v.epicList",returnedValue.data.getEpicsGridViewResult);
                    helper.appendData(component, event, helper, order);
                }if(type === 'scroll'){
                    var epicList = component.get("v.epicList");
                    order = epicList.length;
                    epicList = epicList.concat(returnedValue.data.getEpicsGridViewResult);
                    component.set("v.epicList",epicList);
                    helper.appendData(component, event, helper, order);
                }
            }else{
                component.set("v.callController",false);
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        component.set("v.callController", true);
        $A.enqueueAction(epicBank);
    },
    appendData:function(component, event, helper, order){
        var epicList = component.get("v.epicList");
        for(var index = order;index < epicList.length;index++){
            $A.createComponent(
                "ROQA:AgilingoEpicsGridView",
                {
                    singleEpic : epicList[index],
                    index : index,
                    buttonIndex : component.getReference("v.buttonIndex"),
                    selectedRecord : component.getReference("c.selectedRecord"),
                    selectedValues : component.getReference("v.selectedValues"),
                },
                function(newButton, status, errorMessage){
                    if(status === "SUCCESS"){
                        var body = component.get("v.body");
                        body.push(newButton);
                        component.set("v.body",body);
                        component.set("v.callController",false);
                    }
                }
            );
        }
    },
    epicModel:function(component, event, helper){
        component.set("v.epicRecord",{});
        $A.createComponent(
            "ROQA:ROQEpicCreate",
            {
                epicRecord: component.getReference("v.epicRecord"),
                addToGrid: component.getReference("c.addToGrid"),
                addAndOpen: component.getReference("c.addAndOpen")
            },
            function(newButton,status,errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.epicModal");
                    body.push(newButton);
                    component.set("v.epicModal",body);
                }
            }
        );
    },
    editRecord:function(component, event, helper, name, epicType){
        $A.createComponent(
            "ROQA:ROQEpicCreate",
            {
                header : 'Edit: '+name,
                showNew : false,
                epicType : epicType,
                epicRecord : component.getReference("v.epicRecord"),
                addToGrid : component.getReference("c.updateToGrid")
            },
            function(newButton,status,errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.epicModal");
                    body.push(newButton);
                    component.set("v.epicModal",body);
                }
            }
        );
    },
    deleteRow:function(component, event, helper, epicList, index){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : 'Are you sure you want to Delete '+epicList[index].AutoNumber+'?',
                header : 'Delete Epic',
                type:'Confirm',
                callBack:component.getReference("c.deleteRowItem")
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body",body);
                }
            }
        );
    },
    copyRecord:function(component, event, helper, epicType){
        $A.createComponent(
            "ROQA:ROQEpicCreate",
            {
                epicType : epicType,
                clone : true,
                epicRecord : component.getReference("v.epicRecord"),
                addToGrid : component.getReference("c.addToGrid"),
                addAndOpen : component.getReference("c.addAndOpen")
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.epicModal");
                    body.push(newButton);
                    component.set("v.epicModal",body);
                }
            }
            
        );
    },
    epicInfo:function(component, event, helper, epicList, index){
        $A.createComponent(
            "ROQA:epicTableInfo",
            {
                header:'Record History of '+epicList[index].Name+'.',
                recordId:epicList[index].epicId
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.epicModal");
                    body.push(newButton);
                    component.set("v.epicModal",body);
                }
            }
        );
    },
    chatterFeed:function(component, event, helper, epicList, index){
        $A.createComponent(
            "ROQA:ROQChatter",
            {
                recordId : epicList[index].epicId,
                epic : epicList[index].AutoNumber
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.epicModal");
                    body.push(newButton);
                    component.set("v.epicModal",body);
                    component.set("v.callController",false);
                }
            }
        );
    },
    sortColumn:function(component, event, helper, sortField, order){
        var sortColumn = component.get("c.getSortedEpicRecords");
        sortColumn.setParams({
            'fieldName' : sortField, 
            'order' : order,
            'limitValue' : 50,
            'offSetValue' : 0
        });
        sortColumn.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.epicList",response.getReturnValue());
                component.set("v.body",[]);
                component.set("v.callController",true);
                helper.appendData(component, event, helper, 0);
                (component.get("v.arrowDirection") === 'arrowUp' ? component.set("v.arrowDirection",'arrowDown') : component.set("v.arrowDirection",'arrowUp'));
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(sortColumn);
    },
    saveEpic:function(component, event, helper, popup){
        var recordList = helper.saveRecords(component, event, helper);
        var epicBank = component.get("c.doSaveEpics");
        epicBank.setParams({
            'epicRecords' : JSON.stringify(recordList),
            'offsetValue': 0,
            'limitValue': 50,
            'searchText' : component.get("v.searchName"),
            'epicCount': component.get("v.totalRecords")
        });
        epicBank.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
				if(response.getReturnValue().isSuccess){
                component.set("v.epicList",response.getReturnValue().data.getEpicsGridViewResult.data.getEpicsGridViewResult);
                component.set("v.body",[]);
				component.set("v.epicModal",[]);
                helper.appendData(component, event, helper, 0);
                helper.openSuccess(component, event, helper, response.getReturnValue().Msg, 'slds-theme_success', 'utility:success');
                ((popup === 'open') ? helper.epicModel(component, event, helper) : '');
				}
            else
				{
				component.set("v.callController",false);
				helper.openSuccess(component,event, helper, response.getReturnValue().errorList[0].errorMessage, 'slds-theme_error','utility:error');
			}
			}
			else{
                component.set("v.callController",false);
                helper.handleServerException(component, event, helper, response.getError());
            }
			
        });
        component.set("v.callController",true);
        $A.enqueueAction(epicBank);
    },
    saveRecords:function(component, event, helper){
        var epicList = component.get("v.epicList");
        var recordList = [];
        for(var index=0;index<epicList.length;index++){
            if(epicList[index].recordStatus === 'New' || epicList[index].recordStatus === 'update'){
            recordList.push(epicList[index]);
            break;
        }
    }
        return recordList;
    },
    openSuccess:function(component, event, helper, message, style, iconName){
        $A.createComponent(
            "ROQA:ROQSuccessComponent",
            {
                saveStatus : message,
                style : style,
                iconName : iconName
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.epicModal");
                    body.push(newButton);
                    component.set("v.epicModal",body);
                }
            }
        );
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception',
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.openModal(component, event, helper,message,'Response From Server.');
    }
})