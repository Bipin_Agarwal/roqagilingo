({
    /*doInit : function(component, event, helper) {
        $A.createComponent("c:AgilingoApprovalHistoryCard", {
            "recordId" : component.get("v.recordId")
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
        });
    },*/
    doInit : function(component, event, helper) {
        $A.createComponent("c:ApprovalHistoryTableComponent", {
            "recordId" : component.get("v.recordId")
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
        });
    },
    reassign : function(component, event, helper) {
        if(event.getParam("navigate") == "true"){
            $A.createComponent("c:AgilingoReassignComponent", {
                "recordId" : component.get("v.recordId")
            }, function(newCmp) {
                if (component.isValid()) {
                    component.set("v.popupBody", newCmp);
                }
            });
        }
    },
    
    approveReject : function(component, event, helper) {
        var action = component.get("c.isSignatureForStep");
        action.setParams({"recId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getReturnValue() == true){
                if(event.getParam("navigate") == "true"){
                    $A.createComponent("c:AgilingoSignatureComponent", {
                        "recordId" : component.get("v.recordId"),
                        "type" : event.getParam("type")
                    }, function(newCmp) {
                        if (component.isValid()) {
                            component.set("v.popupBody", newCmp);
                        }
                    }); 
                }
            }else{
                $A.createComponent("c:AgilingoApprovalModalComponent", {
                    "recordId" : component.get("v.recordId"),
                    "type" : event.getParam("type")
                }, function(newCmp) {
                    if (component.isValid()) {
                        component.set("v.popupBody", newCmp);
                    }
                });
            }
            
        });
        $A.enqueueAction(action);
        
    },
    
    closePopupBody : function(component, event, helper) {
        component.set("v.popupBody","");
        $A.get('e.force:refreshView').fire();
    }
    
})