({
    doInit : function(component, event, helper) {
		helper.getProjectList(component, event, helper, 'search');
			jQuery(document).trigger("stickyTable");
            var projectBankClass = document.getElementsByClassName('sticky-table');
			var projectBank = document.getElementsByClassName('projectBank');
			$(projectBankClass).width($(projectBank).width());
			$(projectBankClass).height($(projectBank).height() - 100);
            $(projectBankClass).css('max-height',$(projectBank).height() - 100);
            //$('.sticky-table').css('max-height',$(projectBank).height() - 100);
			var lastScrollTop = 0;
			$(projectBankClass).scroll(function(){
				var documentScrollTop = $(this).scrollTop();
				if (lastScrollTop != documentScrollTop) {
					lastScrollTop = documentScrollTop;
					if(parseInt(component.get("v.projectCount")) > 0 && ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)) {
						var index = component.get("v.paginationIndex"); 
						var count = index+50;
						component.set("v.paginationIndex", count);
						helper.getProjectList(component, event, helper,'scroll');
					}
				}
			});
    },
    openModel:function(component, event, helper){
        $A.createComponent(
            "ROQA:ManageProjectColumns",
            {
                selectedValues : component.getReference("v.selectedValues"),
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.projectModal");
                    body.push(newButton);
                    component.set("v.projectModal",body);
                }
            }
        );
    },
    selectedRecord:function(component, event, helper){
        var projectList = component.get("v.projectList");
        var buttonIndex = component.get("v.buttonIndex");
        if(buttonIndex.button === 'delete'){
            helper.deleteRow(component, event, helper, projectList, buttonIndex.index);
        }else if(buttonIndex.button === 'preview'){
            helper.projectInfo(component, event, helper, projectList, buttonIndex.index);
        }else if(buttonIndex.button === 'edit'){
            component.set("v.projectRecord",Object.assign({},projectList[buttonIndex.index]));
            helper.editRecord(component, event, helper, projectList[buttonIndex.index].projectLabel);
        }else if(buttonIndex.button === 'chat'){
            component.set("v.callController",true);
            helper.chatterFeed(component, event, helper, projectList, buttonIndex.index);
        }else if(buttonIndex.button === 'copy'){
            var projectRecord = Object.assign({}, projectList[buttonIndex.index]);
            delete projectRecord.projectId;
            delete projectRecord.projectName;
            component.set("v.projectRecord",projectRecord);
            helper.copyRecord(component, event, helper);
        }
    },
    projectModel:function(component, event, helper){
        helper.openProjectModel(component, event, helper);
    },
    addToGrid:function(component, event, helper){
        var projectRecord = component.get("v.projectRecord");
        var projectList = component.get("v.projectList");
        projectList.unshift(projectRecord);
        component.set("v.projectList",projectList);
        helper.saveProjectRecord(component, event, helper,'close');
    },
    addAndOpen:function(component,event, helper){
        var projectRecord = component.get("v.projectRecord");
        var projectList = component.get("v.projectList");
        projectList.unshift(projectRecord);
        component.set("v.projectList",projectList);
        helper.saveProjectRecord(component, event, helper,'open');
    },
    updateToGrid:function(component, event, helper){
        var projectRecord = component.get("v.projectRecord");
        var index = component.get("v.buttonIndex.index");
        var projectList = component.get("v.projectList");
        projectList[index] = projectRecord;
        component.set("v.projectList",projectList);
        helper.saveProjectRecord(component, event, helper, 'close');
    },
    deleteRowItem:function(component,event, helper){
        var deleteProject = component.get("c.deleteProject");
        var buttonIndex = component.get("v.buttonIndex");
        var projectList = component.get("v.projectList");
        var projectId = component.get("v.projectList")[buttonIndex.index].projectId;
        deleteProject.setParams({
            recordId : projectId
        });
        deleteProject.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                if(!response.getReturnValue().isChild){
                    projectList.splice(buttonIndex.index,1);
                    component.set("v.projectList",projectList);
                    component.set("v.body",[]);
                    component.set("v.callController",false);
                    helper.appendData(component, event, helper, 0);
                    helper.openSuccess(component, event, helper, 'Deleted Successfully', 'slds-theme_success', 'utility:success');
                }
                else{
                    component.set("v.callController",false);
                    helper.openModal(component, event, helper, response.getReturnValue().Msg, 'Application Exception');
                }
            }else{
                component.set("v.calController",false);
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        component.set("v.callController",true);
        $A.enqueueAction(deleteProject);
    },
    searchRecords:function(component, event, helper){
        if(event.getParams().keyCode == 13 || $A.util.isEmpty(component.get("v.searchName"))){
            var getRecords = component.get("c.getProjectsGridView");
            getRecords.setParams({
                offsetValue: 0,
                limitValue: 50,
                searchText:component.get("v.searchName"),
                fieldName: '',
                order:'',
                projectCount:'',
            });
            getRecords.setCallback(this,function(response){
                var state = response.getState();
                if(state === 'SUCCESS'){
                    if(response.getReturnValue().data.getProjectsGridViewResult.length > 0){
                        component.set("v.projectList",response.getReturnValue().data.getProjectsGridViewResult);
                        component.set("v.body",[]);
                        helper.appendData(component, event, helper, 0);
                    }else{
						component.set("v.callController",false);
                        helper.openModal(component, event, helper, 'No Records Found', 'Search Result');
                    }
                }else{
                    helper.handleServerException(component, event, helper, response.getError());
                }
            });
            component.set("v.callController",true);
            $A.enqueueAction(getRecords);
        }
    },
    sortColumns:function(component, event, helper){
        var order;
        component.set("v.selectedHeader",event.currentTarget.getAttribute('data-header'));
        (component.get("v.arrowDirection") === 'arrowUp') ? order = 'DESC' : order = 'ASC' ;
        helper.sortColumn(component, event, helper, component.get("v.selectedHeader"), order);
    }
})