({
    getProjectList : function(component, event, helper, type) {
        var projectList = component.get("c.getProjectsGridView");
        projectList.setParams({
            offsetValue:  component.get("v.paginationIndex"),
            limitValue: 50,
            searchText: component.get("v.searchName"),
            order:'',
            fieldName:'',
            projectCount:component.get("v.projectCount"),
        });
        projectList.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var returnedValue = response.getReturnValue();
                var order = 0;
                component.set("v.projectCount",returnedValue.data.totalProjectCount);
                component.set("v.isProjectCreatable",returnedValue.data.getObjectPermissions.isProjectCreatable);
                if(response.getReturnValue().data.projectBankColumnList.length > 0){
                    response.getReturnValue().data.projectBankColumnList.unshift('Save as Default');
                    component.set("v.selectedValues",response.getReturnValue().data.projectBankColumnList);
                }
                if(returnedValue.data.getProjectsGridViewResult.length === 0){
                    component.set("v.callController",false);
                }
               if(type === 'search'){
                    component.set("v.projectList",returnedValue.data.getProjectsGridViewResult);
                    helper.appendData(component, event, helper, order);
                }if(type === 'scroll'){
                    var projectList = component.get("v.projectList");
                    order = projectList.length;
                    projectList = projectList.concat(returnedValue.data.getProjectsGridViewResult);
                    component.set("v.projectList",projectList);
                    helper.appendData(component, event, helper, order);
                }
            }else{
                component.set("v.callController",false);
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        component.set("v.callController",true);
        $A.enqueueAction(projectList);
    },
    appendData:function(component, event, helper, order) {
        var projectList = component.get("v.projectList");
        for(var index=0;index<projectList.length;index++){
            $A.createComponent(
                "ROQA:AgilingoProjectGridView",
                {
                    singleProject : projectList[index],
                    index : index,
                    buttonIndex : component.getReference("v.buttonIndex"),
                    selectedValues : component.getReference("v.selectedValues"),
                    selectedRecord : component.getReference("c.selectedRecord")
                },
                function(newButton,status,errorMessage){
                    if(status === 'SUCCESS'){
                        var body = component.get("v.body");
                        body.push(newButton);
                        component.set("v.body",body);
                        component.set("v.callController",false);
                    }
                }
            );
        }
    },
    openProjectModel:function(component, event, helper){
        component.set("v.projectRecord",{});
        $A.createComponent(
            "ROQA:ROQProjectCreate",
            {
                projectRecord : component.getReference("v.projectRecord"),
                addToGrid : component.getReference("c.addToGrid"),
                addAndOpen : component.getReference("c.addAndOpen")
            },
            function(newButton,status,errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.projectModal");
                    body.push(newButton);
                    component.set("v.projectModal",body);
                }
            }
        );
    },
    editRecord:function(component, event, helper, name){
         $A.createComponent(
            "ROQA:ROQProjectCreate",
            {
                header : 'Edit: '+name,
                showNew : false,
                projectRecord : component.getReference("v.projectRecord"),
                addToGrid : component.getReference("c.updateToGrid")
            },
            function(newButton,status,errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.projectModal");
                    body.push(newButton);
                    component.set("v.projectModal",body);
                }
            }
        );
    },
    deleteRow:function(component, event, helper, projectList, index) {
        $A.createComponent (
            "ROQA:MsgCmp",
            {
                message : 'Are you sure you want to Delete '+projectList[index].projectName+'?',
                header : 'Delete Project',
                type:'Confirm',
                callBack:component.getReference("c.deleteRowItem")
            },
            function(newButton,status,errorMessage){
                var body = component.get("v.projectModal");
                body.push(newButton);
                component.set("v.projectModal",body);
            }
        )
    },
    copyRecord:function(component, event, helper){
        $A.createComponent(
            "ROQA:ROQProjectCreate",
            {
                clone: true,
                projectRecord : component.getReference("v.projectRecord"),
                addToGrid : component.getReference("c.addToGrid"),
                addAndOpen : component.getReference("c.addAndOpen")
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.projectModal");
                    body.push(newButton);
                    component.set("v.projectModal",body);
                }
            }
            
        );
    },
    projectInfo:function(component, event, helper, projectList, index){
        $A.createComponent(
            "ROQA:projectTableInfo",
            {
                header:'Record History of '+projectList[index].projectLabel+'.',
                recordId:projectList[index].projectId
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.projectModal");
                    body.push(newButton);
                    component.set("v.projectModal",body);
                }
            }
        );
    },
    chatterFeed:function(component, event, helper, projectList, index){
        $A.createComponent(
            "ROQA:ROQChatter",
            {
                recordId : projectList[index].projectId,
                epic : projectList[index].projectName
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.projectModal");
                    body.push(newButton);
                    component.set("v.projectModal",body);
                    component.set("v.callController",false);
                }
            }
        );
    },
    sortColumn:function(component, event, helper, sortField, order){
        var sortColumn = component.get("c.getProjectsGridView");
        sortColumn.setParams({
            'fieldName' : sortField, 
            'order' : order,
            'limitValue' : 50,
            'offSetValue' : 0,
            'projectCount':'108',
            'searchText' :component.get("v.searchName")
        });
        sortColumn.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.projectList",response.getReturnValue().data.getProjectsGridViewResult);
                component.set("v.body",[]);
                helper.appendData(component, event, helper, 0);
                component.set("v.callController",false);
                (component.get("v.arrowDirection") === 'arrowUp' ? component.set("v.arrowDirection",'arrowDown') : component.set("v.arrowDirection",'arrowUp'));
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        component.set("v.callController",true);
        $A.enqueueAction(sortColumn);
    },
    saveProjectRecord:function(component, event, helper, popup){
        var recordList = helper.saveRecords(component, event, helper);
        var projectBank = component.get("c.doSaveProject");
        projectBank.setParams({
            'project' : JSON.stringify(recordList[0]),
            'fieldName' : '',
            'order' : '',
            'offsetValue' : 0,
            'searchText' : component.get("v.searchName"),
            'projectCount' : '',
            'limitValue' : 50
        });
        projectBank.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
				if(response.getReturnValue().isSuccess){
                component.set("v.callController",false);
                component.set("v.projectList",response.getReturnValue().data.getProjectsGridViewResult.data.getProjectsGridViewResult);
                component.set("v.body",[]);
				component.set("v.projectModal",[]);
                component.set("v.paginationIndex",0);
                helper.appendData(component, event, helper, 0);
                helper.openSuccess(component, event, helper, response.getReturnValue().Msg, 'slds-theme_success', 'utility:success');
                ((popup === 'open') ? helper.openProjectModel(component, event, helper) : '');
				}
            else
			{
				component.set("v.callController",false);
				helper.openSuccess(component,event, helper, response.getReturnValue().errorList[0].errorMessage, 'slds-theme_error','utility:error');
			}
			}
			else{
                component.set("v.callController",false);
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        component.set("v.callController",true);
        $A.enqueueAction(projectBank);
    },
    saveRecords:function(component, event, helper){
        var recordList = [];
        var projectList = component.get("v.projectList");
        for(var index=0;index<projectList.length;index++){
            if(projectList[index].recordStatus === 'New' || projectList[index].recordStatus === 'update'){
                recordList.push(projectList[index]);
                return recordList;
            }
        }
    },
    openSuccess:function(component, event, helper, message, style, iconName){
        $A.createComponent(
            "ROQA:ROQSuccessComponent",
            {
                saveStatus : message,
                style : style,
                iconName : iconName
            },
            function(newButton, status, errorMessage){
                if(status === 'SUCCESS'){
                    var body = component.get("v.projectModal");
                    body.push(newButton);
                    component.set("v.projectModal",body);
                }
            }
        );
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception',
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.projectModal");
                    body.push(newButton);
                    component.set("v.projectModal", body);
                }
            }
        );
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.openModal(component, event, helper,message,'Response From Server.');
    }
})