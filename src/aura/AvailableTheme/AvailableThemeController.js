({
    availThemes: function(component, event, helper){
        component.set("v.disableButton",true);
        helper.availThemes(component, event, helper, 'search');
    },
    selectedTheme: function(component, event, helper){
        var index = event.currentTarget.getAttribute('data-index');  
        var availThemes = component.get("v.themeList")[index];
        var obj = {Name:availThemes.Name,Description:availThemes.Description,themeId:availThemes.themeId};
        component.set("v.disableButton",false);
        component.set("v.themeRecord", obj);
        component.set("v.themeList",[]);
        document.getElementById("missingThemeName").innerHTML = '';
    },
    addTheme: function(component, event, helper){
        component.set("v.disableButton",true);
        var themeName = component.get("v.themeRecord").Name;
        var epicList = component.get("v.epicList");
        var theme = helper.validateAvailTheme(component, event, helper);
        if(component.get("v.newTheme")){
            for(var index=0; index < epicList.length; index++){
                themeName === epicList[index].Name ? component.set("v.newTheme",false) : '';
            }
        }
        if(!$A.util.isEmpty(themeName !== undefined ? themeName.trim() : themeName) && component.get("v.newTheme")){
            $A.enqueueAction(component.get("v.addTheme"));
            component.destroy();
        }
        else{
            component.set("v.disableButton",false);
            if($A.util.isUndefined(themeName) || $A.util.isEmpty(themeName.trim())){
                document.getElementById("missingThemeName").innerHTML = 'Theme Name is Required.';
            }else if(!component.get("v.newTheme")){
                document.getElementById("missingThemeName").innerHTML = 'Theme Exists.Select Other Theme';
            }
            component.set("v.newTheme",true);
        }
    },
    closeModel:function(component, event, helper){
        component.destroy();
    }
})