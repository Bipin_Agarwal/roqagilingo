({
    availThemes : function(component, event, helper, type) {
        var availThemes = component.get("c.getThemeList");
        var themeName = component.get("v.themeRecord").Name;
        availThemes.setParams({
            themeName:themeName,
            numberOfRowsToSkip:0,
        });
        availThemes.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue().data.length <=0){
                    component.set("v.elseValue",true);
                    component.set("v.themeList",response.getReturnValue().data);
                }
                else{
                    var result = response.getReturnValue().data;
                    component.set("v.elseValue",false);
                    if(type === 'search'){
                        component.set("v.themeList", result);
                    }else{
                        var availThemes = component.get("c.themeList");
                        availThemes.concat(result);
                        component.set("v.themeList", availThemes);
                    }
                }
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
            component.set("v.callController", false);
        });  
        $A.enqueueAction(availThemes);
    },
    validateAvailTheme:function(component, event, helper){
        var epicList = component.get("v.epicList");
        var themeRecord = component.get("v.themeRecord");
        for(var index=0;index<epicList.length;index++){
            if(epicList[index].themeId === themeRecord.themeId){
                component.set("v.newTheme",false);
                return epicList[index].Name;
            }
        }
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        component.set("v.disableButton",false);
        helper.openModal(component, event, helper,message,'Response From Server.');
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception'
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    }
})