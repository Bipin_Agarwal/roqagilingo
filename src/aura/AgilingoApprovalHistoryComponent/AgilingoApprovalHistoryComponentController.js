({
    loadSignatureHistories: function(component, event, helper) {
       helper.onLoad(component, event);
       component.checkForCurrentApprover();
    },
 
    approve : function(component, event, helper) {
        var action = component.get("c.isSignatureForStep");
        action.setParams({"recId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getReturnValue() == true){
                $A.createComponent("c:AgilingoSignatureComponent", {
                    "recordId" : component.get("v.recordId"),
                    "type" : "Approve"
                }, function(newCmp) {
                    if (component.isValid()) {
                        component.set("v.popupBody", newCmp);
                    }
                });
            }else{
                $A.createComponent("c:AgilingoApprovalModalComponent", {
                    "recordId" : component.get("v.recordId"),
                    "type" : "Approve"
                }, function(newCmp) {
                    if (component.isValid()) {
                        component.set("v.popupBody", newCmp);
                    }
                });
            }
            
        });
        $A.enqueueAction(action);
        
    },
    
    checkForCurrentApprover : function(component, event, helper){
    	var action = component.get("c.isNextApprover");
        action.setParams({"recId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getReturnValue()){
                component.set("v.isApprover",true);
            } 
        });
        $A.enqueueAction(action);
	},
    
    navigateBack:function(component, event){
        window.open('/'+component.get("v.recordId"),'_self');
    },
    
    reassign : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        $A.createComponent("c:AgilingoReassignComponent", {
            "recordId" : component.get("v.recordId")
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.popupBody", newCmp);
            }
        });  
    },
    
    reject : function(component, event, helper) {
        var action = component.get("c.isSignatureForStep");
        action.setParams({"recId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getReturnValue() == true){
                $A.createComponent("c:AgilingoSignatureComponent", {
                    "recordId" : component.get("v.recordId"),
                    "type" : "Reject"
                }, function(newCmp) {
                    if (component.isValid()) {
                        component.set("v.popupBody", newCmp);
                    }
                });
            }else{
                $A.createComponent("c:AgilingoApprovalModalComponent", {
                    "recordId" : component.get("v.recordId"),
                    "type" : "Reject"
                }, function(newCmp) {
                    if (component.isValid()) {
                        component.set("v.popupBody", newCmp);
                    }
                });
            }
            
        });
        $A.enqueueAction(action);
        
    },
    
    refresh: function(component, event) {
    	 $A.get('e.force:refreshView').fire();   
    },
    
    approvalProcessRecall: function(component, event, helper) {
		var action = component.get('c.recallApprovalProcess');
		action.setParams({
		 "recId" : component.get("v.recordId")
		});
		action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS'){
				$A.get('e.force:refreshView').fire();
            }
		});
		$A.enqueueAction(action);
	}
    
   /* toggle : function(component, event, helper) {
        var toggleMenu = component.find("listMenu");
        $A.util.toggleClass(toggleMenu, "slds-is-open");
    },*/
    
 })