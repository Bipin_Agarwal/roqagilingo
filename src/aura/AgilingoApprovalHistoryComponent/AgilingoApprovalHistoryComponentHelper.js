({
    onLoad: function(component, event) {
        var action = component.get('c.getSignatureHistory');
        action.setParams({
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                var recordList = response.signHistoryList; 
                component.set('v.sHistoryList', recordList);
                var historyList = component.get("v.sHistoryList");
                for (var i = 0; i < historyList.length; i++){
                    if(historyList[i].status == 'Pending'){
                        if(response.submitterId == historyList[i].createdById){
                            component.set("v.isSubmitter",true);
                        }
                        component.set("v.isPending",true);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },

    
})