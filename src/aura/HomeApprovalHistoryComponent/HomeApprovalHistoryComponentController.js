({
    doInit :  function(component) {
        
        
        var action = component.get("c.getItemsToApprove");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                console.log(JSON.stringify(response.getReturnValue()));
                var obj = response.getReturnValue();
                
                for(var i=0; i<obj.length;i++){
                    console.log('object type '+ obj[i].ProcessInstance.TargetObject.Type);
                    obj[i].ProcessInstance.TargetObject.Type =  obj[i].ProcessInstance.TargetObject.Type.slice(0, -3);
                    console.log('object type '+ obj[i].ProcessInstance.TargetObject.Type);
                    obj = obj.slice(0, 5);
                }
                
                component.set("v.recordList",obj);               
            }
        });
        $A.enqueueAction(action);
        
        
        
    },
    
    approvalHistories : function(component, event, helper) { 
        console.log('Event');
        console.log('is there a reocrd id --- '+component.get("v.recordId"));
        console.log('is there a reocrd id --- '+component.get("v.recordList"));
        
        
        var evt = $A.get("e.force:navigateToComponent");
        console.log('Event1'+evt);
        evt.setParams({
            componentDef : "c:HomePageListComponent",
            componentAttributes: {
                "recordList" : component.get("v.recordList"),
                "refreshApprovalList" :component.getReference("c.doInit")
            }
        });
        evt.fire();
        
        /*console.log('Event');        
        $A.createComponent("c:HomePageListComponent", {
            "recordList" : component.get("v.recordList")
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.popupBody", newCmp);
            }
        });*/ 
        /*  component.set("v.view",true);
        component.set("v.home",false);*/
    }, 
    
    checkForApprover : function(component, event, helper){
        var action = component.get("c.isNextApprover");
        action.setParams({"recId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getReturnValue()){
                component.set("v.isApprover",true);
            } 
        });
        $A.enqueueAction(action);
    },
    
    toggle : function(component, event, helper) {
        var toggleMenu = component.find("listMenu");
        $A.util.toggleClass(toggleMenu, "slds-is-open");
    },
    
    
    approve : function(component, event) {
        console.log('TestRecordID----'+ event.currentTarget.getAttribute('data-recid'));
        //console.log('TestRecordID'+component.get("v.recordId"));
        $A.createComponent("c:AgilingoSignatureComponent", {
            "recordId" : event.currentTarget.getAttribute('data-recid'),
            "type" :'Approve',
            "refreshApprovalList" :component.getReference("c.doInit"),
            "homeView" : true
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.popupBody", newCmp);
            }
        }); 
        
    },
    
    reject : function(component, event) {
        
        console.log('TestRecordID----'+ event.currentTarget.getAttribute('data-recid'));
        //console.log('TestRecordID'+component.get("v.recordId"));
        $A.createComponent("c:AgilingoSignatureComponent", {
            "recordId" : event.currentTarget.getAttribute('data-recid'),
            "type" :'Reject',
            "refreshApprovalList" :component.getReference("c.doInit"),
            "homeView" : true
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.popupBody", newCmp);
            }
        }); 
    },
    
    reassign : function(component, event) {
        
        $A.createComponent("c:AgilingoReassignComponent", {
            "recordId" : event.currentTarget.getAttribute('data-recid'),
            "refreshApprovalList" :component.getReference("c.doInit"),
            "homeView" : true
            
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.popupBody", newCmp);
            }
        });
        /*var event = $A.get("e.c:Reassign");
        event.setParams({
            "navigate" : "true"
        });
        event.fire(); */
    },
    
    approvalProcessRecall: function(component, event, helper) {
        var action = component.get('c.recallApprovalProcess');
        action.setParams({
            "recId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    toggleVisibility : function(component, event, helper) {
        var toggle = event.currentTarget.getAttribute("data-index");
        var tooltip = document.getElementById(toggle);
        $A.util.toggleClass(tooltip,'slds-is-open');
    },
    
})