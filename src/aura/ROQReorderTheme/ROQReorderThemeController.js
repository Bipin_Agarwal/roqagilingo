({
    doInit : function(component, event, helper) {
        setTimeout(function(){
            var sortable = document.getElementsByClassName('sortable');
            $(sortable).sortable();
            $(sortable).disableSelection();
        },0);
    },
    setTheme:function(component, event, helper){
        var blackbg = document.getElementsByClassName('blackbg');
        var epicList = component.get("v.epicList");
        var newList = [],swapVar,id,themeBarLength = 0;
        var themeBar = document.getElementsByClassName('themeBar');
        themeBarLength = epicList.length;
        for(var index=0;index<themeBarLength;index++){
            id = themeBar[index].getAttribute('id');
            swapVar = Object.assign({},epicList[id]);
            newList.push(swapVar);
        }
        component.set("v.epicList",newList);
        $(blackbg).fadeOut();
        $A.enqueueAction(component.get("v.reorderTheme"));
        component.destroy();
    },
    closeModal:function(component, event, helper){
        var blackbg = document.getElementsByClassName('blackbg');
        $(blackbg).fadeOut();
        $A.enqueueAction(component.get("v.reorderTheme"));
        component.destroy();
    }
})