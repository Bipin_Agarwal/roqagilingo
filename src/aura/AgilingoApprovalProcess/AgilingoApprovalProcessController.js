({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getLifecycleValue");
        action.setParams({recId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.disableButton(component,helper,event);
                helper.getPicklistValue(component,event,helper,response.getReturnValue().lifecycleValue);
            }else{
                //alert('===state====='+state);
            }
        });
        $A.enqueueAction(action);
    },
    
    doAction : function(component, event, helper) {
        
        //alert('Coming in doAction');
        component.set("v.messageDisplay","");
        var recordId = component.get("v.recordId");
        var approverAction = event.currentTarget.getAttribute("data-name");
        var action = component.get("c.getApprovalResponse");
        action.setParams({
            userId : null,
            recordId : recordId,
            comment : '',
            approverAction : approverAction
        }); 
        
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('=====state====='+state);
            
            //console.log('----response--'+JSON.stringify(response.getReturnValue()));
            //alert('=====returnValue====='+JSON.stringify(response.getReturnValue()));
            //alert('=====selectUsers====='+selectUsers);
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var selectUsers = response.getReturnValue().data.isApproverRequired;
                //alert(selectUsers);
                if(selectUsers){
                    helper.openSelectUserModal(component,helper,event,approverAction,recordId);
                }else{
                    var result = response.getReturnValue();
                    console.log(result);
                    if(result.data.Msg !== undefined){
                        component.set("v.messageDisplay", result.data.Msg);
                    }
                    //var div = document.getElementById('message');
                    //div.setAttribute("style", "color: white ;");
                    //document.getElementById("message").innerHTML="Record has been successfully submitted for approval.";
                    
                    helper.disableButton(component,event,helper);
                    setTimeout(function(){
                        document.getElementById("message").innerHTML="";
                    }, 3000);
                }
            } else {
                alert('Not Working');
            }
        });
        $A.enqueueAction(action);
    },
    
    /* $A.createComponent(
            "c:ApprovalUserListCmp",
            {
                usrComment: component.getReference("v.usrComment")
            },
            function(msgBox){ 
                if (component.isValid()) {
                    var targetCmp = component.find('modalHolder');
                    var body = targetCmp.get("v.body");
                    body=[];
                    body.push(msgBox);
                    targetCmp.set("v.body", body); 
                    
                }
            }
        ); */
    disableButton :function(component, event, helper){
        helper.disableButton(component,helper,event);
    },
    
    removeMessage:function(component, event, helper){
        if(component.get("v.messageDisplay").length > 0){
            var obj = setTimeout(function(){
                component.set("v.messageDisplay","");
                clearInterval(obj);
            }, 5000);
        }
        
    }
    
    
})