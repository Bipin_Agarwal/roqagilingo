({
    getPicklistValue : function(component,event,helper,lfcycleValue) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getDependentpicklistValues");
        action.setParams({
            recordId : recordId,
            lifecycleValue : lfcycleValue
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.buttonLabels",response.getReturnValue());
            }else{
                console.log('===Error====='+state);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    disableButton : function(component, event, helper) {
        var recId = component.get("v.recordId");
        console.log('rec Id'+recId);
        var action = component.get("c.getRecallFlag");
        action.setParams({recordId : recId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue() === true){
                    component.set("v.disableBtn",true);
                } else {
                    component.set("v.disableBtn",false);
                }
            } else {
                console.log('Error in Helper');
            }
        });
        $A.enqueueAction(action);
    },
    
    openSelectUserModal : function(component,event,helper,approverAction, recordId){
        $A.createComponent(
            "c:ApprovalUserListCmp",
            {
                appAction : approverAction,
                recId : recordId,
                disableBtn : component.getReference("c.disableButton"),
                messageDisplay:component.getReference("v.messageDisplay")
            },
            function(msgBox){ 
                if (component.isValid()) {
                    var targetCmp = component.find('modalHolder');
                    var body = targetCmp.get("v.body");
                    body=[];
                    body.push(msgBox);
                    targetCmp.set("v.body", body); 
                    
                }
            }
        );
    }
})