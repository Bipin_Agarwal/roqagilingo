({
    doInit :  function(cmp) {
        var action = cmp.get("c.getApprovalHistoryList");
        action.setParams({"recordId":cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(cmp.isValid() && state === 'SUCCESS') {
                cmp.checkForApprover();
                var recordList = response.getReturnValue();
                
                for (var i = 0; i < recordList.length; i++){
                    if(recordList[i].StepStatus == 'Pending'){
                        cmp.set("v.isPending",true);
                    }
                }
                
                if(recordList.length > 3){
                    cmp.set('v.noOfRecs','(3+)');  
                }else{
                    cmp.set('v.noOfRecs','('+recordList.length+')');
                }
                recordList = recordList.slice(0, 3);
                cmp.set('v.approvalRecs', recordList);
                
                
            }
        });
        $A.enqueueAction(action);
    },
    
    approvalHistories : function(component, event, helper) { 
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:AgilingoApprovalHistoryComponent",
            componentAttributes: {
                "recordId" : component.get("v.recordId")
            }
        });
        evt.fire(); 
    }, 
    
    checkForApprover : function(component, event, helper){
    	var action = component.get("c.isNextApprover");
        action.setParams({"recId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getReturnValue()){
                component.set("v.isApprover",true);
            } 
        });
        $A.enqueueAction(action);
	},
    
    toggle : function(component, event, helper) {
        var toggleMenu = component.find("listMenu");
        $A.util.toggleClass(toggleMenu, "slds-is-open");
    },
    
    
    approve : function(component, event) {
        var event = $A.get("e.c:ApproveReject");
        event.setParams({
            "navigate" : "true",
            "type" : "Approve"
        });
        event.fire(); 
    },
    
    reject : function(component, event) {
        var event = $A.get("e.c:ApproveReject");
        event.setParams({
            "navigate" : "true",
            "type" : "Reject"
        });
        event.fire(); 
    },
    
    reassign : function(component, event) {
        var event = $A.get("e.c:Reassign");
        event.setParams({
            "navigate" : "true"
        });
        event.fire(); 
    },
    
    approvalProcessRecall: function(component, event, helper) {
        var action = component.get('c.recallApprovalProcess');
        action.setParams({
            "recId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
})