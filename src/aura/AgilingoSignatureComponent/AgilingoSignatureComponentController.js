({
    doInit: function(cmp, helper, event){
        var action = cmp.get("c.getInitialSettings");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                // var map = new WeakMap();
                // map = response.getReturnValue();
                // load users
                cmp.loadUsers();
                cmp.set("v.initialSetting", response.getReturnValue());
                //alert(JSON.stringify(cmp.get('v.initialSetting')));
                // open model
                var total = 0;
                cmp.set("v.username", cmp.get("v.initialSetting.user_name"));
                cmp.set("v.name", cmp.get("v.initialSetting.name"));
                cmp.set("v.validatePWD",cmp.get("v.initialSetting.pwd_setup"))
                cmp.set("v.validateOTP",cmp.get("v.initialSetting.otp_setup"));
                cmp.set("v.signatureRequired",cmp.get("v.initialSetting.signature_setup"));
                if(cmp.get("v.validatePWD") == "true"){
                    total++;
                    cmp.set("v.isAuthenticate",true);
                }
                if(cmp.get("v.validateOTP") == "true"){
                    cmp.set("v.isAuthenticate",true);
                    total++;
                    cmp.sendOTPMethod();
                }
                if(cmp.get("v.signatureRequired") == "true"){
                    cmp.set("v.isAuthenticate",true);
                    total++;
                } 
                //cmp.set("v.isOpen", true);
                cmp.set("v.password","");
                cmp.set("v.totalAuthenticationProcess",total);
            }
        });
        $A.enqueueAction(action);
        
        var myMap = cmp.get("v.finalDataMap");
        myMap['recordId'] = cmp.get("v.recordId");
        
    },
    
    checkInputs: function(cmp, event, helper){
        
        var isPasswordEntered = false;
        var isOTPEntered = false;
        var isDocUnameEntered = false;
        var isDocPwdEntered = false;
        
        
        if(cmp.get("v.validatePWD")){
            
            if(cmp.get("v.password").length > 1)
                isPasswordEntered = true;
        }
        else
            isPasswordEntered = true;
        
        if(cmp.get("v.validateOTP")){
            
            if(cmp.get("v.enteredOTP").length > 1)
                isOTPEntered = true;
            
        }
        else
            isOTPEntered = true;
        
        if(cmp.get("v.signatureRequired")){
            
            if(cmp.get("v.docUsername").length > 1)
                isDocUnameEntered = true;
            
        }
        else
            isDocUnameEntered = true;
        
        if(cmp.get("v.signatureRequired")){
            
            if(cmp.get("v.docPassword").length > 1)
                isDocPwdEntered = true;
            
        }
        else
            isDocPwdEntered = true;
        
        if(isPasswordEntered && isOTPEntered && isDocUnameEntered && isDocPwdEntered)
            cmp.set("v.disabled",false);
    },
    
    verification: function(cmp, event, helper){
        var errorDiv = '';
        var myMap = cmp.get("v.finalDataMap");
        myMap['password'] = cmp.get("v.password");
        myMap['otp'] = cmp.get("v.enteredOTP");
        myMap['docusername'] = cmp.get("v.docUsername");
        myMap['docpassword'] = cmp.get("v.docPassword");
        myMap['recordId'] = cmp.get("v.recordId");
        myMap['status'] = cmp.get("v.type");
        var action = cmp.get("c.verifySignature");
        action.setParams({"signData":JSON.stringify(cmp.get("v.finalDataMap"))});
        action.setCallback(this, function(response) {
            //debugger;
            if(response.getReturnValue() == 'pwdblank'){
                cmp.set("v.errorMessage","Please enter the password");
                errorDiv = cmp.find('error');
                cmp.set("v.showError", true);
                //document.querySelector('.lds-ellipsis').style.display = "inline-block";
                $A.util.addClass(errorDiv,'slds-show');
                $A.util.removeClass(errorDiv, 'slds-hide');
            }
            else if(response.getReturnValue() == 'otpblank'){
                cmp.set("v.errorMessage","Please enter the OTP");
                errorDiv = cmp.find('error');
                cmp.set("v.showError", true);
                $A.util.addClass(errorDiv,'slds-show');
                $A.util.removeClass(errorDiv, 'slds-hide');
            }
                else if(response.getReturnValue() == 'docblank'){
                    cmp.set("v.errorMessage","Please enter the docusign username/password");
                    errorDiv = cmp.find('error');
                    cmp.set("v.showError", true);
                    $A.util.removeClass(errorDiv, 'slds-hide');
                    $A.util.addClass(errorDiv,'slds-show');                   
                }
                    else if(response.getReturnValue() == 'invalid'){
                        cmp.set("v.errorMessage","Invalid Docusign Username/Password");
                        errorDiv = cmp.find('error');
                        cmp.set("v.showError", true);
                        $A.util.removeClass(errorDiv, 'slds-hide');
                        $A.util.addClass(errorDiv,'slds-show');
                    }else if(response.getReturnValue() == 'false'){
                        cmp.set("v.errorMessage","Invalid Password");
                        errorDiv = cmp.find('error');
                        cmp.set("v.showError", true);
                        $A.util.removeClass(errorDiv, 'slds-hide');
                        $A.util.addClass(errorDiv,'slds-show');
                    }else if(response.getReturnValue() == 'otpfailed'){
                        cmp.set("v.errorMessage","Invalid OTP");
                        errorDiv = cmp.find('error');
                        cmp.set("v.showError", true);
                        $A.util.removeClass(errorDiv, 'slds-hide');
                        $A.util.addClass(errorDiv,'slds-show');
                    }else if(response.getReturnValue() == 'otptimeout'){
                        cmp.set("v.errorMessage","Your OTP has been expired");
                        errorDiv = cmp.find('error');
                        cmp.set("v.showError", true);
                        $A.util.removeClass(errorDiv, 'slds-hide');
                        $A.util.addClass(errorDiv,'slds-show');
                    }else{
                        if(cmp.get("v.signatureRequired") == "true"){
                            cmp.set("v.esign",'data:image/png;base64,'+response.getReturnValue());
                            myMap['signature'] = response.getReturnValue();
                            cmp.set("v.signatureReceived",true);
                            myMap['isSignatureValidated'] = cmp.get("v.signatureReceived");
                        }
                        cmp.set("v.isAllVerified",true);
                        if(cmp.get("v.initialSetting.theme") == "Theme4d" ){
                            cmp.set("v.showCommentBox",true);
                        }
                        cmp.set("v.signatureRequired",false);
                        cmp.set("v.showError",false);
                    }
            
            if(response.getReturnValue() != "false" && response.getReturnValue() != "pwdblank"){
                cmp.set("v.isPwdValidated",true);
                myMap['isPasswordValidated'] = cmp.get("v.isPwdValidated");
                if(response.getReturnValue() != "otpfailed" && response.getReturnValue() != "otptimeout" && 
                   response.getReturnValue() != "true" && response.getReturnValue() != "otpblank"){
                    cmp.set("v.isOTPValidated",true);
                    myMap['isOTPValidated'] = cmp.get("v.isOTPValidated");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    invokeOTP: function(cmp, event, helper) {
        helper.sendSignatureOTP(cmp,event);
    },
    
    resendOTP: function(cmp, event, helper) {
        helper.sendSignatureOTP(cmp,event);
        cmp.set("v.isOTPTimeout",false);
    },
    
    closeModal: function(component, event, helper) {
        // var isSmarteeva = true;//component.get("v.isSmarteeva");
        // var callback =  component.get("v.customCallback");
        //if(isSmarteeva){
        // window.location.href = callback;
        //}else{
        /*if(component.get("v.initialSetting.theme") == "Theme4d"){
                var event = $A.get("e.c:CloseModal");
                event.setParams({
                    "navigate" : "true"
                });
                event.fire();
            }else{
                System.debug('Location'+window.location);
                window.location.reload(true);
            }   
        }*/
        var refreshButton = $A.get('e.ROQA:ESignRefresh');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('MB-Back');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
        refreshButton.fire();
        
    },
    
    loadUsers: function(component, event, helper){
        var action = component.get('c.getActiveUsers');
        action.setCallback(this, function(actionResult) {
            component.set('v.activeUsers', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    onSelectChange: function(component, event, helper) {
        var selectedUser = component.find("chooseuser").get("v.value");
        component.set("v.nextApprover",selectedUser);
    },
    
    finalSubmit: function(component, event, helper) { 
        var myMap = component.get("v.finalDataMap");
        myMap['nextapprover'] = component.get("v.nextApprover");
        helper.createESignHistory(component, event);
        $A.get('e.force:refreshView').fire();
        $A.get("e.ROQA:ApprovalHistoryEvent").fire();
    },
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
    
    
})