({
    sendSignatureOTP : function(component, event) {
        var action = component.get("c.sendOTP");
        action.setCallback(this, function(response) {
            if(response.getReturnValue() == 'NoConfig'){
                
                cmp.set("v.errorMessage","Mobile number has not been configured");
                var errorDiv = cmp.find('error');
                $A.util.addClass(errorDiv,'slds-show');
                
            }  
        });
        $A.enqueueAction(action);
    },
    
    gotoURL : function (component, event) {
        component.destroy();
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/"+component.get("v.recordId"),
            "isredirect" : true
        });
        urlEvent.fire();
    },
    
    navigateToSObject:function(component, event){
        var sObjectEvent = $A.get("e.force:navigateToSObject");
        sObjectEvent .setParams({
            "recordId": component.get("v.recordId")  ,
            "slideDevName": "detail"
        });
        sObjectEvent.fire();
        // component.destroy();   
    }, 
    
    navigateToSignatureHistoryComponent : function(component, event) { 
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:AgilingoApprovalHistoryComponent",
            componentAttributes: {
                "recordId" : component.get("v.recordId")
            }
        });
        evt.fire(); 
    }, 
    
    createESignHistory: function(cmp, event){
        console.log('Test123');
        console.log('Record ====ID'+cmp.get("v.recordId"));
        var myMap = cmp.get("v.finalDataMap");
        console.log('myMap'+myMap);
        
        myMap['comments'] = cmp.get("v.comments");
        var action = cmp.get("c.createSignatureHistory");
        console.log('Stringify'+JSON.stringify(cmp.get("v.finalDataMap")));
        
        action.setParams({"data":JSON.stringify(cmp.get("v.finalDataMap"))});
        action.setCallback(this, function(response) {
            console.log('Response'+response.getReturnValue());
            if(response.getReturnValue() == 'SUCCESS'){
                
                //var isCustom = cmp.get("v.isCustom");
                // console.log('isCustom'+isCustom);
                //var callback =  cmp.get("v.customCallback");
                // console.log('callback'+callback);
                /* if(isCustom && callback != '' && callback != null){
                    console.log('Inside custom if');
                    window.location.href = callback; 
                    
                }*/
                //  else if(cmp.get("v.initialSetting.theme") == "Theme4d"){
                /* console.log('Inside custom else');
                    var event = $A.get("e.c:CloseModal");
                    event.setParams({
                        "navigate" : "true"
                    });
                    event.fire();*/ 
                //}else{
                // window.location.assign('/'+cmp.get("v.recordId"));
                if(cmp.get("v.homeView") == true){
                    $A.enqueueAction(cmp.get("v.refreshApprovalList"));
                    cmp.destroy();
                }else{
                    var refreshButton = $A.get("e.c:RefreshButtons");
                    var cmpTarget = cmp.find('Modalbox');
                    var cmpBack = cmp.find('MB-Back');
                    $A.util.removeClass(cmpBack,'slds-backdrop--open');
                    $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
                    refreshButton.fire();
                    
                }
                //}
                
            }else if(response.getReturnValue() == 'next approver required'){
                cmp.set("v.isNextApproverRequired",true);
            }else{
                cmp.set("v.errorMessage",response.getReturnValue());
                var errorDiv = cmp.find('error');
                $A.util.addClass(errorDiv,'slds-show');
            }
        });
        
        
        $A.enqueueAction(action);
    }
    
})