({
    checkForApprover : function(component, event, helper){
        console.log('Test1');
        var action = component.get("c.isNextApprover");
        console.log('Action'+action);
        action.setParams({"recId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            console.log('Test2'+response.getReturnValue());
            if(response.getReturnValue()){
                console.log('Test3');
                component.set("v.isApprover",true);
                
            } 
            console.log('ISApprover'+component.get("v.isApprover"));
        });
        $A.enqueueAction(action);
        
    },
    
    toggleHelper : function(component,event) {
        var toggleText = component.find("tooltip");
        $A.util.toggleClass(toggleText, "toggle");
    }
})