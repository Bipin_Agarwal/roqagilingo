({
    approvalHistories : function(component, event, helper) { 
        
        var evt = $A.get("e.force:navigateToComponent");
        console.log('--Navigate--'+evt);
        evt.setParams({
            componentDef : "c:AgilingoApprovalHistoryComponent",
            componentAttributes: {
                "recordId" : component.get("v.recordId")
            }
        });
        evt.fire(); 
    }, 
    
    
    
    toggle : function(component, event, helper) {
        var toggleMenu = component.find("listMenu");
        $A.util.toggleClass(toggleMenu, "slds-is-open");
    },
    
    
    approve : function(component, event) {
        var event = $A.get("e.c:ApproveReject");
        event.setParams({
            "navigate" : "true",
            "type" : "Approve"
        });
        event.fire();
        component.set("v.isPending",false);
        
    },
    
    reject : function(component, event) {
        var event = $A.get("e.c:ApproveReject");
        event.setParams({
            "navigate" : "true",
            "type" : "Reject"
        });
        event.fire();
        component.set("v.isPending",false);
        
    },
    
    reassign : function(component, event) {
        var event = $A.get("e.c:Reassign");
        event.setParams({
            "navigate" : "true"
        });
        event.fire(); 
    },
    
    approvalProcessRecall: function(component, event, helper) {
        var action = component.get('c.recallApprovalProcess');
        action.setParams({
            "recId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    toggleVisibility : function(component, event, helper)
    {
        var toggleButton = component.find('toggleButton');
        $A.util.toggleClass(toggleButton,'slds-is-open');
    },
    
    init: function (cmp, event, helper) {
            var action = cmp.get("c.getApprovalHistoryList");
            action.setParams({"recordId":cmp.get("v.recordId")});
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if(cmp.isValid() && state === 'SUCCESS') {
                    helper.checkForApprover(cmp,event, helper);
                    var response = response.getReturnValue();
                    var recordList = response.historyList; 
                    
                    for (var i = 0; i < recordList.length; i++){
                        if(recordList[i].StepStatus == 'Pending'){
                            if(response.submitterId == recordList[i].CreatedById){
                                cmp.set("v.isSubmitter",true);
                            }
                            cmp.set("v.isPending",true);
                        } 
                        if(recordList.length >= 6){
                            cmp.set('v.noOfRecs','(6+)');  
                        }else{
                            cmp.set('v.noOfRecs','('+recordList.length+')');
                        }
                        recordList = recordList.slice(0, 6);
                        cmp.set('v.approvalRecs', recordList);
                    }
                }
            });
            $A.enqueueAction(action);
    },
    display : function(component, event, helper) {
        var elementToToogle = event.currentTarget.getAttribute("data-name");
        console.log('the id id----'+elementToToogle);
        //alert(elementToToogle);
        var divs = document.getElementsByClassName("tooltips");
        for(var i=0; i < divs.length; i++){
            
            if(divs[i].classList.contains(elementToToogle)){
                document.getElementById(elementToToogle).style.display = "block";
            }else{
                document.getElementById(elementToToogle).style.display = "none";
            }
        }
        //helper.toggleHelper(component, event);
    },
    
    displayOut : function(component, event, helper) {
        
        
    },
    showpopover : function(component, event, helper){
        // alert('onmouseenter');
        console.log('inside show');
        var recId = event.currentTarget.getAttribute("data-index");
        console.log('inside show'+recId);
        
        
        var tooltip = document.getElementById(recId);
        $A.util.addClass(tooltip,'showpopup');
        
        $A.util.removeClass(tooltip,'hidepopup');
    },
    hidepopover : function(component, event, helper){
        console.log('============');
        var recId = event.currentTarget.getAttribute("data-index");
        
        var tooltip = document.getElementById(recId);
        $A.util.removeClass(tooltip,'showpopup');
        $A.util.addClass(tooltip,'hidepopup');    
    }
    
})