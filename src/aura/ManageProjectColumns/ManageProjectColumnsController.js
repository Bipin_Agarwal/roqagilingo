({
    closeModel : function(component, event, helper) {
        var columnList = helper.createListColumns(component, event, helper);
        if(columnList.length > 0){
            var saveColumns = component.get("c.doSaveProjectBankColumns");
            saveColumns.setParams({
                'fieldsList' : columnList
            });
            saveColumns.setCallback(this, function(response){
                var state = response.getState();
                if(state === 'SUCCESS'){
                }else{
                    helper.handleserverException(component, event, helper, response.getError());
                }
            });
            $A.enqueueAction(saveColumns);
        }
        component.destroy();
    },
})