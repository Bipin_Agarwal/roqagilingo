({
    doInit :  function(component) {        
        /*var action = cmp.get("c.getApprovalHistoryList");
        action.setParams({"recordId":cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(cmp.isValid() && state === 'SUCCESS') {
                cmp.checkForApprover();
                var recordList = response.getReturnValue();
                
                for (var i = 0; i < recordList.length; i++){
                    if(recordList[i].StepStatus == 'Pending'){
                        cmp.set("v.isPending",true);
                    }
                }
                
                if(recordList.length > 3){
                    cmp.set('v.noOfRecs','(3+)');  
                }else{
                    cmp.set('v.noOfRecs','('+recordList.length+')');
                }
                recordList = recordList.slice(0, 3);
                cmp.set('v.approvalRecs', recordList);
            }
        });
        $A.enqueueAction(action);*/
        
        console.log('Init');
        console.log('List1---'+JSON.stringify(component.get("v.recordList")));
        /* var action = component.get("c.getItemsToApprove");
         console.log('Action'+action);
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('State'+state);
            if(state === 'SUCCESS') {
                console.log('Sucess');
                console.log('RecordList---'+JSON.stringify(response.getReturnValue()));
                component.set("v.recordList",response.getReturnValue());               
            }
        });
        $A.enqueueAction(action);*/
        
    },    
    
    toggle : function(component, event, helper) {
        var toggleMenu = component.find("listMenu");
        $A.util.toggleClass(toggleMenu, "slds-is-open");
    },
    
    
    approveRecord : function(component, event) {   
        console.log('Approve');
        console.log('TestRecordID----'+ event.currentTarget.getAttribute('data-recid'));
        $A.createComponent("c:AgilingoSignatureComponent", {
            "recordId" : event.currentTarget.getAttribute('data-recid'),
            "type" :'Approve',
            "refreshApprovalList" :component.getReference("v.refreshApprovalList")
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.popupBody", newCmp);
            }
        }); 
        
    },
    
    reject : function(component, event) {
        console.log('TestRecordID----'+ event.currentTarget.getAttribute('data-recid'));
        $A.createComponent("c:AgilingoSignatureComponent", {
            "recordId" : event.currentTarget.getAttribute('data-recid'),
            "type" :'Reject',
            "refreshApprovalList" :component.getReference("v.refreshApprovalList")
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.popupBody", newCmp);
            }
        }); 
        
    },
    
    reassign : function(component, event) {
        $A.createComponent("c:AgilingoReassignComponent", {
            "recordId" : event.currentTarget.getAttribute('data-recid'),
            "refreshApprovalList" :component.getReference("c.doInit")
            
        }, function(newCmp) {
            if (component.isValid()) {
                component.set("v.popupBody", newCmp);
            }
        });
        
    },
    toggleVisibility : function(component, event, helper) {
        var toggle = event.currentTarget.getAttribute("data-index");
        var tooltip = document.getElementById(toggle);
        $A.util.toggleClass(tooltip,'slds-is-open');
    },
    
})