({
    validThemeDate:function(component, event, helper, type){
        var themeStartDate = component.get("v.themeStartDate");
        if(type === 'months'){
            var themeMonths = moment(themeStartDate,'YYYY-MM-DD').month();
            if(themeMonths > 0){
                var themeDays = moment(themeStartDate,'YYYY-MM-DD').date();
                var startDate = moment(themeStartDate,'YYYY-MM-DD').subtract(themeDays-1,'days').format('YYYY-MM-DD');
                return (moment(startDate,'YYYY-MM-DD').subtract(themeMonths,type).format('YYYY-MM-DD'));
            }
        }
        var themeDays = moment(themeStartDate,'YYYY-MM-DD').date();
        return (moment(themeStartDate,'YYYY-MM-DD').subtract(themeDays-1,'days').format('YYYY-MM-DD'));
    },
    saveMap:function(component, event, helper){
        if(component.get("v.epicType") === 'Roadmap'){
            var epicList = component.get("v.epicList");
            var roadmapId = component.get("v.rmId");
            var obj = new Object();
            obj.themes = epicList;
            var saveRecord = component.get("c.manageDoSaveRoadmap");
            saveRecord.setParams({
                "rmId" : roadmapId,
                "roadMapRecord" : JSON.stringify(obj)
            });
            saveRecord.setCallback(this,function(response){
                var state = response.getState();
                if(state === "SUCCESS"){
                    var message;
                    var result = response.getReturnValue();
                    if(result.isSuccess){
						helper.openSuccess(component, event, helper, 'Encountered Exception While Dragging, We are Saving the Record and Reloading the page','slds-theme_warning','utility:warning');
                        helper.openSuccess(component, event, helper, result.Msg, 'slds-theme_success','utility:success');
                        location.reload();
                    }else{
                        var message = result.errorList[0].errorMessage;
                        helper.openModal(component, event, helper, message, 'Application Exception');
                    }
                }else{
                    helper.handleServerException(component, event, helper, response.getError());
                }
            });
            $A.enqueueAction(saveRecord);
        }else if(component.get("v.epicType") === 'Initiative'){
            var epicList = component.get("v.epicList");
            var intiativeId = component.get("v.rmId");
            var obj = new Object();
            obj.themes = epicList;
            var saveRecord = component.get("c.manageDoSaveInitiative");
            saveRecord.setParams({
                "initId" : intiativeId,
                "initiativeRecord" : JSON.stringify(obj)
            });
            saveRecord.setCallback(this,function(response){
                var state = response.getState();
                if(state === "SUCCESS"){
                    var message;
                    var result = response.getReturnValue();
                    if(result.isSuccess){
						helper.openSuccess(component, event, helper, 'Encountered Exception While Dragging, We are Saving the Record and Reloading the page','slds-theme_warning','utility:warning');
                        helper.openSuccess(component, event, helper, result.Msg, 'slds-theme_success','utility:success');
						location.reload();
                    }else{
                        var message = result.errorList[0].errorMessage;
                        helper.openModal(component, event, helper, message, 'Application Exception');
                    }
                }else{
                    helper.handleServerException(component, event, helper, response.getError());
                }
            });
            $A.enqueueAction(saveRecord);
        }else if(component.get("v.epicType") === 'Feature'){
            var epicList = component.get("v.epicList");
            var piId = component.get("v.rmId");
            var obj = new Object();
            obj.themes = epicList;
            var saveRecord = component.get("c.manageDoSaveProgramIncrement");
            saveRecord.setParams({
                "releaseRecord": JSON.stringify(obj),
                "piId":piId
            });
            saveRecord.setCallback(this,function(response){
                var state = response.getState();
                if(state === "SUCCESS"){
                    var message;
                    var result = response.getReturnValue();
                    if(result.isSuccess){
                        helper.openSuccess(component, event, helper, 'Encountered Exception While Dragging, We are Saving the Record and Reloading the page','slds-theme_warning','utility:warning');
                        helper.openSuccess(component, event, helper, result.Msg, 'slds-theme_success','utility:success');
						location.reload();
                    }else{
                        var message = result.errorList[0].errorMessage;
                        helper.openModal(component, event, helper, message, 'Application Exception');
                    }
                }else{
                    helper.handleServerException(component, event, helper, response.getError());
                }
            });
            $A.enqueueAction(saveRecord);
        }
    },
    openSuccess:function(component, event, helper, message, style, iconName){
        $A.createComponent(
            "ROQA:ROQSuccessComponent",
            {
                saveStatus : message,
                style:style,
                iconName:iconName
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception'
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        component.set("v.disableButton",false);
        helper.openModal(component, event, helper,message,'Response From Server.');
    }
})