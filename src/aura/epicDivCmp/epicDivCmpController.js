({
    doInit:function(component,event,helper){
        var format = component.get("v.format");
        var showFormat = component.get("v.showFormat");
        var epicData =  component.get("v.epicData");
        var epicIndex = component.get("v.epicIndex");
        component.set("v.epics",epicData.epics[epicIndex]);
        var epics = component.get("v.epics");
        var epicName = epics.Name;
        var themeIndex = component.get("v.themeIndex");
        var epicIndex = component.get("v.epicIndex");
        var date = component.get("v.date");
        date.StartDate = moment(epics.StartDate).format(showFormat.toUpperCase());
        date.EndDate = moment(epics.EndDate).format(showFormat.toUpperCase());
        component.set("v.date",date);
        var endDate = component.get("v.epicType") === 'Initiative' ? parseInt(moment(epics.EndDate,'YYYY-MM-DD').date()) : '';
        var pixelValue = component.get("v.pixelValue");
        var type = component.get("v.type");
        var pixel = ((type === 'months') ? 4 : 4);
        var startDate,startLeft,lastDate,oldParentId,newParentId;
        var thimInitialStartDate = moment(component.get("v.themeStartDate"), "YYYY-MM-DD").format('YYYY-MM-DD');
        var themeStartDate = helper.validThemeDate(component, event, helper, type);
        var themeEndDate = moment(component.get("v.themeEndDate"), "YYYY-MM-DD").format('YYYY-MM-DD');
        var startDateBeyond = (component.get("v.epicType") === 'Initiative') ? 
            moment(thimInitialStartDate,'YYYY-MM-DD').subtract(2,'year').format('YYYY-MM-DD') : 
        moment(thimInitialStartDate,'YYYY-MM-DD').subtract(2,'month').format('YYYY-MM-DD');
        
        setTimeout(function(){ 
            var epicObj = document.getElementsByClassName(epicName+themeIndex+epicIndex);
            var bar = document.getElementsByClassName('bar');
            var containmentClass = document.getElementsByClassName('containmentClass');
            $(epicObj).draggable({
                cursor: "move",
                containment: containmentClass, 
                revert: function(event, ui) {
                    var obj = ((moment(startDate,'YYYY-MM-DD').isBefore(moment(thimInitialStartDate,'YYYY-MM-DD'))) || (moment(lastDate,'YYYY-MM-DD').isAfter(moment(themeEndDate,'YYYY-MM-DD'))));
                    if(moment(startDate,'YYYY-MM-DD').isBefore(moment(startDateBeyond,'YYYY-MM-DD'))){
                        helper.saveMap(component, event, helper);
                    }
                    return obj;
                },
                start: function(event, ui) {
                    startLeft = ui.position.left;
                    component.set("v.zIndex",99);
                    localStorage.setItem('oldParentId',$(this).parent().attr('id'));
                    localStorage.setItem('epicIndex',component.get("v.epicIndex"));
                },
                drag : function(event, ui) {                    
                    var difference = new moment(date.EndDate,showFormat.toUpperCase()).diff(new moment(date.StartDate,showFormat.toUpperCase()),type);
                    if(ui.position.left === 0){
                        startDate = themeStartDate;
                        
                    }else{
                        startDate = moment(themeStartDate, 'YYYY-MM-DD').add(Math.floor((ui.position.left)/pixelValue),type).format('YYYY-MM-DD');
                    }
                    lastDate = moment(startDate,'YYYY-MM-DD').add(difference,type).format('YYYY-MM-DD');
                    lastDate = (component.get("v.epicType") === 'Initiative' ? (difference === 0 ? moment(startDate,'YYYY-MM-DD').endOf('month').format('YYYY-MM-DD') : lastDate) : lastDate);
                    
                    if(!moment(startDate,'YYYY-MM-DD').isBefore(moment(thimInitialStartDate,'YYYY-MM-DD')) && !moment(lastDate,'YYYY-MM-DD').isAfter(moment(themeEndDate,'YYYY-MM-DD'))){
                        epics.StartDate = startDate;
                        epics.EndDate = lastDate;
                        epics.EpicStyle = ui.position.top;
                        date.StartDate = moment(startDate,'YYYY-MM-DD').format(showFormat.toUpperCase());
                        date.EndDate = moment(lastDate , 'YYYY-MM-DD').format(showFormat.toUpperCase());
                        var left =  ((moment(epics.StartDate,'YYYY-MM-DD').diff(moment(themeStartDate,'YYYY-MM-DD'), type)*pixelValue)+pixel)+"px";
                        component.set("v.left",left);
                        component.set("v.date",date);
                        component.set("v.epics", epics);
                    }else{
                        date.StartDate = moment(themeStartDate, 'YYYY-MM-DD').add(Math.floor((startLeft)/pixelValue),type).format('YYYY-MM-DD');
                        date.EndDate = moment(date.StartDate,'YYYY-MM-DD').add(difference,type).format('YYYY-MM-DD');
                        epics.StartDate = date.StartDate;
                        epics.EndDate = date.EndDate;
                        epics.EpicStyle = ui.position.top;
                        date.StartDate = moment(date.StartDate,'YYYY-MM-DD').format(showFormat.toUpperCase());
                        date.EndDate = moment(date.EndDate,'YYYY-MM-DD').format(showFormat.toUpperCase());
                        component.set("v.date",date);
                        component.set("v.epics", epics);
                    }
                },
            });
            $(bar).mouseover(function(){
                $(this).css('z-index', 999);
                $(this).find('.tooltiptext').show();
            });
            $(bar).mouseout(function(){
                $(this).css('z-index', 0);
                $(this).find('.tooltiptext').hide();
            });
        },0);
    },
    performAction:function(component,event,helper){
        component.set("v.epicOrder",event.currentTarget.getAttribute('data-index'));
        if(event.currentTarget.getAttribute('data-action') === 'New'){
            $A.enqueueAction(component.get("v.openEpicModel"));
        }else if(event.currentTarget.getAttribute('data-action') === 'Edit'){
            $A.enqueueAction(component.get("v.editEpicModel"));
        }else if(event.currentTarget.getAttribute('data-action') === 'Delete'){
            $A.enqueueAction(component.get("v.deleteEpic"));
        }else if(event.currentTarget.getAttribute('data-action') === 'Available'){
            $A.enqueueAction(component.get("v.availEpicModel"));
        }
    },
})