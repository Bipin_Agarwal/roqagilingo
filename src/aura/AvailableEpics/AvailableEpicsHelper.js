({
    availEpic : function(component, event, helper, type) {
        var availableEpics = component.get("c.getAvailableEpics");
        var epicName = component.get("v.epicRecord").Name;
        availableEpics.setParams({
            epicName:epicName,
            noOfRecord:10,
            type:component.get("v.type")
        });
        availableEpics.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue().data.length <=0){
                    component.set("v.elseValue",true);
                    component.set("v.epicList",response.getReturnValue().data);
                }
                if(response.getReturnValue().data.length >0){
                    var result = response.getReturnValue().data;
                    component.set("v.elseValue",false);
                    if(type === 'search'){
                        component.set("v.epicList", result);
                    }else{
                        var availEpics = component.get("v.epicList");
                        availEpics.concat(result);
                        component.set("v.epicList", availEpics);
                    }
                }
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(availableEpics);
    },
    validateAvailEpic:function(component, event, helper){
        var epicList = component.get("v.parentEpicList");
        var epicRecord = component.get("v.epicRecord");
        for(var index1=0;index1<epicList.length;index1++){
            for(var index2=0;index2<epicList[index1].epics.length;index2++){
                if(epicList[index1].epics[index2].epicId === epicRecord.epicId){
                    component.set("v.availEpic",false);
                    return epicList[index1].Name;
                }
            }
        }
    },
    handleServerException:function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        component.set("v.disableButton",false);
        helper.openModal(component, event, helper,message,'Response From Server.');
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception'
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    }
})