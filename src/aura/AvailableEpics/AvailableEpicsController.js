({
    availEpics: function(component, event, helper){
        component.set("v.disableButton",true);
        helper.availEpic(component, event, helper, 'search');
    },
    selectedEpic: function(component, event, helper){
        var index = event.currentTarget.getAttribute('data-index');  
        var availEpics = component.get("v.epicList")[index];
        var obj = {Name:availEpics.Name,Description:availEpics.Description,epicId:availEpics.epicId,Category:availEpics.Category,CategoryTitle:availEpics.CategoryTitle,CategoryAutonumber:availEpics.CategoryAutonumber,Size:availEpics.Size,
                   Priority:availEpics.Priority,Status:availEpics.Status,epicOwnerId:availEpics.epicOwnerId,epicOwnerName:availEpics.epicOwnerName,Tag:availEpics.Tag,
                   requirementNumber:availEpics.requirementNumber,regulation:availEpics.regulation,ParentEpicId:availEpics.ParentEpicId,parentEpicTitle:availEpics.parentEpicTitle};
        document.getElementById("missingEpic").innerHTML = '';
        component.set("v.disableButton",false);
        component.set("v.epicRecord", obj);
        component.set("v.epicList",[]);
    },
    addEpic: function(component, event, helper){
        component.set("v.disableButton",true);
        var epicName = component.get("v.epicRecord").Name;
        document.getElementById("missingEpic").innerHTML = '';
        var theme = helper.validateAvailEpic(component, event, helper);
        if(!$A.util.isEmpty(epicName !== undefined ? epicName.trim() : epicName) && component.get("v.availEpic")){
            component.set("v.selectedAvailEpic",true);
            $A.enqueueAction(component.get("v.addEpic"));
            component.destroy();
        }
        else{
            if($A.util.isUndefined(epicName) || $A.util.isEmpty(epicName.trim())){
                document.getElementById("missingEpic").innerHTML = 'Epic name is required.';
            }else if(!component.get("v.availEpic")){
                var themeType = (component.get("v.type") === 'Feature') ? 'Program Increment' : component.get("v.type");
                document.getElementById("missingEpic").innerHTML = 'Epic already present in current '+themeType+' of theme - '+theme;
            }
            component.set("v.disableButton",false);
            component.set("v.availEpic",true);
        }
    },
    closeModel:function(component, event, helper){
        component.set("v.epicRecord",{});
        component.destroy();
    }
})