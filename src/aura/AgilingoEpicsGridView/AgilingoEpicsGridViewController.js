({
    buttonClicked : function(component, event, helper) {
        component.set("v.buttonIndex.index",event.currentTarget.getAttribute('data-index'));
        component.set("v.buttonIndex.button",event.currentTarget.getAttribute('data-label'));
        $A.enqueueAction(component.get("v.selectedRecord"));
    },
    recordPage:function(component, event, helper){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId" : event.currentTarget.getAttribute('data-id'),
        });
        navEvt.fire();
    },
})