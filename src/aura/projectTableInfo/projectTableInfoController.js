({
	doInit : function(component, event, helper) {
		var getProjectInfo = component.get("c.getRecordHistory");
        getProjectInfo.setParams({
            'recordId' : component.get("v.recordId")
        });
        getProjectInfo.setCallback(this,function(response){
           var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.projectResult",response.getReturnValue().data);
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(getProjectInfo);
	},
    print:function(component, event, helper) {
        event.preventDefault();
        window.open('/apex/ROQA__AgilingoProjectPrintPreview?id='+component.get("v.recordId")+'&title='+component.get("v.header"),'_blank');
        
    },
    closeModal:function(component, event, helper){
        component.destroy();
    }
})