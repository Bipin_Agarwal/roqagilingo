({
    doInit : function(component, event, helper) {
        helper.getOwner(component, event, helper);
        helper.getTagValues(component, event, helper);
        //helper.getCategory(component, event, helper);
        /*if(component.get("v.header") != 'New Epic Kanban'){
            component.set("v.showOwner",true);
            helper.parEpic(component, event, helper);
        }
        (component.get("v.clone") ? helper.parEpic(component, event, helper) : '');
		*/
        var fieldValues = component.get("c.getPicklistValues");
        fieldValues.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.pickListValues",response.getReturnValue().data);
                helper.setValue(component, event, helper, component.get("v.epicRecord"));
                component.set("v.callController",false);
            }else{
                component.set("v.callcontroller",false);
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        component.set("v.callController",true);
        $A.enqueueAction(fieldValues);
    },
	selectedCategory:function(component, event, helper){
        if(!$A.util.isEmpty(component.get("v.categoryRecord.Name"))){
            component.set("v.epicRecord.Category",component.get("v.categoryRecord.Id"));
            component.set("v.epicRecord.CategoryTitle",component.get("v.categoryRecord.ROQA__Title__c"));
            component.set("v.epicRecord.categoryName",component.get("v.categoryRecord.Name"));
        }else{
            component.set("v.epicRecord.Category",'');
            component.set("v.epicRecord.CategoryTitle",'');
            component.set("v.epicRecord.categoryName",'');            
        }
    },
    selectedSprintValues:function(component, event, helper){
        if(!$A.util.isEmpty(component.get("v.sprintRecord.Name"))){
            component.set("v.epicRecord.sprintId",component.get("v.sprintRecord.Id"));
            component.set("v.epicRecord.sprintTitle",component.get("v.sprintRecord.ROQA__Title__c"));
            component.set("v.epicRecord.sprint",component.get("v.sprintRecord.Name"))
        }else{
            component.set("v.epicRecord.sprintId",'');
            component.set("v.epicRecord.sprintTitle",'');
            component.set("v.epicRecord.sprint",'');
        }
    },
    selectedEpic:function(component, event, helper){
        component.set("v.epicRecord.ParentEpicId",component.get("v.parentEpicRecord.Id"));
        component.set("v.epicRecord.parentEpicTitle",component.get("v.parentEpicRecord.Title__c"));
        component.set("v.epicRecord.parentEpicAutonumber",component.get("v.parentEpicRecord.Name"));
    },
    selectedAssignedToUser:function(component, event, helper){
        component.set("v.epicRecord.assignedTo",component.get("v.assignedToUser"));
    },
    getSprint:function(component, event, helper){
        var sprint = component.get("c.getSprints");
        sprint.setParams({
            'searchText' : component.get("v.selectSprint")
        });
        sprint.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                if(response.getReturnValue().data.length > 0){
                    component.set("v.sprintList",response.getReturnValue().data);
                    component.set("v.elseValue",false);
                }else{
                    component.set("v.sprintList",[]);
                    component.set("v.elseValue",true);
                }
            }
        });
        $A.enqueueAction(sprint);
    },
    setLifecycle:function(component, event, helper){
        component.set("v.epicRecord.Lifecycle",component.find('InputDynamicLifecycle').get("v.value"));
    },
    selectedSprint:function(component, event, helper){
        var index = event.currentTarget.getAttribute('data-index');
        var sprintSelected = component.get("v.sprintList")[index];
        component.set("v.selectSprint",sprintSelected.name+'  '+'('+sprintSelected.title+')');
        component.set("v.epicRecord.sprint",sprintSelected.name);
        component.set("v.epicRecord.sprintId",sprintSelected.id);
        component.set("v.epicRecord.sprintTitle",sprintSelected.title);
        component.set("v.sprintList",[]);
    },
    setType:function(component, event, helper){
        component.set("v.epicRecord.EpicType",component.find("InputDynamicType").get("v.value"));
        component.set("v.epicRecord.ParentEpicId",'');
        component.set("v.parentEpicRecord",{});
        component.find("InputDynamicType").get("v.value") === 'Roadmap' ? component.set("v.parentType",'Initiative') : (component.find("InputDynamicType").get("v.value") === 'Feature' ? 
                                                                                                                                         component.set("v.parentType",'Roadmap') : component.find("InputDynamicType").get("v.value") === 'Story' ?
                                                                                                                                         component.set("v.parentType",'Feature') : component.set("v.parentType",''));
        (component.find("InputDynamicType").get("v.value") === 'Story') ? component.set("v.showAcceptance",true) : component.set("v.showAcceptance",false);
    },
	selectedParentEpicValues:function(component, event, helper){
        if(!$A.util.isEmpty(component.get("v.parentEpicRecord.Name"))){
            component.set("v.epicRecord.ParentEpicId",component.get("v.parentEpicRecord.Id"));
            component.set("v.epicRecord.parentEpicTitle",component.get("v.parentEpicRecord.ROQA__Epic_Title__c"));
            component.set("v.epicRecord.parentEpicAutonumber", component.get("v.parentEpicRecord.Name"));
        }else{
            component.set("v.epicRecord.ParentEpicId",'');
            component.set("v.epicRecord.parentEpicTitle",'');
            component.set("v.epicRecord.parentEpicAutonumber", '');
        }
    },
    setPriority:function(component, event, helper){
        component.set("v.epicRecord.Priority",component.find("InputDynamicPriority").get("v.value"));
    },
    setSize:function(component, event, helper){
        component.set("v.epicRecord.Size",component.find("InputDynamicSize").get("v.value"));
    },
    setStatus:function(component, event, helper){
        component.set("v.epicRecord.Status",component.find("InputDynamicStatus").get("v.value"));
    },
    setParentEpic:function(component, event, helper){
        helper.setParentEpic(component, event, helper);
    },
    setcategory:function(component, event, helper){
        helper.setCategoryValue(component, event, helper);
    },
    setOwner:function(component, event, helper){
        var epicRecord = component.get("v.epicRecord");
        var ownerDetails = component.find("InputDynamicOwner").get("v.value");
        if(ownerDetails.indexOf('-') > -1){
            epicRecord.epicOwnerId = ownerDetails.split('-')[0];
            epicRecord.epicOwnerName = ownerDetails.split('-')[1];
        }else{
            epicRecord.epicOwnerId = ownerDetails;
            epicRecord.epicOwnerName = ownerDetails;
        }
        component.set("v.epicRecord",epicRecord);
    },
    selectedTag:function(component, event, helper){
        var selectedId = component.find('selectedTag').get("v.value");
        var selectedIndex = helper.getSelectedTag(component, event, helper, selectedId);
        var selectedTag = component.get("v.tagValues")[selectedIndex];
        if(!$A.util.isEmpty(selectedTag)){
            var epicRecord = component.get("v.epicRecord");
            if(!epicRecord.Tag){
                epicRecord.Tag = [];
            }
            var existingEpic = helper.checkTag(component, event, helper,selectedTag, epicRecord);  
            if(existingEpic == 0){
                epicRecord.Tag.push(selectedTag);
                component.set("v.epicRecord",epicRecord);
            }
        }
        component.find('selectedTag').set("v.value",'');
    },
    removeTag:function(component, event, helper){
        var index = event.currentTarget.getAttribute('data-index');
        var epicRecord = component.get("v.epicRecord");
        var tags = epicRecord.Tag;
        if(!epicRecord.epicTagDetails){
            epicRecord.epicTagDetails = [];
        }
        var tagRemoved = tags[index];
        tags.splice(index,1);
        epicRecord.epicTagDetails.push(tagRemoved);
        component.set("v.epicRecord.epicTagDetails",epicRecord.epicTagDetails);
        component.set("v.epicRecord.Tag",tags);
    },
    saveEpic:function(component, event, helper){
        var clickedButton = event.currentTarget.getAttribute('data-label');
        var epicRecord = component.get("v.epicRecord");
        var validDesc = (!$A.util.isEmpty(epicRecord.Description) ? (epicRecord.Description.length <= 131072 ? true : false) : false);
		var validAcceptance = (!$A.util.isEmpty(epicRecord.acceptanceCriteria) ? (epicRecord.acceptanceCriteria.length <= 131072 ? true : false) : true);
        var validActivity = true;
        validActivity = helper.checkActivity(component, event, helper, epicRecord);
        helper.clearHTML(component, event, helper);
       // helper.checkSprint(component, event, helper);
        
        if(!$A.util.isEmpty(epicRecord.Name) && !$A.util.isEmpty(epicRecord.Description) && validDesc && validAcceptance
           && !$A.util.isEmpty(epicRecord.Status) && !$A.util.isEmpty(epicRecord.Size) && validActivity
           && !$A.util.isEmpty(epicRecord.EpicType)){
            ((component.get("v.header") !== 'New Epic Kanban') ? component.set("v.epicRecord.recordStatus",'update') : component.set("v.epicRecord.recordStatus",'New'));
            ((clickedButton === 'save') ? $A.enqueueAction(component.get("v.addToGrid")) : $A.enqueueAction(component.get("v.addAndOpen")));
            //component.destroy();
        }else{
            var message = '';
            if($A.util.isEmpty(epicRecord.Name)){
                message = 'Title is required.';
                document.getElementById('missingName').innerHTML = 'Title is required';
            }else if($A.util.isEmpty(epicRecord.Description)){
                message = 'Description is required.';
                document.getElementById('missingDescription').innerHTML = 'Description is required';
            }else if($A.util.isEmpty(epicRecord.Status)){
                message = 'Status is required.';
                document.getElementById('missingStatus').innerHTML = 'Status is required';
            }else if($A.util.isEmpty(epicRecord.Size)){
                message = 'Size is required.';
                document.getElementById('missingSize').innerHTML = 'Size is required';
            }else if($A.util.isEmpty(epicRecord.EpicType)){
                message = 'EpicType is required.';
                document.getElementById('missingEpicType').innerHTML = 'EpicType is required';
            }else if(!validDesc){
                message = 'Maxlength of Description is 1,31,072 but you entered '+epicRecord.Description.length;;
                document.getElementById('missingDescription').innerHTML = 'Maxlength of Description is 1,31,072 but you entered '+epicRecord.Description.length;
            }else if(!validAcceptance){
                message = 'Maxlength of Acceptance Criteria is 1,31,072 but you entered '+epicRecord.acceptanceCriteria.length;;
                document.getElementById('missingAcceptanceCriteria').innerHTML = 'Maxlength of Description is 1,31,072 but you entered '+epicRecord.acceptanceCriteria.length;
            }else if(!validActivity && !moment(epicRecord.dateGroomed).isValid()){
                message = 'Error in Date Groomed field';
                document.getElementById("groomed").innerHTML = 'Date Groomed should be in correct format.';
            }else if(!validActivity && !moment(epicRecord.dateApproved).isValid()){
                message = 'Error in Date Approved field';
                document.getElementById("approved").innerHTML = 'Date Approved should be in correct format.';
            }else if(!validActivity && !moment(epicRecord.dateCommitted).isValid()){
                message = 'Error in Date Committed field';
                document.getElementById("committed").innerHTML = 'Date Committed should be in correct format.';
            }else if(!validActivity && !moment(epicRecord.dateCompleted).isValid()){
                message = 'Error in Date Completed field';
                document.getElementById("completed").innerHTML = 'Date Completed should be in correct format.';
            }
            helper.openSuccess(component, event, helper, message, 'slds-theme_error', 'utility:error');
        }
    },
    closeModel:function(component, event, helper){
        component.destroy();
    }
})