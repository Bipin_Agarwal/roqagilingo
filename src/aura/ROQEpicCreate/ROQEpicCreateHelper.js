({
    clearHTML:function(component, event, helper){
        document.getElementById('missingName').innerHTML = '';
        document.getElementById('missingEpicType').innerHTML = '';
        document.getElementById('missingSize').innerHTML = '';
        document.getElementById('missingDescription').innerHTML = '';
        document.getElementById('missingStatus').innerHTML = '';
        document.getElementById('groomed').innerHTML = '';
        document.getElementById('approved').innerHTML = '';
        document.getElementById('committed').innerHTML = '';
        document.getElementById('completed').innerHTML = '';
    },
    getOwner:function(component, event, helper){
        var ownerList = component.get("c.getEpicUsers");
        var epicRecord = component.get("v.epicRecord");
        ownerList.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.ownerValues",response.getReturnValue());
                if(component.get("v.showOwner")){
                    component.find('InputDynamicOwner').set("v.value",epicRecord.epicOwnerId+'-'+epicRecord.epicOwnerName);
                }else{
                    epicRecord.epicOwnerName = response.getReturnValue().Roadmap[0].LoggedInUserName;
                    epicRecord.epicOwnerId = response.getReturnValue().Roadmap[0].LoggedInUserId;
                    component.set("v.epicRecord",epicRecord);
                }
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(ownerList);
    },/*
    getCategory:function(component, event, helper){
        var categoryList = component.get("c.getEpicCategories");
        categoryList.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.categoryList",response.getReturnValue().data);
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(categoryList);
    },
    setCategoryValue:function(component, event, helper){
        var epicRecord = component.get("v.epicRecord");
        var categoryList = component.get("v.categoryList");
        var categoryId = component.find('InputDynamicCategory').get("v.value");
        component.set("v.epicRecord.CategoryTitle",'');
        component.set("v.epicRecord.Category",'');
        component.set("v.epicRecord.CategoryColor",'');
        if(!$A.util.isEmpty(categoryId)){
            for(var index=0;index<categoryList.length;index++){
                if(categoryId === categoryList[index].Id){
                    epicRecord.CategoryTitle = categoryList[index].Title__c;
                    epicRecord.Category = categoryList[index].Id;
                    epicRecord.CategoryColor = categoryList[index].Properties__c;
                    component.set("v.epicRecord",epicRecord);
                    break;
                }
            }
        }
    },*/
    getTagValues :function(component, event, helper){
        component.set("v.callController",true);
        var tagValues = component.get("c.getEpicTags");
        tagValues.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                var tagsList = response.getReturnValue().data;
                component.set("v.tagValues",tagsList);
                component.set("v.callController",false);
            }else{
                component.set("v.callcontroller",false);
                helper.handleServerException(component, event, helper, response.errorList);
            }
        });
        $A.enqueueAction(tagValues);
    },
    getSelectedTag:function(component, event, helper, selectedId){
        var tagData = component.get("v.tagValues");
        for(var index=0;index<tagData.length;index++){
            if(selectedId === tagData[index].Id){
                return index;
            }
        }
        return -1;
    },
    checkTag:function(component, event, helper, selectedTag,epicRecord){
        var tags = epicRecord.Tag;
        for(var index=0;index<tags.length;index++){
            if(tags[index].Id === selectedTag.Id){
                return 1;
            }
        }
        return 0;
    },
    checkActivity:function(component, event, helper, record){
        var returnedValue = true;
        if(!$A.util.isEmpty(record.dateGroomed) && !moment(record.dateGroomed).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.dateApproved) &&!moment(record.dateApproved).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.dateCommitted) &&!moment(record.dateCommitted).isValid()){
            returnedValue = false;
        }if(!$A.util.isEmpty(record.dateCompleted) &&!moment(record.dateCompleted).isValid()){
            returnedValue = false;
       }
        return returnedValue;
    },
    setValue:function(component, event, helper, epicRecord){
        //(!$A.util.isEmpty(epicRecord.Category) ? component.find('InputDynamicCategory').set("v.value",epicRecord.Category) : component.find('InputDynamicCategory').set("v.value",'')); 
        //(!$A.util.isEmpty(epicRecord.sprintId)) ? component.set("v.selectSprint",epicRecord.sprint+'  '+'('+epicRecord.sprintTitle+')') : '';
        (!$A.util.isEmpty(epicRecord.Size) ? component.find('InputDynamicSize').set("v.value",epicRecord.Size) : component.find('InputDynamicSize').set("v.value",'')); 
        (!$A.util.isEmpty(epicRecord.EpicType) ? component.find('InputDynamicType').set("v.value",epicRecord.EpicType) : component.find('InputDynamicType').set("v.value",''));
        (!$A.util.isEmpty(epicRecord.Priority) ? component.find('InputDynamicPriority').set("v.value",epicRecord.Priority) : component.find('InputDynamicPriority').set("v.value",'')); 
        (!$A.util.isEmpty(epicRecord.assignedTo)) ? component.set("v.assignedToUser",epicRecord.assignedTo) : '';
        (component.find('InputDynamicType').get("v.value") === 'Story') ? component.set("v.showAcceptance",true) : component.set("v.showAcceptance",false);
        
        if(!$A.util.isEmpty(epicRecord.Status)){
            component.find('InputDynamicStatus').set("v.value",epicRecord.Status);
        }else{
            component.set("v.epicRecord.Status",'On Track');
            component.find('InputDynamicStatus').set("v.value",'On Track');
        }
        if(!$A.util.isEmpty(epicRecord.Lifecycle)){
            component.find('InputDynamicLifecycle').set("v.value",epicRecord.Lifecycle);
        }else{
            component.set("v.epicRecord.Lifecycle",'Backlog');
            component.find('InputDynamicLifecycle').set("v.value",'Backlog');
        }
        if(!$A.util.isEmpty(epicRecord.sprintId)){
            component.set("v.sprintRecord.Name",epicRecord.sprint);
            component.set("v.sprintRecord.Id",epicRecord.sprintId);
        }
        if(!$A.util.isEmpty(epicRecord.Category)){
            component.set("v.categoryRecord.Id",epicRecord.Category);
            component.set("v.categoryRecord.Name",epicRecord.CategoryAutonumber);
        }if(!$A.util.isEmpty(epicRecord.ParentEpicId)){
            epicRecord.EpicType === 'Roadmap' ? component.set("v.parentType",'Initiative') : epicRecord.EpicType === 'Feature' ?  component.set("v.parentType",'Roadmap') : epicRecord.EpicType === 'Story' ?
                component.set("v.parentType",'Feature') : component.set("v.parentType",'');
            component.set("v.parentEpicRecord.Id",epicRecord.ParentEpicId);
            component.set("v.parentEpicRecord.Name",epicRecord.parentEpicAutonumber);
        }else{
            epicRecord.EpicType === 'Roadmap' ? component.set("v.parentType",'Initiative') : epicRecord.EpicType === 'Feature' ?  component.set("v.parentType",'Roadmap') : epicRecord.EpicType === 'Story' ?
                component.set("v.parentType",'Feature') : component.set("v.parentType",'');
        }
    },
   /* checkSprint:function(component, event, helper){
        if($A.util.isEmpty(component.get("v.selectSprint"))){
            component.set("v.epicRecord.sprint",'');
            component.set("v.epicRecord.sprintTitle",'');
            component.set("v.epicRecord.sprintId",'')
        }
    },
    parEpic:function(component, event, helper){
        var parentEpic = component.get("c.getParentEpics");
        parentEpic.setParams({
            epicType : component.get("v.epicRecord.EpicType")
        });
        parentEpic.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.parentEpics",response.getReturnValue().data);
                (component.get("v.header") != 'New Epic Kanban' || component.get("v.clone")) ? component.find('InputDynamicParentEpic').set("v.value",component.get("v.epicRecord.ParentEpicId")) : '';
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
        });
        $A.enqueueAction(parentEpic);
    },
    setParentEpic:function(component, event, helper){
        var parentEpics = component.get("v.parentEpics");
        var parentId = component.find('InputDynamicParentEpic').get("v.value");
        component.set("v.epicRecord.parentEpicAutonumber",'');
        component.set("v.epicRecord.parentEpicTitle",'');
        component.set("v.epicRecord.ParentEpicId",'');
        for(var index=0;index<parentEpics.length;index++){
            if(parentId == parentEpics[index].Id){
                component.set("v.epicRecord.parentEpicAutonumber",parentEpics[index].parentEpicAutonumber);
                component.set("v.epicRecord.parentEpicTitle",parentEpics[index].Epic_Title__c);
                component.set("v.epicRecord.ParentEpicId",parentEpics[index].Id);
                break;
            }
        }
    },*/
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception',
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    openSuccess:function(component, event, helper, message, style, iconName){
        $A.createComponent(
            "ROQA:ROQSuccessComponent",
            {
                saveStatus : message,
                style:style,
                iconName:iconName
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.openModal(component, event, helper,message,'Response from server.');
    },
})