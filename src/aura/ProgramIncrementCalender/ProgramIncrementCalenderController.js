({
    doInit : function(component, event, helper) {
        component.set("v.structure",[]);
        if(component.get("v.recordId").length > 0){
            helper.getProgramIncrement(component, event, helper);
        }
        var containmentClassLength = document.getElementsByClassName('containmentClass').length;
        var containmentClass = document.getElementsByClassName('containmentClass');
        (containmentClassLength > 1) ? $(containmentClass).first().remove() : '';
    },
    openThemeModel : function(component, event, helper) {
        component.set("v.body",'');
        component.set("v.disableButton",true);
        component.set("v.themeRecord",{});
        $A.createComponent(
            "ROQA:ROQTheme",
            {
                epicList:component.get("v.epicList"),
                themeRecord:component.getReference("v.themeRecord"),
                addTheme:component.getReference("c.addTheme"),
                availTheme:component.getReference("c.availTheme")
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                    component.set("v.disableButton",false);
                }
            }
        );
    },
    addTheme :function(component,event,helper){
        var selectedTheme = component.get("v.selectedTheme");
        var epicList = component.get("v.epicList");
        var themeRecord = component.get("v.themeRecord");
        themeRecord.epics = [];
        themeRecord.ThemeOrder;
        themeRecord.ThemeStyle = '';
        themeRecord.themeId = ((!$A.util.isEmpty(themeRecord.themeId)) ? themeRecord.themeId : '');
        var themeIndex;
        if(selectedTheme.length > 0){
            for(var index=0;index<epicList.length;index++){
                var themeId = (epicList[index].themeId !== undefined ? epicList[index].themeId : '');
                if(selectedTheme.split(' ').join('')+themeId === epicList[index].Name.split(' ').join('')+themeId){
                    epicList.splice((index+1),0,themeRecord);
                    themeIndex = index+1;
                    component.set("v.epicList",epicList);
                    helper.changeThemeOrder(component, event, helper);
                    helper.updateId(component, event, helper, themeIndex);
                    break;
                }
            }
        }
        else{
            epicList.push(themeRecord);
            themeRecord.ThemeOrder = epicList.length;
            themeIndex = epicList.length-1;
            component.set("v.epicList",epicList);
        }
        helper.createTheme(component,event,helper,themeIndex);
    },
    availTheme:function(component, event, helper){
        component.set("v.themeRecord",{});
        $A.createComponent(
            "ROQA:AvailableTheme",
            {
                epicList:component.get("v.epicList"),
                themeRecord:component.getReference("v.themeRecord"),
                addTheme:component.getReference("c.addTheme")
            },
            function(newButton, status, errorMessage){
                if(status === "SUCCESS"){
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body",body);
                }
            }
        );
    },
    openEpicModel:function(component, event, helper) {
        component.set("v.disableButton",true);
        var epicList = component.get("v.epicList");
        var thimList = [];
        (!component.get("v.availableEpic") ? component.set("v.epicRecord",{}) : '');
        thimList.push({label :'---Select---', value :''});
        for(var index=0;index<epicList.length;index++){
            var themeId = (!$A.util.isEmpty(epicList[index].themeId) ? epicList[index].themeId : '');
            thimList.push({label :epicList[index].Name,value :epicList[index].Name+themeId});
        }
        $A.createComponent(
            "ROQA:ROQEpic",
            {
                epicType:'Feature',
                thimList:thimList,
                epicList:component.get("v.epicList"),
                roadMapId:component.get("v.roadMapId"),
                selectedAvailEpic:component.getReference("v.selectedAvailEpic"),
                themeStartDate:component.get("v.themeStartDate"),
                themeEndDate:component.get("v.themeEndDate"),
                epicRecord:component.getReference("v.epicRecord"),
                addEpic:component.getReference("c.addEpic"),
                availEpic:component.getReference("c.availEpic"),
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                    component.set("v.availableEpic",false);
                    component.set("v.disableButton",false);
                }
            }
        );
    },
    addEpic:function(component,event,helper){
        var epicRecord = component.get("v.epicRecord");
        epicRecord.theme = epicRecord.theme.split(' ').join('');
        var epicBar = $A.get("e.ROQA:AddEpic");
        epicBar.setParams({
            "theme" : epicRecord.theme,
            "epicRecord" : epicRecord
        });
        epicBar.fire();
    },
    availEpic:function(component, event, helper){
        component.set("v.epicRecord",{});
        $A.createComponent(
            "ROQA:AvailableEpics",
            {
                type:'Feature',
                parentEpicList:component.get("v.epicList"),
                epicRecord:component.getReference("v.epicRecord"),
                selectedAvailEpic:component.getReference("v.selectedAvailEpic"),
                addEpic:component.getReference("c.openEpicModel")
            },
            function(newButton, status, errorMessage){
                if(status === "SUCCESS"){
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body",body);
                    component.set("v.availableEpic",true);
                }
            }
        );
    },
    themeReorder:function(component, event, helper){
        var themePanel = document.getElementsByClassName('theme_panel');
        var blackbg = document.getElementsByClassName('blackbg');
        $(themePanel).remove();
        component.set("v.structure",[]);
        $A.createComponent(
            "ROQA:ROQReorderTheme",
            {
                epicList:component.getReference("v.epicList"),
                reorderTheme:component.getReference("c.redesignMap")
            },
            function(newButton, status, errorMessage){
                if(status === "SUCCESS"){
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body",body);
                    $(blackbg).fadeIn();
                }
            }
        );
    },
    redesignMap:function(component, event, helper){
        component.set("v.structure",[]);
        component.set("v.callController",true);
        helper.changeThemeOrder(component, event, helper);
        setTimeout(function(){
            helper.calenderDOMStructure(component, event, helper, component.get("v.calenderList"));
            helper.createStructure(component, event, helper,component.get("v.epicList"),0);
        },100);
    },
    saveProgram:function(component,event,helper){
        var epicList = component.get("v.epicList");
        var obj = new Object();
        obj.themes = epicList;
        var saveRecord = component.get("c.manageDoSaveProgramIncrement");
        saveRecord.setParams({
            "releaseRecord": JSON.stringify(obj),
            "piId":component.get("v.recordId")
        });
        saveRecord.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var message;
                var result = response.getReturnValue();
                if(result.isSuccess){
                    helper.openSuccess(component, event, helper, result.Msg, 'slds-theme_success','utility:success');
                    setTimeout(function(){
                        $A.get("e.force:refreshView").fire();
                    }, 1000);
                }else{
                    var message = result.errorList[0].errorMessage;
                    helper.openModal(component, event, helper, message, 'Application Exception');
                }
                component.set("v.disableButton",false);
            }else{
                helper.handleServerException(component, event, helper, response.getError());
            }
            component.set("v.callController",false);
        });
        if(epicList.length > 0 || component.get("v.actionPerformed")){
            component.set("v.disableButton",true);
            component.set("v.callController",true);
            $A.enqueueAction(saveRecord);
        }
    }
})