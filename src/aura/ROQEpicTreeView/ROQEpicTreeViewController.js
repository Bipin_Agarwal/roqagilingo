({
    doInit:function(component, event, helper){
        var getEpicDetails = component.get("c.getEpicDetails");
        getEpicDetails.setParams({
            epicId : component.get("v.recordId")
        });
        getEpicDetails.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.treeList",response.getReturnValue()[0]);
                helper.designTreeView(component, event, helper, component.get("v.treeList"));
                component.set("v.callController",false);
            }else{
                helper.handleServerException(component, event, helper,response.getError()) ;                  
            }
        });
        component.set("v.callController",true);
        $A.enqueueAction(getEpicDetails);
    },
    getEpicInfo:function(component, event, helper){
        var EpicDetails = component.get("c.getEpicDetails");
        var epicId = event.currentTarget.getAttribute('data-id');
        var treeList = component.get("v.treeList");
        EpicDetails.setParams({
            epicId : epicId
        });
        EpicDetails.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.epicUsed",response.getReturnValue()[0]);
                helper.setDetails(component, event, helper);
            }else{
                helper.handleServerException(component, event, helper,response.getError()) ;                  
            }
        });
        $A.enqueueAction(EpicDetails);
    },
    mappingPage:function(component, event, helper){
        var recordId = event.currentTarget.getAttribute('data-id');
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId" : recordId,
        });
        navEvt.fire();
    }
})