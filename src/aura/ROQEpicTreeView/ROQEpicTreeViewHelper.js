({
    designTreeView:function(component, event, helper, treeList){
        var epicDetails = component.get("v.epicDetails");
        (!$A.util.isEmpty(treeList.epicObjectDetails.ROQA__Parent_Epic__c)) ? component.set("v.parentIsThere",true) : '';
        if(!$A.util.isEmpty(treeList.epicUsed)){
            epicDetails.Id = treeList.epicUsed.EpicData.Id;
            epicDetails.EpicType = treeList.epicUsed.EpicData.Type;
            epicDetails.mapId = treeList.epicUsed.EpicData.Link;
            epicDetails.LifeCycle = treeList.epicUsed.EpicData.LifeCycle;
            epicDetails.Status = treeList.epicUsed.EpicData.Status;
            epicDetails.Title = treeList.epicUsed.EpicData.Title;
        }
        else{
            component.set("v.emptyTreeList",false);
        }
        component.set("v.epicDetails",epicDetails);
    },
    setDetails : function(component, event, helper) {
        var epicDetails = component.get("v.epicDetails");
        var epicUsed = component.get("v.epicUsed");
        if(!$A.util.isEmpty(epicUsed.epicUsed)){
            epicDetails.Id = epicUsed.epicUsed.EpicData.Id;
            epicDetails.EpicType = epicUsed.epicUsed.EpicData.Type;
            epicDetails.mapId = epicUsed.epicUsed.EpicData.Link;
            epicDetails.LifeCycle = epicUsed.epicUsed.EpicData.LifeCycle;
            epicDetails.Status = epicUsed.epicUsed.EpicData.Status;
            epicDetails.Title = epicUsed.epicUsed.EpicData.Title;
        }
        else{
            component.set("v.emptyTreeList",false);
        }
        component.set("v.callController",false);
        component.set("v.epicDetails",epicDetails);
    },
    openModal:function(component, event, helper, message, header){
        $A.createComponent(
            "ROQA:MsgCmp",
            {
                message : message,
                header : header,
                type:'Exception',
            },
            function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
            }
        );
    },
    handleServerException: function(component, event, helper, exception){
        let errors = exception;
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.openModal(component, event, helper,message,'Response From Server.');
    }
})