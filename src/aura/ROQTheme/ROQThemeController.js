({
    saveTheme:function(component, event, helper) {
        component.set("v.disableButton",true);
        var record = component.get("v.themeRecord");
        var epicList = component.get("v.epicList");
        document.getElementById("name").innerHTML = '';
        document.getElementById("description").innerHTML = '';
        component.set("v.editedThemeRecord",component.get("v.themeRecord"));
        
        for(var index=0;index<epicList.length;index++){
            if(record.Name.toLowerCase() === epicList[index].Name.toLowerCase()){
                component.set("v.newTheme",false);
                break;
            }
        }
        (record.Name === component.get("v.oldName")) ? component.set("v.newTheme",true) : '';
        if(!$A.util.isEmpty(record.Name !== undefined ? record.Name.trim() : record.Name) && !$A.util.isEmpty(record.Description !== undefined ? record.Description.trim() : record.Description) &&
           component.get("v.newTheme")){
            $A.enqueueAction(component.get("v.addTheme"));
            component.destroy();
        }
        else{
            component.set("v.disableButton",false);
            if($A.util.isUndefined(record.Name) || $A.util.isEmpty(record.Name.trim())){
                document.getElementById("name").innerHTML = 'This is Required.';
            }else if(!component.get("v.newTheme")){
                document.getElementById("name").innerHTML = 'Theme Name Already Exists';
            }else if($A.util.isUndefined(record.Description) || $A.util.isEmpty(record.Description.trim())){
                document.getElementById("description").innerHTML = 'This is Required.';
            }
            component.set("v.newTheme",true);
        }
    },
    closeModel : function(component, event, helper) {
        component.destroy();
    },
    availTheme:function(component, event, helper){
        $A.enqueueAction(component.get("v.availTheme"));
        component.destroy();
        
    }
})