<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Canceled</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Canceled</literalValue>
        <name>Canceled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Closed</literalValue>
        <name>Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_Progress</fullName>
        <field>Lifecycle__c</field>
        <literalValue>In Progress</literalValue>
        <name>In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
