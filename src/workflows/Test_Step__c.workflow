<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Lifecycle_Blocked</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Blocked</literalValue>
        <name>Lifecycle-Blocked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Completed</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Completed</literalValue>
        <name>Lifecycle-Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
