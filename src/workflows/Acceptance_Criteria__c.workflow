<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Lifecycle_Backlog</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Backlog</literalValue>
        <name>Lifecycle-Backlog</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Closed</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Closed</literalValue>
        <name>Lifecycle-Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Commit</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Committed</literalValue>
        <name>Lifecycle-Commit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Committed</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Committed</literalValue>
        <name>Lifecycle-Committed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Development_Complete</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Development Complete</literalValue>
        <name>Lifecycle-Development Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Groomed</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Groomed</literalValue>
        <name>Lifecycle-Groomed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Ready_for_Release</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Ready for Release</literalValue>
        <name>Lifecycle-Ready for Release</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Release</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Released</literalValue>
        <name>Lifecycle-Release</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Request_Info</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Groomed</literalValue>
        <name>Lifecycle-Request Info</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lifecycle_Testing_in_Progress</fullName>
        <field>Lifecycle__c</field>
        <literalValue>Testing in Progress</literalValue>
        <name>Lifecycle-Testing in Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
